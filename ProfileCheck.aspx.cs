﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;


    public partial class ProfileCheck : System.Web.UI.Page
    {
        string cs = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = null;
        SqlCommand cmd = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UID"] == "" || Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");

            }

            TextBox1.Attributes["type"] = "password";
          
            try
            {
                if (!IsPostBack)
                {
                    Userid1.ReadOnly = true;
                    TextBox1.ReadOnly = true;
                 
                    TextBox4.ReadOnly = true;
                    TextBox5.ReadOnly = true;
                    TextBox6.ReadOnly = true;
                    TextBox7.ReadOnly = true;
                 
                    con = new SqlConnection(cs);
                    con.Open();
                    cmd = new SqlCommand("sp_getprofile", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@action", "selectval");
                    cmd.Parameters.AddWithValue("@userid", Session["UID"].ToString());
                   
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);


                    Userid1.Text = dt.Rows[0]["User_id"].ToString();
                    TextBox1.Text = dt.Rows[0]["password"].ToString();
                    TextBox4.Text = dt.Rows[0]["name"].ToString();
                    TextBox5.Text = dt.Rows[0]["email_id"].ToString();
                    TextBox6.Text = dt.Rows[0]["mobile_no"].ToString();
                    TextBox7.Text = dt.Rows[0]["status"].ToString();
                   
                    con.Close();
                }
            }

            catch (Exception ex)
            {
            }


        }



        protected void bt_Click(object sender, EventArgs e)
        {


            //Userid1.ReadOnly = false;
            TextBox4.ReadOnly = false;
            TextBox1.ReadOnly = false;
            TextBox5.ReadOnly = false;
            TextBox6.ReadOnly = false;
            TextBox7.ReadOnly = false;
            Button1.Visible = true;
            bt.Visible = false;
            Button1.Visible = true;
        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
           con = new SqlConnection(cs);
            con.Open();
            cmd = new SqlCommand("sp_getprofile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@action", "update");
            cmd.Parameters.AddWithValue("@password", TextBox1.Text);

            cmd.Parameters.AddWithValue("@name", TextBox4.Text);
            cmd.Parameters.AddWithValue("@email_id", TextBox5.Text);
            cmd.Parameters.AddWithValue("@mobile_no", TextBox6.Text);
            cmd.Parameters.AddWithValue("@status", TextBox7.Text);
          
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            string.IsNullOrWhiteSpace(TextBox1.Text);
            
            da.Fill(ds);
         
            cmd.ExecuteNonQuery();
            Response.Write("<script>alert('Sussesfully Changed');</script>");
            //Response.Redirect("ProfileCheck.aspx");
            con.Close();


        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //            if (FileUpload1.HasFile)
            //            {
            //                // Get the file extension
            //                string fileExtension = System.IO.Path.GetExtension(FileUpload1.FileName);

            //                if (fileExtension.ToLower() != ".jpg" && fileExtension.ToUpper() != ".png")
            //                {
            //                    lblMessage.ForeColor = System.Drawing.Color.Red;
            //                    lblMessage.Text = "Only files with .jpg and .png extension are allowed";
            //                }
            //                else
            //                {
            //                    // Get the file size
            //                    int fileSize = FileUpload1.PostedFile.ContentLength;
            //                    // If file size is greater than 2 MB
            //                    if (fileSize > 2097152)
            //                    {
            //                        lblMessage.ForeColor = System.Drawing.Color.Red;
            //                        lblMessage.Text = "File size cannot be greater than 2 MB";
            //                    }
            //                    else
            //                    {
            //                        // Upload the file
            //                        FileUpload1.SaveAs(Server.MapPath("~/Uploadprofile/" + FileUpload1.FileName));
            //                        lblMessage.ForeColor = System.Drawing.Color.Green;
            //                        lblMessage.Text = "File uploaded successfully";
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                lblMessage.ForeColor = System.Drawing.Color.Red;
            //                lblMessage.Text = "Please select a file";
            //            }
            //        }
            //    }
            //}
        }
    }

