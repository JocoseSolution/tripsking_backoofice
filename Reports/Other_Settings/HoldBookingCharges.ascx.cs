﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_Other_Settings_HoldBookingCharges : System.Web.UI.UserControl
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;

    public string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            try
            {
                if (!IsPostBack)
                {
                    ddl_ptype.AppendDataBoundItems = true;
                    ddl_ptype.Items.Clear();
                    ddl_ptype.DataSource = STDom.GetAllGroupType().Tables[0];
                    ddl_ptype.DataTextField = "GroupType";
                    ddl_ptype.DataValueField = "GroupType";
                    ddl_ptype.DataBind();
                    ddl_ptype.Items.Insert(0, new ListItem("SELECT GROUP TYPE", "ALL"));

                    BindGrid();
                }


            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }
    public DataTable GetAirline()
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SP_GetAirlinenames", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(Convert.ToString(TxtCharges.Text)))
            {
                #region Insert
                string GroupType = ddl_ptype.SelectedValue;
                string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
                if (!string.IsNullOrEmpty(AgentId))
                {
                    GroupType = "";
                }
                string Airline = Convert.ToString(Request["hidtxtAirline"]);
                string FlightName = Convert.ToString(Request["txtAirline"]);
                string AirCode = "";
                string AirlineName = "";

                if (!string.IsNullOrEmpty(FlightName))
                {
                    if (!string.IsNullOrEmpty(Airline))
                    {
                        AirlineName = Airline.Split(',')[0];
                        if (Airline.Split(',').Length > 1)
                        {
                            AirCode = Airline.Split(',')[1];
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                            return;
                        }
                    }
                }
                else
                {
                    AirlineName = "ALL";
                    AirCode = "ALL";
                }
                msgout = "";
                string TripType = Convert.ToString(DdlTripType.SelectedValue);
                string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);
                string IsActive = Convert.ToString(DdlStatus.SelectedValue);
                double Charges = 0;
                Charges = Convert.ToDouble(TxtCharges.Text);
                int flag = Insert(GroupType, AgentId, AirCode, AirlineName, TripType, TripTypeName, IsActive, Charges);
                if (flag > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');", true);
                    BindGrid();
                }
                else
                {
                    if (msgout == "EXISTS")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('already exists.');", true);
                        BindGrid();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');window.location='HoldBookingCharges.aspx'; ", true);
                    }
                }
                #endregion
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Enter Charges!!');", true);
                return;
            }

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='HoldBookingCharges.aspx'; ", true);
            return;
        }

    }
    private int Insert(string GroupType, string AgentId, string AirCode, string AirlineName, string TripType, string TripTypeName, string IsActive, double Charges)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "insert";
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightHoldBookingCharges", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@GroupType", GroupType);
            cmd.Parameters.AddWithValue("@AgentId", AgentId);
            cmd.Parameters.AddWithValue("@AirCode", AirCode);
            cmd.Parameters.AddWithValue("@AirlineName", AirlineName);
            cmd.Parameters.AddWithValue("@TripType", TripType);
            cmd.Parameters.AddWithValue("@TripTypeName", TripTypeName);
            cmd.Parameters.AddWithValue("@Charges", Charges);
            cmd.Parameters.AddWithValue("@IsActive", Convert.ToBoolean(IsActive));
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
            //lblName.Text = "Name: " + cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }
    public void BindGrid()
    {
        try
        {
            grd_P_IntlDiscount.DataSource = GetRecord();
            grd_P_IntlDiscount.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord()
    {
        DataTable dt = new DataTable();
        try
        {
            string TripType = Convert.ToString(DdlTripType.SelectedValue);
            adap = new SqlDataAdapter("SpFlightHoldBookingCharges", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@GroupType", ddl_ptype.SelectedValue);
            adap.SelectCommand.Parameters.AddWithValue("@TripType", TripType);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "select");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }


    protected void grd_P_IntlDiscount_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            grd_P_IntlDiscount.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void grd_P_IntlDiscount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            grd_P_IntlDiscount.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void grd_P_IntlDiscount_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int result = 0;
            Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
            int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());

            double Charges = 0;
            //Charges = Convert.ToDouble(TxtCharges.Text);
            TextBox GrdTxtCharges = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txt_Charges"));
            if (string.IsNullOrEmpty(Convert.ToString(GrdTxtCharges.Text)))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter charges!');", true);
                return;
            }
            else
            {
                Charges = Convert.ToDouble(GrdTxtCharges.Text);
            }
            // DropDownList ddl_Active = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_IsActive");
            //string Active = ddl_Active.SelectedValue;
            string IsActive = "True";//ddl_Active.SelectedValue;//           
            result = UpdateRecords(Id, Charges, IsActive);
            grd_P_IntlDiscount.EditIndex = -1;
            BindGrid();
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                SqlCommand cmd = new SqlCommand("SpFlightHoldBookingCharges", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }
            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_P_IntlDiscount.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }

    private int UpdateRecords(int Id, double Charges, string IsActive)
    {
        int flag = 0;
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightHoldBookingCharges", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", Id);
            cmd.Parameters.AddWithValue("@Charges", Charges);
            cmd.Parameters.AddWithValue("@IsActive", Convert.ToBoolean(IsActive));
            cmd.Parameters.AddWithValue("@CreatedBy", Convert.ToString(Session["UID"]));
            cmd.Parameters.AddWithValue("@ActionType", "GRIDUPDATE");
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;
    }
}