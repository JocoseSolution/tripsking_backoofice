﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NotificationMaster2.master" AutoEventWireup="true" CodeFile="HomeImage.aspx.cs" Inherits="Reports_banner_HomeImage" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../SummernotCss/summernote.min.css" rel="stylesheet" />
    <script src="../../SummernotCss/summernote.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <style>
        body {
            overflow-x: hidden;
        }

        .table img {
            max-height: 100px !important;
            max-width: 100px !important;
        }
    </style>
    <br />
    <br />
    <br />
    <div class="row">
        <div class="col-sm-10">
            <a href="<%=ResolveUrl("/DetailsPort/privilegePanel/Splashboard.aspx")%>" style="background: #0090c7; padding: 10px; border-radius: 5px; color: #fff; position: relative; top: -20px; margin-left: 133px;">Back to Dashboard</a>
        </div>
    </div>


    <div class="page-content">
        <div class="page-container index-section-1">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6" style="border: 1px solid #00000040; padding: 50px; border-radius: 10px;">
                    <label class="form-label" for="customFile">Default file input example</label>
                    <asp:FileUpload ID="fluHomeImage" runat="server" class="form-control" />
                    <br />
                    <asp:Button ID="btnHomeNote" runat="server" CssClass="btn btn-success pull-right" OnClientClick="return summerhaibhai();" Text="Save Home Image" OnClick="btnHomeNote_Click" />
                </div>
                <div class="col-sm-3">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <br />
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-10">
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                AllowSorting="True" PageSize="25" CssClass="table table-striped table-bordered table-hover" GridLines="None" Width="100%" OnRowDeleting="GridView1_RowDeleting">
                                <Columns>
                                    <asp:TemplateField HeaderText="Content">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdnId" Value='<%# Eval("id") %>' />
                                            <img src="/HomeImage/<%#Eval("ImageName")%>" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnNDelete" runat="server" OnClientClick="return confirm('Are you sure to delete this image?');" CssClass="pull-right btn btn-danger" CausesValidation="True" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="col-sm-1"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

