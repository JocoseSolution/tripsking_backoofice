﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NotificationMaster2.master" AutoEventWireup="true" CodeFile="b2bnotification.aspx.cs" Inherits="DetailsPort_Admin_b2bnotification" ValidateRequest="false" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="../../SummernotCss/summernote.min.css" rel="stylesheet" />
    <script src="../../SummernotCss/summernote.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <style>
        body {
            overflow-x: hidden;
        }

        .table img {
            max-height: 100px !important;
            max-width: 100px !important;
        }
    </style>
    <br />
    <br />
    <br />
    <div class="row">
        <div class="col-sm-10">
            <a href="<%=ResolveUrl("/DetailsPort/privilegePanel/Splashboard.aspx")%>" style="background: #0090c7; padding: 10px; border-radius: 5px; color: #fff; position: relative; top: -20px; margin-left: 133px;">Back to Dashboard</a>
        </div>
    </div>


    <div class="page-content">
        <div class="page-container index-section-1">
            <div class="col-sm-12">


                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txtSummerNote" CssClass="summernote" runat="server"></asp:TextBox>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
            <div class="col-sm-12" runat="server" id="hiddenTarea" style="display: none;">
                <asp:TextBox ID="txtNotTextArea" CssClass="NotTextArea" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-5">
                        <asp:DropDownList ID="ddlBannerType" runat="server" class="btn btn-secondary dropdown-toggle" Style="border: 1px solid #000; width: 50%;">
                            <asp:ListItem Value="afterlogin">After Login Banner</asp:ListItem>
                            <asp:ListItem Value="beforelogin">Before Login Banner</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-sm-5">
                        <asp:Label ID="lblmsg" runat="server" CssClass="pull-left text-left"></asp:Label>
                        <%--<input id="btnSummerNote" type="button" value="Save Notification" class="btn btn-success pull-right" onclick="return summerhaibhai();" />--%>
                        <asp:Button ID="btnSummerNote" runat="server" CssClass="btn btn-success pull-right" OnClientClick="return summerhaibhai();" Text="Save Notification" OnClick="btnSummerNote_Click" />
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>

            <div class="col-sm-12">
                <br />
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                            AllowSorting="True" PageSize="25" CssClass="table table-striped table-bordered table-hover" GridLines="None" Width="100%" OnRowDeleting="GridView1_RowDeleting">
                            <Columns>
                                <asp:TemplateField HeaderText="Content">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnId" Value='<%# Eval("id") %>' />
                                        <asp:Label ID="lblContent" runat="server" Text='<%#Eval("Content")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Location">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Type")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <%--<a href="#" class="btn btn-danger">Delete</a>--%>
                                        <asp:LinkButton ID="btnNDelete" runat="server" OnClientClick="return confirm('Are you sure to delete this notification?');" CssClass="pull-right btn btn-danger" CausesValidation="True" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Position">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPosition" runat="server" Text='<%#Eval("Position")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>

            <script type="text/javascript">
                $('.summernote').summernote({
                    height: 200
                });

                function summerhaibhai() {
                    debugger;
                    var myHtml = $('.summernote').summernote('code');
                    if (myHtml != "<p><br></p>") {
                        $(".NotTextArea").val(myHtml);
                        return true;
                    }
                    else {
                        alert("Please enter content.");
                        $("#txtSummerNote").focus();
                    }
                    return false;
                }
            </script>
        </div>
    </div>
</asp:Content>

