﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_banner_HomeImage : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (!Page.IsPostBack)
        {
            BindGrid();
        }
    }

    private void BindGrid()
    {
        try
        {
            DataTable dt = new DataTable();
            adap = new SqlDataAdapter("sp_GetHomeImageBanner", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.Fill(dt);

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string counterValue = ((HiddenField)GridView1.Rows[e.RowIndex].FindControl("hdnId")).Value;
            SqlCommand cmd = new SqlCommand("delete from HomeImageBanner where id=" + counterValue, con);
            cmd.CommandType = CommandType.Text;
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            con.Open();
            int isSuccess = cmd.ExecuteNonQuery();
            if (isSuccess > 0)
            {
                Response.Write("<script>alert('Image deleted succesfully.')</script>");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        BindGrid();
    }

    protected void btnHomeNote_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        adap = new SqlDataAdapter("sp_GetHomeImageBanner", con);
        adap.SelectCommand.CommandType = CommandType.StoredProcedure;
        adap.Fill(dt);

        if (dt.Rows.Count == 0)
        {
            int fluImg = fluHomeImage.PostedFile.ContentLength;
            if (fluImg > 0)
            {
                FileInfo finfo_pan = new FileInfo(fluHomeImage.FileName);
                string fileExtension_Img = finfo_pan.Extension.ToLower();

                if (fileExtension_Img == ".jpg" || fileExtension_Img == ".jpeg" || fileExtension_Img == ".png")
                {
                    if (fluHomeImage.HasFile)
                    {
                        string file_pan = string.Empty;

                        Random r = new Random();
                        int CID = r.Next(10, 50);


                        string kkpath = "E:\\Software\\QuickStartFile\\nolonger\\Bitbucket\\tripsking_front_newserver\\HomeSlideImage\\" + "HImg_" + CID;
                        kkpath= kkpath+ ".jpg";
                        fluHomeImage.SaveAs(kkpath.ToString());

                        string filepath_pan = Server.MapPath("/HomeImage/" + "HImg_" + CID);
                        filepath_pan = filepath_pan + ".jpg";
                        fluHomeImage.SaveAs(filepath_pan.ToString());
                        file_pan = Path.GetFileName("HImg_" + CID + ".jpg");

                        if (!string.IsNullOrEmpty(file_pan))
                        {
                            SqlCommand cmd = new SqlCommand("sp_InsertHomeImageBanner", con);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@ImageName", file_pan);
                            if (con.State == ConnectionState.Closed)
                            {
                                con.Open();
                            }
                            int k = cmd.ExecuteNonQuery();
                            if (k != 0)
                            {
                                BindGrid();
                                Response.Write("<script>alert('Image inserted succesfully.')</script>");
                            }
                            con.Close();
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('Please select image.')</script>");
                    }
                }
                else
                {
                    Response.Write("<script>alert('JPG image support only.')</script>");
                }
            }
        }
        else
        {
            Response.Write("<script>alert('You can set only one(1) image at a time.')</script>");
        }
    }
}