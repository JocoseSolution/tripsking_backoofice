﻿var QSHandler;
$(document).ready(function () {
    QSHandler = new QSHelper;
    QSHandler.BindEvents()
});
var QSHelper = function () {
    this.flight = $("flight");
    this.txtDepCity1 = $("#txtDepCity1");
    this.txtArrCity1 = $("#txtArrCity1");
    this.hidtxtDepCity1 = $("#hidtxtDepCity1");
    this.hidtxtArrCity1 = $("#hidtxtArrCity1");
    this.rdbOneWay = $("#rdbOneWay");
    this.rdbRoundTrip = $("#rdbRoundTrip");
    this.txtDepDate = $("#txtDepDate");
    this.txtRetDate = $("#txtRetDate");
    this.hidtxtDepDate = $("#hidtxtDepDate");
    this.hidtxtRetDate = $("#hidtxtRetDate");
    this.trRetDateRow = $("#trRetDateRow");
    this.TripType = $("input[name=TripType]");
    this.Adult = $("#Adult");
    this.Child = $("#Child");
    this.Infant = $("#Infant");
    this.Cabin = $("#Cabin");
    this.txtAirline = $("#txtAirline");
    this.hidtxtAirline = $("#hidtxtAirline");
    this.chkNonstop = $("#chkNonstop");
    this.chkAdvSearch = $("#chkAdvSearch");
    this.trAdvSearchRow = $("#trAdvSearchRow");
    this.LCC_RTF = $("#LCC_RTF");
    this.GDS_RTF = $("#GDS_RTF")
};
QSHelper.prototype.BindEvents = function() {
    var e = this;
    var t = e.queryStr();
    e.flight.val(t.flight);
    e.txtDepCity1.val(t.txtDepCity1);
    e.txtArrCity1.val(t.txtArrCity1);
    e.hidtxtDepCity1.val(t.hidtxtDepCity1);
    e.hidtxtArrCity1.val(t.hidtxtArrCity1);
    e.txtDepDate.val(t.txtDepDate);
    e.txtRetDate.val(t.txtRetDate);
    e.hidtxtDepDate.val(t.hidtxtDepDate);
    e.hidtxtRetDate.val(t.hidtxtRetDate);
    e.Adult.val(t.Adult);
    e.Child.val(t.Child);
    e.Infant.val(t.Infant);
    e.Cabin.val(t.Cabin);
    e.txtAirline.val(t.txtAirline);
    e.hidtxtAirline.val(t.hidtxtAirline);
    e.chkAdvSearch.val(t.chkAdvSearch);
    e.trAdvSearchRow.val(t.trAdvSearchRow);
    if (t.TripType == "rdbOneWay") {
        $("#rdbOneWay").attr("checked", true); e.rdbOneWay.val("rdbOneWay")
    }
    else if (t.TripType == "rdbRoundTrip") {
        $("#rdbRoundTrip").attr("checked", true);
        e.rdbRoundTrip.val("rdbRoundTrip")
        $("#trRetDateRow").show();
    }
    if (t.Nstop == "TRUE") {
        $("#chkNonstop").attr("checked", true); e.chkNonstop.val("TRUE")
    }
    else if (t.Nstop == "FALSE") {
        $("#chkNonstop").attr("checked", false); e.chkNonstop.val("FALSE")
    }
    if (t.RTF == "TRUE") {
        $("#LCC_RTF").attr("checked", true); e.LCC_RTF.val("TRUE")
    }
    else if (t.RTF == "FALSE") {
        $("#LCC_RTF").attr("checked", false); e.LCC_RTF.val("FALSE")
    }
    if (t.GRTF == "TRUE") {
        $("#GDS_RTF").attr("checked", true); e.GDS_RTF.val("TRUE")
    }
    else if (t.GRTF == "FALSE") {
        $("#GDS_RTF").attr("checked", false); e.GDS_RTF.val("FALSE")
    }
};
QSHelper.prototype.queryStr = function () {
    var e = decodeURI(window.location.search.substring(1));
    if (e == false | e == "") return null;
    var t = e.split("&");
    var n = {};
    var r;
    for (r = 0; r < t.length; r++)
    {
        var i = t[r].indexOf("=");
        if (i == -1) n[t[r]] = "";
        else n[t[r].substring(0, i)] = t[r].substr(i + 1);
        if (r == 17) { break }
    }
   
    return n
}