﻿<%@ WebHandler Language="C#" Class="AirHandler" %>

using System;
using System.Web;
using System.Web.SessionState;
using Newtonsoft.Json;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class AirHandler : IHttpHandler, IReadOnlySessionState
{
    private System.Data.SqlClient.SqlConnection Con = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/json";
        STD.BAL.FlightCommonBAL FCBAL = new STD.BAL.FlightCommonBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        List<STD.Shared.UserFlightSearch> FCSHARED = new List<STD.Shared.UserFlightSearch>();
        FCSHARED = FCBAL.GetUserFlightSearch(context.Session["UID"].ToString(), context.Session["User_Type"].ToString());
        bool isCoupon = false;
        STD.Shared.FlightSearch req = new STD.Shared.FlightSearch();
        if (FCSHARED.Count > 0)
        {
            req.Trip1 = context.Request["Trip1"].ToString();
            req.Trip = req.Trip1 == "D" ? STD.Shared.Trip.D : STD.Shared.Trip.I;
            req.TripType1 = context.Request["TripType1"].ToString();
            req.TripType = req.TripType1 == "rdbOneWay" ? STD.Shared.TripType.OneWay : STD.Shared.TripType.RoundTrip;
            req.Adult = Convert.ToInt16(context.Request["Adult"]);
            req.Child = Convert.ToInt16(context.Request["Child"]);
            req.AgentType = FCSHARED[0].AgentType; 
            req.AirLine = context.Request["AirLine"].ToString();
            req.ArrivalCity = context.Request["ArrivalCity"].ToString();
            req.DepartureCity = context.Request["DepartureCity"].ToString();
            req.DepDate = context.Request["DepDate"].ToString();
            req.DISTRID = "SPRING";
            req.GDSRTF = Convert.ToBoolean(context.Request["GDSRTF"]);
            req.HidTxtAirLine = context.Request["HidTxtAirLine"].ToString();
            req.HidTxtArrCity = context.Request["HidTxtArrCity"].ToString();
            req.HidTxtDepCity = context.Request["HidTxtDepCity"].ToString();
            req.Infant = Convert.ToInt16(context.Request["Infant"].ToString());
            req.IsCorp = Convert.ToBoolean(context.Request["IsCorp"]);
            req.NStop = Convert.ToBoolean(context.Request["NStop"]);
            req.OwnerId = FCSHARED[0].UserId;
            req.Provider = context.Request["Provider"].ToString();
            req.RetDate = context.Request["RetDate"].ToString();
            req.RTF = Convert.ToBoolean(context.Request["RTF"]);
            req.TypeId = FCSHARED[0].TypeId; 
            req.UserType = FCSHARED[0].UserType;
            isCoupon = Convert.ToBoolean(context.Request["isCoupon"]);
            req.TDS = STD.BAL.Data.Calc_TDS(Con.ConnectionString, req.OwnerId);
            req.UID = req.OwnerId;
            req.Cabin = context.Request["Cabin"].ToString();
        }
        STD.Shared.IFlt objI = new STD.BAL.FltService();
        string resultJson = "";
        string provider = "";

        if (req.Provider == "OF")
        {
            provider = "OF";
        }
        else if (req.Trip == STD.Shared.Trip.I)
        {
            provider = req.Provider;
        }
        else
        {
            provider = req.HidTxtAirLine.Replace(",", "");
        }


        if (isCoupon)
        {
            resultJson = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objI.FltSearchResultCoupon(req)) + ",\"Provider\":\"" + provider + "CPN\"}";
        }
        else
        {
            resultJson = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objI.FltSearchResult(req)) + ",\"Provider\":\"" + provider + "NRML\"}";

        }
        context.Response.Write(resultJson);
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}
