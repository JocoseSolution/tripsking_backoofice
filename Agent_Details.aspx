﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="Agent_Details.aspx.vb" Inherits="DetailsPort_Admin_Agent_Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%=ResolveUrl("~/CSS/lytebox.css")%>" rel="stylesheet" type="text/css" />

    

    <%-- <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <%-- <style type="text/css">
        input[type="text"], input[type="password"], select, radio, legend, fieldset
        {
            border: 1px solid #004b91;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>--%>
    <%--<link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />--%>
    <%--<script src="../../JS/lytebox.js" type="text/javascript"></script>--%>
    <%--  <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <%--<style type="text/css">
        .txtBox
        {
            width: 140px;
            height: 18px;
            line-height: 18px;
            border: 2px #D6D6D6 solid;
            padding: 0 3px;
            font-size: 11px;
        }
        .txtCalander
        {
            width: 100px;
            background-image: url(../../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
        }
    </style>--%>
   
    <style type="text/css">
        .page-wrapperss
        {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <div class="row">
        <div class="col-md-12 container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div id="ag_detail" runat="server">
                        <div class="row">
                            <div class="col-xs-2">
                                <div class="input-group">
                                   
                                    <input type="text" name="From" id="From" class="form-control input-text lg-width" readonly="readonly" placeholder="REG.FROM" />
                                 <span class="input-group-addon" style="background-color: #49cced">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="input-group">
                               
                                    <input type="text" name="To" id="To" class="form-control input-text lg-width" readonly="readonly" placeholder="REG.TO" />
                                           <span class="input-group-addon" style="background-color: #49cced">
                        <span class="glyphicon glyphicon-calendar"></span>
                                               </span>
                                </div>
                            </div>
                        
                            <div class="col-xs-2">
                                <div class="input-group">
                                
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" placeholder="AGENCY Name" 
                                        autocomplete="off" value=""
                                        class="form-control input-text med-width" />
                                                          <span class="input-group-addon" style="background-color: #49cced">
                        <span class="fa fa-black-tie"></span>
                                               </span>
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                                </div>
                            </div>
                                <div class="col-xs-2" style="margin-top: -5px">
                                <div class="input-group" id="td_SBS" runat="server">
                                    <label for="exampleInputPassword1" id="td_ddlSBS" runat="server"></label>
                                    <asp:DropDownList ID="ddl_stock" runat="server" CssClass="input-text med-width" Width="180px" >
                                        <asp:ListItem Text="MENCHANDISER:" Value=""></asp:ListItem>
                                        <asp:ListItem Text="All MENCHANDISER" Value="ALL"></asp:ListItem>
                                        <asp:ListItem Text="BRAND" Value="STAG"></asp:ListItem>
                                    </asp:DropDownList>
                        <%--                                        <span class="input-group-addon" style="background-color: #49cced">
                        <span class="glyphicon glyphicon-cog"></span>
                                               </span>--%>
                                     
                               
                                </div>
                            </div>
                             <div class="col-sm-2">
                                <div class="form-group" id="tr_AgentType" runat="server">
                                   <label for="exampleInputPassword1" id="tr_GroupType" runat="server"></label>
                                    <asp:DropDownList ID="DropDownListType" runat="server" CssClass="input-text sm-width" Width="180px" >
                                        <asp:ListItem Text="BRAND_TYPE" Value="" ></asp:ListItem>
                                        <asp:ListItem Text="Select" Value="Select" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                       
                                <div  class="col-sm-2" style="margin-top: 5px">
                                <div class="form-group">
     <asp:Button ID="btn_Search" runat="server" Text="Go" Width="74px" CssClass="btn btn-success" />

                                    <asp:Button ID="export" runat="server" Text="Export" CssClass="btn btn-success" />
                                </div>
                            </div>



                        </div>
                        <div class="row">
                           
                            <div class="col-md-2">
                                <div class="form-group" id="tr_SalesPerson" runat="server">
                                    <label for="exampleInputPassword1" id="tr_ddlSalesPerson" runat="server">Search Sales Person :</label>
                                    <asp:DropDownList ID="DropDownListSalesPerson" runat="server" AppendDataBoundItems="true"
                                        CssClass="selector">
                                        <asp:ListItem Text="Select" Value="Select" Selected="True"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        
                        </div>

                     </div>


                       

                            <div id="hidden_fld" style="display:none" runat="server">
<br />
                    <br />
                       <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>BRAND INFORMATION</h3>
                    </div>
                </div>

                <div class="panel-body">
                   
                    <div class="row tablesss">
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="col-lg-4 col-sm-4 col-xs-5">
                            USER ID
                </div>
                        <div class="col-lg-8 col-sm-8 col-xs-7" id="td_AgentID" runat="server">

                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="col-lg-4 col-sm-4 col-xs-5">
                           AVAILABLE BALANCE
                        </div>
                        <div class="col-lg-8 col-sm-8 col-xs-7" id="td_CrLimit" runat="server">

                    </div>
                    </div>
                </div>
                    <div class="row tablesss">
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="col-lg-4 col-sm-4 col-xs-5">
                          CREDIT LIMIT

                        </div>
                        <div class="col-lg-8 col-sm-8 col-xs-7">
                            <asp:Label ID="lblAgentLimit" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="col-lg-4 col-sm-4 col-xs-5">
                           DUE AMOUNT
                        </div>
                        <div class="col-lg-8 col-sm-8 col-xs-7">
                          <asp:Label ID="lblDueAmount" runat="server"></asp:Label>  
                        </div>
                    </div>
                    

                    <div class="row tablesss" style="display:none;">
                    <div class="col-lg-6 col-sm-6 col-xs-12">
                        <div class="col-lg-4 col-sm-4 col-xs-5">
                      TDS
                    </div>
                        <div class="col-lg-8 col-sm-8 col-xs-7" id="td_tds" runat="server">

                        </div>
                    </div>
                     <div class="col-lg-2">
                            <label for="exampleInputPassword1"> ADDRESS</label>
                     LAST TRANSACTION DATE

                        </div>
                        <div class="col-lg-8 col-sm-8 col-xs-7" id="td_LTDate" runat="server">
                          
                    </div>
                    </div>
               <br />
                    <br />
              <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>BRAND ADDRESS</h3>
                    </div>
                </div>
                <div class="row tablesss">
                   
                        
                        <div class="col-lg-2">
                            <label for="exampleInputPassword1"> ADDRESS</label>  
                            <asp:TextBox ID="txt_Address"  runat="server" TextMode="MultiLine" Enabled="False" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                   
                    <div class="col-lg-2">
                       
                       <label for="exampleInputPassword1">BRAND NAME</label> 
                            <asp:TextBox ID="txt_AgencyName"  runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz0123456789');" MaxLength="50"></asp:TextBox>
                        </div>
                   
               

               
                 
                       
                        <div class="col-lg-2">
                            <label for="exampleInputPassword1">CITY</label> 
                            <asp:TextBox ID="txt_City" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>
                    
                    <div class="col-lg-2">
                       <label for="exampleInputPassword1">STATE</label> 
                            <asp:TextBox ID="txt_State" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>
                      
                  

                
                    <div class="col-lg-2">
                          
                       <label for="exampleInputPassword1">ZIP CODE</label> 
                            <asp:TextBox ID="txt_zip"  runat="server" Enabled="False" MaxLength="8" CssClass="input-text full-width" onkeypress="return checkitt(event)"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="regexpcontactZipCode" runat="server" ControlToValidate="txt_zip"
                                ValidationGroup="contactValidation" ForeColor="Red" ErrorMessage="Enter Only Digit"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                        </div>
                      
                    
                        <div class="col-lg-2">
                             <label for="exampleInputPassword1">COUNTRY</label> 
                            <asp:TextBox ID="txt_Country" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>

               <br />
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>BRAND DETAILS</h3>
                    </div>
                </div>

                <div class="row tablesss">
                    
                       <div class="col-lg-2">
                           <label for="exampleInputPassword1"> NAME</label> 
                            <asp:TextBox ID="txt_title" runat="server" placeholder="" Enabled="False" Width="100px" CssClass="input-text full-width"></asp:TextBox>

                        </div>
                   
                    <div class="col-lg-2">
                        <label for="exampleInputPassword1">FIRST NAME</label>
                        <asp:TextBox ID="txt_Fname" runat="server" Enabled="False" Width="90%" CssClass="input-text full-width"></asp:TextBox>


                    </div>
                     <div class="col-lg-2">
                         <label for="exampleInputPassword1">SURNAME</label>
                        <asp:TextBox ID="txt_Lname" runat="server" Enabled="False" Width="90%" CssClass="input-text full-width"></asp:TextBox>
                    </div>
                
                   
                         <div class="col-lg-2"><label for="exampleInputPassword1">MOBILE</label>
                            <asp:TextBox ID="txt_Mobile" runat="server"  MaxLength="10" Enabled="False" onkeypress="return checkitt(event)" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                  
                         <div class="col-lg-2">
                             <label for="exampleInputPassword1">EMAIL</label>
                            <asp:TextBox ID="txt_Email"  runat="server" Enabled="False" CssClass="input-text full-width" MaxLength="50"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_Email" ValidationGroup="group1" ErrorMessage=" enter valid email " ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                            </asp:RegularExpressionValidator>
                        </div>
                   

               

                    
                         <div class="col-lg-2">
                             <label for="exampleInputPassword1">PAN NAME</label>
                            <asp:TextBox ID="TxtNameOnPancard"  runat="server" Enabled="False" CssClass="input-text full-width" MaxLength="50" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');"></asp:TextBox>
                        </div>
                     </div>
                   

                <div class="row tablesss">
                        <div class="col-lg-2">
                            <label for="exampleInputPassword1">PAN NO</label>
                            <asp:TextBox ID="txt_Pan"  runat="server" Enabled="False" MaxLength ="15"  CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz1234567890');"></asp:TextBox>
                        </div>
                   
                   
                    
                         <div class="col-lg-2">
                             <label for="exampleInputPassword1">FAX NO</label>

                            <asp:TextBox ID="txt_Fax"   runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,'1234567890');"></asp:TextBox>
                        </div>

                   
                  
                         <div class="col-lg-2">
                              <label for="exampleInputPassword1">OTP LOGIN</label>
                            <asp:CheckBox ID="OPTLOGIN"  runat="server" />
                        </div>
                   

                        
                        <div class="col-lg-2">
                             <label for="exampleInputPassword1">EXPPASS</label>
                            <asp:CheckBox ID="EXPPASS" placeholder="" runat="server" />
                        </div>
                   
               

               
                       <%-- <div class="row tablesss"  >
                    <div class="col-lg-6 col-sm-6 col-xs-12" id="td_pwd" runat="server"  >
                        <div class="col-lg-4 col-sm-4 col-xs-5">
                            Password
                        </div>
                        <div class="col-lg-8 col-sm-8 col-xs-7">
                            <asp:TextBox ID="txt_pwd" runat="server" Enabled="False" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                    </div>
                </div>--%>
                        <div class="col-lg-2" id="td_pwd" runat="server" style="display:none;">
                             <label for="exampleInputPassword1"> PASSWORD</label>
                            <asp:TextBox ID="txt_pwd" placeholder="" runat="server" Enabled="False" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                    </div>
                
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>UPDATE DETAILS</h3>
                    </div>
                </div>
                <div class="row tablesss">
                  <%-- <div>TYPE</div>--%>
                        <div class="col-lg-2">
                            <label for="exampleInputPassword1">TYPE</label> 
                            <asp:DropDownList ID="ddl_type" runat="server"  CssClass="input-text full-width">
                            </asp:DropDownList>
                        </div>
                   
                   <%-- <div> SALES TREF</div>--%>
                        <div class="col-lg-2">
                            <label for="exampleInputPassword1">SALES</label> 
                            <%--<asp:TextBox ID="txt_saleref" runat="server" CssClass="form-control"></asp:TextBox>--%>
                             <asp:DropDownList ID="Sales_DDL" runat="server"  CssClass="input-text full-width">
                                    </asp:DropDownList>
                        </div>
                   
                   <%-- <div>
                           ACTIVATION
                        </div>--%>
                        <div class="col-lg-2">
                            <label for="exampleInputPassword1">ACTIVATION</label> 
                            <asp:DropDownList ID="ddl_activation" runat="server" CssClass="input-text full-width">
                                <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                <asp:ListItem Value="NOT ACTIVE">NOT ACTIVE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    
                        
                   <%-- <div>
                            TICKETING ACTIVATION
                        </div>--%>
                       <div class="col-lg-2">
                           <label for="exampleInputPassword1">TICKETING ACTIVITY</label> 
                                    
                            <asp:DropDownList ID="ddl_TicketingActiv" runat="server" CssClass="input-text full-width">
                                <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                <asp:ListItem Value="NOT ACTIVE">NOT ACTIVE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
              
               
                    
                    
                        <div class="col-lg-2">
                             <label for="exampleInputPassword1">CREDIT</label>
                           <asp:TextBox ID="TxtAgentCredit"   placeholder=" " runat="server" CssClass="input-text full-width" ReadOnly="true"></asp:TextBox>
                        </div>
                   
              

                 <%--Update Password box
                <div id="showHeadingBox">--%>
                    <%--<div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">--%>
                            <asp:HyperLink ID="btnHyper"  runat="server" onclick="showBox();"></asp:HyperLink>
                        </div>
                    </div>
                </div>
                <div id="td_displayBox" style="display:none;width:95%;margin:auto;padding-bottom :10px;border:1px solid #94aee5;border-radius:5px;">
                    <br />
                    <h4 style="font-size:16px;font-weight:bold;"></h4>
                    <br />
                    
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                 <label for="exampleInputPassword1">UPDATE BRAND PASSWORD</label>
                              <asp:Label ID="lbl_currentPassMsg" placeholder="" runat="server" Text="Current Password:" Visible="false" Font-Size="12px"></asp:Label>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <label for="exampleInputPassword1">CURRENT PASSWORD</label>
                               <asp:Label ID="lbl_currentPassword"   runat ="server" Text="" Visible="false" Font-Size="12px" Font-Bold ="true"></asp:Label>
                            </div>
                        </div>
                    </div>
             
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <label for="exampleInputPassword1">NEW PASSWORD</label>
                                <asp:TextBox ID="txtNewPassword" placeholder="" runat="server" TextMode="Password" MaxLength="16" CssClass="input-text full-width"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                 <label for="exampleInputPassword1">CONFIRM PASSWORD</label>
                                <asp:TextBox ID="txtConfirmPassword" placeholder="CONFIRM PASSWORD" runat="server" TextMode="Password" MaxLength="16" CssClass="input-text full-width"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="large-2 medium-2 small-12">
                        <a onclick="hideBox();" href="#">
                            <img src="<%= ResolveUrl("~/Images/close.png")%>" align="right" alt="Close" title="Close this box" /></a>
                    </div>
                    <br />
                </div>
               
               
                <%--End password updation--%>

                <div class="container">
                   <%-- OnClientClick="return validatetype()"--%>
                        <asp:Button ID="btn_update" runat="server" ValidationGroup="group1" Text="Update" CssClass="btn btn-success" OnClientClick="return validatetype()"/>
                    </div>
                   </div>
                
                           
        <asp:HiddenField ID="HdnId" runat="server" />

                        <div class="row" style="background-color: #fff; overflow: auto;" runat="server">

                          
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                  OnRowEditing="grd_P_IntlDiscount_RowEditing"       PageSize="40"   CssClass="table" GridLines="None" Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Debit/Credit" SortExpression="user_id" Visible="false">
                                                <ItemTemplate>
                                                   <a target="_blank" href="../Distr/UploadAmount.aspx?AgentID=<%#Eval("user_id")%>"
                                                        rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Debit/Credit </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BRAND NAME" SortExpression="Agency_Name" ItemStyle-ForeColor="Black">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="USER_ID" SortExpression="user_id" ItemStyle-ForeColor="Black">
                                                <ItemTemplate>
                                                   <%-- <a href='Update_Agent.aspx?AgentID=<%#Eval("user_id")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                        target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                        --%>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("user_id")%>'></asp:Label>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AgencyId" HeaderText="BRAND_ID" SortExpression="AgencyId" />
                                            <asp:BoundField DataField="Agent_Type" HeaderText="BRAND_TYPE" SortExpression="Agent_Type" />
                                            <%-- <asp:BoundField DataField="Crd_Limit" HeaderText="Credit Limit" SortExpression="Crd_Limit" />--%>
                                            <asp:BoundField DataField="timestamp_create" HeaderText="REGISTRATION_DATE" SortExpression="timestamp_create" />
                                            <asp:BoundField DataField="Crd_Trns_Date" HeaderText="TRANSACTION_DATE" SortExpression="timestamp_create" />
                                            <asp:BoundField DataField="Mobile" HeaderText="MOBILE" SortExpression="Mobile" />
                                            <asp:BoundField DataField="Email" HeaderText="EMAIL" SortExpression="Email" />
                                            <asp:BoundField DataField="SalesExecID" HeaderText="TRADESPERSON Ref." SortExpression="SalesExecID" />
                                            <asp:BoundField DataField="Agent_status" HeaderText="STATUS" SortExpression="Agent_status" />
                                            <asp:BoundField DataField="Online_Tkt" HeaderText="ONLINE_TKT" SortExpression="Online_Tkt" />

                                            <asp:BoundField DataField="Balance" HeaderText="BALANCE" SortExpression="Balance" />
                                            <asp:BoundField DataField="DueAmount" HeaderText="DUEAMOUNT" SortExpression="DueAmount" />
                                            <asp:BoundField DataField="AgentLimit" HeaderText="LIMIT" SortExpression="AgentLimit" />
                                            <asp:BoundField DataField="NamePanCard" HeaderText="PANCARD NAME" SortExpression="NamePanCard" />
                                            <asp:BoundField DataField="PanNo" HeaderText="PAN NO." SortExpression="AgentLimit" />
                                       
                                             <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                       
                                                         <asp:Button ID="Button1" CommandArgument='<%#Eval("user_id")%>' CommandName="Edit" OnClientClick="showdiv()"  CssClass="btn btn-info" runat="server" Text="Edit"  Font-Bold="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField> </Columns>
                                      
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                 
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                   
                     
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                          <EmptyDataTemplate>No records found</EmptyDataTemplate>
                                    </asp:GridView>
                        
                        </div>


                      <%--  <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function Confirm(form) {
            alert("Record insert successfully!");
            form.submit();
        }
</script>


    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
     <script type="text/javascript">
         function showdiv() {
             debugger;
             $("#hiddenfield").show();
             $("#divcrd").hide();
             //$("#show").show();
         }
         ////$("#show").click(function () {
         ////    $("#divMoreFilter").show();
         ////    $("#hide").show();
         ////    $("#show").hide();
         ////});

    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>


    </span>


</asp:Content>
