﻿<%@ WebHandler Language="C#" Class="NotificationHandler" %>

using System;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;

public class NotificationHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain"; string myHtml = "";
        try
        {

            DataSet ds = new DataSet();
            string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            SqlConnection con = new SqlConnection(constr);
            con.Open();
            SqlCommand cmd = new SqlCommand("NotificationSp_PP");
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@startDate", "2016-06-15 15:15:23.210");

            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            sda.SelectCommand = cmd;

     
            sda.Fill(ds);

            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            myHtml += "<div><b>CancelOrder_Id:</b> " + (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["OrderId"])) ? "<a href='" + context.Request.Url.GetLeftPart(System.UriPartial.Authority) + "/DetailsPort/Refund/TktRptDom_RefundRequest.aspx'>" + Convert.ToString(ds.Tables[0].Rows[i]["OrderId"]) : "").ToString() + "</a>" + "_" + (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["Agency_Name"])) ? Convert.ToString(ds.Tables[0].Rows[i]["Agency_Name"]) : "").ToString() + "</div>";
                            myHtml += "<div class='clear'></div>";
                        }

                    }
                    if (ds.Tables[1].Rows.Count > 0)
                    {


                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                            {
                                myHtml += "<div><b>HoldOrder_Id:</b> " + (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["OrderId"])) ? "<a href='" + context.Request.Url.GetLeftPart(System.UriPartial.Authority) + "/DetailsPort/HoldPNR/DomHoldPNRRequest.aspx'>" + Convert.ToString(ds.Tables[1].Rows[i]["OrderId"]) : "").ToString() + "</a>" + "_" + (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["AgencyName"])) ? Convert.ToString(ds.Tables[1].Rows[i]["AgencyName"]) : "").ToString() + "</div>";

                                myHtml += "<div class='clear'></div>";
                            }

                        }

                    }
                    if (ds.Tables[2].Rows.Count > 0)
                    {

                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                            {
                                myHtml += "<div><b>ReissueOrder_Id:</b> " + (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[2].Rows[i]["OrderId"])) ? "<a href='" + context.Request.Url.GetLeftPart(System.UriPartial.Authority) + "/DetailsPort/Reissue/TktRptDom_ReIssueRequest.aspx'>" + Convert.ToString(ds.Tables[2].Rows[i]["OrderId"]) : "").ToString() + "</a>" + "_" + (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[2].Rows[i]["Agency_Name"])) ? Convert.ToString(ds.Tables[2].Rows[i]["Agency_Name"]) : "").ToString() + "</div>";

                                myHtml += "<div class='clear'></div>";

                            }

                        }
                        myHtml += "<hr/>";
                    }



                }

            }


            context.Session.Timeout = context.Session.Timeout * 6000000;
        }
        catch(Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        
        context.Response.Write(myHtml);

    
    }
        

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}