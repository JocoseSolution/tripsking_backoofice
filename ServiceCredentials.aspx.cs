﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_ServiceCredentials : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href='PrivilegePanel/Dashboard.aspx' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight Setting </a><a class='current' href='#'>Search Credential</a>";
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }
            //if (Convert.ToString(Session["User_Type"]).ToUpper() != "ADMIN")
            //{
            //    Response.Redirect("~/Login.aspx");
            //}        
            if (!IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }
    public void BindGrid()
    {
        try
        {
            grd_P_IntlDiscount.DataSource = GetServiceCredential();
            grd_P_IntlDiscount.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    public DataTable GetServiceCredential()
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SpFlightServiceCredential", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "select");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {

            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            con.Close();
            adap.Dispose();
        }
        return dt;
    }
    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                //string IPAddress = Request.ServerVariables["REMOTE_ADDR"];
                SqlCommand cmd = new SqlCommand("SpFlightServiceCredential", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Counter", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }

            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            //ErrorLogTrace.WriteErrorLog(ex, "Flight")
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete?')){ return false; };";
                }
            }
        }
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_P_IntlDiscount.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (DdlProvider.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select provider');", true);
                return;
            }
            else
            {
                #region Insert

                string Airline = Convert.ToString(Request["hidtxtAirline"]);
                string FlightName = Convert.ToString(Request["txtAirline"]);
                string AirlineCode = "";
                string AirlineName = "";

                if (!string.IsNullOrEmpty(FlightName))
                {
                    if (!string.IsNullOrEmpty(Airline))
                    {
                        AirlineName = Airline.Split(',')[0];
                        if (Airline.Split(',').Length > 1)
                        {
                            AirlineCode = Airline.Split(',')[1];
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                            return;
                        }
                    }
                }
                else
                {
                    AirlineName = "ALL";
                    AirlineCode = "ALL";
                }


                string CorporateID = Convert.ToString(TxtCorporateID.Text);
                string UserID = Convert.ToString(TxtUserID.Text);
                string Password = Convert.ToString(TxtPassword.Text);
                string LoginID = Convert.ToString(TxtLoginID.Text);
                string LoginPwd = Convert.ToString(TxtLoginPwd.Text);
                string ServerUrlOrIP = Convert.ToString(TxtServerUrlOrIP.Text);
                string BkgServerUrlOrIP = Convert.ToString(TxtBkgServerUrlOrIP.Text);
                string Port = "";//Convert.ToString(TxtPort.Text);
                string Provider = Convert.ToString(DdlProvider.Text);
                string CarrierAcc = Convert.ToString(TxtCarrierAcc.Text);
                string SearchType = "";//Convert.ToString(TxtSearchType.Text);
                string Status = Convert.ToString(DdlStatus.SelectedValue);
                //string ResultFrom = Convert.ToString(TxtResultFrom.Text);
                string ResultFrom = Convert.ToString(DdlResultFrom.SelectedValue);
                //string CrdType = Convert.ToString(TxtCrdType.Text);
                string CrdType = Convert.ToString(DdlCrdType.SelectedValue);

                string TripType = Convert.ToString(DdlTripType.SelectedValue);
                string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);
                if (Provider == "6E")
                {
                    AirlineName = "Indigo";
                    AirlineCode = Provider;
                }
                else if (Provider == "SG")
                {
                    AirlineName = "Spicejet";
                    AirlineCode = Provider;
                }
                else if (Provider == "G8")
                {
                    AirlineName = "GoAir";
                    AirlineCode = Provider;
                }
                msgout = "";
                int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType, TripType, TripTypeName, AirlineCode, AirlineName);
                if (flag > 0)
                {
                    // ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');window.location='ServiceCredentials.aspx'; ", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.!!');", true);
                    ClearValue();
                    BindGrid();
                }
                else
                {
                    if (msgout == "EXISTS")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Already exists,Please update..');", true);
                        BindGrid();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');window.location='ServiceCredentials.aspx'; ", true);
                    }
                }
                #endregion
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='ServiceCredentials.aspx'; ", true);
        }

    }

    private int InsertServiceCredential(string CorporateID, string UserID, string Password, string LoginID, string LoginPwd, string ServerUrlOrIP, string BkgServerUrlOrIP, string Port, string Provider, string CarrierAcc, string SearchType, string Status, string ResultFrom, string CrdType, string TripType, string TripTypeName, string AirlineCode, string AirlineName)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "insert";
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightServiceCredential", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CorporateID", CorporateID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@Password", Password);
            cmd.Parameters.AddWithValue("@LoginID", LoginID);
            cmd.Parameters.AddWithValue("@LoginPwd", LoginPwd);
            cmd.Parameters.AddWithValue("@ServerUrlOrIP", ServerUrlOrIP);
            cmd.Parameters.AddWithValue("@BkgServerUrlOrIP", BkgServerUrlOrIP);
            cmd.Parameters.AddWithValue("@Port", Port);
            cmd.Parameters.AddWithValue("@Provider", Provider);
            cmd.Parameters.AddWithValue("@CarrierAcc", CarrierAcc);
            cmd.Parameters.AddWithValue("@SearchType", SearchType);
            cmd.Parameters.AddWithValue("@Status", Convert.ToBoolean(Status));
            cmd.Parameters.AddWithValue("@ResultFrom", ResultFrom);
            cmd.Parameters.AddWithValue("@CrdType", CrdType);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);

            cmd.Parameters.AddWithValue("@TripType", TripType);
            cmd.Parameters.AddWithValue("@TripTypeName", TripTypeName);
            cmd.Parameters.AddWithValue("@AirlineCode", AirlineCode);
            cmd.Parameters.AddWithValue("@AirlineName", AirlineName);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;

    }

    public void ClearValue()
    {
        TxtCorporateID.Text = "";
        TxtUserID.Text = "";
        TxtPassword.Text = "";
        TxtLoginID.Text = "";
        TxtLoginPwd.Text = "";
        TxtServerUrlOrIP.Text = "";
        TxtBkgServerUrlOrIP.Text = "";
        string Port = "";//Convert.ToString(TxtPort.Text);
        DdlProvider.SelectedValue = "0";
        TxtCarrierAcc.Text = "";
        // DdlStatus.SelectedValue="";        
        // DdlResultFrom.SelectedValue="";        
        // DdlCrdType.SelectedValue = "";
        //DdlStatus.SelectedValue= "";
        //DdlStatus.SelectedValue = "";  

    }



    protected void grd_P_IntlDiscount_RowEditing(object sender, GridViewEditEventArgs e)
    {
       
        //var arg = (sender as Button).CommandArgument;
        //string ID = arg.ToString();
        Label txt = grd_P_IntlDiscount.Rows[e.NewEditIndex].FindControl("lblId") as Label;
        string name = txt.Text;

        HdnId.Value = ID;
 
                BindCredential(name);
     
      
    }
    private void BindCredential(string Id)
    {
        DataTable dt = new DataTable();
        try
        {

            if (!string.IsNullOrEmpty(Convert.ToString(Id)))
            {
                dt = GetCredential(Convert.ToInt32(Id));
                if (dt.Rows.Count > 0)
                {
                    //string CorporateID = Convert.ToString(TxtCorporateID.Text);
                    //string UserID = Convert.ToString(TxtUserID.Text);
                    //string Password = Convert.ToString(TxtPassword.Text);
                    //string LoginID = Convert.ToString(TxtLoginID.Text);
                    //string LoginPwd = Convert.ToString(TxtLoginPwd.Text);
                    //string ServerUrlOrIP = Convert.ToString(TxtServerUrlOrIP.Text);
                    //string BkgServerUrlOrIP = Convert.ToString(TxtBkgServerUrlOrIP.Text);
                    //string Port = Convert.ToString(TxtPort.Text);
                    //string Provider = Convert.ToString(DdlProvider.Text);
                    //string CarrierAcc = Convert.ToString(TxtCarrierAcc.Text);
                    //string SearchType = Convert.ToString(TxtSearchType.Text);
                    //string Status = Convert.ToString(DdlStatus.Text);
                    //string ResultFrom = Convert.ToString(TxtResultFrom.Text);
                    //string CrdType = Convert.ToString(TxtCrdType.Text);
                    //int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);

                    TxtAirline.Text = Convert.ToString(dt.Rows[0]["AirlineName"]);
                    txtTripType.Text = Convert.ToString(dt.Rows[0]["TripTypeName"]);

                    TxtCorporateID1.Text = Convert.ToString(dt.Rows[0]["CorporateID"]);
                    TxtUserID1.Text = Convert.ToString(dt.Rows[0]["UserID"]);
                    TxtPassword1.Text = Convert.ToString(dt.Rows[0]["Password"]);
                    TxtLoginID1.Text = Convert.ToString(dt.Rows[0]["LoginID"]);
                    TxtServerUrlOrIP1.Text = Convert.ToString(dt.Rows[0]["ServerUrlOrIP"]);
                    TxtBkgServerUrlOrIP1.Text = Convert.ToString(dt.Rows[0]["BkgServerUrlOrIP"]);
                    // TxtPort.Text = Convert.ToString(dt.Rows[0]["Port"]);                
                    TxtCarrierAcc1.Text = Convert.ToString(dt.Rows[0]["CarrierAcc"]);
                    // TxtSearchType.Text = Convert.ToString(dt.Rows[0]["SearchType"]);                
                    //DdlResultFrom1.SelectedValue = Convert.ToString(dt.Rows[0]["ResultFrom"]);
                    //DdlCrdType1.SelectedValue = Convert.ToString(dt.Rows[0]["CrdType"]);
                    lblProvider.Text = Convert.ToString(dt.Rows[0]["Provider"]);
                    TxtLoginPwd1.Text = Convert.ToString(dt.Rows[0]["LoginPwd"]);
                    string Status = Convert.ToString(dt.Rows[0]["Status"]).ToLower();
                    string Provider = Convert.ToString(dt.Rows[0]["Provider"]);
                    if (Provider == "1G" || Provider == "1GINT")
                    {

                        CorporateID.Visible = true;
                        CarrierAcc.Visible = true;
                        ResultFrom.Visible = false;
                        Url.Visible = false;
                        LoginID.Visible = false;
                        LoginPassword.Visible = false;

                    }
                    //else if(Provider=="G8")
                    //{
                    //    CorporateID.Visible=true;
                    //    CarrierAcc.Visible=false;
                    //    ResultFrom.Visible=true;
                    //    Url.Visible=false;
                    //    LoginID.Visible=true;
                    //    LoginPassword.Visible=true;
                    //}
                    else
                    {
                        CorporateID.Visible = true;
                        CarrierAcc.Visible = false;

                        
                        ResultFrom.Visible = true;
                        Url.Visible = false;
                        LoginID.Visible = false;
                        LoginPassword.Visible = false;
                    }
                    if (Status == "true")
                    {
                        DdlStatus.SelectedValue = "true";
                    }
                    else if (Status == "false")
                    {
                        DdlStatus.SelectedValue = "false";
                    }
                    // int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);               
                }
                else
                {
                    BtnSubmit.Visible = false;
                }
            }
            divcrd.Style.Add("display", "none");
            hiddenfield.Style.Add("display", "block");
            //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "text", "show()", true);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    public DataTable GetCredential(int ID)
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SpFlightServiceCredential", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@Counter", ID);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETBYID");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }



    private int UpdateCredential(int Counter, string CorporateID, string UserID, string Password, string LoginID, string LoginPwd, string ServerUrlOrIP, string BkgServerUrlOrIP, string Port, string Provider, string CarrierAcc, string SearchType, string Status, string ResultFrom, string CrdType)
    {

        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "update";
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightServiceCredential", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Counter", Counter);
            cmd.Parameters.AddWithValue("@CorporateID", CorporateID);
            cmd.Parameters.AddWithValue("@UserID", UserID);
            cmd.Parameters.AddWithValue("@Password", Password);
            cmd.Parameters.AddWithValue("@LoginID", LoginID);
            cmd.Parameters.AddWithValue("@LoginPwd", LoginPwd);
            cmd.Parameters.AddWithValue("@ServerUrlOrIP", ServerUrlOrIP);
            cmd.Parameters.AddWithValue("@BkgServerUrlOrIP", BkgServerUrlOrIP);
            cmd.Parameters.AddWithValue("@Port", Port);
            cmd.Parameters.AddWithValue("@Provider", Provider);
            cmd.Parameters.AddWithValue("@CarrierAcc", CarrierAcc);
            cmd.Parameters.AddWithValue("@SearchType", SearchType);
            cmd.Parameters.AddWithValue("@Status", Convert.ToBoolean(Status));
            cmd.Parameters.AddWithValue("@ResultFrom", ResultFrom);
            cmd.Parameters.AddWithValue("@CrdType", CrdType);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;

    }


    protected void updatecrd_Click(object sender, EventArgs e)
    {
        try
        {
            string CorporateID = Convert.ToString(TxtCorporateID.Text);
            string UserID = Convert.ToString(TxtUserID.Text);
            string Password = Convert.ToString(TxtPassword.Text);
            string LoginID = Convert.ToString(TxtLoginID.Text);
            string LoginPwd = Convert.ToString(TxtLoginPwd.Text);
            string ServerUrlOrIP = Convert.ToString(TxtServerUrlOrIP.Text);
            string BkgServerUrlOrIP = Convert.ToString(TxtBkgServerUrlOrIP.Text);
            string Port = "";// Convert.ToString(TxtPort.Text);
            string Provider = Convert.ToString(lblProvider.Text);
            string CarrierAcc = Convert.ToString(TxtCarrierAcc.Text);
            string SearchType = "";//Convert.ToString(TxtSearchType.Text);
            string Status = Convert.ToString(DdlStatus.SelectedValue);
            string ResultFrom = Convert.ToString(DdlResultFrom.SelectedValue);
            string CrdType = Convert.ToString(DdlCrdType.SelectedValue);
            int flag = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(HdnId.Value)))
            {
                int Counter = Convert.ToInt32(HdnId.Value);
                //int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);
                flag = UpdateCredential(Counter, CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);
                if (flag > 0)
                {
                    BindCredential(Convert.ToString(HdnId.Value));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Record successfully updated.');window.location='ServiceCredentials.aspx'; ", true);
                }
                else
                {
                    BindCredential(Convert.ToString(HdnId.Value));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record not updated, try again!!');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');window.location='CommissionMaster.aspx'; ", true);
            }


        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='CommissionMaster.aspx'; ", true);
        }

    }
}