﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="GroupDetails.aspx.cs" Inherits="GroupSearch_GroupDetails" %>

<%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
      <script type="text/javascript">
          function openPopup(Remarks) {
              $('#lbl_Remarks').text(Remarks);
              $("#popupdiv").dialog({
                  title: "Remarks",
                  width: 400,
                  height: 350,
                  modal: true,
                  buttons: {
                      Close: function () {
                          $(this).dialog('close');
                      }
                  }
              });
          }
          function CloseAndRefresh() {
              open(location, '_self').close();
              window.open("ExecRequestDetails.aspx");
          }
    </script>
    <script type="text/javascript">
        $(".loader").click(function (e) {
            $("#waitMessage").show();
        });
    </script>
    <script type="text/javascript">
        function loadimg() {
            $("#waitMessage").show();
        }
    </script>
    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Search Flight Hold Pnrs </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status:</label>
                                    <asp:DropDownList ID="ddl_status" runat="server"  class="form-control">
                                        <asp:ListItem Selected="True" Value="N">---All ---</asp:ListItem>
                                        <asp:ListItem>Requested</asp:ListItem>
                                        <asp:ListItem>Freezed</asp:ListItem>
                                        <asp:ListItem>Paid</asp:ListItem>
                                        <asp:ListItem>Ticketed</asp:ListItem>
                                        <asp:ListItem>Cancelled</asp:ListItem>
                                        <asp:ListItem>Rejected</asp:ListItem>
                                        <asp:ListItem>Cancellation Requested</asp:ListItem>
                                          <asp:ListItem>Refunded</asp:ListItem>
                                         <asp:ListItem>Quoted</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">Request Id:</label>
                                <asp:TextBox ID="txt_RequestID" runat="server" class="form-control"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">Date From :</label>
                                <asp:TextBox ID="txt_fromDate" runat="server" class="form-control date"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Date To:</label>
                                    <asp:TextBox ID="txt_todate" class="form-control date" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btn_submit" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="btn_submit_Click" />
                                </div>
                            </div>
                        </div>                    
                        <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server" >
                                <div class="col-md-12" >
                                     <div id="DivExec" runat="server"></div>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always" ChildrenAsTriggers="true">
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="GrpBookingDetails" />
                                        </Triggers>
                                        <ContentTemplate>

                                            <asp:GridView ID="GrpBookingDetails" runat="server" AllowPaging="True" AllowSorting="True"
                                                AutoGenerateColumns="False" CssClass="table " GridLines="None" Width="100%"
                                               PageSize="30" OnRowCommand="GrpBookingDetails_RowCommand" OnRowDataBound="GrpBookingDetails_RowDataBound" OnPageIndexChanging="GrpBookingDetails_PageIndexChanging">

                                              <%--  <Columns>
                                                    <asp:TemplateField HeaderText="RequestID">
                                                        <ItemTemplate>

                                                            <a href="#" onclick='openWindow("<%# Eval("RequestID") %>");'>
                                                                <asp:Label ID="lblRequestID" runat="server" Text='<%#Eval("RequestID")%>'></asp:Label></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Booking Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblbookingname" runat="server" Text='<%#Eval("BookingName")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Trip">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTrip" runat="server" Text='<%#Eval("Trip")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Trip Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Passangers">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNoOfPax" runat="server" Text='<%#Eval("NoOfPax")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expected Fare">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblExpectedPrice" runat="server" Text='<%#Eval("ExpactedPrice")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Journey">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblJourney" runat="server" Text='<%#Eval("Journey")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Journey Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblJourneydate" runat="server" Text='<%#Eval("JourneyDate")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Payment Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPaymentStatus" runat="server" Text='<%#Eval("PaymentStatus")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnk_AcceptBy" runat="server" CommandName="Accept" CssClass="loader" CommandArgument='<%#Eval("RequestID") %>'
                                                                Font-Bold="True" Font-Underline="False">Accept</asp:LinkButton>
                                                            || 
                                 <asp:LinkButton ID="lnk_RejectBy" runat="server" CommandName="Reject" CssClass="loader" CommandArgument='<%#Eval("RequestID") %>'
                                     Font-Bold="True" Font-Underline="False">Reject</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PaymentMode">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PgCharges">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPgCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>--%>
                                                   <Columns>
                        <asp:TemplateField HeaderText="RequestID">
                            <ItemTemplate>
                                <a href="#" onclick='openWindow("<%# Eval("RequestID") %>");'>
                                    <asp:Label ID="lblRequestID" runat="server" Text='<%#Eval("RequestID")%>'></asp:Label></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Booking Name">
                            <ItemTemplate>
                                <asp:Label ID="lblbookingname" runat="server" Text='<%#Eval("BookingName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Trip">
                            <ItemTemplate>
                                <asp:Label ID="lblTrip" runat="server" Text='<%#Eval("Trip")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Trip Type">
                            <ItemTemplate>
                                <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("TripType")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Passangers">
                            <ItemTemplate>
                                <asp:Label ID="lblNoOfPax" runat="server" Text='<%#Eval("NoOfPax")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expected Fare">
                            <ItemTemplate>
                                <asp:Label ID="lblExpectedPrice" runat="server" Text='<%#Eval("ExpactedPrice")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Journey">
                            <ItemTemplate>
                                <asp:Label ID="lblJourney" runat="server" Text='<%#Eval("Journey")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Journey Date">
                            <ItemTemplate>
                                <asp:Label ID="lblJourneydate" runat="server" Text='<%#Eval("JourneyDate")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Status">
                            <ItemTemplate>
                                <asp:Label ID="lblPaymentStatus" runat="server" Text='<%#Eval("PaymentStatus")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnk_AcceptBy" runat="server" CommandName="Accept" OnClientClick="return loadimg();" CommandArgument='<%#Eval("RequestID") %>'
                                    Font-Bold="True" Font-Underline="False">Accept</asp:LinkButton>
                                || 
                                 <asp:LinkButton ID="lnk_RejectBy" runat="server" CommandName="Reject" CssClass="loader" CommandArgument='<%#Eval("RequestID") %>'
                                     Font-Bold="True" Font-Underline="False">Reject</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Booking Amount">
                            <ItemTemplate>
                                <asp:Label ID="lbl_BookedPrice" runat="server" Text='<%#Eval("BookedPrice")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PaymentMode">
                            <ItemTemplate>
                                <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PgCharges">
                            <ItemTemplate>
                                <asp:Label ID="lblPgCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CancellationRemarks">
                            <ItemTemplate>
                                <asp:Label ID="lblCancellationRemarks" runat="server" Text='<%#Eval("CancellationRemarks")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:TextBox ID="txtRemark" Visible="false" runat="server" Height="47px" TextMode="MultiLine" Width="175px"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ControlStyle-Width="100px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSubmit_1" runat="server" Visible="false"
                                    CommandArgument='<%# Eval("RequestID") %>' OnClientClick="return loadimg();" CommandName="CancelReqSubmit"><img src="../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                <asp:LinkButton ID="lnkHides_1" runat="server" Visible="false"
                                    CommandName="ReqCancel" CommandArgument='<%# Eval("RequestID") %>'><img src="../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:LinkButton ID="link_Invoice" runat="server" CommandArgument='<%#Eval("RequestID") %>'
                                    Font-Bold="True" Font-Underline="False" OnClick="link_Invoice_Click">View Invoice</asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                                                <RowStyle CssClass="RowStyle" />
                                                <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                <PagerStyle CssClass="PagerStyle" />
                                                <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <EditRowStyle CssClass="EditRowStyle" />
                                                <AlternatingRowStyle CssClass="AltRowStyle" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                        <ProgressTemplate>
                                            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                            </div>
                                            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                                Please Wait....<br />
                                                <br />
                                                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                                <br />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>

                                </div>
                                <div id="waitMessage" style="display: none;">

                                    <div class="" style="text-align: center; opacity: 0.7; position: absolute; z-index: 99999; top: 10px; width: 1020px; height: 100%; background-color: #f9f9f9; font-size: 12px; font-weight: bold; padding: 20px; box-shadow: 0px 1px 5px #000; border-radius: 10px;">
                                        Please wait....<br />
                                        <br />
                                        <img alt="loading" src="<%=ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    



    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
   <%-- <script type="text/javascript">
        $(function () {
            $(".date").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
    <script type="text/javascript">
        function openWindow(Requestid) {
            window.open('GroupRequestidDetails.aspx?RequestID=' + Requestid, 'open_window', ' width=1024, height=720, left=0, top=0,status=yes,toolbar=no,scrollbars=yes');
        }
    </script>
--%>


    <script type="text/javascript">
        $(function () {
            $(".date").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
    <script type="text/javascript">
        function openWindow(Requestid) {
            window.open('GroupRequestidDetails.aspx?RequestID=' + Requestid, 'open_window', ' width=1024, height=720, left=0, top=0,status=yes,toolbar=no,scrollbars=yes');
        }
    </script>
    <script type="text/javascript">
        function openPopup(Remarks) {
            $('#lbl_Remarks').text(Remarks);
            $("#popupdiv").dialog({
                title: "Remarks",
                width: 400,
                height: 350,
                modal: true,
                buttons: {
                    Close: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
        function MyFunc(strmsg) {
            switch (strmsg) {
                case 1: {
                    alert("Remark can not be blank,Please Fill Remark");
                    $("#waitMessage").hide();
                }
                    break;
            }
        }
    </script>
</asp:Content>

