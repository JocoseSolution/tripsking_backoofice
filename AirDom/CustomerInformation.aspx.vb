﻿Imports System.Data
Imports System.Collections.Generic
Imports System.Linq
Imports System.Xml.Linq
Imports STD.BAL
Imports STD.Shared

Partial Class AirDom_CustomerInfo
    Inherits System.Web.UI.Page

    Dim objSelectedfltCls As New clsInsertSelectedFlight
    Dim objFareBreakup As New clsCalcCommAndPlb
    Dim objDA As New SqlTransaction

    Dim DomAirDt As DataTable
    Dim trackId As String, LIN As String
    Dim OBTrackId As String, IBTrackId As String
    Dim FT As String = ""
    Dim Adult As Integer
    Dim Child As Integer
    Dim Infant As Integer
    Dim SelectedFltArray As Array
    Dim strFlt As String = "", strFare As String = ""
    Dim fareHashtbl As Hashtable
    Dim STDom As New SqlTransactionDom()
    Dim clsCorp As New ClsCorporate()
    'varaibles
    Dim objSql As New SqlTransactionNew
    Dim VCOB As String = "", VCIB As String = ""
    Dim VCOBSPL As String = "", VCIBSPL As String = ""
    Dim TripOB As String = "", TripIB As String = ""
    Dim ATOB As String = "", ATIB As String = ""
    Dim FLT_STAT As String = ""
    Dim Adti As Integer = 0, Chdi As Integer = 0, SERCHVALO As String = "", SERCHVALR As String = ""
    Dim objUMSvc As New FltSearch1()
    Dim Provider As String = "", ProviderIB As String = ""
    Dim TBOSSR As New STD.BAL.TBO.SSR.SSRResponse()
    Dim TBOSSRIB As New STD.BAL.TBO.SSR.SSRResponse()

    Dim YASSR As New List(Of YASSR)
    Dim YASSRIB As New List(Of YASSR)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Dim a As Integer = 0
            Dim aaa As Integer = 10 / a

            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("../Login.aspx")
            Else
                If Not Page.IsPostBack Then
                    'LIN = Request.QueryString("Linenumber")
                    'FT = Request.QueryString("FT")
                    'ViewState("FT") = FT

                    Dim ds As DataSet = clsCorp.Get_Corp_Project_Details_By_AgentID(Session("UID").ToString(), Session("User_Type"))
                    If ds Is Nothing Then

                    Else
                        If ds.Tables(0).Rows.Count > 0 Then
                            DropDownListProject.Items.Clear()
                            Dim item As New ListItem("Select")
                            DropDownListProject.AppendDataBoundItems = True
                            DropDownListProject.Items.Insert(0, item)
                            DropDownListProject.DataSource = ds.Tables(0)
                            DropDownListProject.DataTextField = "ProjectName"
                            DropDownListProject.DataValueField = "ProjectId"
                            DropDownListProject.DataBind()
                            spn_Projects.Visible = True
                            spn_Projects1.Visible = True
                        Else
                            spn_Projects.Visible = False
                            spn_Projects1.Visible = False

                        End If

                    End If


                    Dim dsbooked As DataSet = clsCorp.Get_Corp_BookedBy(Session("UID").ToString(), "BB")
                    If dsbooked.Tables(0).Rows.Count > 0 Then
                        DropDownListBookedBy.AppendDataBoundItems = True
                        DropDownListBookedBy.Items.Clear()
                        DropDownListBookedBy.Items.Insert(0, "Select")
                        DropDownListBookedBy.DataSource = dsbooked
                        DropDownListBookedBy.DataTextField = "BOOKEDBY"
                        DropDownListBookedBy.DataValueField = "BOOKEDBY"
                        DropDownListBookedBy.DataBind()

                        Spn_BookedBy.Visible = True
                        Spn_BookedBy1.Visible = True
                    Else
                        Spn_BookedBy.Visible = False
                        Spn_BookedBy1.Visible = False

                    End If





                    Dim query As String = HttpContext.Current.Request.QueryString(0).ToString()
                    Dim Track As String() = query.Split(",")
                    If (Track.Length = 2) Then
                        FT = "InBound"

                    Else
                        FT = "OutBound"
                    End If
                    ViewState("FT") = FT
                    'fareHashtbl = objFareBreakup.getDomFareDetails(LIN, FT)
                    If FT = "OutBound" Then
                        OBTrackId = Track(0) ' objSelectedfltCls.getRndNum
                        ViewState("OBTrackId") = OBTrackId
                        'objSelectedfltCls.InsertFlightData(OBTrackId, LIN, Session("DomAirDt"), "", fareHashtbl("AdtTax").ToString, fareHashtbl("ChdTax").ToString, fareHashtbl("InfTax").ToString, fareHashtbl("SrvTax"), fareHashtbl("TFee"), fareHashtbl("TC"), fareHashtbl("adtTds"), fareHashtbl("chdTds"), fareHashtbl("adtComm"), fareHashtbl("chdComm"), fareHashtbl("adtCB"), fareHashtbl("chdCB"), fareHashtbl("totFare"), fareHashtbl("netFare"), Session("UID"))
                    Else
                        OBTrackId = Track(0) 'Request.QueryString("TID")
                        ViewState("OBTrackId") = OBTrackId
                        IBTrackId = Track(1) ' objSelectedfltCls.getRndNum
                        ViewState("IBTrackId") = IBTrackId
                        ' objSelectedfltCls.InsertFlightData(IBTrackId, LIN, Session("DomAirDtR"), "", fareHashtbl("AdtTax").ToString, fareHashtbl("ChdTax").ToString, fareHashtbl("InfTax").ToString, fareHashtbl("SrvTax"), fareHashtbl("TFee"), fareHashtbl("TC"), fareHashtbl("adtTds"), fareHashtbl("chdTds"), fareHashtbl("adtComm"), fareHashtbl("chdComm"), fareHashtbl("adtCB"), fareHashtbl("chdCB"), fareHashtbl("totFare"), fareHashtbl("netFare"), Session("UID"))
                    End If


                    Dim OBFltDs, IBFltDs As DataSet
                    OBFltDs = objDA.GetFltDtls(OBTrackId, Session("UID"))
                    IBFltDs = objDA.GetFltDtls(IBTrackId, Session("UID"))

                    Adult = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Adult"))
                    Child = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Child"))
                    Infant = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Infant"))

                    lbl_adult.Text = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Adult"))
                    lbl_child.Text = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Child"))
                    lbl_infant.Text = Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Infant"))

                    'Code 5/04/2014
                    VCOB = OBFltDs.Tables(0).Rows(0)("ValiDatingCarrier")
                    VCOBSPL = OBFltDs.Tables(0).Rows(0)("AdtFareType")
                    TripOB = OBFltDs.Tables(0).Rows(0)("Trip")

                    Provider = OBFltDs.Tables(0).Rows(0)("Provider")

                    If FT = "InBound" Then
                        ProviderIB = IBFltDs.Tables(0).Rows(0)("Provider")

                    End If

                    If Provider.Trim.ToUpper = "TB" Or Provider.Trim.ToUpper = "YA" Then
                        If OBFltDs.Tables(0).Rows(OBFltDs.Tables(0).Rows.Count - 1)("TripType").ToString().Trim() = "R" Then
                            FLT_STAT = "RTF"

                        End If

                    Else : FLT_STAT = OBFltDs.Tables(0).Rows(0)("FlightStatus")
                    End If

                    SERCHVALO = Convert.ToString(OBFltDs.Tables(0).Rows(0)("Searchvalue"))
                    hdn_vc.Value = VCOB


                    Dim Eq As Integer

                    Try
                        If InStr(VCOBSPL, "Special") = 0 And (VCOB = "SG" Or VCOB = "6E") And Provider.Trim().ToUpper() <> "TB" Then
                            If (VCOB = "SG") Then
                                If (Convert.ToInt16(OBFltDs.Tables(0).Rows(0)("Tot_Dur").ToString().Substring(0, 2)) < 1) Then
                                    ATOB = "Q400"
                                Else
                                    If (OBFltDs.Tables(0).Rows(0)("EQ").ToString().Trim() = "DH8") Then
                                        ATOB = "Q400"
                                    ElseIf (Int32.TryParse(OBFltDs.Tables(0).Rows(0)("EQ").ToString().Trim(), Eq)) Then
                                        If (Eq >= 737 And Eq <= 900) Then
                                            ATOB = "Boeing"
                                        Else
                                            ATOB = ""
                                        End If
                                    End If
                                End If
                            ElseIf (VCOB = "6E") Then
                                ATOB = "ALL"
                            End If
                        End If
                    Catch ex As Exception

                    End Try

                    Dim exep As String = ""


                    If Provider.Trim().ToUpper() = "TB" Then


                        Dim dsCrd As DataSet = objSql.GetCredentials("TB")
                        Dim snoArr As String() = Convert.ToString(OBFltDs.Tables(0).Rows(0)("sno")).Split(":")

                        Dim objBook As New STD.BAL.TBO.TBOBook()

                        Dim log As New Dictionary(Of String, String)

                        objBook.GetFareQuote(dsCrd, snoArr(1), snoArr(0), log, exep)



                        Dim objssr As New STD.BAL.TBO.SSR.TOBSSR()

                        TBOSSR = objssr.GetSSR("", snoArr(1), snoArr(0), dsCrd)



                        If FLT_STAT = "RTF" Then

                            Dim snoArrR As String() = Convert.ToString(OBFltDs.Tables(0).Rows(OBFltDs.Tables(0).Rows.Count - 1)("sno")).Split(":")


                            Dim log1 As New Dictionary(Of String, String)
                            objBook.GetFareQuote(dsCrd, snoArrR(1), snoArr(0) & "," & snoArrR(0), log1, exep)
                            TBOSSR = objssr.GetSSR("", snoArr(1), snoArr(0) & "," & snoArrR(0), dsCrd)
                            'TBOSSRIB = objssr.GetSSR("", snoArrR(1), snoArrR(0), dsCrd)

                        End If

                    End If


                    If Provider.Trim().ToUpper() = "YA" Then
                        Dim FltO As DataRow()
                        Dim FltR As DataRow()
                        Dim airPriceResp As String = ""
                        Dim dsCrd As DataSet = objSql.GetCredentials("YA")
                        FltO = OBFltDs.Tables(0).Select("TripType='O'", "counter ASC")

                        Dim srchValArr As String = Convert.ToString(FltO(0)("Searchvalue"))

                        Dim objAirPrice As New STD.BAL.YAAirPrice()

                        Dim log As New Dictionary(Of String, String)




                        'Dim objssr As New STD.BAL.TBO.SSR.TOBSSR()

                        'TBOSSR = objssr.GetSSR("", snoArr(1), snoArr(0), dsCrd)



                        If FLT_STAT = "RTF" Then



                            FltR = OBFltDs.Tables(0).Select("TripType='R'", "counter ASC")


                            Dim srchValArrR As String = Convert.ToString(FltR(0)("Searchvalue"))


                            Dim log1 As New Dictionary(Of String, String)

                            airPriceResp = objAirPrice.AirPrice(srchValArr, srchValArrR, Adult, Child, Infant, STD.BAL.MedthodSVCUrl.SvcURL, STD.BAL.MedthodSVCUrl.AirPriceMTHD, log, exep)


                            Dim ssrM1 As List(Of YASSR) = objAirPrice.GetSSR(airPriceResp)



                            YASSR = objAirPrice.GetSSRTripTypeWise(TripType.O, ssrM1)

                            YASSRIB = objAirPrice.GetSSRTripTypeWise(TripType.R, ssrM1)





                        Else

                            Dim srchRI As String = ""

                            If ProviderIB.Trim().ToUpper() = "YA" Then

                                srchRI = Convert.ToString(IBFltDs.Tables(0).Rows(0)("Searchvalue"))
                            End If


                            airPriceResp = objAirPrice.AirPrice(srchValArr, srchRI, Adult, Child, Infant, STD.BAL.MedthodSVCUrl.SvcURL, STD.BAL.MedthodSVCUrl.AirPriceMTHD, log, exep)

                            Dim totfare As Decimal = objAirPrice.ParseAirPrice(airPriceResp)

                            Dim ssrM As List(Of YASSR) = objAirPrice.GetSSR(airPriceResp)



                            YASSR = objAirPrice.GetSSRTripTypeWise(TripType.O, ssrM)

                            If ProviderIB.Trim().ToUpper() = "YA" Then
                                YASSRIB = objAirPrice.GetSSRTripTypeWise(TripType.R, ssrM)
                            End If



                        End If




                    End If








                    'divFareDtls.InnerHtml = fareBreakupfun_Tot(OBFltDs, IBFltDs, FT)
                    div_fare.InnerHtml = "<div class='f18'>OutBound</div><div class='hr'></div>" & fareBreakupfun(OBFltDs, "OutBound")
                    If FT = "InBound" Then
                        div_fareR.InnerHtml = "<div class='f18'>InBound</div><div class='hr'></div>" & fareBreakupfun(IBFltDs, FT)
                        VCIB = IBFltDs.Tables(0).Rows(0)("ValiDatingCarrier")
                        VCIBSPL = IBFltDs.Tables(0).Rows(0)("AdtFareType")
                        TripIB = IBFltDs.Tables(0).Rows(0)("Trip")
                        SERCHVALR = Convert.ToString(IBFltDs.Tables(0).Rows(0)("Searchvalue"))

                        ProviderIB = IBFltDs.Tables(0).Rows(0)("Provider")
                        Dim EqIB As Integer
                        Try
                            If InStr(VCIBSPL, "Special") = 0 And (VCIB = "SG" Or VCIB = "6E") And ProviderIB.Trim().ToUpper() <> "TB" Then
                                If (VCIB = "SG") Then
                                    If (Convert.ToInt16(IBFltDs.Tables(0).Rows(0)("Tot_Dur").ToString().Substring(0, 2)) < 1) Then
                                        ATIB = "Q400"
                                    Else
                                        If (IBFltDs.Tables(0).Rows(0)("EQ").ToString().Trim() = "DH8") Then
                                            ATIB = "Q400"
                                        ElseIf (Int32.TryParse(IBFltDs.Tables(0).Rows(0)("EQ").ToString().Trim(), EqIB)) Then
                                            If (EqIB >= 737 And EqIB <= 900) Then
                                                ATIB = "Boeing"
                                            Else
                                                ATIB = ""
                                            End If
                                        End If
                                    End If
                                ElseIf (VCIB = "6E") Then
                                    ATIB = "ALL"
                                End If
                            End If


                            If ProviderIB.Trim().ToUpper() = "TB" Then
                                Dim dsCrd As DataSet = objSql.GetCredentials("TB")
                                Dim snoArr As String() = Convert.ToString(IBFltDs.Tables(0).Rows(0)("sno")).Split(":")

                                Dim objBook As New STD.BAL.TBO.TBOBook()
                                Dim log As New Dictionary(Of String, String)
                                objBook.GetFareQuote(dsCrd, snoArr(1), snoArr(0), log, exep)
                                Dim objssr As New STD.BAL.TBO.SSR.TOBSSR()
                                TBOSSRIB = objssr.GetSSR("", snoArr(1), snoArr(0), dsCrd)

                            End If


                        Catch ex As Exception

                        End Try

                    End If

                    If FLT_STAT = "RTF" And (VCOB = "SG" Or VCOB = "6E") And Provider.Trim().ToUpper() <> "TB" Then
                        Dim Org As String = OBFltDs.Tables(0).Rows(0)("OrgDestFrom")
                        Dim Dest As String = OBFltDs.Tables(0).Rows(0)("OrgDestTo")
                        Dim Dt As DataRow() = OBFltDs.Tables(0).Select("OrgDestFrom = '" & Dest & "'")
                        Dim row As Integer = OBFltDs.Tables(0).Rows.Count
                        SERCHVALR = Convert.ToString(OBFltDs.Tables(0).Rows(OBFltDs.Tables(0).Rows.Count - 1)("Searchvalue"))
                        Dim EqIB As Integer
                        If (VCOB = "SG") Then
                            If (Dt(0)("EQ").ToString().Trim() = "DH8") Then
                                ATIB = "Q400"
                            ElseIf (Int32.TryParse(Dt(0)("EQ").ToString().Trim(), EqIB)) Then
                                If (EqIB >= 737 And EqIB <= 900) Then
                                    ATIB = "Boeing"
                                Else
                                    ATIB = ""
                                End If
                            End If
                        ElseIf (VCOB = "6E") Then
                            ATIB = "ALL"
                        End If
                    End If
                    '''Code End
                    Bind_pax(Adult, Child, Infant)
                    divFltDtls1.InnerHtml = showFltDetails(OBFltDs, IBFltDs, FT)
                    'divtotFlightDetails.clea
                    divtotFlightDetails.InnerHtml = STDom.CustFltDetails_Dom(OBFltDs, IBFltDs, FT)
                Else
                    'Page Post Back

                End If

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub
    Private Function showFltDetails(ByVal OBDS As DataSet, ByVal IBDS As DataSet, ByVal FT As String) As String
        Try

            Dim droneway As DataRow()
            Dim drround As DataRow() = New DataRow(-1) {}

            'If FltHdr.Tables(0).Rows(0)("TripType").ToString().ToUpper() = "O" Then

            'droneway = FltDsGAL.Tables(0).[Select]("flight=1", "counter asc")
            'Else
            droneway = OBDS.Tables(0).[Select]("flight=1", "counter asc")
            drround = OBDS.Tables(0).[Select]("flight=2", "counter asc")
            'End If
            'Dim kk As Integer = VCCount1(droneway)
            strFlt = ""
            Dim Logo As String = ""
            Dim Airline As String = ""
            Dim DepartureTime As String = ""
            Dim ArrivalTime As String = ""
            If (VCCount1(droneway) = 0) Then
                Logo = "../Airlogo/sm" & droneway(0)("MarketingCarrier") & ".gif" 'MultiValueFunction(OBDS.Tables(0), "Logo")
                'Airline = MultiValueFunction(OBDS.Tables(0), "Airline")
                Airline = droneway(0)("AirlineName") & "(" & droneway(0)("MarketingCarrier") & "-" & droneway(0)("FlightIdentification") & ")"
            Else
                Logo = "../Airlogo/multiple.png"
                Airline = "Multiple Airline"
            End If
            strFlt = strFlt & "<div class='row'>"
            strFlt = strFlt & "<div class='large-12 medium-12 small-12'><b>Flight Details</b><br/>&nbsp;</div>"

            strFlt = strFlt & "<div class='large-12 medium-12 small-12'>"
            strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'><img alt='' src='" & Logo & "'/><br />&nbsp;" & Airline & "</div>"
            strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & droneway(0)("DepartureLocation") & "(" & droneway(0)("DepartureCityName") & ")</div>"
            strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & droneway(droneway.Length - 1)("ArrivalLocation") & "(" & droneway(droneway.Length - 1)("ArrivalCityName") & ")</div>"
            strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & droneway(0)("Stops") & "</div>"
            strFlt = strFlt & "</div>"

            strFlt = strFlt & "<div class='large-12 medium-12 small-12'>"
            DepartureTime = MultiValueFunction(OBDS.Tables(0), "Deprow", 0, droneway(0)("DepartureTime"))
            ArrivalTime = MultiValueFunction(OBDS.Tables(0), "Arrrow", 0, droneway(droneway.Length - 1)("ArrivalTime"))

            strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & droneway(0)("Departure_Date") & " (" & DepartureTime & ")</div>"
            strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & droneway(droneway.Length - 1)("Arrival_Date") & " (" & ArrivalTime & ")</div>"
            If VCOB = "SG" And (SERCHVALO.Contains("P1") Or SERCHVALO.Contains("P2")) Then
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & If(droneway(0)("AdtFareType").ToString().Trim().ToLower() = "refundable", "<img src='../images/refundable.png' title='Refundable Fare' />", "<img src='../images/non-refundable.png' title='Non-Refundable Fare' />") & "<br/>7kg Hand Bag Only.</div>"
            Else
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & If(droneway(0)("AdtFareType").ToString().Trim().ToLower() = "refundable", "<img src='../images/refundable.png' title='Refundable Fare' />", "<img src='../images/non-refundable.png' title='Non-Refundable Fare' />") & "</div>"
            End If

            strFlt = strFlt & "</div>"
            strFlt = strFlt & "</div>"

            strFlt = strFlt & "<div class='clear1'></div>"

            If (drround.Length > 0) Then
                Airline = ""
                Logo = ""
                If (VCCount1(drround) = 0) Then
                    Logo = "../Airlogo/sm" & drround(0)("MarketingCarrier") & ".gif" 'MultiValueFunction(OBDS.Tables(0), "Logo")
                    'Airline = MultiValueFunction(OBDS.Tables(0), "Airline")
                    Airline = drround(0)("AirlineName") & "(" & drround(0)("MarketingCarrier") & "-" & drround(0)("FlightIdentification") & ")"
                Else
                    Logo = "../Airlogo/multiple.png"
                    Airline = "Multiple Airline"
                End If


                'Airline = drround(0)("AirlineName") & "(" & drround(0)("MarketingCarrier") & "-" & drround(0)("FlightIdentification") & ")"

                strFlt = strFlt & "<div class='row'>"
                strFlt = strFlt & "<div class='large-12 medium-12 small-12'>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'><img alt='' src='" & Logo & "'/><br />" & Airline & "</div>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & drround(0)("DepartureLocation") & "(" & drround(0)("DepartureCityName") & ")</div>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & drround(drround.Length - 1)("ArrivalLocation") & "(" & drround(drround.Length - 1)("ArrivalCityName") & ")</div>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & drround(0)("Stops") & "</div>"
                strFlt = strFlt & "</div>"

                strFlt = strFlt & "<div class='large-12 medium-12 small-12'>"

                DepartureTime = MultiValueFunction(OBDS.Tables(0), "Deprow", 0, drround(0)("DepartureTime"))
                ArrivalTime = MultiValueFunction(OBDS.Tables(0), "Arrrow", 0, drround(drround.Length - 1)("ArrivalTime"))

                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & drround(0)("Departure_Date") & " (" & DepartureTime & ")</div>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & drround(drround.Length - 1)("Arrival_Date") & " (" & ArrivalTime & ")</div>"
                If VCOB = "SG" And (SERCHVALR.Contains("P1") Or SERCHVALR.Contains("P2")) Then
                    strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & If(drround(0)("AdtFareType").ToString().Trim().ToLower() = "refundable", "<img src='../images/refundable.png' title='Refundable Fare' />", "<img src='../images/non-refundable.png' title='Non-Refundable Fare' />") & "<br/>7kg Hand Bag Only.</div>"
                Else
                    strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & If(drround(0)("AdtFareType").ToString().Trim().ToLower() = "refundable", "<img src='../images/refundable.png' title='Refundable Fare' />", "<img src='../images/non-refundable.png' title='Non-Refundable Fare' />") & "</div>"
                End If
                strFlt = strFlt & "</div>"

                strFlt = strFlt & "</div>"
                strFlt = strFlt & "<div class='clear1'></div>"
            End If
            If FT = "InBound" Then
                If (VCCount(IBDS.Tables(0)) = 0) Then
                    Logo = MultiValueFunction(IBDS.Tables(0), "Logo")
                    Airline = MultiValueFunction(IBDS.Tables(0), "Airline")
                Else
                    Logo = "../Airlogo/multiple.png"
                    Airline = "Multiple Airline"
                End If
                strFlt = strFlt & "<div class='row'>"
                strFlt = strFlt & "<div class='large-12 medium-12 small-12'>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'><img alt='' src='" & Logo & "'/><br />" & Airline & "</div>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & IBDS.Tables(0).Rows(0)("DepartureLocation") & "(" & IBDS.Tables(0).Rows(0)("DepartureCityName") & ")</div>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & IBDS.Tables(0).Rows(IBDS.Tables(0).Rows.Count - 1)("ArrivalLocation") & "(" & IBDS.Tables(0).Rows(IBDS.Tables(0).Rows.Count - 1)("ArrivalCityName") & ")</div>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & IBDS.Tables(0).Rows(0)("Stops") & "</div>"
                strFlt = strFlt & "</div>"

                strFlt = strFlt & "<div class='large-12 medium-12 small-12'>"
                DepartureTime = MultiValueFunction(IBDS.Tables(0), "Dep")
                ArrivalTime = MultiValueFunction(IBDS.Tables(0), "Arr")

                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & IBDS.Tables(0).Rows(0)("Departure_Date") & " (" & DepartureTime & ")</div>"
                strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & IBDS.Tables(0).Rows(IBDS.Tables(0).Rows.Count - 1)("Arrival_Date") & " (" & ArrivalTime & ")</div>"
                If VCIB = "SG" And (SERCHVALR.Contains("P1") Or SERCHVALR.Contains("P2")) Then
                    strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & If(IBDS.Tables(0).Rows(0)("AdtFareType").ToString().Trim().ToLower() = "refundable", "<img src='../images/refundable.png' title='Refundable Fare' />", "<img src='../images/non-refundable.png' title='Non-Refundable Fare' />") & "<br/>7kg Hand Bag Only.</div>"
                Else
                    strFlt = strFlt & "<div class='large-3 medium-3 small-3 columns'>" & If(IBDS.Tables(0).Rows(0)("AdtFareType").ToString().Trim().ToLower() = "refundable", "<img src='../images/refundable.png' title='Refundable Fare' />", "<img src='../images/non-refundable.png' title='Non-Refundable Fare' />") & "</div>"
                End If

                strFlt = strFlt & "</div>"
                strFlt = strFlt & "</div>"


            End If


            Dim strPax As String = ""
            strPax = strPax & "<div class='row'>"

            strPax = strPax & "<div class='large-12 medium-12 small-12 columns'>"
            strPax = strPax & "<div class='large-4 medium-4 small-4 columns'>Adult</div>"
            strPax = strPax & "<div class='large-4 medium-4 small-4 columns'>Child</div>"
            strPax = strPax & "<div class='large-4 medium-4 small-4 columns'>Infant</div>"
            strPax = strPax & "</div>"
            strPax = strPax & "<div class='large-12 medium-12 small-12 columns'>"
            strPax = strPax & "<div class='large-4 medium-4 small-4 columns'> &nbsp; <img alt='' src='../images/adt.png'/>(" & OBDS.Tables(0).Rows(0)("Adult") & ")</div>"
            strPax = strPax & "<div class='large-4 medium-4 small-4 columns'> &nbsp; <img alt='' src='../images/chd.png'/>(" & OBDS.Tables(0).Rows(0)("Child") & ")</div>"
            strPax = strPax & "<div class='large-4 medium-4 small-4 columns'> &nbsp; <img alt='' src='../images/inf.png'/>(" & OBDS.Tables(0).Rows(0)("Infant") & ")</div>"
            strPax = strPax & "</div>"
            'strPax = strPax & "<tr id='tr_tottotfare' onmouseover=funcnetfare('block','tr_totnetfare'); onmouseout=funcnetfare('none','tr_totnetfare'); style='cursor:pointer;color: #004b91'>"
            Dim TotalFare As Double
            Dim NetFare As Double
            If FT = "InBound" Then
                TotalFare = Convert.ToDouble(OBDS.Tables(0).Rows(0)("TotFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("TotFare"))
                NetFare = Convert.ToDouble(OBDS.Tables(0).Rows(0)("NetFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("NetFare"))
            Else
                TotalFare = Convert.ToDouble(OBDS.Tables(0).Rows(0)("TotFare"))
                NetFare = Convert.ToDouble(OBDS.Tables(0).Rows(0)("NetFare"))
            End If

            strPax = strPax & "<div class='large-12 medium-12 small-12 columns'><span style='font-size:14px; font-weight:bold; color:#000042; line-height:40px;'>Total Fare: " & TotalFare & "</span><div id='tr_totnetfare' style='display:none;position:absolute;background:#F1F1F1;border: thin solid #D1D1D1;padding:10px; font-size:14px; font-weight:bold; color:#000;'>Net Fare: " & NetFare & "</div></div>"


            'strPax = strPax & "<tr id='tr_totnetfare' style='display:none;position:absolute;background:#D1D1D1;padding:5x;'>"
            'strPax = strPax & "<td>Net Fare:" & NetFare & "</td>"
            'strPax = strPax & "</tr>"
            strPax = strPax & "<div class='large-12 medium-12 small-12 columns'>"
            strPax = strPax & "<div class='large-5 medium-12 small-6 columns btn' id='ctl00_ContentPlaceHolder1_divtotFlight' onclick='ddshow(this.id);'>Flight Summary</div><div class='large-5 medium-12 small-6 columns btn' id='div_faredd' onclick='ddshow(this.id);'>Fare Summary</div>"
            strPax = strPax & "</div>"
            strPax = strPax & "</div>"


            divtotalpax.InnerHtml = strPax
            ' divtotFlightDetails.InnerHtml = CustFltDetails(OBDS, IBDS, FT)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


        Return strFlt
    End Function
    'Private Function CustFltDetails(ByVal OBDS As DataSet, ByVal IBDS As DataSet, ByVal FT As String) As String
    '    Dim FlightDtlsTotalInfo As String = ""
    '    Dim DepTerminal As String
    '    Dim ArrTerminal As String
    '    Dim FlightType = ""
    '    If FT = "InBound" Then
    '        FlightType = "OutBound"
    '    End If

    '    FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<table  width='100%' border='0' cellspacing='0' cellpadding='0'>"
    '    FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr ><td colspan='2' style='font-size:18px; line-height:35px; border-bottom:2px solid #d1d1d1;' >" & FlightType & " Flight Details</td><td align='left' style='font-size:14px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91; '>" & OBDS.Tables(0).Rows(0)("AdtFareType") & "</td><tr>"
    '    For i As Integer = 0 To OBDS.Tables(0).Rows.Count - 1
    '        DepTerminal = ""
    '        ArrTerminal = ""
    '        'Dim AirportName As String =STDom.GetAirportName(OBDS.Tables(0).Rows(i)("DepartureLocation")
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td ><img alt='' src='../Airlogo/sm" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & OBDS.Tables(0).Rows(i)("AirlineName") & "</td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>(" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & OBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & OBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction(OBDS.Tables(0), "Depall", i) & ")</td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & OBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction(OBDS.Tables(0), "Arrall", i) & ")</td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >Class(" & OBDS.Tables(0).Rows(i)("RBD") & ") </td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & OBDS.Tables(0).Rows(i)("DepartureCityName") & "(" & OBDS.Tables(0).Rows(i)("DepartureLocation") & ")</td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & OBDS.Tables(0).Rows(i)("ArrivalCityName") & "(" & OBDS.Tables(0).Rows(i)("ArrivalLocation") & ")</td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    '        If OBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim() <> "" Then
    '            DepTerminal = "Terminal - " & OBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim()
    '        End If
    '        If OBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim() <> "" Then
    '            ArrTerminal = "Terminal - " & OBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim()
    '        End If

    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='padding-left: 25px'></td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & STDom.GetAirportName(OBDS.Tables(0).Rows(i)("DepartureLocation")) & " " & DepTerminal & "</td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & STDom.GetAirportName(OBDS.Tables(0).Rows(i)("ArrivalLocation")) & " " & ArrTerminal & "</td>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
    '    Next
    '    If FT = "InBound" Then
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr><td style='padding-top: 20px'> </td></tr>"
    '        ' FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr><td colspan='4' style='font-size:18px; line-height:35px; border-bottom:2px solid #d1d1d1;'>Outbound Flight Details<td><tr>"
    '        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr ><td colspan='2' style='font-size:18px; line-height:35px; border-bottom:2px solid #d1d1d1;' >InBound Flight Details</td><td align='left' style='font-size:14px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91; '>" & IBDS.Tables(0).Rows(0)("AdtFareType") & "</td><tr>"
    '        For i As Integer = 0 To IBDS.Tables(0).Rows.Count - 1
    '            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td><img alt='' src='../Airlogo/sm" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & IBDS.Tables(0).Rows(i)("AirlineName") & "(" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & IBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
    '            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("DepartureLocation") & "(" & IBDS.Tables(0).Rows(i)("DepartureCityName") & ")</td>"
    '            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("ArrivalLocation") & "(" & IBDS.Tables(0).Rows(i)("ArrivalCityName") & ")</td>"
    '            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td ><img alt='' src='../Airlogo/sm" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & IBDS.Tables(0).Rows(i)("AirlineName") & "</td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>(" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & IBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction(IBDS.Tables(0), "Depall", i) & ")</td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction(IBDS.Tables(0), "Arrall", i) & ")</td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >Class(" & IBDS.Tables(0).Rows(i)("RBD") & ") </td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & IBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction(IBDS.Tables(0), "Depall", i) & ")</td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & IBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction(IBDS.Tables(0), "Arrall", i) & ")</td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
    '            DepTerminal = ""
    '            ArrTerminal = ""
    '            If IBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim() <> "" Then
    '                DepTerminal = "Terminal - " & IBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim()
    '            End If
    '            If IBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim() <> "" Then
    '                ArrTerminal = "Terminal - " & IBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim()
    '            End If
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='padding-left: 25px'></td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & STDom.GetAirportName(IBDS.Tables(0).Rows(i)("DepartureLocation")) & " " & DepTerminal & "</td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & STDom.GetAirportName(IBDS.Tables(0).Rows(i)("ArrivalLocation")) & " " & ArrTerminal & "</td>"
    '            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
    '        Next
    '    End If
    '    FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</table>"


    '    Return FlightDtlsTotalInfo
    'End Function
    'Private Function fareBreakupfun_Tot(ByVal OBDS As DataSet, ByVal IBDS As DataSet, ByVal FT As String) As String
    '    Dim fbfstr As String = ""
    '    fbfstr = fbfstr & "<table  width='250px' border='0' cellspacing='2' cellpadding='2'>"
    '    fbfstr = fbfstr & "<tr>"
    '    fbfstr = fbfstr & "<td colspan='2'>Outbound Fare</td>"
    '    fbfstr = fbfstr & "</tr>"
    '    fbfstr = fbfstr & "<tr>"
    '    fbfstr = fbfstr & "<td>Adult Fare(1)</td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ADTAgentMrk")) & "</td>"
    '    fbfstr = fbfstr & "</tr>"
    '    If Child > 0 Then
    '        fbfstr = fbfstr & "<tr>"
    '        fbfstr = fbfstr & "<td>Child Fare(1)</td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("CHDAgentMrk")) & "</td>"
    '        fbfstr = fbfstr & "</tr>"
    '    End If
    '    If Infant > 0 Then
    '        fbfstr = fbfstr & "<tr>"
    '        fbfstr = fbfstr & "<td>Infant Fare(1)</td><td>" & OBDS.Tables(0).Rows(0)("InfFare") & "</td>"
    '        fbfstr = fbfstr & "</tr>"
    '    End If
    '    'fbfstr = fbfstr & "<tr>"
    '    'fbfstr = fbfstr & "<td>Total Fare</td><td>" & OBDS.Tables(0).Rows(0)("totFare") & "</td>"
    '    'fbfstr = fbfstr & "</tr>"

    '    If (FT = "InBound") Then
    '        fbfstr = fbfstr & "<tr>"
    '        fbfstr = fbfstr & "<td colspan='2'>Inbound Fare</td>"
    '        fbfstr = fbfstr & "</tr>"
    '        fbfstr = fbfstr & "<tr>"
    '        fbfstr = fbfstr & "<td>Adult Fare(1)</td><td>" & Convert.ToDouble(IBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("ADTAgentMrk")) & "</td>"
    '        fbfstr = fbfstr & "</tr>"
    '        If Child > 0 Then
    '            fbfstr = fbfstr & "<tr>"
    '            fbfstr = fbfstr & "<td>Child Fare(1)</td><td>" & Convert.ToDouble(IBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("CHDAgentMrk")) & "</td>"
    '            fbfstr = fbfstr & "</tr>"
    '        End If
    '        If Infant > 0 Then
    '            fbfstr = fbfstr & "<tr>"
    '            fbfstr = fbfstr & "<td>Infant Fare(1)</td><td>" & IBDS.Tables(0).Rows(0)("InfFare") & "</td>"
    '            fbfstr = fbfstr & "</tr>"
    '        End If
    '        'fbfstr = fbfstr & "<tr>"
    '        'fbfstr = fbfstr & "<td>Total Fare</td><td>" & IBDS.Tables(0).Rows(0)("totFare") & "</td>"
    '        'fbfstr = fbfstr & "</tr>"

    '    End If
    '    Dim K As String = ""
    '    If (FT <> "InBound") Then
    '        fbfstr = fbfstr & "<tr>"
    '        fbfstr = fbfstr & "<td>Total(Per Pax) </td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ADTAgentMrk")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("CHDAgentMrk")) + OBDS.Tables(0).Rows(0)("InfFare") & "</td>"
    '        fbfstr = fbfstr & "</tr>"
    '        fbfstr = fbfstr & "<tr>"
    '        fbfstr = fbfstr & "<td>Grand Total</td><td>" & OBDS.Tables(0).Rows(0)("totFare") & "</td>"
    '        fbfstr = fbfstr & "</tr>"
    '    Else
    '        fbfstr = fbfstr & "<tr>"
    '        fbfstr = fbfstr & "<td>Total(Per Pax) </td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ADTAgentMrk")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("CHDAgentMrk")) + OBDS.Tables(0).Rows(0)("InfFare") + Convert.ToDouble(IBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("ADTAgentMrk")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("CHDAgentMrk")) + IBDS.Tables(0).Rows(0)("InfFare") & "</td>"
    '        fbfstr = fbfstr & "</tr>"
    '        fbfstr = fbfstr & "<tr>"
    '        fbfstr = fbfstr & "<td>Grand Total</td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("totFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("totFare")) & "</td>"
    '        fbfstr = fbfstr & "</tr>"
    '        K = "(InBound+OutBound)"
    '    End If
    '    fbfstr = fbfstr & "<tr>"
    '    fbfstr = fbfstr & "<td colspan='2'>" & OBDS.Tables(0).Rows(0)("Adult") & " ADT," & OBDS.Tables(0).Rows(0)("Child") & " CHD," & OBDS.Tables(0).Rows(0)("Infant") & " INF " & K & "</td>"
    '    fbfstr = fbfstr & "</tr>"
    '    fbfstr = fbfstr & "</table>"
    '    Return fbfstr

    'End Function
    'Private Function showFltDetails(ByVal OBDS As DataSet, ByVal IBDS As DataSet, ByVal FT As String) As String
    '    Try

    '        Dim droneway As DataRow()
    '        Dim drround As DataRow() = New DataRow(-1) {}

    '        'If FltHdr.Tables(0).Rows(0)("TripType").ToString().ToUpper() = "O" Then

    '        'droneway = FltDsGAL.Tables(0).[Select]("flight=1", "counter asc")
    '        'Else
    '        droneway = OBDS.Tables(0).[Select]("flight=1", "counter asc")
    '        drround = OBDS.Tables(0).[Select]("flight=2", "counter asc")
    '        'End If
    '        'Dim kk As Integer = VCCount1(droneway)
    '        strFlt = ""
    '        Dim Logo As String = ""
    '        Dim Airline As String = ""
    '        Dim DepartureTime As String = ""
    '        Dim ArrivalTime As String = ""
    '        If (VCCount1(droneway) = 0) Then
    '            Logo = "../Airlogo/sm" & droneway(0)("MarketingCarrier") & ".gif" 'MultiValueFunction(OBDS.Tables(0), "Logo")
    '            'Airline = MultiValueFunction(OBDS.Tables(0), "Airline")
    '            Airline = droneway(0)("AirlineName") & "(" & droneway(0)("MarketingCarrier") & "-" & droneway(0)("FlightIdentification") & ")"
    '        Else
    '            Logo = "../Airlogo/multiple.png"
    '            Airline = "Multiple Airline"
    '        End If
    '        strFlt = strFlt & "<table  width='100%' border='0' cellspacing='2' cellpadding='2'>"
    '        strFlt = strFlt & "<tr>"
    '        strFlt = strFlt & "<td  valign='top' >"

    '        strFlt = strFlt & "<table  width='100%' border='0' cellspacing='2' cellpadding='2'>"
    '        strFlt = strFlt & "<tr>"
    '        strFlt = strFlt & "<td style='font-size:14px; font-weight:bold; color:#fff; margin-bottom:15px;' colspan='4'>Flight Details</td>"
    '        strFlt = strFlt & "</tr>"
    '        strFlt = strFlt & "<tr>"
    '        strFlt = strFlt & "<td><img alt='' src='" & Logo & "' class='mtop'/><br />" & Airline & "</td>"
    '        strFlt = strFlt & "<td style='font-size:14px; font-weight:bold;'>" & droneway(0)("DepartureLocation") & "(" & droneway(0)("DepartureCityName") & ")</td>"
    '        strFlt = strFlt & "<td style='font-size:14px; font-weight:bold;'>" & droneway(droneway.Length - 1)("ArrivalLocation") & "(" & droneway(droneway.Length - 1)("ArrivalCityName") & ")</td>"
    '        strFlt = strFlt & "<td>" & droneway(0)("Stops") & "</td>"
    '        strFlt = strFlt & "</tr>"
    '        strFlt = strFlt & "<tr>"
    '        strFlt = strFlt & "<td></td>"
    '        DepartureTime = MultiValueFunction(OBDS.Tables(0), "Deprow", 0, droneway(0)("DepartureTime"))
    '        ArrivalTime = MultiValueFunction(OBDS.Tables(0), "Arrrow", 0, droneway(droneway.Length - 1)("ArrivalTime"))
    '        strFlt = strFlt & "<td>" & droneway(0)("Departure_Date") & " (" & DepartureTime & ")</td>"
    '        strFlt = strFlt & "<td>" & droneway(droneway.Length - 1)("Arrival_Date") & " (" & ArrivalTime & ")</td>"
    '        If VCOB = "SG" And (SERCHVALO.Contains("P1") Or SERCHVALO.Contains("P2")) Then
    '            strFlt = strFlt & "<td style='font-size:16px; color:#004b91;'>" & droneway(0)("AdtFareType") & "<br/>7kg Hand Bag Only.</td>"
    '        Else
    '            strFlt = strFlt & "<td style='font-size:16px; color:#004b91;'>" & droneway(0)("AdtFareType") & "</td>"
    '        End If

    '        strFlt = strFlt & "</tr>"


    '        If (drround.Length > 0) Then
    '            Airline = ""
    '            Logo = ""
    '            If (VCCount1(drround) = 0) Then
    '                Logo = "../Airlogo/sm" & drround(0)("MarketingCarrier") & ".gif" 'MultiValueFunction(OBDS.Tables(0), "Logo")
    '                'Airline = MultiValueFunction(OBDS.Tables(0), "Airline")
    '                Airline = drround(0)("AirlineName") & "(" & drround(0)("MarketingCarrier") & "-" & drround(0)("FlightIdentification") & ")"
    '            Else
    '                Logo = "../Airlogo/multiple.png"
    '                Airline = "Multiple Airline"
    '            End If


    '            'Airline = drround(0)("AirlineName") & "(" & drround(0)("MarketingCarrier") & "-" & drround(0)("FlightIdentification") & ")"
    '            strFlt = strFlt & "<tr>"
    '            strFlt = strFlt & "<td><img alt='' src='" & Logo & "'/><br />" & Airline & "</td>"
    '            strFlt = strFlt & "<td style='font-size:14px; font-weight:bold;'>" & drround(0)("DepartureLocation") & "(" & drround(0)("DepartureCityName") & ")</td>"
    '            strFlt = strFlt & "<td style='font-size:14px; font-weight:bold;'>" & drround(drround.Length - 1)("ArrivalLocation") & "(" & drround(drround.Length - 1)("ArrivalCityName") & ")</td>"
    '            strFlt = strFlt & "<td>" & drround(0)("Stops") & "</td>"
    '            strFlt = strFlt & "</tr>"
    '            strFlt = strFlt & "<tr>"
    '            strFlt = strFlt & "<td></td>"
    '            DepartureTime = MultiValueFunction(OBDS.Tables(0), "Deprow", 0, drround(0)("DepartureTime"))
    '            ArrivalTime = MultiValueFunction(OBDS.Tables(0), "Arrrow", 0, drround(drround.Length - 1)("ArrivalTime"))
    '            strFlt = strFlt & "<td>" & drround(0)("Departure_Date") & " (" & DepartureTime & ")</td>"
    '            strFlt = strFlt & "<td>" & drround(drround.Length - 1)("Arrival_Date") & " (" & ArrivalTime & ")</td>"
    '            If VCOB = "SG" And (SERCHVALR.Contains("P1") Or SERCHVALR.Contains("P2")) Then
    '                strFlt = strFlt & "<td style='font-size:16px; color:#004b91;'>" & drround(0)("AdtFareType") & "<br/>7kg Hand Bag Only.</td>"
    '            Else
    '                strFlt = strFlt & "<td style='font-size:16px; color:#004b91;'>" & drround(0)("AdtFareType") & "</td>"
    '            End If

    '            strFlt = strFlt & "</tr>"
    '        End If
    '        If FT = "InBound" Then
    '            If (VCCount(IBDS.Tables(0)) = 0) Then
    '                Logo = MultiValueFunction(IBDS.Tables(0), "Logo")
    '                Airline = MultiValueFunction(IBDS.Tables(0), "Airline")
    '            Else
    '                Logo = "../Airlogo/multiple.png"
    '                Airline = "Multiple Airline"
    '            End If
    '            strFlt = strFlt & "<tr>"
    '            strFlt = strFlt & "<td><img alt='' src='" & Logo & "'/><br />" & Airline & "</td>"
    '            strFlt = strFlt & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(0)("DepartureLocation") & "(" & IBDS.Tables(0).Rows(0)("DepartureCityName") & ")</td>"
    '            strFlt = strFlt & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(IBDS.Tables(0).Rows.Count - 1)("ArrivalLocation") & "(" & IBDS.Tables(0).Rows(IBDS.Tables(0).Rows.Count - 1)("ArrivalCityName") & ")</td>"
    '            strFlt = strFlt & "<td>" & IBDS.Tables(0).Rows(0)("Stops") & "</td>"
    '            strFlt = strFlt & "</tr>"
    '            strFlt = strFlt & "<tr>"
    '            strFlt = strFlt & "<td></td>"
    '            DepartureTime = MultiValueFunction(IBDS.Tables(0), "Dep")
    '            ArrivalTime = MultiValueFunction(IBDS.Tables(0), "Arr")
    '            strFlt = strFlt & "<td>" & IBDS.Tables(0).Rows(0)("Departure_Date") & " (" & DepartureTime & ")</td>"
    '            strFlt = strFlt & "<td>" & IBDS.Tables(0).Rows(IBDS.Tables(0).Rows.Count - 1)("Arrival_Date") & " (" & ArrivalTime & ")</td>"
    '            If VCIB = "SG" And (SERCHVALR.Contains("P1") Or SERCHVALR.Contains("P2")) Then
    '                strFlt = strFlt & "<td style='font-size:16px; color:#004b91;'>" & IBDS.Tables(0).Rows(0)("AdtFareType") & "<br/>7kg Hand Bag Only.</td>"
    '            Else
    '                strFlt = strFlt & "<td style='font-size:16px; color:#004b91;'>" & IBDS.Tables(0).Rows(0)("AdtFareType") & "</td>"
    '            End If

    '            strFlt = strFlt & "</tr>"

    '        End If

    '        strFlt = strFlt & "</table>"
    '        strFlt = strFlt & "</td>"
    '        strFlt = strFlt & "</tr>"

    '        strFlt = strFlt & "</table>"

    '        Dim strPax As String = ""
    '        strPax = strPax & "<table  width='100%' border='0' cellspacing='2' cellpadding='2'>"
    '        strFlt = strFlt & "<tr>"
    '        strFlt = strFlt & "<td style='font-size:14px; font-weight:bold; color:#fff; margin-bottom:15px;' colspan='3'>Flight Details</td>"
    '        strFlt = strFlt & "</tr>"
    '        strPax = strPax & "<tr style='font-size:13px;font-weight:bold'>"
    '        strPax = strPax & "<td>Adult</td>"
    '        strPax = strPax & "<td>Child</td>"
    '        strPax = strPax & "<td>Infant</td>"
    '        strPax = strPax & "</tr>"
    '        strPax = strPax & "<tr>"
    '        strPax = strPax & "<td valign='top'> &nbsp; <img alt='' src='../images/adt.png'/>(" & OBDS.Tables(0).Rows(0)("Adult") & ")</td>"
    '        strPax = strPax & "<td> &nbsp; <img alt='' src='../images/chd.png'/>(" & OBDS.Tables(0).Rows(0)("Child") & ")</td>"
    '        strPax = strPax & "<td> &nbsp; <img alt='' src='../images/inf.png'/>(" & OBDS.Tables(0).Rows(0)("Infant") & ")</td>"
    '        strPax = strPax & "</tr>"
    '        strPax = strPax & "<tr id='tr_tottotfare' onmouseover=funcnetfare('block','tr_totnetfare'); onmouseout=funcnetfare('none','tr_totnetfare'); style='cursor:pointer;color: #004b91'>"
    '        Dim TotalFare As Double
    '        Dim NetFare As Double
    '        If FT = "InBound" Then
    '            TotalFare = Convert.ToDouble(OBDS.Tables(0).Rows(0)("TotFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("TotFare"))
    '            NetFare = Convert.ToDouble(OBDS.Tables(0).Rows(0)("NetFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("NetFare"))
    '        Else
    '            TotalFare = Convert.ToDouble(OBDS.Tables(0).Rows(0)("TotFare"))
    '            NetFare = Convert.ToDouble(OBDS.Tables(0).Rows(0)("NetFare"))
    '        End If
    '        strPax = strPax & "<td colspan='3'><span style='font-size:14px; font-weight:bold; color:#000042; line-height:40px;'>Total Fare: " & TotalFare & "</span><div id='tr_totnetfare' style='display:none;position:absolute;background:#F1F1F1;border: thin solid #D1D1D1;padding:10px; font-size:14px; font-weight:bold; color:#000;'>Net Fare: " & NetFare & "</div></td>"
    '        strPax = strPax & "</tr>"

    '        'strPax = strPax & "<tr id='tr_totnetfare' style='display:none;position:absolute;background:#D1D1D1;padding:5x;'>"
    '        'strPax = strPax & "<td>Net Fare:" & NetFare & "</td>"
    '        'strPax = strPax & "</tr>"

    '        strPax = strPax & "<tr><td colspan='3'><div style='float:left; cursor:pointer; background:#004b91; padding:5px 10px; color:#fff; font-weight:bold;' id='ctl00_ContentPlaceHolder1_divtotFlight' onclick='ddshow(this.id);'>Flight Summary</div><div style='float:right; font-weight:bold; cursor:pointer; background:#004b91; padding:5px 10px; color:#fff;' id='div_faredd' onclick='ddshow(this.id);'>Fare Summary</div></td></tr>"
    '        strPax = strPax & "</table>"
    '        divtotalpax.InnerHtml = strPax
    '        ' divtotFlightDetails.InnerHtml = CustFltDetails(OBDS, IBDS, FT)
    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)
    '    End Try


    '    Return strFlt
    'End Function
    ''Private Function CustFltDetails(ByVal OBDS As DataSet, ByVal IBDS As DataSet, ByVal FT As String) As String
    ''    Dim FlightDtlsTotalInfo As String = ""
    ''    Dim DepTerminal As String
    ''    Dim ArrTerminal As String
    ''    Dim FlightType = ""
    ''    If FT = "InBound" Then
    ''        FlightType = "OutBound"
    ''    End If

    ''    FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<table  width='100%' border='0' cellspacing='0' cellpadding='0'>"
    ''    FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr ><td colspan='2' style='font-size:18px; line-height:35px; border-bottom:2px solid #d1d1d1;' >" & FlightType & " Flight Details</td><td align='left' style='font-size:14px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91; '>" & OBDS.Tables(0).Rows(0)("AdtFareType") & "</td><tr>"
    ''    For i As Integer = 0 To OBDS.Tables(0).Rows.Count - 1
    ''        DepTerminal = ""
    ''        ArrTerminal = ""
    ''        'Dim AirportName As String =STDom.GetAirportName(OBDS.Tables(0).Rows(i)("DepartureLocation")
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td ><img alt='' src='../Airlogo/sm" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & OBDS.Tables(0).Rows(i)("AirlineName") & "</td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>(" & OBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & OBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & OBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction(OBDS.Tables(0), "Depall", i) & ")</td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & OBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction(OBDS.Tables(0), "Arrall", i) & ")</td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >Class(" & OBDS.Tables(0).Rows(i)("RBD") & ") </td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & OBDS.Tables(0).Rows(i)("DepartureCityName") & "(" & OBDS.Tables(0).Rows(i)("DepartureLocation") & ")</td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >" & OBDS.Tables(0).Rows(i)("ArrivalCityName") & "(" & OBDS.Tables(0).Rows(i)("ArrivalLocation") & ")</td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    ''        If OBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim() <> "" Then
    ''            DepTerminal = "Terminal - " & OBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim()
    ''        End If
    ''        If OBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim() <> "" Then
    ''            ArrTerminal = "Terminal - " & OBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim()
    ''        End If

    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='padding-left: 25px'></td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & STDom.GetAirportName(OBDS.Tables(0).Rows(i)("DepartureLocation")) & " " & DepTerminal & "</td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & STDom.GetAirportName(OBDS.Tables(0).Rows(i)("ArrivalLocation")) & " " & ArrTerminal & "</td>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
    ''    Next
    ''    If FT = "InBound" Then
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr><td style='padding-top: 20px'> </td></tr>"
    ''        ' FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr><td colspan='4' style='font-size:18px; line-height:35px; border-bottom:2px solid #d1d1d1;'>Outbound Flight Details<td><tr>"
    ''        FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr ><td colspan='2' style='font-size:18px; line-height:35px; border-bottom:2px solid #d1d1d1;' >InBound Flight Details</td><td align='left' style='font-size:14px; line-height:35px; border-bottom:2px solid #d1d1d1;color:#004b91; '>" & IBDS.Tables(0).Rows(0)("AdtFareType") & "</td><tr>"
    ''        For i As Integer = 0 To IBDS.Tables(0).Rows.Count - 1
    ''            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td><img alt='' src='../Airlogo/sm" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & IBDS.Tables(0).Rows(i)("AirlineName") & "(" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & IBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
    ''            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("DepartureLocation") & "(" & IBDS.Tables(0).Rows(i)("DepartureCityName") & ")</td>"
    ''            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("ArrivalLocation") & "(" & IBDS.Tables(0).Rows(i)("ArrivalCityName") & ")</td>"
    ''            'FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td ><img alt='' src='../Airlogo/sm" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & ".gif'/> " & IBDS.Tables(0).Rows(i)("AirlineName") & "</td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>(" & IBDS.Tables(0).Rows(i)("MarketingCarrier") & "-" & IBDS.Tables(0).Rows(i)("FlightIdentification") & ")" & "</td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction(IBDS.Tables(0), "Depall", i) & ")</td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='font-size:14px; font-weight:bold;'>" & IBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction(IBDS.Tables(0), "Arrall", i) & ")</td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"

    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td >Class(" & IBDS.Tables(0).Rows(i)("RBD") & ") </td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & IBDS.Tables(0).Rows(i)("Departure_Date") & " (" & MultiValueFunction(IBDS.Tables(0), "Depall", i) & ")</td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & IBDS.Tables(0).Rows(i)("Arrival_Date") & " (" & MultiValueFunction(IBDS.Tables(0), "Arrall", i) & ")</td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
    ''            DepTerminal = ""
    ''            ArrTerminal = ""
    ''            If IBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim() <> "" Then
    ''                DepTerminal = "Terminal - " & IBDS.Tables(0).Rows(i)("DepartureTerminal").ToString().Trim()
    ''            End If
    ''            If IBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim() <> "" Then
    ''                ArrTerminal = "Terminal - " & IBDS.Tables(0).Rows(i)("ArrivalTerminal").ToString().Trim()
    ''            End If
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<tr>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td style='padding-left: 25px'></td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & STDom.GetAirportName(IBDS.Tables(0).Rows(i)("DepartureLocation")) & " " & DepTerminal & "</td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "<td>" & STDom.GetAirportName(IBDS.Tables(0).Rows(i)("ArrivalLocation")) & " " & ArrTerminal & "</td>"
    ''            FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</tr>"
    ''        Next
    ''    End If
    ''    FlightDtlsTotalInfo = FlightDtlsTotalInfo & "</table>"


    ''    Return FlightDtlsTotalInfo
    ''End Function
    ''Private Function fareBreakupfun_Tot(ByVal OBDS As DataSet, ByVal IBDS As DataSet, ByVal FT As String) As String
    ''    Dim fbfstr As String = ""
    ''    fbfstr = fbfstr & "<table  width='250px' border='0' cellspacing='2' cellpadding='2'>"
    ''    fbfstr = fbfstr & "<tr>"
    ''    fbfstr = fbfstr & "<td colspan='2'>Outbound Fare</td>"
    ''    fbfstr = fbfstr & "</tr>"
    ''    fbfstr = fbfstr & "<tr>"
    ''    fbfstr = fbfstr & "<td>Adult Fare(1)</td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ADTAgentMrk")) & "</td>"
    ''    fbfstr = fbfstr & "</tr>"
    ''    If Child > 0 Then
    ''        fbfstr = fbfstr & "<tr>"
    ''        fbfstr = fbfstr & "<td>Child Fare(1)</td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("CHDAgentMrk")) & "</td>"
    ''        fbfstr = fbfstr & "</tr>"
    ''    End If
    ''    If Infant > 0 Then
    ''        fbfstr = fbfstr & "<tr>"
    ''        fbfstr = fbfstr & "<td>Infant Fare(1)</td><td>" & OBDS.Tables(0).Rows(0)("InfFare") & "</td>"
    ''        fbfstr = fbfstr & "</tr>"
    ''    End If
    ''    'fbfstr = fbfstr & "<tr>"
    ''    'fbfstr = fbfstr & "<td>Total Fare</td><td>" & OBDS.Tables(0).Rows(0)("totFare") & "</td>"
    ''    'fbfstr = fbfstr & "</tr>"

    ''    If (FT = "InBound") Then
    ''        fbfstr = fbfstr & "<tr>"
    ''        fbfstr = fbfstr & "<td colspan='2'>Inbound Fare</td>"
    ''        fbfstr = fbfstr & "</tr>"
    ''        fbfstr = fbfstr & "<tr>"
    ''        fbfstr = fbfstr & "<td>Adult Fare(1)</td><td>" & Convert.ToDouble(IBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("ADTAgentMrk")) & "</td>"
    ''        fbfstr = fbfstr & "</tr>"
    ''        If Child > 0 Then
    ''            fbfstr = fbfstr & "<tr>"
    ''            fbfstr = fbfstr & "<td>Child Fare(1)</td><td>" & Convert.ToDouble(IBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("CHDAgentMrk")) & "</td>"
    ''            fbfstr = fbfstr & "</tr>"
    ''        End If
    ''        If Infant > 0 Then
    ''            fbfstr = fbfstr & "<tr>"
    ''            fbfstr = fbfstr & "<td>Infant Fare(1)</td><td>" & IBDS.Tables(0).Rows(0)("InfFare") & "</td>"
    ''            fbfstr = fbfstr & "</tr>"
    ''        End If
    ''        'fbfstr = fbfstr & "<tr>"
    ''        'fbfstr = fbfstr & "<td>Total Fare</td><td>" & IBDS.Tables(0).Rows(0)("totFare") & "</td>"
    ''        'fbfstr = fbfstr & "</tr>"

    ''    End If
    ''    Dim K As String = ""
    ''    If (FT <> "InBound") Then
    ''        fbfstr = fbfstr & "<tr>"
    ''        fbfstr = fbfstr & "<td>Total(Per Pax) </td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ADTAgentMrk")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("CHDAgentMrk")) + OBDS.Tables(0).Rows(0)("InfFare") & "</td>"
    ''        fbfstr = fbfstr & "</tr>"
    ''        fbfstr = fbfstr & "<tr>"
    ''        fbfstr = fbfstr & "<td>Grand Total</td><td>" & OBDS.Tables(0).Rows(0)("totFare") & "</td>"
    ''        fbfstr = fbfstr & "</tr>"
    ''    Else
    ''        fbfstr = fbfstr & "<tr>"
    ''        fbfstr = fbfstr & "<td>Total(Per Pax) </td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ADTAgentMrk")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(OBDS.Tables(0).Rows(0)("CHDAgentMrk")) + OBDS.Tables(0).Rows(0)("InfFare") + Convert.ToDouble(IBDS.Tables(0).Rows(0)("AdtFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("ADTAgentMrk")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("ChdFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("CHDAgentMrk")) + IBDS.Tables(0).Rows(0)("InfFare") & "</td>"
    ''        fbfstr = fbfstr & "</tr>"
    ''        fbfstr = fbfstr & "<tr>"
    ''        fbfstr = fbfstr & "<td>Grand Total</td><td>" & Convert.ToDouble(OBDS.Tables(0).Rows(0)("totFare")) + Convert.ToDouble(IBDS.Tables(0).Rows(0)("totFare")) & "</td>"
    ''        fbfstr = fbfstr & "</tr>"
    ''        K = "(InBound+OutBound)"
    ''    End If
    ''    fbfstr = fbfstr & "<tr>"
    ''    fbfstr = fbfstr & "<td colspan='2'>" & OBDS.Tables(0).Rows(0)("Adult") & " ADT," & OBDS.Tables(0).Rows(0)("Child") & " CHD," & OBDS.Tables(0).Rows(0)("Infant") & " INF " & K & "</td>"
    ''    fbfstr = fbfstr & "</tr>"
    ''    fbfstr = fbfstr & "</table>"
    ''    Return fbfstr

    ''End Function
    Private Function fareBreakupfun(ByVal OFareDS As DataSet, ByVal Ft As String) As String
        Try
            Dim tax(), tax1() As String, yq As Integer = 0, tx As Integer = 0
            tax = OFareDS.Tables(0).Rows(0)("Adt_Tax").ToString.Split("#")
            For i As Integer = 0 To tax.Length - 2
                If InStr(tax(i), "YQ") Then
                    tax1 = tax(i).Split(":")
                    yq = yq + Convert.ToInt32(tax1(1))
                Else
                    tax1 = tax(i).Split(":")
                    tx = tx + Convert.ToInt32(tax1(1))
                End If
            Next
            Dim T_ID As String = ""
            If Ft = "OutBound" Then
                T_ID = "OB_FT"
                strFare = "<div id='" + T_ID + "' class='w100'>"
            ElseIf Ft = "InBound" Then
                T_ID = "IB_FT"
                strFare = "<div id='" + T_ID + "' class='w100'>"
            End If
            strFare = strFare & "<div class='w101 brdr bgw'>"
            strFare = strFare & "<div class='lft w33'>"
            strFare = strFare & "<div>"
            strFare = strFare & "<div class='bld lft w70'>Adult Fare</div>"
            strFare = strFare & "<div class='w30 rgt'>" & OFareDS.Tables(0).Rows(0)("AdtBFare") & "</div>"
            strFare = strFare & "</div>"
            strFare = strFare & "<div>"
            strFare = strFare & "<div class='bld lft w70'>Fuel Surcharge</div>"
            strFare = strFare & "<div class='w30 rgt'>" & yq & "</div>"
            strFare = strFare & "</div>"
            strFare = strFare & "<div>"
            strFare = strFare & "<div class='bld lft w70'>Tax</div>"
            strFare = strFare & "<div class='w30 rgt'>" & tx & "</div>"
            strFare = strFare & "</div>"
            strFare = strFare & "<div>"
            strFare = strFare & "<div class='bld lft w70'>Total</div>"
            strFare = strFare & "<div class='w30 rgt'>" & OFareDS.Tables(0).Rows(0)("AdtFare") & "</div>"
            strFare = strFare & "</div>"
            strFare = strFare & "</div>"

            If Child > 0 Then
                Try
                    yq = 0
                    tx = 0
                    tax = OFareDS.Tables(0).Rows(0)("Chd_Tax").ToString.Split("#")
                    For i As Integer = 0 To tax.Length - 2
                        If InStr(tax(i), "YQ") Then
                            tax1 = tax(i).Split(":")
                            yq = yq + Convert.ToInt32(tax1(1))
                        Else
                            tax1 = tax(i).Split(":")
                            tx = tx + Convert.ToInt32(tax1(1))
                        End If
                    Next
                Catch ex As Exception
                End Try

                strFare = strFare & "<div class='lft w33'>"
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Child Fare</div>"
                strFare = strFare & "<div class='w30 rgt'>" & OFareDS.Tables(0).Rows(0)("ChdBFare") & "</div>"
                strFare = strFare & "</div>"
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Fuel Surcharge </div>"
                strFare = strFare & "<div class='w30 rgt'>" & yq & "</div>"
                strFare = strFare & "</div>"
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Tax</div>"
                strFare = strFare & "<div class='w30 rgt'>" & tx & "</div>"
                strFare = strFare & "</div>"
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Total</div>"
                strFare = strFare & "<div class='w30 rgt'>" & OFareDS.Tables(0).Rows(0)("ChdFare") & "</div>"
                strFare = strFare & "</div>"
                strFare = strFare & "</div>"
            End If

            If Infant > 0 Then
                Try
                    yq = 0
                    tx = 0
                    tax = OFareDS.Tables(0).Rows(0)("Inf_Tax").ToString.Split("#")
                    For i As Integer = 0 To tax.Length - 2
                        If InStr(tax(i), "YQ") Then
                            tax1 = tax(i).Split(":")
                            yq = yq + Convert.ToInt32(tax1(1))
                        Else
                            tax1 = tax(i).Split(":")
                            tx = tx + Convert.ToInt32(tax1(1))
                        End If
                    Next
                Catch ex As Exception
                End Try
                strFare = strFare & "<div class='lft w33'>"
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Infant Fare</div>"
                strFare = strFare & "<div class='rgt w30'>" & OFareDS.Tables(0).Rows(0)("InfBFare") & "</div>"
                strFare = strFare & "</div>"
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Tax</div>"
                strFare = strFare & "<div class='rgt w30'>" & tx & "</div>"
                strFare = strFare & "</div>"
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Total</div>"
                strFare = strFare & "<div class='rgt w30'>" & OFareDS.Tables(0).Rows(0)("InfFare") & "</div>"
                strFare = strFare & "</div>"
                strFare = strFare & "</div>"
            End If
            strFare = strFare & "<div class='hr'></div>"
            strFare = strFare & "<div class='w33'>"
            strFare = strFare & "<div>"
            strFare = strFare & "<div class='bld lft w70'>Srv. Tax</div>"
            strFare = strFare & "<div class='rgt w30'>" & OFareDS.Tables(0).Rows(0)("SrvTax") & "</div>"
            strFare = strFare & "</div>"
            If (OFareDS.Tables(0).Rows(0)("IsCorp") = True) Then
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Mgnt. Fee</div>"
                strFare = strFare & "<div class='rgt w30'>" & OFareDS.Tables(0).Rows(0)("TOTMGTFEE") & "</div>"
                strFare = strFare & "</div>"
            Else
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Tran. Fee</div>"
                strFare = strFare & "<div class='rgt w30'>" & OFareDS.Tables(0).Rows(0)("TFee") & "</div>"
                strFare = strFare & "</div>"
                strFare = strFare & "<div>"
                strFare = strFare & "<div class='bld lft w70'>Tran. Charge</div>"
                'strFare = strFare & "<div>" & OFareDS.Tables(0).Rows(0)("TC") & "</div>"
                If OFareDS.Tables(0).Rows(0)("AdtFareType").ToString() = "Special Fare" Then
                    strFare = strFare & "<div class='rgt w30'>" & (Convert.ToDouble(OFareDS.Tables(0).Rows(0)("ADTAgentMrk")) * Adult) + (Convert.ToDouble(OFareDS.Tables(0).Rows(0)("CHDAgentMrk")) * Child) & "</div>"
                Else
                    strFare = strFare & "<div class='rgt w30'>" & OFareDS.Tables(0).Rows(0)("TC") & "</div>"
                End If
                strFare = strFare & "</div>"
            End If

            If Ft = "OutBound" Then
                strFare = strFare & "<div id='trtotfare' onmouseover=funcnetfare('block','trnetfare'); onmouseout=funcnetfare('none','trnetfare'); style='cursor:pointer;color: #004b91'>"
            ElseIf Ft = "InBound" Then
                strFare = strFare & "<div id='trtotfareR'onmouseover=funcnetfare('block','trnetfareR'); onmouseout=funcnetfare('none','trnetfareR'); style='cursor:pointer;color: #004b91'>"
            End If

            strFare = strFare & "<div class='bld lft w70'>Total Fare(" & Adult & " Adt," & Child & " Chd," & Infant & " Inf)</div>"
            strFare = strFare & "<div class='rgt w30'>" & OFareDS.Tables(0).Rows(0)("totFare") & "</div>"
            strFare = strFare & "</div>"
            If Ft = "OutBound" Then
                strFare = strFare & "<div id='trnetfare' class='hide'>"
            ElseIf Ft = "InBound" Then
                strFare = strFare & "<div id='trnetfareR' class='hide'>"
            End If
            strFare = strFare & "<div class='w70 lft bld'>Net Fare</div>"
            strFare = strFare & "<div class='rgt w30'>" & OFareDS.Tables(0).Rows(0)("netFare") & "</div>"
            strFare = strFare & "</div>"
            strFare = strFare & "</div>"
            strFare = strFare & "<div class='clear'></div>"
            strFare = strFare & "</div>"
            strFare = strFare & "</div>"
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


        Return strFare
    End Function
    Public Sub Bind_pax(ByVal cntAdult As Integer, ByVal cntChild As Integer, ByVal cntInfant As Integer)
        Try
            Dim PaxTbl As New DataTable()
            Dim cntTblColumn As DataColumn = Nothing
            cntTblColumn = New DataColumn()
            cntTblColumn.DataType = Type.[GetType]("System.Double")
            cntTblColumn.ColumnName = "Counter"
            PaxTbl.Columns.Add(cntTblColumn)

            cntTblColumn = New DataColumn()
            cntTblColumn.DataType = Type.[GetType]("System.String")
            cntTblColumn.ColumnName = "PaxTP"
            PaxTbl.Columns.Add(cntTblColumn)
            Dim cntrow As DataRow = Nothing
            For i As Integer = 1 To cntAdult
                cntrow = PaxTbl.NewRow()
                cntrow("Counter") = i
                cntrow("PaxTP") = "Passenger " & i.ToString() & " (Adult)"
                PaxTbl.Rows.Add(cntrow)
            Next
            Repeater_Adult.DataSource = PaxTbl
            Repeater_Adult.DataBind()


            PaxTbl.Clear()
            If cntChild > 0 Then

                For i As Integer = 1 To cntChild
                    cntrow = PaxTbl.NewRow()
                    cntrow("Counter") = i
                    cntrow("PaxTP") = "Passenger " & i.ToString() & " (Child)"
                    PaxTbl.Rows.Add(cntrow)
                Next
                Repeater_Child.DataSource = PaxTbl
                Repeater_Child.DataBind()
            End If


            PaxTbl.Clear()

            If cntInfant > 0 Then

                For i As Integer = 1 To cntInfant
                    cntrow = PaxTbl.NewRow()
                    cntrow("Counter") = i
                    cntrow("PaxTP") = "Passenger " & i.ToString() & " (Infant)"
                    PaxTbl.Rows.Add(cntrow)
                Next
                Repeater_Infant.DataSource = PaxTbl
                Repeater_Infant.DataBind()
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try



    End Sub
    Protected Sub book_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles book.Click
        Session("search_type") = "Flt"
        Dim AgencyDs As DataSet
        Dim OBFltDs, IBFltDs As DataSet
        Dim totFare As Double = 0
        Dim netFare As Double = 0
        Dim FltHdrO, FltHdrR As New DataSet
        Dim ProjectId As String = If(DropDownListProject.Visible = True, If(DropDownListProject.SelectedValue.ToLower() <> "select", DropDownListProject.SelectedValue, Nothing), Nothing)
        Dim BookedBy As String = If(DropDownListBookedBy.Visible = True, If(DropDownListBookedBy.SelectedValue.ToLower() <> "select", DropDownListBookedBy.SelectedValue, Nothing), Nothing)
        Dim CorpBillNo As String = Nothing
        Dim Prvdr As String = ""
        Dim PrvdrIB As String = ""

        OBFltDs = objDA.GetFltDtls(ViewState("OBTrackId"), Session("UID"))
        VCOB = OBFltDs.Tables(0).Rows(0)("ValiDatingCarrier") 'New Code 
        VCOBSPL = OBFltDs.Tables(0).Rows(0)("AdtFareType") 'New Code
        'TripOB = OBFltDs.Tables(0).Rows(0)("Trip") 'New Code 

        Prvdr = OBFltDs.Tables(0).Rows(0)("Provider")

        If Prvdr.Trim().ToUpper() = "TB" Or Prvdr.Trim().ToUpper() = "YA" Then
            If OBFltDs.Tables(0).Rows(OBFltDs.Tables(0).Rows.Count - 1)("TripType").ToString().Trim() = "R" Then
                FLT_STAT = "RTF"

            End If
        Else
            FLT_STAT = OBFltDs.Tables(0).Rows(0)("FlightStatus")
        End If
        'New Code


        Dim BookingRefNo As String = ViewState("OBTrackId")
        If ViewState("FT") = "InBound" Then
            IBFltDs = objDA.GetFltDtls(ViewState("IBTrackId"), Session("UID"))
            VCIB = IBFltDs.Tables(0).Rows(0)("ValiDatingCarrier") 'New Code
            VCIBSPL = IBFltDs.Tables(0).Rows(0)("AdtFareType") 'New Code
            'TripIB = IBFltDs.Tables(0).Rows(0)("Trip") 'New Code
            PrvdrIB = IBFltDs.Tables(0).Rows(0)("Provider")
        End If
        AgencyDs = objDA.GetAgencyDetails(Session("UID"))

        Try
            If AgencyDs.Tables.Count > 0 And OBFltDs.Tables.Count > 0 Then
                If AgencyDs.Tables(0).Rows.Count > 0 And OBFltDs.Tables(0).Rows.Count > 0 Then
                    If AgencyDs.Tables(0).Rows(0)("Agent_Status").ToString.Trim <> "NOT ACTIVE" And AgencyDs.Tables(0).Rows(0)("Online_tkt").ToString.Trim <> "NOT ACTIVE" Then
                        totFare = OBFltDs.Tables(0).Rows(0)("totFare")
                        netFare = OBFltDs.Tables(0).Rows(0)("netFare")
                        If ViewState("FT") = "InBound" Then
                            totFare = totFare + IBFltDs.Tables(0).Rows(0)("totFare")
                            netFare = netFare + IBFltDs.Tables(0).Rows(0)("netFare")
                        End If
                        Dim agentBal As String = ""
                        agentBal = objUMSvc.GetAgencyBal()
                        '' ''If Convert.ToDouble(AgencyDs.Tables(0).Rows(0)("Crd_Limit").ToString.Trim) > netFare Then
                        If Convert.ToDouble(agentBal) > netFare Then

                            FltHdrO = objDA.GetHdrDetails(ViewState("OBTrackId"))
                            If FltHdrO.Tables.Count > 0 Then
                                If FltHdrO.Tables(0).Rows.Count = 0 Then
                                    If Not IsDBNull(AgencyDs.Tables(0).Rows(0)("IsCorp")) Then
                                        If Convert.ToBoolean(AgencyDs.Tables(0).Rows(0)("IsCorp")) Then
                                            CorpBillNo = clsCorp.GenerateBillNoCorp("D")
                                        End If

                                    End If
                                    objDA.insertFltHdrDetails(OBFltDs, AgencyDs, Session("UID"), ddl_PGTitle.SelectedValue, txt_PGFName.Text, txt_PGLName.Text, txt_MobNo.Text.Trim(), txt_Email.Text, "D", ProjectId, BookedBy, CorpBillNo, "OutBound", BookingRefNo)
                                    objDA.insertFlightDetails(OBFltDs)
                                    objDA.insertFareDetails(OBFltDs, "D")
                                    InsertPaxDetail(ViewState("OBTrackId"), OBFltDs, "OB")
                                    If (VCOB = "SG" Or VCOB = "6E") And InStr(VCOBSPL, "Special") = 0 Or Prvdr.Trim().ToUpper() = "TB" Or Prvdr.Trim().ToUpper() = "YA" Then

                                        Dim Paxdt As DataSet = objDA.Get_MEAL_BAG_PaxDetails(ViewState("OBTrackId").ToString())
                                        Insert_MEAL_BAG_Detail(ViewState("OBTrackId"), OBFltDs, Paxdt, "OB", "O")
                                        If FLT_STAT = "RTF" Then 'New Code
                                            Insert_MEAL_BAG_Detail(ViewState("OBTrackId"), OBFltDs, Paxdt, "IB", "R")
                                        End If

                                        If Prvdr.Trim().ToUpper() <> "TB" And Prvdr.Trim().ToUpper() <> "YA" Then
                                            SELL_SSR(ViewState("OBTrackId"))

                                        ElseIf Prvdr.Trim().ToUpper() = "TB" Or Prvdr.Trim().ToUpper() = "YA" Then

                                            UpadteFare(ViewState("OBTrackId"))
                                        End If

                                    End If

                                    If ViewState("FT") = "InBound" Then
                                        FltHdrR = objDA.GetHdrDetails(ViewState("IBTrackId"))
                                        If FltHdrR.Tables.Count > 0 Then
                                            If FltHdrR.Tables(0).Rows.Count = 0 Then
                                                If Not IsDBNull(AgencyDs.Tables(0).Rows(0)("IsCorp")) Then
                                                    If Convert.ToBoolean(AgencyDs.Tables(0).Rows(0)("IsCorp")) Then
                                                        CorpBillNo = clsCorp.GenerateBillNoCorp("D")
                                                    End If

                                                End If
                                                objDA.insertFltHdrDetails(IBFltDs, AgencyDs, Session("UID"), ddl_PGTitle.SelectedValue, txt_PGFName.Text, txt_PGLName.Text, txt_MobNo.Text.Trim(), txt_Email.Text, "D", ProjectId, BookedBy, CorpBillNo, "InBound", BookingRefNo)
                                                objDA.insertFlightDetails(IBFltDs)
                                                objDA.insertFareDetails(IBFltDs, "D")
                                                InsertPaxDetail(ViewState("IBTrackId"), IBFltDs, "IB")
                                                If (VCIB = "SG" Or VCIB = "6E") And InStr(VCIBSPL, "Special") = 0 Or PrvdrIB.Trim().ToUpper() = "TB" Or PrvdrIB.Trim().ToUpper() = "YA" Then
                                                    Dim PaxdtRT As DataSet = objDA.Get_MEAL_BAG_PaxDetails(ViewState("IBTrackId").ToString())
                                                    Insert_MEAL_BAG_Detail(ViewState("IBTrackId"), IBFltDs, PaxdtRT, "IB", "O") 'New Code

                                                    If PrvdrIB.Trim().ToUpper() <> "TB" And PrvdrIB.Trim().ToUpper() <> "YA" Then
                                                        SELL_SSR(ViewState("IBTrackId"))

                                                    ElseIf PrvdrIB.Trim().ToUpper() = "TB" Or PrvdrIB.Trim().ToUpper() = "YA" Then

                                                        UpadteFare(ViewState("IBTrackId"))
                                                    End If

                                                End If
                                                Response.Redirect("../FlightDom/PriceDetails.aspx?OBTID=" & ViewState("OBTrackId") & "&IBTID=" & ViewState("IBTrackId") & "&FT=" & ViewState("FT") & "")
                                                'Dim um As String = ""
                                                ''um = objUMSvc.GetMUForPage("FlightDom/PriceDetails.aspx")
                                                ''Response.Redirect(um & "?OBTID=" & ViewState("OBTrackId") & "&IBTID=" & ViewState("IBTrackId") & "&FT=" & ViewState("FT") & "")
                                                ''''Response.Redirect("../wait.aspx?OBTID=" & ViewState("OBTrackId") & "&IBTID=" & ViewState("IBTrackId") & "&FT=" & ViewState("FT") & "")
                                            Else
                                                ''Dim um As String = ""
                                                ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                                                ''Response.Redirect(um & "?msg=1")
                                                Response.Redirect("../FlightInt/BookingMsg.aspx?msg=1")
                                            End If
                                        Else
                                            ''Dim um As String = ""
                                            ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                                            ''Response.Redirect(um & "?msg=2")
                                            Response.Redirect("../FlightInt/BookingMsg.aspx?msg=2")
                                        End If
                                    Else
                                        ''''Response.Redirect("../wait.aspx?OBTID=" & ViewState("OBTrackId") & "&IBTID=" & ViewState("IBTrackId") & "&FT=" & ViewState("FT") & "")
                                        ''Dim um As String = ""
                                        ''um = objUMSvc.GetMUForPage("FlightDom/PriceDetails.aspx")
                                        ''Response.Redirect(um & "?OBTID=" & ViewState("OBTrackId") & "&FT=" & ViewState("FT") & "")
                                        Response.Redirect("../FlightDom/PriceDetails.aspx?OBTID=" & ViewState("OBTrackId") & "&FT=" & ViewState("FT") & "")
                                    End If
                                Else
                                    ''Dim um As String = ""
                                    ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                                    ''Response.Redirect(um & "?msg=1")
                                    Response.Redirect("../FlightInt/BookingMsg.aspx?msg=1")
                                End If
                            Else
                                ''Dim um As String = ""
                                ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                                ''Response.Redirect(um & "?msg=2")
                                Response.Redirect("../FlightInt/BookingMsg.aspx?msg=2")
                            End If
                        Else
                            ''Dim um As String = ""
                            ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                            ''Response.Redirect(um & "?msg=CL")
                            Response.Redirect("../FlightInt/BookingMsg.aspx?msg=CL")
                        End If
                    Else
                        ''Dim um As String = ""
                        ''um = objUMSvc.GetMUForPage("FlightInt/BookingMsg.aspx")
                        ''Response.Redirect(um & "?msg=NA")
                        Response.Redirect("../FlightInt/BookingMsg.aspx?msg=NA")
                    End If
                End If
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub InsertPaxDetail(ByVal trackid As String, ByVal FltDs As DataSet, ByVal Trip As String)
        Try
            Adult = Convert.ToInt16(FltDs.Tables(0).Rows(0)("Adult"))
            Child = Convert.ToInt16(FltDs.Tables(0).Rows(0)("Child"))
            Infant = Convert.ToInt16(FltDs.Tables(0).Rows(0)("Infant"))

            Dim counter As Integer = 0

            For Each rw As RepeaterItem In Repeater_Adult.Items
                counter += 1
                ''New Code
                Dim ddl_AMealPrefer As DropDownList
                Dim ddl_ASeatPrefer As DropDownList
                Dim txt_AAirline As HtmlInputHidden
                Dim txt_ANumber As TextBox
                '''
                Dim ddl_ATitle As DropDownList = DirectCast(rw.FindControl("ddl_ATitle"), DropDownList)
                Dim ddl_AGender As DropDownList = DirectCast(rw.FindControl("ddl_AGender"), DropDownList)
                Dim txtAFirstName As TextBox = DirectCast(rw.FindControl("txtAFirstName"), TextBox)
                Dim txtAMiddleName As TextBox = DirectCast(rw.FindControl("txtAMiddleName"), TextBox)
                If txtAMiddleName.Text = "Middle Name" Then
                    txtAMiddleName.Text = ""
                End If
                Dim txtALastName As TextBox = DirectCast(rw.FindControl("txtALastName"), TextBox)
                Dim gender As String = "F"

                If ddl_ATitle.SelectedValue.Trim.ToLower = "dr" Or ddl_ATitle.SelectedValue.Trim.ToLower = "prof" Then
                    gender = ddl_AGender.SelectedValue.Trim

                ElseIf ddl_ATitle.SelectedValue.Trim.ToLower = "mr" Then
                    gender = "M"

                End If

                'Dim ddl_ADate As DropDownList = DirectCast(rw.FindControl("ddl_ADate"), DropDownList)
                'Dim ddl_AMonth As DropDownList = DirectCast(rw.FindControl("ddl_AMonth"), DropDownList)
                Dim txtadultDOB As TextBox = DirectCast(rw.FindControl("Txt_AdtDOB"), TextBox)
                Dim DOB As String = ""
                DOB = txtadultDOB.Text.Trim

                If (Not (Trip = "IB")) Then
                    ddl_AMealPrefer = DirectCast(rw.FindControl("ddl_AMealPrefer"), DropDownList)
                    ddl_ASeatPrefer = DirectCast(rw.FindControl("ddl_ASeatPrefer"), DropDownList)
                    txt_AAirline = DirectCast(rw.FindControl("hidtxtAirline_Dom"), HtmlInputHidden)
                    txt_ANumber = DirectCast(rw.FindControl("txt_ANumber"), TextBox)
                Else
                    ddl_AMealPrefer = DirectCast(rw.FindControl("ddl_AMealPrefer_R"), DropDownList)
                    ddl_ASeatPrefer = DirectCast(rw.FindControl("ddl_ASeatPrefer_R"), DropDownList)
                    txt_AAirline = DirectCast(rw.FindControl("hidtxtAirline_R_Dom"), HtmlInputHidden)
                    txt_ANumber = DirectCast(rw.FindControl("txt_ANumber_R"), TextBox)
                End If


                If txt_AAirline.Value = "Airline" Then
                    txt_AAirline.Value = ""
                End If
                If txt_ANumber.Text = "Number" Then
                    txt_ANumber.Text = ""
                End If
                If counter <= Infant Then
                    objDA.insertPaxDetails(trackid, ddl_ATitle.SelectedValue, txtAFirstName.Text.Trim(), txtAMiddleName.Text.Trim(), txtALastName.Text.Trim(), _
                     "ADT", DOB, txt_ANumber.Text.Trim(), txt_AAirline.Value.Trim(), ddl_AMealPrefer.SelectedValue, ddl_ASeatPrefer.SelectedValue, _
                     "true", "", gender, "", "", "", "")
                Else

                    objDA.insertPaxDetails(trackid, ddl_ATitle.SelectedValue, txtAFirstName.Text.Trim(), txtAMiddleName.Text.Trim(), txtALastName.Text.Trim(), _
                     "ADT", DOB, txt_ANumber.Text.Trim(), txt_AAirline.Value.Trim(), ddl_AMealPrefer.SelectedValue, ddl_ASeatPrefer.SelectedValue, _
                     "false", "", gender, "", "", "", "")

                End If
            Next

            If Child > 0 Then
                For Each rw As RepeaterItem In Repeater_Child.Items
                    Dim ddl_CMealPrefer As DropDownList
                    Dim ddl_CSeatPrefer As DropDownList

                    Dim ddl_CTitle As DropDownList = DirectCast(rw.FindControl("ddl_CTitle"), DropDownList)
                    Dim txtCFirstName As TextBox = DirectCast(rw.FindControl("txtCFirstName"), TextBox)
                    Dim txtCMiddleName As TextBox = DirectCast(rw.FindControl("txtCMiddleName"), TextBox)
                    If txtCMiddleName.Text = "Middle Name" Then
                        txtCMiddleName.Text = ""
                    End If
                    Dim txtCLastName As TextBox = DirectCast(rw.FindControl("txtCLastName"), TextBox)

                    Dim txtchildDOB As TextBox = DirectCast(rw.FindControl("Txt_chDOB"), TextBox)
                    Dim DOB As String = ""
                    DOB = txtchildDOB.Text.Trim

                    Dim gender As String = "F"

                    If ddl_CTitle.SelectedValue.Trim.ToLower = "mstr" Then
                        gender = "M"

                    End If

                    If (Not (Trip = "IB")) Then
                        ddl_CMealPrefer = DirectCast(rw.FindControl("ddl_CMealPrefer"), DropDownList)
                        ddl_CSeatPrefer = DirectCast(rw.FindControl("ddl_CSeatPrefer"), DropDownList)

                    Else
                        ddl_CMealPrefer = DirectCast(rw.FindControl("ddl_CMealPrefer_R"), DropDownList)
                        ddl_CSeatPrefer = DirectCast(rw.FindControl("ddl_CSeatPrefer_R"), DropDownList)
                    End If


                    objDA.insertPaxDetails(trackid, ddl_CTitle.SelectedValue, txtCFirstName.Text.Trim(), txtCMiddleName.Text.Trim(), txtCLastName.Text.Trim(), _
                     "CHD", DOB, "", "", ddl_CMealPrefer.SelectedValue, ddl_CSeatPrefer.SelectedValue, _
                     "false", "", gender, "", "", "", "")
                Next
            End If

            If Infant > 0 Then
                Dim counter1 As Integer = 0
                For Each rw As RepeaterItem In Repeater_Infant.Items

                    Dim ddl_ITitle As DropDownList = DirectCast(rw.FindControl("ddl_ITitle"), DropDownList)
                    Dim txtIFirstName As TextBox = DirectCast(rw.FindControl("txtIFirstName"), TextBox)
                    Dim txtIMiddleName As TextBox = DirectCast(rw.FindControl("txtIMiddleName"), TextBox)
                    If txtIMiddleName.Text = "Middle Name" Then
                        txtIMiddleName.Text = ""
                    End If
                    Dim txtILastName As TextBox = DirectCast(rw.FindControl("txtILastName"), TextBox)

                    Dim txtinfantDOB As TextBox = DirectCast(rw.FindControl("Txt_InfantDOB"), TextBox)
                    Dim DOB As String = ""
                    DOB = txtinfantDOB.Text.Trim
                    Dim gender As String = "F"
                    If ddl_ITitle.SelectedValue.Trim.ToLower = "mstr" Then
                        gender = "M"

                    End If
                    Dim txtAFirstName As TextBox = DirectCast(Repeater_Adult.Items(counter1).FindControl("txtAFirstName"), TextBox)
                    Dim txtAMiddleName As TextBox = DirectCast(Repeater_Adult.Items(counter1).FindControl("txtAMiddleName"), TextBox)
                    Dim txtALastName As TextBox = DirectCast(Repeater_Adult.Items(counter1).FindControl("txtALastName"), TextBox)

                    Dim Name As String = ""
                    Name = txtAFirstName.Text.Trim() + txtAMiddleName.Text.Trim() + txtALastName.Text.Trim()
                    If counter1 <= Infant Then
                        objDA.insertPaxDetails(trackid, ddl_ITitle.SelectedValue, txtIFirstName.Text.Trim(), txtIMiddleName.Text.Trim(), txtILastName.Text.Trim(), _
                         "INF", DOB, "", "", "", "", _
                         "false", Name, gender, "", "", "", "")
                    End If
                    counter1 += 1
                Next
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
    Protected Sub Repeater_Adult_ItemCreated(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim Flight As Char = "1" 'For RoundTrip SpecialCase'
        Adti = Adti + 1

        If (VCOB = "SG" Or VCOB = "6E") And InStr(VCOBSPL, "Special") = 0 And Provider.Trim.ToUpper <> "TB" And Provider.Trim.ToUpper <> "YA" Then
            Try
                DirectCast(e.Item.FindControl("tranchor1"), HtmlGenericControl).Style.Add("display", "none")
                DirectCast(e.Item.FindControl("A_ALL"), HtmlGenericControl).Style.Add("display", "none")
                Dim div_ADT As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_ADT"), HtmlControls.HtmlGenericControl)

                Dim ddl As DropDownList = TryCast(e.Item.FindControl("Ddl_A_EB_Ob"), DropDownList)
                Dim ds As DataSet = objDA.GetSSR_EB(TripOB, VCOB)
                If VCOB = "SG" And (SERCHVALO.Contains("P1") Or SERCHVALO.Contains("P2")) Then
                    ddl.Items.Add(New ListItem("Hand Bag Only 7kg--INR0", "HBAG" + Adti.ToString()))
                    ddl.Items.Add(New ListItem("First Check-in Baggage 15kg--INR750", "CBAG" + Adti.ToString()))
                Else
                    ddl.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                        ddl.Items.Add(New ListItem(ds.Tables(0).Rows(i)("DESCRIPTION").ToString() + "--INR" + ds.Tables(0).Rows(i)("PRICE").ToString(), ds.Tables(0).Rows(i)("SSR_CODE").ToString() + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                    Next
                End If

                'ddl.AutoPostBack = True
                ddl.DataBind()

                Dim ddl2 As DropDownList = TryCast(e.Item.FindControl("Ddl_A_Meal_Ob"), DropDownList)
                ds.Clear()
                ds = objDA.GetSSR_Meal(TripOB, VCOB, ATOB, ViewState("OBTrackId").ToString(), Flight)

                ddl2.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    ddl2.Items.Add(New ListItem(ds.Tables(0).Rows(i)("DESCRIPTION").ToString() + "--INR" + ds.Tables(0).Rows(i)("PRICE").ToString(), ds.Tables(0).Rows(i)("SSR_CODE").ToString() + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next
                ddl2.DataBind()

                div_ADT.Style("Display") = "block"

            Catch ex As Exception

            End Try


        ElseIf Provider.Trim.ToUpper = "TB" Then

            Try
                DirectCast(e.Item.FindControl("tranchor1"), HtmlGenericControl).Style.Add("display", "none")
                DirectCast(e.Item.FindControl("A_ALL"), HtmlGenericControl).Style.Add("display", "none")
                Dim div_ADT As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_ADT"), HtmlControls.HtmlGenericControl)

                Dim ddl As DropDownList = TryCast(e.Item.FindControl("Ddl_A_EB_Ob"), DropDownList)
                Dim ds As DataSet = objDA.GetSSR_EB(TripOB, VCOB)

                Dim objmB As New STD.BAL.TBO.SSR.TOBSSR()
                Dim baglist As List(Of STD.BAL.TBO.SSR.Baggage) = objmB.GetTBOBaggage(TBOSSR, "O")


                ddl.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                For i As Integer = 0 To baglist.Count - 1
                    ddl.Items.Add(New ListItem(baglist(i).Code.ToString() + "( " + baglist(i).Weight.ToString() + " KG)" + "--INR" + baglist(i).Price.ToString(), baglist(i).Code.ToString() + ":" + baglist(i).Description.ToString() + "_" + baglist(i).WayType.ToString() + "_" + baglist(i).Weight.ToString() + "_" + baglist(i).Destination.ToString() + "_" + baglist(i).Origin.ToString() + ":" + baglist(i).Price.ToString() + ":" + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next


                'ddl.AutoPostBack = True
                ddl.DataBind()

                Dim ddl2 As DropDownList = TryCast(e.Item.FindControl("Ddl_A_Meal_Ob"), DropDownList)
                ds.Clear()
                ' ds = objDA.GetSSR_Meal(TripOB, VCOB, ATOB, ViewState("OBTrackId").ToString(), Flight)


                Dim Meallist As List(Of STD.BAL.TBO.SSR.MealDynamic) = objmB.GetTBOMeals(TBOSSR, "O")

                ddl2.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To Meallist.Count - 1
                    ddl2.Items.Add(New ListItem(Meallist(i).AirlineDescription.ToString() + "--INR" + Meallist(i).Price.ToString(), Meallist(i).Code.ToString() + ":" + Meallist(i).Description.ToString() + "_" + Meallist(i).WayType.ToString() + "_" + Meallist(i).AirlineDescription.ToString() + "_" + Meallist(i).Destination.ToString() + "_" + Meallist(i).Origin.ToString() + ":" + Meallist(i).Price.ToString() + ":" + Adti.ToString()))
                Next
                ddl2.DataBind()

                div_ADT.Style("Display") = "block"

            Catch ex As Exception

            End Try


        ElseIf Provider.Trim.ToUpper = "YA" Then

            Try
                DirectCast(e.Item.FindControl("tranchor1"), HtmlGenericControl).Style.Add("display", "none")
                DirectCast(e.Item.FindControl("A_ALL"), HtmlGenericControl).Style.Add("display", "none")
                Dim div_ADT As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_ADT"), HtmlControls.HtmlGenericControl)

                Dim ddl As DropDownList = TryCast(e.Item.FindControl("Ddl_A_EB_Ob"), DropDownList)
                '' Dim ds As DataSet = objDA.GetSSR_EB(TripOB, VCOB)
                Dim objAirPrice As New STD.BAL.YAAirPrice()

                Dim baglist As List(Of STD.BAL.YASSR) = objAirPrice.GetYaSSrMealBagList(YASSRType.Baggage, YASSR)


                ddl.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                For i As Integer = 0 To baglist.Count - 1
                    ddl.Items.Add(New ListItem(baglist(i).Code.ToString() + "( " + baglist(i).Desc.ToString() + ")" + "--INR" + baglist(i).Amount.ToString(), baglist(i).Code.ToString() + ":" + baglist(i).Desc.ToString() + "_" + baglist(i).SSRType.ToString() + "_" + baglist(i).Desc.ToString() + ":" + baglist(i).Amount.ToString() + ":" + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next


                'ddl.AutoPostBack = True
                ddl.DataBind()

                Dim ddl2 As DropDownList = TryCast(e.Item.FindControl("Ddl_A_Meal_Ob"), DropDownList)
                ''ds.Clear()
                ' ds = objDA.GetSSR_Meal(TripOB, VCOB, ATOB, ViewState("OBTrackId").ToString(), Flight)


                Dim Meallist As List(Of STD.BAL.YASSR) = objAirPrice.GetYaSSrMealBagList(YASSRType.Meal, YASSR)

                ddl2.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To Meallist.Count - 1
                    ddl2.Items.Add(New ListItem(Meallist(i).Desc.ToString() + "--INR" + Meallist(i).Amount.ToString(), Meallist(i).Code.ToString() + ":" + Meallist(i).Desc.ToString() + "_" + Meallist(i).SSRType.ToString() + "_" + Meallist(i).Desc.ToString() + ":" + Meallist(i).Amount.ToString() + ":" + Adti.ToString()))
                Next
                ddl2.DataBind()

                div_ADT.Style("Display") = "block"

            Catch ex As Exception

            End Try




        Else
            DirectCast(e.Item.FindControl("tranchor1"), HtmlGenericControl).Style.Add("display", "block")
            DirectCast(e.Item.FindControl("A_ALL"), HtmlGenericControl).Style.Add("display", "block")
        End If
        If FLT_STAT = "RTF" And (VCOB = "SG" Or VCOB = "6E") Then
            VCIB = VCOB
            TripIB = TripOB
            Flight = "2"
        End If

        If (VCIB = "SG" Or VCIB = "6E") And InStr(VCIBSPL, "Special") = 0 And ProviderIB.Trim.ToUpper <> "TB" And ProviderIB.Trim <> "" And ProviderIB.Trim.ToUpper <> "YA" Then
            Try

                Dim ddl_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_A_EB_Ib"), DropDownList)
                Dim div_Ib As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_ADT_Ib"), HtmlControls.HtmlGenericControl)

                Dim ds_Ib As DataSet = objDA.GetSSR_EB(TripIB, VCIB)

                If VCIB = "SG" And (SERCHVALR.Contains("P1") Or SERCHVALR.Contains("P2")) Then
                    ddl_Ib.Items.Add(New ListItem("Hand Bag Only 7kg--INR0", "HBAG" + Adti.ToString()))
                    ddl_Ib.Items.Add(New ListItem("First Check-in Baggage 15kg--INR750", "CBAG" + Adti.ToString()))
                Else
                    ddl_Ib.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                    For i As Integer = 0 To ds_Ib.Tables(0).Rows.Count - 1
                        ddl_Ib.Items.Add(New ListItem(ds_Ib.Tables(0).Rows(i)("DESCRIPTION").ToString() + "--INR" + ds_Ib.Tables(0).Rows(i)("PRICE").ToString(), ds_Ib.Tables(0).Rows(i)("SSR_CODE").ToString() + Adti.ToString())) '+ "-" + ds_Ib.Tables(0).Rows(i)("PRICE").ToString()
                    Next
                End If

                ddl_Ib.DataBind()

                Dim ddl2_Ib As DropDownList = TryCast(e.Item.FindControl("ddl_A_Meal_Ib"), DropDownList)
                ds_Ib.Clear()
                If FLT_STAT = "RTF" Then
                    ds_Ib = objDA.GetSSR_Meal(TripIB, VCIB, ATIB, ViewState("OBTrackId").ToString(), Flight)
                Else
                    ds_Ib = objDA.GetSSR_Meal(TripIB, VCIB, ATIB, ViewState("IBTrackId").ToString(), Flight)
                End If


                ddl2_Ib.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To ds_Ib.Tables(0).Rows.Count - 1
                    ddl2_Ib.Items.Add(New ListItem(ds_Ib.Tables(0).Rows(i)("DESCRIPTION").ToString() + "--INR" + ds_Ib.Tables(0).Rows(i)("PRICE").ToString(), ds_Ib.Tables(0).Rows(i)("SSR_CODE").ToString() + Adti.ToString())) '+ "-" + ds_Ib.Tables(0).Rows(i)("PRICE").ToString()
                Next
                ddl2_Ib.DataBind()

                div_Ib.Style("Display") = "block"
            Catch ex As Exception

            End Try

        ElseIf ProviderIB.Trim.ToUpper = "TB" Or (FLT_STAT = "RTF" And Provider.Trim.ToUpper = "TB") Then

            Try

                Dim ddl_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_A_EB_Ib"), DropDownList)
                Dim div_Ib As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_ADT_Ib"), HtmlControls.HtmlGenericControl)

                ''Dim ds_Ib As DataSet = objDA.GetSSR_EB(TripIB, VCIB)
                Dim objmB As New STD.BAL.TBO.SSR.TOBSSR()
                Dim baglist As List(Of STD.BAL.TBO.SSR.Baggage)
                If FLT_STAT = "RTF" Then
                    baglist = objmB.GetTBOBaggage(TBOSSR, "R")
                Else
                    baglist = objmB.GetTBOBaggage(TBOSSRIB, "O")
                End If
                'baglist = objmB.GetTBOBaggage(TBOSSRIB, "O")
                ddl_Ib.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                For i As Integer = 0 To baglist.Count - 1
                    ddl_Ib.Items.Add(New ListItem(baglist(i).Code.ToString() + "( " + baglist(i).Weight.ToString() + " KG)" + "--INR" + baglist(i).Price.ToString(), baglist(i).Code.ToString() + ":" + baglist(i).Description.ToString() + "_" + baglist(i).WayType.ToString() + "_" + baglist(i).Weight.ToString() + "_" + baglist(i).Destination.ToString() + "_" + baglist(i).Origin.ToString() + ":" + baglist(i).Price.ToString() + ":" + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next

                ddl_Ib.DataBind()

                Dim ddl2_Ib As DropDownList = TryCast(e.Item.FindControl("ddl_A_Meal_Ib"), DropDownList)
                ''ds_Ib.Clear()
                Dim Meallist As List(Of STD.BAL.TBO.SSR.MealDynamic)
                If FLT_STAT = "RTF" Then
                    Meallist = objmB.GetTBOMeals(TBOSSR, "R")
                Else
                    Meallist = objmB.GetTBOMeals(TBOSSRIB, "O")
                End If

                'Meallist = objmB.GetTBOMeals(TBOSSRIB, "O")

                ddl2_Ib.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To Meallist.Count - 1
                    ddl2_Ib.Items.Add(New ListItem(Meallist(i).AirlineDescription.ToString() + "--INR" + Meallist(i).Price.ToString(), Meallist(i).Code.ToString() + ":" + Meallist(i).Description.ToString() + "_" + Meallist(i).WayType.ToString() + "_" + Meallist(i).AirlineDescription.ToString() + "_" + Meallist(i).Destination.ToString() + "_" + Meallist(i).Origin.ToString() + ":" + Meallist(i).Price.ToString() + ":" + Adti.ToString()))
                Next
                ddl2_Ib.DataBind()

                div_Ib.Style("Display") = "block"
            Catch ex As Exception

            End Try


        ElseIf ProviderIB.Trim.ToUpper = "YA" Or (FLT_STAT = "RTF" And Provider.Trim.ToUpper = "YA") Then

            Try

                Dim ddl_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_A_EB_Ib"), DropDownList)
                Dim div_Ib As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_ADT_Ib"), HtmlControls.HtmlGenericControl)

                ''Dim ds_Ib As DataSet = objDA.GetSSR_EB(TripIB, VCIB)

                Dim objAirPrice As New STD.BAL.YAAirPrice()

                Dim baglist As List(Of STD.BAL.YASSR) = objAirPrice.GetYaSSrMealBagList(YASSRType.Baggage, YASSRIB)

                'baglist = objmB.GetTBOBaggage(TBOSSRIB, "O")
                ddl_Ib.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                For i As Integer = 0 To baglist.Count - 1
                    ddl_Ib.Items.Add(New ListItem(baglist(i).Code.ToString() + "( " + baglist(i).Desc.ToString() + ")" + "--INR" + baglist(i).Amount.ToString(), baglist(i).Code.ToString() + ":" + baglist(i).Desc.ToString() + "_" + baglist(i).SSRType.ToString() + "_" + baglist(i).Desc.ToString() + ":" + baglist(i).Amount.ToString() + ":" + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next

                ddl_Ib.DataBind()

                Dim ddl2_Ib As DropDownList = TryCast(e.Item.FindControl("ddl_A_Meal_Ib"), DropDownList)
                ''ds_Ib.Clear()
                Dim Meallist As List(Of STD.BAL.YASSR)

                Meallist = objAirPrice.GetYaSSrMealBagList(YASSRType.Meal, YASSRIB)

                'Meallist = objmB.GetTBOMeals(TBOSSRIB, "O")

                ddl2_Ib.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To Meallist.Count - 1
                    ddl2_Ib.Items.Add(New ListItem(Meallist(i).Desc.ToString() + "--INR" + Meallist(i).Amount.ToString(), Meallist(i).Code.ToString() + ":" + Meallist(i).Desc.ToString() + "_" + Meallist(i).SSRType.ToString() + "_" + Meallist(i).Desc.ToString() + ":" + Meallist(i).Amount.ToString() + ":" + Adti.ToString()))
                Next
                ddl2_Ib.DataBind()

                div_Ib.Style("Display") = "block"
            Catch ex As Exception

            End Try




        ElseIf (VCIB = "") And InStr(VCIBSPL, "Special") = 0 Then

        Else
            DirectCast(e.Item.FindControl("tranchor1_R"), HtmlGenericControl).Style.Add("display", "block")
            DirectCast(e.Item.FindControl("A_ALL_R"), HtmlGenericControl).Style.Add("display", "block")
        End If
    End Sub
    Protected Sub Repeater_Child_ItemCreated(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim Flight As Char = "1" 'For RoundTrip SpecialCase'
        Chdi = Chdi + 1
        If (VCOB = "SG" Or VCOB = "6E") And InStr(VCOBSPL, "Special") = 0 And Provider.Trim.ToUpper <> "TB" And Provider.Trim.ToUpper <> "YA" Then
            Try
                DirectCast(e.Item.FindControl("tranchor2"), HtmlGenericControl).Style.Add("display", "none")
                DirectCast(e.Item.FindControl("C_ALL"), HtmlGenericControl).Style.Add("display", "none")
                Dim div_CHD As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_CHILD"), HtmlControls.HtmlGenericControl)
                Dim ddl As DropDownList = TryCast(e.Item.FindControl("Ddl_C_EB_Ob"), DropDownList)
                Dim ds As DataSet = objDA.GetSSR_EB(TripOB, VCOB)

                If VCOB = "SG" And (SERCHVALO.Contains("P1") Or SERCHVALO.Contains("P2")) Then
                    ddl.Items.Add(New ListItem("Hand Bag Only 7kg--INR0", "HBAG" + Chdi.ToString()))
                    ddl.Items.Add(New ListItem("First Check-in Baggage 15kg--INR750", "CBAG" + Chdi.ToString()))
                Else
                    ddl.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                        ddl.Items.Add(New ListItem(ds.Tables(0).Rows(i)("DESCRIPTION").ToString() + "--INR" + ds.Tables(0).Rows(i)("PRICE").ToString(), ds.Tables(0).Rows(i)("SSR_CODE").ToString() + Chdi.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                    Next
                End If

                ds.Clear()
                ds = objDA.GetSSR_Meal(TripOB, VCOB, ATOB, ViewState("OBTrackId").ToString(), Flight)

                Dim ddl2 As DropDownList = TryCast(e.Item.FindControl("Ddl_C_Meal_Ob"), DropDownList)
                ddl2.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    ddl2.Items.Add(New ListItem(ds.Tables(0).Rows(i)("DESCRIPTION").ToString() + "--INR" + ds.Tables(0).Rows(i)("PRICE").ToString(), ds.Tables(0).Rows(i)("SSR_CODE").ToString() + Chdi.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next
                div_CHD.Style("Display") = "block"


            Catch ex As Exception

            End Try


        ElseIf Provider.Trim.ToUpper = "TB" Then
            Try
                DirectCast(e.Item.FindControl("tranchor2"), HtmlGenericControl).Style.Add("display", "none")
                DirectCast(e.Item.FindControl("C_ALL"), HtmlGenericControl).Style.Add("display", "none")
                Dim div_CHD As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_CHILD"), HtmlControls.HtmlGenericControl)
                Dim ddl As DropDownList = TryCast(e.Item.FindControl("Ddl_C_EB_Ob"), DropDownList)
                ''Dim ds As DataSet = objDA.GetSSR_EB(TripOB, VCOB)

                Dim objmB As New STD.BAL.TBO.SSR.TOBSSR()
                Dim baglist As List(Of STD.BAL.TBO.SSR.Baggage) = objmB.GetTBOBaggage(TBOSSR, "O")


                ddl.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                For i As Integer = 0 To baglist.Count - 1
                    ddl.Items.Add(New ListItem(baglist(i).Code.ToString() + "( " + baglist(i).Weight.ToString() + " KG)" + "--INR" + baglist(i).Price.ToString(), baglist(i).Code.ToString() + ":" + baglist(i).Description.ToString() + "_" + baglist(i).WayType.ToString() + "_" + baglist(i).Weight.ToString() + "_" + baglist(i).Destination.ToString() + "_" + baglist(i).Origin.ToString() + ":" + baglist(i).Price.ToString() + ":" + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next




                'ds.Clear()
                'ds = objDA.GetSSR_Meal(TripOB, VCOB, ATOB, ViewState("OBTrackId").ToString(), Flight)
                Dim ddl2 As DropDownList = TryCast(e.Item.FindControl("Ddl_C_Meal_Ob"), DropDownList)
                Dim Meallist As List(Of STD.BAL.TBO.SSR.MealDynamic) = objmB.GetTBOMeals(TBOSSR, "O")

                ddl2.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To Meallist.Count - 1
                    ddl2.Items.Add(New ListItem(Meallist(i).AirlineDescription.ToString() + "--INR" + Meallist(i).Price.ToString(), Meallist(i).Code.ToString() + ":" + Meallist(i).Description.ToString() + "_" + Meallist(i).WayType.ToString() + "_" + Meallist(i).AirlineDescription.ToString() + "_" + Meallist(i).Destination.ToString() + "_" + Meallist(i).Origin.ToString() + ":" + Meallist(i).Price.ToString() + ":" + Adti.ToString()))
                Next



                div_CHD.Style("Display") = "block"


            Catch ex As Exception

            End Try


        ElseIf Provider.Trim.ToUpper = "YA" Then
            Try
                DirectCast(e.Item.FindControl("tranchor2"), HtmlGenericControl).Style.Add("display", "none")
                DirectCast(e.Item.FindControl("C_ALL"), HtmlGenericControl).Style.Add("display", "none")
                Dim div_CHD As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_CHILD"), HtmlControls.HtmlGenericControl)
                Dim ddl As DropDownList = TryCast(e.Item.FindControl("Ddl_C_EB_Ob"), DropDownList)
                ''Dim ds As DataSet = objDA.GetSSR_EB(TripOB, VCOB)


                Dim objAirPrice As New STD.BAL.YAAirPrice()

                Dim baglist As List(Of STD.BAL.YASSR) = objAirPrice.GetYaSSrMealBagList(YASSRType.Baggage, YASSR)




                ddl.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                For i As Integer = 0 To baglist.Count - 1
                    ddl.Items.Add(New ListItem(baglist(i).Code.ToString() + "( " + baglist(i).Desc.ToString() + " )" + "--INR" + baglist(i).Amount.ToString(), baglist(i).Code.ToString() + ":" + baglist(i).Desc.ToString() + "_" + baglist(i).SSRType.ToString() + "_" + baglist(i).Desc.ToString() + "_" + ":" + baglist(i).Amount.ToString() + ":" + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next




                'ds.Clear()
                'ds = objDA.GetSSR_Meal(TripOB, VCOB, ATOB, ViewState("OBTrackId").ToString(), Flight)
                Dim ddl2 As DropDownList = TryCast(e.Item.FindControl("Ddl_C_Meal_Ob"), DropDownList)
                Dim Meallist As List(Of STD.BAL.YASSR) = objAirPrice.GetYaSSrMealBagList(YASSRType.Meal, YASSR)

                ddl2.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To Meallist.Count - 1
                    ddl2.Items.Add(New ListItem(Meallist(i).Desc.ToString() + "--INR" + Meallist(i).Amount.ToString(), Meallist(i).Code.ToString() + ":" + Meallist(i).Desc.ToString() + "_" + Meallist(i).SSRType.ToString() + "_" + Meallist(i).Desc.ToString() + ":" + Meallist(i).Amount.ToString() + ":" + Adti.ToString()))
                Next



                div_CHD.Style("Display") = "block"


            Catch ex As Exception

            End Try




        Else
            DirectCast(e.Item.FindControl("tranchor2"), HtmlGenericControl).Style.Add("display", "block")
            DirectCast(e.Item.FindControl("C_ALL"), HtmlGenericControl).Style.Add("display", "block")
        End If
        If FLT_STAT = "RTF" And (VCOB = "SG" Or VCOB = "6E") Then
            VCIB = VCOB
            TripIB = TripOB
            Flight = "2"
        End If


        If (VCIB = "SG" Or VCIB = "6E") And InStr(VCIBSPL, "Special") = 0 And ProviderIB.Trim.ToUpper <> "TB" And ProviderIB.Trim <> "" And ProviderIB.Trim.ToUpper <> "YA" Then
            Try
                Dim ddl_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_C_EB_Ib"), DropDownList)
                Dim div_Ib As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_CHILD_Ib"), HtmlControls.HtmlGenericControl)

                Dim ds_Ib As DataSet = objDA.GetSSR_EB(TripIB, VCIB)

                If VCIB = "SG" And (SERCHVALR.Contains("P1") Or SERCHVALR.Contains("P2")) Then
                    ddl_Ib.Items.Add(New ListItem("Hand Bag Only 7kg--INR0", "HBAG" + Chdi.ToString()))
                    ddl_Ib.Items.Add(New ListItem("First Check-in Baggage 15kg--INR750", "CBAG" + Chdi.ToString()))
                Else
                    ddl_Ib.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                    For i As Integer = 0 To ds_Ib.Tables(0).Rows.Count - 1
                        ddl_Ib.Items.Add(New ListItem(ds_Ib.Tables(0).Rows(i)("DESCRIPTION").ToString() + "--INR" + ds_Ib.Tables(0).Rows(i)("PRICE").ToString(), ds_Ib.Tables(0).Rows(i)("SSR_CODE").ToString() + Chdi.ToString())) '+ "-" + ds_Ib.Tables(0).Rows(i)("PRICE").ToString()
                    Next
                End If
                ds_Ib.Clear()
                If FLT_STAT = "RTF" Then
                    ds_Ib = objDA.GetSSR_Meal(TripIB, VCIB, ATIB, ViewState("OBTrackId").ToString(), Flight)
                Else
                    ds_Ib = objDA.GetSSR_Meal(TripIB, VCIB, ATIB, ViewState("IBTrackId").ToString(), Flight)
                End If


                Dim ddl2_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_C_Meal_Ib"), DropDownList)
                ddl2_Ib.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To ds_Ib.Tables(0).Rows.Count - 1
                    ddl2_Ib.Items.Add(New ListItem(ds_Ib.Tables(0).Rows(i)("DESCRIPTION").ToString() + "--INR" + ds_Ib.Tables(0).Rows(i)("PRICE").ToString(), ds_Ib.Tables(0).Rows(i)("SSR_CODE").ToString() + Chdi.ToString())) '+ "-" + ds_Ib.Tables(0).Rows(i)("PRICE").ToString()
                Next

                div_Ib.Style("Display") = "block"
            Catch ex As Exception

            End Try
        ElseIf ProviderIB.Trim.ToUpper = "TB" Or (FLT_STAT = "RTF" And Provider.Trim.ToUpper = "TB") Then

            Try
                Dim ddl_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_C_EB_Ib"), DropDownList)
                Dim div_Ib As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_CHILD_Ib"), HtmlControls.HtmlGenericControl)

                ''Dim ds_Ib As DataSet = objDA.GetSSR_EB(TripIB, VCIB)
                Dim objmB As New STD.BAL.TBO.SSR.TOBSSR()
                Dim baglist As List(Of STD.BAL.TBO.SSR.Baggage)
                If FLT_STAT = "RTF" Then
                    baglist = objmB.GetTBOBaggage(TBOSSR, "R")
                Else
                    baglist = objmB.GetTBOBaggage(TBOSSRIB, "O")
                End If
                'baglist = objmB.GetTBOBaggage(TBOSSRIB, "O")
                ddl_Ib.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                For i As Integer = 0 To baglist.Count - 1
                    ddl_Ib.Items.Add(New ListItem(baglist(i).Code.ToString() + "( " + baglist(i).Weight.ToString() + " KG)" + "--INR" + baglist(i).Price.ToString(), baglist(i).Code.ToString() + ":" + baglist(i).Description.ToString() + "_" + baglist(i).WayType.ToString() + "_" + baglist(i).Weight.ToString() + "_" + baglist(i).Destination.ToString() + "_" + baglist(i).Origin.ToString() + ":" + baglist(i).Price.ToString() + ":" + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next



                Dim ddl2_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_C_Meal_Ib"), DropDownList)
                ''ds_Ib.Clear()
                Dim Meallist As List(Of STD.BAL.TBO.SSR.MealDynamic)
                If FLT_STAT = "RTF" Then
                    Meallist = objmB.GetTBOMeals(TBOSSR, "R")
                Else
                    Meallist = objmB.GetTBOMeals(TBOSSRIB, "O")
                End If

                'Meallist = objmB.GetTBOMeals(TBOSSRIB, "O")

                ddl2_Ib.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To Meallist.Count - 1
                    ddl2_Ib.Items.Add(New ListItem(Meallist(i).AirlineDescription.ToString() + "--INR" + Meallist(i).Price.ToString(), Meallist(i).Code.ToString() + ":" + Meallist(i).Description.ToString() + "_" + Meallist(i).WayType.ToString() + "_" + Meallist(i).AirlineDescription.ToString() + "_" + Meallist(i).Destination.ToString() + "_" + Meallist(i).Origin.ToString() + ":" + Meallist(i).Price.ToString() + ":" + Adti.ToString()))
                Next
                ddl2_Ib.DataBind()


                div_Ib.Style("Display") = "block"
            Catch ex As Exception

            End Try

        ElseIf ProviderIB.Trim.ToUpper = "YA" Or (FLT_STAT = "RTF" And Provider.Trim.ToUpper = "YA") Then

            Try
                Dim ddl_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_C_EB_Ib"), DropDownList)
                Dim div_Ib As System.Web.UI.HtmlControls.HtmlGenericControl = TryCast(e.Item.FindControl("div_CHILD_Ib"), HtmlControls.HtmlGenericControl)

                ''Dim ds_Ib As DataSet = objDA.GetSSR_EB(TripIB, VCIB)
                Dim objAirPrice As New STD.BAL.YAAirPrice()

                Dim baglist As List(Of STD.BAL.YASSR) = objAirPrice.GetYaSSrMealBagList(YASSRType.Baggage, YASSRIB)
                'baglist = objmB.GetTBOBaggage(TBOSSRIB, "O")
                ddl_Ib.Items.Add(New ListItem("---Select Excess Bagage Options---", "select"))
                For i As Integer = 0 To baglist.Count - 1
                    ddl_Ib.Items.Add(New ListItem(baglist(i).Code.ToString() + "( " + baglist(i).Desc.ToString() + ")" + "--INR" + baglist(i).Amount.ToString(), baglist(i).Code.ToString() + ":" + baglist(i).Desc.ToString() + "_" + baglist(i).SSRType.ToString() + "_" + baglist(i).Desc.ToString() + ":" + baglist(i).Amount.ToString() + ":" + Adti.ToString())) '+ "-" + ds.Tables(0).Rows(i)("PRICE").ToString()
                Next



                Dim ddl2_Ib As DropDownList = TryCast(e.Item.FindControl("Ddl_C_Meal_Ib"), DropDownList)
                ''ds_Ib.Clear()
                Dim Meallist As List(Of STD.BAL.YASSR) = objAirPrice.GetYaSSrMealBagList(YASSRType.Meal, YASSRIB)


                'Meallist = objmB.GetTBOMeals(TBOSSRIB, "O")

                ddl2_Ib.Items.Add(New ListItem("---Select Meal Options---", "select"))
                For i As Integer = 0 To Meallist.Count - 1
                    ddl2_Ib.Items.Add(New ListItem(Meallist(i).Desc.ToString() + "--INR" + Meallist(i).Amount.ToString(), Meallist(i).Code.ToString() + ":" + Meallist(i).Desc.ToString() + "_" + Meallist(i).SSRType.ToString() + "_" + Meallist(i).Desc.ToString() + ":" + Meallist(i).Amount.ToString() + ":" + Adti.ToString()))
                Next
                ddl2_Ib.DataBind()


                div_Ib.Style("Display") = "block"
            Catch ex As Exception

            End Try



        ElseIf (VCIB = "") And InStr(VCIBSPL, "Special") = 0 Then

        Else
            DirectCast(e.Item.FindControl("tranchor2_R"), HtmlGenericControl).Style.Add("display", "block")
            DirectCast(e.Item.FindControl("C_ALL_R"), HtmlGenericControl).Style.Add("display", "block")
        End If
    End Sub
    Private Sub Insert_MEAL_BAG_Detail(ByVal trackid As String, ByVal FltDs As DataSet, ByVal Paxdt As DataSet, ByVal Type As String, ByVal TripType As String)
        Try
            Adult = Convert.ToInt16(FltDs.Tables(0).Rows(0)("Adult"))
            Child = Convert.ToInt16(FltDs.Tables(0).Rows(0)("Child"))
            Infant = Convert.ToInt16(FltDs.Tables(0).Rows(0)("Infant"))

            'NO Need for this only assign Code in the DDL option value
            Dim MealCd_OB As String = "", BagCd_OB As String = "", MealPr_OB As Decimal = 0, BagPr_OB As Decimal = 0
            Dim MealCd_IB As String = "", BagCd_IB As String = "", MealPr_IB As Decimal = 0, BagPr_IB As Decimal = 0
            Dim counter As Integer = 0
            Dim Dt As New DataTable
            getTableColumn(Dt)

            If (Type = "OB") Then

                Split_MB_Detail(lbl_A_MB_OB.Value, Adult, Dt, "ADT", FltDs.Tables(0).Rows(0)("Provider").ToString())
                If (Child > 0) Then
                    Split_MB_Detail(lbl_C_MB_OB.Value, Child, Dt, "CHD", FltDs.Tables(0).Rows(0)("Provider").ToString())
                End If
                CreateFinalTable(Dt, Adult, Child, Paxdt.Tables(0), trackid, TripType, FltDs.Tables(0).Rows(0)("ValiDatingCarrier"), FltDs.Tables(0).Rows(0)("Provider").ToString())
                ''Code To insert in T_Flt_Meal_And_Baggage_Request
            ElseIf (Type = "IB") Then

                Split_MB_Detail(lbl_A_MB_IB.Value, Adult, Dt, "ADT", FltDs.Tables(0).Rows(0)("Provider").ToString())
                If (Child > 0) Then
                    Split_MB_Detail(lbl_C_MB_IB.Value, Child, Dt, "CHD", FltDs.Tables(0).Rows(0)("Provider").ToString())
                End If
                CreateFinalTable(Dt, Adult, Child, Paxdt.Tables(0), trackid, TripType, FltDs.Tables(0).Rows(0)("ValiDatingCarrier"), FltDs.Tables(0).Rows(0)("Provider").ToString())
                ''Code To insert in T_Flt_Meal_And_Baggage_Request
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
    Public Sub Split_MB_Detail(ByVal Text As String, ByVal Pxcnt As Integer, ByRef Dt As DataTable, ByVal PaxType As String, ByVal Provider As String)

        Dim MB() As String
        Try
            MB = Text.Split("#")
            Dim tax() As String
            tax = MB(0).Split("}".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
            For i As Integer = 0 To tax.Length - 1
                Dim dr As DataRow = Dt.NewRow()
                If Provider.Trim() = "TB" Or Provider.Trim() = "YA" Then

                    dr("PaxType") = PaxType
                    dr("PaxID") = tax(i).Split("@")(0).Split(":")(3)
                    dr("MealCode") = tax(i).Split("@")(0).Split(":")(0)
                    dr("MealPrice") = Convert.ToDecimal(tax(i).Split("@")(1))
                    dr("MealDesc") = tax(i).Split("@")(0).Split(":")(1)
                    '' dr("MealCategory") = tax(i).Split("@")(0).Split(":")(1)
                    ''dr("MealPriceWithNoTax") = tax(i).Split("@")(0).Split(":")(2)
                    ''VGML:2_2_VEG MEAL_BOM_DEL:250:1@250}#XBPB:2_2_10_BOM_DEL:1750:1@1750}
                Else

                    dr("PaxType") = PaxType
                    dr("PaxID") = tax(i).Split("@")(0).Substring(4, 1)
                    dr("MealCode") = tax(i).Split("@")(0).Substring(0, 4)
                    dr("MealPrice") = Convert.ToDecimal(tax(i).Split("@")(1))

                End If

                Dt.Rows.Add(dr)
            Next
            ''Baggage
            Array.Clear(tax, 0, tax.Length)
            tax = MB(1).Split("}".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
            For i As Integer = 0 To tax.Length - 1
                Dim dr As DataRow = Dt.NewRow()

                If Provider.Trim() = "TB" Or Provider.Trim() = "YA" Then
                    dr("PaxType") = PaxType
                    dr("PaxID") = tax(i).Split("@")(0).Split(":")(3)
                    dr("BaggageCode") = tax(i).Split("@")(0).Split(":")(0)
                    dr("BaggagePrice") = Convert.ToDecimal(tax(i).Split("@")(1))
                    dr("BaggageDesc") = tax(i).Split("@")(0).Split(":")(1)
                    dr("BaggageCategory") = tax(i).Split("@")(0).Split(":")(1)
                    dr("BaggagePriceWithNoTax") = tax(i).Split("@")(0).Split(":")(2)

                Else

                    dr("PaxType") = PaxType
                    dr("PaxID") = tax(i).Split("@")(0).Substring(4, 1)
                    dr("BaggageCode") = tax(i).Split("@")(0).Substring(0, 4)
                    dr("BaggagePrice") = Convert.ToDecimal(tax(i).Split("@")(1))

                End If

                Dt.Rows.Add(dr)
            Next

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try


    End Sub
    Public Function getTableColumn(ByRef dt As DataTable) As DataTable

        Try
            'dt.Columns.Add("BookingRefNo", GetType(String))
            'dt.Columns.Add("Flt_HeaderID", GetType(String))
            'dt.Columns.Add("PaxID", GetType(Integer))
            'dt.Columns.Add("PaxType", GetType(String)) 'Extra
            'dt.Columns.Add("TripType", GetType(String))
            'dt.Columns.Add("MealCode", GetType(String))
            'dt.Columns.Add("MealPrice", GetType(Decimal))
            'dt.Columns.Add("BaggageCode", GetType(String))
            'dt.Columns.Add("BaggagePrice", GetType(Decimal))
            'dt.Columns.Add("AirLineCode", GetType(String))


            dt.Columns.Add("BookingRefNo", GetType(String))
            dt.Columns.Add("Flt_HeaderID", GetType(String))
            dt.Columns.Add("PaxID", GetType(Integer))
            dt.Columns.Add("PaxType", GetType(String)) 'Extra
            dt.Columns.Add("TripType", GetType(String))
            dt.Columns.Add("MealCode", GetType(String))
            dt.Columns.Add("MealPrice", GetType(Decimal))
            dt.Columns.Add("MealDesc", GetType(String))
            dt.Columns.Add("BaggageCode", GetType(String))
            dt.Columns.Add("BaggagePrice", GetType(Decimal))
            dt.Columns.Add("AirLineCode", GetType(String))
            dt.Columns.Add("BaggageDesc", GetType(String))
            dt.Columns.Add("BaggageCategory", GetType(String))
            dt.Columns.Add("BaggagePriceWithNoTax", GetType(Decimal))

            Return dt
        Catch ex As Exception
            Return dt
        End Try
    End Function
    Public Function CreateFinalTable(ByVal Dt As DataTable, ByVal Adult As Integer, ByVal Child As Integer, ByVal Paxdt As DataTable, ByVal OID As String, ByVal Trip As String, ByVal VC As String, ByVal Prvdr As String) As DataTable
        Dim Ft As New DataTable
        getTableColumn(Ft)
        Try

            For i As Integer = 1 To Adult

                Dim fltml As DataRow() = Dt.Select("PaxType ='ADT' AND PaxID =" + i.ToString() + "AND MealPrice >0")
                Dim fltbg As DataRow() = Dt.Select("PaxType ='ADT' AND PaxID =" + i.ToString() + "AND BaggagePrice >0") 'AND BaggagePrice >0
                Dim dr As DataRow = Ft.NewRow()
                dr("PaxID") = Paxdt.Rows(i - 1)("PaxID")
                dr("Flt_HeaderID") = Paxdt.Rows(i - 1)("Flt_HeaderID")
                If (fltml.Length > 0) Then
                    dr("MealCode") = fltml(0)("MealCode")
                    dr("MealPrice") = Convert.ToDecimal(fltml(0)("MealPrice"))
                    dr("MealDesc") = Convert.ToString(fltml(0)("MealDesc"))
                Else
                    dr("MealCode") = ""
                    dr("MealPrice") = 0
                    dr("MealDesc") = ""
                End If
                If (fltbg.Length > 0) Then
                    dr("BaggageCode") = fltbg(0)("BaggageCode")
                    dr("BaggagePrice") = Convert.ToDecimal(fltbg(0)("BaggagePrice"))
                    dr("BaggageDesc") = fltbg(0)("BaggageDesc")
                    dr("BaggageCategory") = fltbg(0)("BaggageCategory")
                    dr("BaggagePriceWithNoTax") = fltbg(0)("BaggagePriceWithNoTax")

                Else
                    dr("BaggageCode") = ""
                    dr("BaggagePrice") = 0
                    dr("BaggageDesc") = ""
                    dr("BaggageCategory") = ""
                    dr("BaggagePriceWithNoTax") = 0
                End If
                dr("TripType") = Trip
                Ft.Rows.Add(dr)
                'objDA.insert_MEAL_BAGDetails(OID, dr("Flt_HeaderID"), Convert.ToInt32(dr("PaxID")), dr("TripType"), dr("MealCode"), Convert.ToDouble(dr("MealPrice")), dr("BaggageCode"), dr("BaggagePrice"), VC)
                Dim ret As Integer = objDA.insert_MEAL_BAGDetails(OID, dr("Flt_HeaderID"), Convert.ToInt32(dr("PaxID")), dr("TripType"), dr("MealCode"), Convert.ToDouble(dr("MealPrice")), dr("BaggageCode"), dr("BaggagePrice"), VC, dr("BaggageDesc"), dr("BaggageCategory"), dr("BaggagePriceWithNoTax"), dr("MealDesc"), Prvdr)
LAB:
                If (ret > 0) Then
                Else
                    ret = objDA.insert_MEAL_BAGDetails(OID, dr("Flt_HeaderID"), Convert.ToInt32(dr("PaxID")), dr("TripType"), dr("MealCode"), Convert.ToDouble(dr("MealPrice")), dr("BaggageCode"), dr("BaggagePrice"), VC, dr("BaggageDesc"), dr("BaggageCategory"), dr("BaggagePriceWithNoTax"), dr("MealDesc"), Prvdr)
                    GoTo LAB
                End If
            Next
            If (Child > 0) Then
                For i As Integer = 1 To Child
                    Dim fltml As DataRow() = Dt.Select("PaxType ='CHD' AND PaxID =" + i.ToString() + "AND MealPrice >0")
                    Dim fltbg As DataRow() = Dt.Select("PaxType ='CHD' AND PaxID =" + i.ToString() + "AND BaggagePrice >0") 'AND BaggagePrice >0
                    Dim dr As DataRow = Ft.NewRow()
                    dr("PaxID") = Paxdt.Rows((Adult + i) - 1)("PaxID")
                    dr("Flt_HeaderID") = Paxdt.Rows((Adult + i) - 1)("Flt_HeaderID")
                    If (fltml.Length > 0) Then
                        dr("MealCode") = fltml(0)("MealCode")
                        dr("MealPrice") = Convert.ToDecimal(fltml(0)("MealPrice"))
                        dr("MealDesc") = Convert.ToString(fltml(0)("MealDesc"))
                    Else
                        dr("MealCode") = ""
                        dr("MealPrice") = 0
                        dr("MealDesc") = ""
                    End If
                    If (fltbg.Length > 0) Then
                        dr("BaggageCode") = fltbg(0)("BaggageCode")
                        dr("BaggagePrice") = Convert.ToDecimal(fltbg(0)("BaggagePrice"))
                        dr("BaggageDesc") = fltbg(0)("BaggageDesc")
                        dr("BaggageCategory") = fltbg(0)("BaggageCategory")
                        dr("BaggagePriceWithNoTax") = fltbg(0)("BaggagePriceWithNoTax")

                    Else
                        dr("BaggageCode") = ""
                        dr("BaggagePrice") = 0
                        dr("BaggageDesc") = ""
                        dr("BaggageCategory") = ""
                        dr("BaggagePriceWithNoTax") = 0
                    End If
                    dr("TripType") = Trip
                    Ft.Rows.Add(dr)
                    'objDA.insert_MEAL_BAGDetails(OID, dr("Flt_HeaderID"), Convert.ToInt32(dr("PaxID")), dr("TripType"), dr("MealCode"), Convert.ToDouble(dr("MealPrice")), dr("BaggageCode"), dr("BaggagePrice"), VC)
                    Dim ret As Integer = objDA.insert_MEAL_BAGDetails(OID, dr("Flt_HeaderID"), Convert.ToInt32(dr("PaxID")), dr("TripType"), dr("MealCode"), Convert.ToDouble(dr("MealPrice")), dr("BaggageCode"), dr("BaggagePrice"), VC, dr("BaggageDesc"), dr("BaggageCategory"), dr("BaggagePriceWithNoTax"), dr("MealDesc"), Prvdr)
LAB2:
                    If (ret > 0) Then
                    Else
                        ret = objDA.insert_MEAL_BAGDetails(OID, dr("Flt_HeaderID"), Convert.ToInt32(dr("PaxID")), dr("TripType"), dr("MealCode"), Convert.ToDouble(dr("MealPrice")), dr("BaggageCode"), dr("BaggagePrice"), VC, dr("BaggageDesc"), dr("BaggageCategory"), dr("BaggagePriceWithNoTax"), dr("MealDesc"), Prvdr)
                        GoTo LAB2
                    End If
                Next

            End If
        Catch ex As Exception

        End Try

        Return Ft
    End Function
    'Private Function SELL_SSR(ByVal TrackId As String) As String
    '    Dim SSRPRICE As String = "", SJKAMT As String = ""
    '    Dim Signature As String = ""
    '    Dim Trip As Integer = 1
    '    Dim Diff As Decimal = 0
    '    Try

    '        Dim FltDs As DataSet = objDA.GetFltDtls(TrackId, Session("UID"))
    '        Dim PaxDs As DataSet = objDA.GetPaxDetails(TrackId)
    '        Dim VC As String = FltDs.Tables(0).Rows(0)("ValiDatingCarrier").ToString.Trim()
    '        Dim OriginalTF As Decimal = Convert.ToDecimal(FltDs.Tables(0).Rows(0)("OriginalTF").ToString())
    '        Dim MBDT As DataSet = objSql.Get_MEAL_BAG_FareDetails(FltDs.Tables(0).Rows(0)("Track_id").ToString(), "")
    '        Dim MBPR As Decimal = 0, MealPr As Decimal = 0, BgPr As Decimal = 0
    '        If (MBDT.Tables(0).Rows.Count > 0) Then
    '            For jj As Integer = 0 To MBDT.Tables(0).Rows.Count - 1
    '                MealPr = MealPr + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice"))
    '                BgPr = BgPr + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice"))
    '                MBPR = MBPR + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice")) + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice"))
    '                OriginalTF = OriginalTF + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice")) + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice"))
    '            Next
    '        End If

    '        Dim InfFare As Decimal = Convert.ToDecimal(FltDs.Tables(0).Rows(0)("InfFare").ToString())
    '        Dim dsCrd As DataSet = objSql.GetCredentials(VC)
    '        Dim Org As String = "", Dest As String = ""
    '        Dim objInputs As New STD.Shared.FlightSearch
    '        If FltDs.Tables(0).Rows(FltDs.Tables(0).Rows.Count - 1)("TripType") = "R" Then objInputs.TripType = STD.Shared.TripType.RoundTrip Else objInputs.TripType = STD.Shared.TripType.OneWay
    '        If FltDs.Tables(0).Rows(0)("Trip") = "D" Then objInputs.Trip = STD.Shared.Trip.D Else objInputs.Trip = STD.Shared.Trip.I
    '        objInputs.Adult = FltDs.Tables(0).Rows(0)("Adult")
    '        objInputs.Child = FltDs.Tables(0).Rows(0)("Child")
    '        objInputs.Infant = FltDs.Tables(0).Rows(0)("Infant")
    '        objInputs.HidTxtAirLine = VC
    '        Dim inx As Integer = 0
    '        If (objInputs.TripType = STD.Shared.TripType.RoundTrip) Then
    '            inx = 1
    '            Trip = 2
    '        End If

    '        Dim seginfo As New ArrayList()

    '        Dim FNO As String = ""
    '        Dim JSK(inx), FSK(inx) As String 'CC(inx), FNO(inx), DD(inx) 

    '        Dim dt = FltDs.Tables(0).DefaultView.ToTable(True, "FlightIdentification") 'Sorted By FNo
    '        For jj As Integer = 0 To dt.Rows.Count - 1
    '            Dim dt1 = FltDs.Tables(0).Select("FlightIdentification='" & dt.Rows(jj)("FlightIdentification") & "'", "")
    '            FNO = dt1(0)("FlightIdentification").Trim()
    '            Dim Seg As New Dictionary(Of String, String)
    '            Seg.Add("FNO", FNO)
    '            Seg.Add("STD", dt1(0)("depdatelcc"))
    '            Seg.Add("Departure", dt1(0)("DepartureLocation"))
    '            Seg.Add("Arrival", dt1(dt1.Length - 1)("ArrivalLocation"))
    '            Seg.Add("VC", VC)
    '            Seg.Add("Flight", dt1(0)("Flight"))
    '            seginfo.Add(Seg)
    '        Next
    '        For ii As Integer = 0 To FltDs.Tables(0).Rows.Count - 1
    '            If (ii = 0) Then
    '                Dim Seg As New Dictionary(Of String, String)
    '                Org = FltDs.Tables(0).Rows(ii)("OrgDestFrom")
    '                Dest = FltDs.Tables(0).Rows(ii)("OrgDestTo")
    '                'Changes 8March
    '                'OriginalTF = Convert.ToDecimal(FltDs.Tables(0).Rows(ii)("OriginalTF").ToString())
    '                objInputs.HidTxtDepCity = Org
    '                objInputs.HidTxtArrCity = Dest
    '            End If
    '            If (Org = FltDs.Tables(0).Rows(ii)("OrgDestFrom").ToString()) Then
    '                JSK(0) = FltDs.Tables(0).Rows(ii)("SNO")
    '                FSK(0) = FltDs.Tables(0).Rows(ii)("Searchvalue")
    '            ElseIf (Org = FltDs.Tables(0).Rows(ii)("OrgDestTo").ToString()) Then
    '                JSK(1) = FltDs.Tables(0).Rows(ii)("SNO")
    '                FSK(1) = FltDs.Tables(0).Rows(ii)("Searchvalue")
    '            End If
    '        Next
    '        objInputs.HidTxtAirLine = VC
    '        Dim Xml As New Dictionary(Of String, String)
    '        If (objInputs.Infant > 0) Then
    '            objInputs.Infant = 0 ' Set Infant to 0
    '        End If
    '        If (MBPR > 0) Then
    '            If (VC = "6E") Then
    '                Dim obj6E As New STD.BAL.SpiceAPI(dsCrd.Tables(0).Rows(0)("UserID"), dsCrd.Tables(0).Rows(0)("Password"), dsCrd.Tables(0).Rows(0)("BkgServerUrlOrIP"), ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString, dsCrd.Tables(0).Rows(0)("CorporateID"), objInputs.HidTxtDepCity, objInputs.HidTxtArrCity, objInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", dsCrd.Tables(0).Rows(0)("LoginID"), dsCrd.Tables(0).Rows(0)("LoginPwd"), 340)
    '                Signature = obj6E.Spice_Login()
    '                SJKAMT = obj6E.Spice_SellJourneyByKey(Signature, objInputs, JSK, FSK, Xml)
    '                SSRPRICE = obj6E.Spice_Sell_SSR(Signature, objInputs, seginfo, Xml, PaxDs.Tables(0), MBDT.Tables(0))
    '                obj6E.Spice_Logout(Signature)
    '            ElseIf (VC = "SG") Then
    '                Dim objSG As New STD.BAL.SpiceAPI(dsCrd.Tables(0).Rows(0)("UserID"), dsCrd.Tables(0).Rows(0)("Password"), dsCrd.Tables(0).Rows(0)("BkgServerUrlOrIP"), ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString, dsCrd.Tables(0).Rows(0)("CorporateID"), objInputs.HidTxtDepCity, objInputs.HidTxtArrCity, objInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", dsCrd.Tables(0).Rows(0)("LoginID"), dsCrd.Tables(0).Rows(0)("LoginPwd"), 0)
    '                Signature = objSG.Spice_Login()
    '                SJKAMT = objSG.Spice_SellJourneyByKey(Signature, objInputs, JSK, FSK, Xml)
    '                SSRPRICE = objSG.Spice_Sell_SSR(Signature, objInputs, seginfo, Xml, PaxDs.Tables(0), MBDT.Tables(0))
    '                objSG.Spice_Logout(Signature)
    '            End If
    '            Try
    '                objSql.Insert_SSR_Log(TrackId, Signature, Xml("SSR"), SSRPRICE) ' Enter Log
    '            Catch ex As Exception
    '                SSRPRICE = "FAILURE"
    '            End Try
    '            If (SSRPRICE <> "FAILURE") Then
    '                If (Convert.ToDecimal(SSRPRICE) > (OriginalTF)) Then
    '                    ' Logic to Update MB table
    '                    Diff = (Convert.ToDecimal(SSRPRICE) - (OriginalTF))
    '                    'Dim DifPerpax As Decimal = Math.Round(Diff / ((objInputs.Adult + objInputs.Child) * Trip), 0)
    '                    objSql.Update_PAX_BG_Price(TrackId, Diff.ToString())
    '                End If
    '            Else
    '                'Alert(Code')
    '                If (Xml("SSR").Contains("The requested class of service is sold out")) Then
    '                    Response.Redirect("../FlightInt/BookingMsg.aspx?msg=1")
    '                ElseIf (Xml("SSR").Contains("not available on flight")) Then
    '                    Response.Redirect("../FlightInt/BookingMsg.aspx?msg=ML")
    '                End If
    '            End If
    '            objSql.Update_NET_TOT_Fare(TrackId, (MBPR + Diff).ToString())
    '        End If
    '    Catch ex As Exception
    '        Response.Redirect("../FlightInt/BookingMsg.aspx?msg=1")
    '    End Try
    '    Return SSRPRICE
    'End Function
    Private Function SELL_SSR(ByVal TrackId As String) As String
        Dim SSRPRICE As String = "", SJKAMT As String = "" ', ViaArrv As String = ""
        Dim Signature As String = ""
        Dim Trip As Integer = 1
        Dim Diff As Decimal = 0
        Try

            Dim FltDs As DataSet = objDA.GetFltDtls(TrackId, Session("UID"))
            Dim PaxDs As DataSet = objDA.GetPaxDetails(TrackId)
            Dim VC As String = FltDs.Tables(0).Rows(0)("ValiDatingCarrier").ToString.Trim()
            Dim OriginalTF As Decimal = Convert.ToDecimal(FltDs.Tables(0).Rows(0)("OriginalTF").ToString())
            Dim MBDT As DataSet = objSql.Get_MEAL_BAG_FareDetails(FltDs.Tables(0).Rows(0)("Track_id").ToString(), "")
            Dim MBPR As Decimal = 0, MealPr As Decimal = 0, BgPr As Decimal = 0
            If (MBDT.Tables(0).Rows.Count > 0) Then
                For jj As Integer = 0 To MBDT.Tables(0).Rows.Count - 1
                    MealPr = MealPr + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice"))
                    BgPr = BgPr + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice"))
                    MBPR = MBPR + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice")) + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice"))
                    OriginalTF = OriginalTF + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice")) + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice"))
                Next
            End If

            Dim InfFare As Decimal = Convert.ToDecimal(FltDs.Tables(0).Rows(0)("InfFare").ToString())
            Dim dsCrd As DataSet = objSql.GetCredentials(VC)
            Dim Org As String = "", Dest As String = ""
            Dim objInputs As New STD.Shared.FlightSearch
            If FltDs.Tables(0).Rows(FltDs.Tables(0).Rows.Count - 1)("TripType") = "R" Then objInputs.TripType = STD.Shared.TripType.RoundTrip Else objInputs.TripType = STD.Shared.TripType.OneWay
            If FltDs.Tables(0).Rows(0)("Trip") = "D" Then objInputs.Trip = STD.Shared.Trip.D Else objInputs.Trip = STD.Shared.Trip.I
            objInputs.Adult = FltDs.Tables(0).Rows(0)("Adult")
            objInputs.Child = FltDs.Tables(0).Rows(0)("Child")
            objInputs.Infant = FltDs.Tables(0).Rows(0)("Infant")
            objInputs.HidTxtAirLine = VC
            Dim inx As Integer = 0
            If (objInputs.TripType = STD.Shared.TripType.RoundTrip) Then
                inx = 1
                Trip = 2
            End If

            Dim seginfo As New ArrayList()
            Dim Utlobj As New SpiceIndigoUTL()

            Dim FNO As String = ""
            Dim JSK(inx), FSK(inx) As String
            Dim ViaArr(inx) As String

            Dim dt = FltDs.Tables(0).DefaultView.ToTable(True, "FlightIdentification") 'Sorted By FNo
            For jj As Integer = 0 To dt.Rows.Count - 1
                Dim dt1 = FltDs.Tables(0).Select("FlightIdentification='" & dt.Rows(jj)("FlightIdentification") & "'", "")
                FNO = dt1(0)("FlightIdentification").Trim()
                Dim Seg As New Dictionary(Of String, String)
                Seg.Add("FNO", FNO)
                Seg.Add("STD", dt1(0)("depdatelcc"))
                Seg.Add("Departure", dt1(0)("DepartureLocation"))
                Seg.Add("Arrival", dt1(dt1.Length - 1)("ArrivalLocation"))
                Seg.Add("VC", VC)
                Seg.Add("Flight", dt1(0)("Flight"))
                seginfo.Add(Seg)

            Next
            For ii As Integer = 0 To FltDs.Tables(0).Rows.Count - 1
                If (ii = 0) Then
                    Dim Seg As New Dictionary(Of String, String)
                    Org = FltDs.Tables(0).Rows(ii)("OrgDestFrom")
                    Dest = FltDs.Tables(0).Rows(ii)("OrgDestTo")
                    'Changes 8March
                    'OriginalTF = Convert.ToDecimal(FltDs.Tables(0).Rows(ii)("OriginalTF").ToString())
                    objInputs.HidTxtDepCity = Org
                    objInputs.HidTxtArrCity = Dest
                End If
                If (Org = FltDs.Tables(0).Rows(ii)("OrgDestFrom").ToString()) Then
                    JSK(0) = FltDs.Tables(0).Rows(ii)("SNO")
                    FSK(0) = FltDs.Tables(0).Rows(ii)("Searchvalue")
                    ViaArr(0) = Utlobj.Check_Via_Connecting(FltDs.Tables(0), FltDs.Tables(0).Rows(ii)("Flight"), VC)
                ElseIf (Org = FltDs.Tables(0).Rows(ii)("OrgDestTo").ToString()) Then
                    JSK(1) = FltDs.Tables(0).Rows(ii)("SNO")
                    FSK(1) = FltDs.Tables(0).Rows(ii)("Searchvalue")
                    ViaArr(1) = Utlobj.Check_Via_Connecting(FltDs.Tables(0), FltDs.Tables(0).Rows(ii)("Flight"), VC)
                End If
            Next

            objInputs.HidTxtAirLine = VC
            Dim Xml As New Dictionary(Of String, String)
            If (objInputs.Infant > 0) Then
                objInputs.Infant = 0 ' Set Infant to 0
            End If
            If (MBPR > 0) Then
                If (VC = "6E") Then
                    Dim obj6E As New STD.BAL.SpiceAPI(dsCrd.Tables(0).Rows(0)("UserID"), dsCrd.Tables(0).Rows(0)("Password"), dsCrd.Tables(0).Rows(0)("BkgServerUrlOrIP"), ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString, dsCrd.Tables(0).Rows(0)("CorporateID"), objInputs.HidTxtDepCity, objInputs.HidTxtArrCity, objInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", dsCrd.Tables(0).Rows(0)("LoginID"), dsCrd.Tables(0).Rows(0)("LoginPwd"), 340) ', Convert.ToString(dsCrd.Tables(0).Rows(0)("APISource"))
                    Signature = obj6E.Spice_Login()
                    SJKAMT = obj6E.Spice_SellJourneyByKey(Signature, objInputs, JSK, FSK, Xml)
                    SSRPRICE = obj6E.Spice_Sell_SSR(Signature, objInputs, seginfo, Xml, PaxDs.Tables(0), MBDT.Tables(0), ViaArr)
                    obj6E.Spice_Logout(Signature)
                ElseIf (VC = "SG") Then
                    Dim objSG As New STD.BAL.SpiceAPI(dsCrd.Tables(0).Rows(0)("UserID"), dsCrd.Tables(0).Rows(0)("Password"), dsCrd.Tables(0).Rows(0)("BkgServerUrlOrIP"), ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString, dsCrd.Tables(0).Rows(0)("CorporateID"), objInputs.HidTxtDepCity, objInputs.HidTxtArrCity, objInputs.OwnerId, "BasicHttpBinding_ISessionManager", "BasicHttpBinding_IBookingManager", dsCrd.Tables(0).Rows(0)("LoginID"), dsCrd.Tables(0).Rows(0)("LoginPwd"), 0) ', Convert.ToString(dsCrd.Tables(0).Rows(0)("APISource"))
                    Signature = objSG.Spice_Login()
                    SJKAMT = objSG.Spice_SellJourneyByKey(Signature, objInputs, JSK, FSK, Xml)
                    SSRPRICE = objSG.Spice_Sell_SSR(Signature, objInputs, seginfo, Xml, PaxDs.Tables(0), MBDT.Tables(0), ViaArr)
                    objSG.Spice_Logout(Signature)
                End If
                Try
                    objSql.Insert_SSR_Log(TrackId, Signature, Xml("SSR"), SSRPRICE, Convert.ToDecimal(FltDs.Tables(0).Rows(0)("totFare"))) ' Enter Log
                Catch ex As Exception
                    SSRPRICE = "FAILURE"
                End Try
                If (SSRPRICE <> "FAILURE") Then
                    If (Convert.ToDecimal(SSRPRICE) > (OriginalTF)) Then
                        ' Logic to Update MB table
                        Diff = (Convert.ToDecimal(SSRPRICE) - (OriginalTF))
                        'Dim DifPerpax As Decimal = Math.Round(Diff / ((objInputs.Adult + objInputs.Child) * Trip), 0)
                        objSql.Update_PAX_BG_Price(TrackId, Diff.ToString())
                    End If
                Else
                    'Alert(Code')
                    If (Xml("SSR").Contains("The requested class of service is sold out")) Then
                        Response.Redirect("../FlightInt/BookingMsg.aspx?msg=1", False)
                    ElseIf (Xml("SSR").Contains("not available on flight")) Then
                        Response.Redirect("../FlightInt/BookingMsg.aspx?msg=ML", False)
                    End If
                End If
                objSql.Update_NET_TOT_Fare(TrackId, (MBPR + Diff).ToString())
            End If
        Catch ex As Exception
            Response.Redirect("../FlightInt/BookingMsg.aspx?msg=1", False)
        End Try
        Return SSRPRICE
    End Function
    Private Function VCCount(ByVal dt As DataTable) As Integer
        Dim Count As Integer = (From row In dt.Rows Select row Where row("MarketingCarrier").ToString <> dt.Rows(0)("MarketingCarrier")).Count
        Return Count
    End Function
    Private Function VCCount1(ByVal drr As DataRow()) As Integer

        Dim Count1 As Integer = (From row In drr Select row Where row("MarketingCarrier").ToString <> drr(0)("MarketingCarrier")).Count
        Return Count1
    End Function
    Private Function MultiValueFunction(ByVal dt As DataTable, ByVal Type As String, Optional ByVal Position As Int32 = 0, Optional ByVal dtrow As String = "") As String
        Dim OutputString As String = ""
        If (Type = "Logo") Then
            OutputString = "../Airlogo/sm" & dt.Rows(0)("MarketingCarrier") & ".gif"
        ElseIf Type = "Airline" Then
            OutputString = dt.Rows(0)("AirlineName") & "(" & dt.Rows(0)("MarketingCarrier") & "-" & dt.Rows(0)("FlightIdentification") & ")"
        ElseIf Type = "Dep" Then
            If (dt.Rows(0)("DepartureTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(0)("DepartureTime").ToString()

            Else
                OutputString = dt.Rows(0)("DepartureTime").ToString().Substring(0, 2) & ":" & dt.Rows(0)("DepartureTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Deprow" Then
            If (dtrow.Contains(":") = True) Then
                OutputString = dtrow

            Else
                OutputString = dtrow.Substring(0, 2) & ":" & dtrow.Substring(2, 2)

            End If

        ElseIf Type = "Arrrow" Then
            If (dtrow.Contains(":") = True) Then
                OutputString = dtrow

            Else
                OutputString = dtrow.Substring(0, 2) & ":" & dtrow.Substring(2, 2)

            End If
        ElseIf Type = "Arr" Then
            If (dt.Rows(0)("ArrivalTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString()

            Else
                OutputString = dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString().Substring(0, 2) & ":" & dt.Rows(dt.Rows.Count - 1)("ArrivalTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Depall" Then
            If (dt.Rows(Position)("DepartureTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(Position)("DepartureTime").ToString()

            Else
                OutputString = dt.Rows(Position)("DepartureTime").ToString().Substring(0, 2) & ":" & dt.Rows(Position)("DepartureTime").ToString().Substring(2, 2)

            End If
        ElseIf Type = "Arrall" Then
            If (dt.Rows(Position)("ArrivalTime").ToString().Contains(":") = True) Then
                OutputString = dt.Rows(Position)("ArrivalTime").ToString()

            Else
                OutputString = dt.Rows(Position)("ArrivalTime").ToString().Substring(0, 2) & ":" & dt.Rows(Position)("ArrivalTime").ToString().Substring(2, 2)

            End If
        End If
        Return OutputString
    End Function
    Public Function UpadteFare(ByVal trackId As String) As String

        Dim MBDT As DataSet = objSql.Get_MEAL_BAG_FareDetails(trackId, "")
        Dim BgPr As Decimal = 0
        If (MBDT.Tables(0).Rows.Count > 0) Then
            For jj As Integer = 0 To MBDT.Tables(0).Rows.Count - 1
                BgPr = BgPr + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("BaggagePrice")) + Convert.ToDecimal(MBDT.Tables(0).Rows(jj)("MealPrice"))
            Next
        End If
        Return objSql.Update_NET_TOT_Fare(trackId, (BgPr).ToString())


    End Function
End Class



