﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Partial Class DetailsPort_RejectedTicket
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()
    Private ST As New SqlTransaction()
    Dim AgencyDDLDS, grdds As New DataSet()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim AgentID As String = Nothing
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight </a><a class='current' href='#'>Search Flight Rejected Ticket</a>"
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            divPaymentMode.Visible = False
            divPartnerName.Visible = False
            If Not IsPostBack Then
                BindPartner()
                If Session("User_Type") = "AGENT" Then
                    td_Agency.Visible = False
                    ' Partnerid.Visible = False

                    AgentID = Session("UID").ToString()
                Else
                    ' Bind Agency Name DDL
                    'If (Session("user_type") = "SALES") Then
                    '    AgencyDDLDS= STDom.getAgencybySalesRef(Session("UID").ToString)
                    'Else
                    '    AgencyDDLDS = ST.GetAgencyDetailsDDL()
                    'End If
                    'If AgencyDDLDS.Tables(0).Rows.Count > 0 Then
                    '    ddl_AgencyName.AppendDataBoundItems = True
                    '    ddl_AgencyName.Items.Clear()
                    '    ddl_AgencyName.Items.Insert(0, "--Select Agency Name--")
                    '    ddl_AgencyName.DataSource = AgencyDDLDS
                    '    ddl_AgencyName.DataTextField = "Agency_Name"
                    '    ddl_AgencyName.DataValueField = "user_id"
                    '    ddl_AgencyName.DataBind()
                    'End If
                End If

                Dim curr_date = Now.Date() & " " & "12:00:00 AM"
                Dim curr_date1 = Now()
                Dim Partnername As String = If([String].IsNullOrEmpty(txtPartnerName.Text), "", txtPartnerName.Text.Trim)
                Dim PaymentMode As String = If([String].IsNullOrEmpty(txtPaymentmode.Text), "", txtPaymentmode.Text.Trim)
                'GridView Date Set
                grdds.Clear()
                grdds = ST.USP_GetTicketDetail_Intl(Session("UID").ToString, Session("User_Type").ToString, curr_date, curr_date1, Nothing, Nothing, Nothing, Nothing, Nothing, AgentID, Nothing, StatusClass.Rejected, Partnername, PaymentMode, "")
                BindGrid(grdds)
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Private Sub BindGrid(ByVal Gds As DataSet)
        Try
            ViewState("Grdds") = Gds
            'If Session("User_Type").ToString.ToUpper <> "ADMIN" Then
            '    ticket_grdview.Columns(6).Visible = False
            'End If
            ticket_grdview.DataSource = Gds
            ticket_grdview.DataBind()
            txtPartnerName.SelectedValue = "0"
            txtPaymentmode.SelectedValue = "All"
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
   
    'Filtering data from Search click
    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        Try
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If

            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))

            Dim trip As String
            If ddlTrip.SelectedIndex > 0 Then
                trip = ddlTrip.SelectedValue
            Else
                trip = ""
            End If

            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PNR As String = If([String].IsNullOrEmpty(txt_PNR.Text), "", txt_PNR.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txt_TktNo.Text), "", txt_TktNo.Text.Trim)
            Dim AirPNR As String = If([String].IsNullOrEmpty(txt_AirPNR.Text), "", txt_AirPNR.Text.Trim)
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
            If Partnername = "0" Then
                Partnername = ""

            End If
            grdds.Clear()

            grdds = ST.USP_GetTicketDetail_Intl_Rejected(Session("UID").ToString, Session("User_Type").ToString, FromDate, ToDate, OrderID, PNR, PaxName, TicketNo, AirPNR, AgentID, trip, StatusClass.Rejected, Partnername, PaymentMode)

            BindGrid(grdds)

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub



    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try

            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If

            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))

            Dim trip As String
            If ddlTrip.SelectedIndex > 0 Then
                trip = ddlTrip.SelectedValue
            Else
                trip = ""
            End If

            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PNR As String = If([String].IsNullOrEmpty(txt_PNR.Text), "", txt_PNR.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txt_TktNo.Text), "", txt_TktNo.Text.Trim)
            Dim AirPNR As String = If([String].IsNullOrEmpty(txt_AirPNR.Text), "", txt_AirPNR.Text.Trim)
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
            If Partnername = "0" Then
                Partnername = ""

            End If
            grdds.Clear()

            grdds = ST.USP_GetTicketDetail_Intl_Rejected(Session("UID").ToString, Session("User_Type").ToString, FromDate, ToDate, OrderID, PNR, PaxName, TicketNo, AirPNR, AgentID, trip, StatusClass.Rejected, Partnername, PaymentMode)


            STDom.ExportData(grdds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub



    Protected Sub ticket_grdview_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ticket_grdview.PageIndexChanging
        Try
            ticket_grdview.PageIndex = e.NewPageIndex
            BindGrid(ViewState("Grdds"))
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub


    Public Sub BindPartner()
        Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        Dim con As New SqlConnection(constr)
        Dim cmd As New SqlCommand("BindPartnerNameSP_PP")

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        con.Open()
        txtPartnerName.DataSource = cmd.ExecuteReader()
        txtPartnerName.DataTextField = "PartnerName"
        txtPartnerName.DataValueField = "PartnerName"


        txtPartnerName.DataBind()
        con.Close()
        txtPartnerName.Items.Insert(0, New ListItem("--Select PartnerName--", "0"))
    End Sub
End Class

