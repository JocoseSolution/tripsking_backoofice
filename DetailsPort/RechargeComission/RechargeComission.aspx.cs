﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class DetailsPort_RechargeComission_RechargeComission : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Recharge </a><a class='current' href='#'>Recharge comission</a>";
        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}
        try
        {
            if (!IsPostBack)
            {
                ddlagentType.AppendDataBoundItems = true;
                ddlagentType.Items.Clear();

                //Dim item As New ListItem("All Type", "0")
                //ddl_ptype.Items.Insert(0, item)
                ddlagentType.DataSource = STDom.GetAllGroupType().Tables[0];
                ddlagentType.DataTextField = "GroupType";
                ddlagentType.DataValueField = "GroupType";
                ddlagentType.DataBind();
                ddlagentType.Items.Insert(0, new ListItem("GROUP TYPE", ""));

                DdlProcesstype.AppendDataBoundItems = true;
                DdlProcesstype.Items.Clear();

                //Dim item As New ListItem("All Type", "0")
                //ddl_ptype.Items.Insert(0, item)
                string Action = "Processtype";
                DdlProcesstype.DataSource = STDom.GetRechargeTypes(Action,"").Tables[0];
                DdlProcesstype.DataTextField = "operatortype";
                DdlProcesstype.DataValueField = "operatortype";
                DdlProcesstype.DataBind();
                DdlProcesstype.Items.Insert(0, new ListItem("PROCESS TYPE", ""));
               
            }
        }
        catch (Exception ex)
        {
        }

    }
    //public void BindRole()
    //{
    //    string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    //    SqlConnection con = new SqlConnection(constr);

    //    SqlCommand cmd = new SqlCommand("BindRoleNewExecu_PP");

    //    cmd.CommandType = CommandType.StoredProcedure;
    //    cmd.Connection = con;
    //    con.Open();
    //    ddlRole_type.DataSource = cmd.ExecuteReader();
    //    ddlRole_type.DataTextField = "Role";
    //    ddlRole_type.DataValueField = "Role_id";
    //    ddlRole_type.DataBind();

    //    con.Close();
    //    ddlRole_type.Items.Insert(0, new ListItem("--Select Role--", "0"));
    //}
    public string GetStatusVal(object val)
    {
        string value = "";
        bool result = Convert.ToBoolean(val);

        if (result == false)
        {
            value = "Inactive";
        }
        else
        {
            value = "Active";
        }
        return value;
    }
    protected void Submit_Click(object sender, EventArgs e)
    {
        string Processtype = DdlProcesstype.SelectedItem.Text;
        string Operator = Ddlsupplier.SelectedItem.Text;
        string OperatorCode = Ddlsupplier.SelectedValue;
        string AgentType = ddlagentType.SelectedItem.Text;
        string comtype = ddlcomtype.SelectedValue;
        decimal Commission = Convert.ToDecimal(txtComm.Text);
        con.Open();
        SqlCommand cmd = new SqlCommand("Sp_InsertRechargeCommission",con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Processtype",Processtype);
        cmd.Parameters.AddWithValue("@Operator",Operator);
        cmd.Parameters.AddWithValue("@OperatorCode",OperatorCode);
        cmd.Parameters.AddWithValue("@AgentType",AgentType);
        cmd.Parameters.AddWithValue("@comtype",comtype);
        cmd.Parameters.AddWithValue("@Commission", Commission);
       int a= cmd.ExecuteNonQuery();
       if (a == 1)
       {
           ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Data Inserted Sucessfully');", true);
       }
       else
       {
           ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('No Data Inserted');", true);
       }
        con.Close();
    }


    protected void BindGridview()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();
        string Processtype = "";
        string Operator = "";
        string AgentType = "";
        if(!string.IsNullOrEmpty(DdlProcesstype.SelectedValue))
        {
         Processtype = DdlProcesstype.SelectedItem.Text;
        }
        if (!string.IsNullOrEmpty(Ddlsupplier.SelectedValue))
        {
             Operator = Ddlsupplier.SelectedItem.Text;
        }
        if (!string.IsNullOrEmpty(ddlagentType.SelectedValue))
        {
            AgentType = ddlagentType.SelectedItem.Text;
        }
        SqlCommand cmd = new SqlCommand("Sp_Rechargegrid");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@Processtype", Processtype);
        cmd.Parameters.AddWithValue("@Operator", Operator);
        cmd.Parameters.AddWithValue("@AgentType", AgentType);
        cmd.Parameters.AddWithValue("@Action","Select");
        SqlDataAdapter sda = new SqlDataAdapter();

        cmd.Connection = con;
        sda.SelectCommand = cmd;
        DataTable dt = new DataTable();

        sda.Fill(dt);
        con.Close();
        GridView1.DataSource = dt;

        GridView1.DataBind();

    }

    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();

    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();


        

    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string user_id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Sp_Rechargegrid"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_Id", user_id);
                cmd.Parameters.AddWithValue("@Action", "Delete");
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();

    }


    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        string user_id = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
        string Type = (row.FindControl("ddlType") as DropDownList).SelectedItem.ToString();
        TextBox TXTCommision = (row.FindControl("txtCommision") as TextBox);
        string Commision = TXTCommision.Text;

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("Sp_Rechargegrid"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@User_Id", user_id);
                cmd.Parameters.AddWithValue("@UpdatedBy", Session["UID"].ToString());
                cmd.Parameters.AddWithValue("@Commision",Convert.ToDecimal(Commision));
                cmd.Parameters.AddWithValue("@Type", Type);
                //cmd.Parameters.AddWithValue("@status", status.ToUpper() == "ACTIVE" ? 1 : 0);
                cmd.Parameters.AddWithValue("@Action", "Update");

                cmd.Connection = con;
                con.Open();
                int i = Convert.ToInt32(cmd.ExecuteNonQuery());
                con.Close();
                if (i > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record updated successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record not updated.');", true);
                }

            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();

    }

    //protected void Cancel_Click(object sender, EventArgs e)
    //{
    //    txtemail.Text = "";
    //    txtpassword.Text = "";
    //    txtname.Text = "";
    //    txtmobile.Text = "";
    //    ddlRole_type.SelectedIndex = 0;
    //}
    public DataTable CheckUserDetails()
    {
        DataTable dt = new DataTable();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB1"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("CheckUserDetails");
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter();
            cmd.Connection = con;
            sda.SelectCommand = cmd;
            sda.Fill(dt);
            con.Close();
            cmd.Dispose();
        }
        catch (Exception ex)
        {
            con.Close();
        }

        return dt;
    }
    protected void DdlProcesstype_SelectedIndexChanged(object sender, EventArgs e)
    {
        Ddlsupplier.AppendDataBoundItems = true;
        Ddlsupplier.Items.Clear();

        //Dim item As New ListItem("All Type", "0")
        //ddl_ptype.Items.Insert(0, item)
        string Action = "GetAllSupplier";
        Ddlsupplier.DataSource = STDom.GetRechargeTypes(Action, DdlProcesstype.SelectedItem.ToString()).Tables[0];
        Ddlsupplier.DataTextField = "OperatorName";
        Ddlsupplier.DataValueField = "OperatorCode";
        Ddlsupplier.DataBind();
        Ddlsupplier.Items.Insert(0, new ListItem("Operator", ""));
    }
    protected void Search_Click(object sender, EventArgs e)
    {
        BindGridview();
    }
}
