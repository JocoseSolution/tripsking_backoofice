﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="RollBackByOrderid.aspx.cs" Inherits="DetailsPort_RollBackByOrderid" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .updatebtn {
            font-weight: bold;
            background: red;
            border: none;
            padding: 5px 14px;
            border-radius: 4px;
            color: #fff;
        }
    </style>
    <script>
        function CheckPnr() {
            let pnr = $("#<%=txtOrderid.ClientID%>").val();
            if (pnr.trim() == "") {
                alert("Please enter Orderid number.");
                $("#<%=txtOrderid.ClientID%>").focus();
                return false;
            }
        }
        function confirmRefund() {
            if (confirm("Are you sure want to refund this record amount ?")) {
                return true;
            }
            return false;
        }
    </script>
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group">
                                    <asp:TextBox ID="txtOrderid" runat="server" placeholder="Enter Order Id" class="form-control input-text full-width" Style="width: 250px!important; float: right;"></asp:TextBox>
                                    <%--<input type="text" placeholder="Enter PNR Number" name="PNR" id="PNR" class="form-control input-text full-width" style="width: 250px!important; float: right;" />--%>
                                    <span class="input-group-addon" style="background: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd1" style="cursor: pointer;"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <asp:Button ID="btn_search" runat="server" Text="Find" CssClass="btn btn-success" Style="width: 200px; height: 33px;" OnClick="btn_search_Click" OnClientClick="return CheckPnr();" />
                                </div>
                            </div>
                        </div>
                        <br />
                        <div id="Div1" class="row" style="background-color: #fff; overflow-y: scroll;" runat="server" visible="true">
                            <div class="col-md-12">

                                <asp:UpdatePanel ID="up" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover" Style="text-transform: uppercase; color: #000;"
                                            GridLines="None" PageSize="30" OnRowDeleting="Grid_Ledger_RowDeleting" OnRowDataBound="Grid_Ledger_RowDataBound" OnRowUpdating="Grid_Ledger_RowUpdating"
                                            OnRowCancelingEdit="Grid_Ledger_RowCancelingEdit" OnRowEditing="Grid_Ledger_RowEditing">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRecordId" runat="server" Text='<%#Eval("OrderID") %>'></asp:Label>
                                                        <asp:Label ID="lblAgencyName" runat="server" Visible="false" Text='<%#Eval("Agencyname") %>'></asp:Label>
                                                        <asp:Label ID="lblUserId" runat="server" Visible="false" Text='<%#Eval("Agentid") %>'></asp:Label>
                                                        <asp:Label ID="lblAgencyId" runat="server" Visible="false" Text='<%#Eval("Agentid") %>'></asp:Label>
                                                        <asp:Label ID="lblTotalAmt" runat="server" Visible="false" Text='<%#Eval("TotalAfterDis") %>'></asp:Label>
                                                        <asp:Label ID="lblInvoice" runat="server" Visible="false" Text='<%#Eval("OrderID") %>'></asp:Label>
                                                        <asp:Label ID="lblPnrNo" runat="server" Visible="false" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Agencyname" HeaderText="Agency Name" />
                                                <asp:BoundField DataField="Agentid" HeaderText="Userid" />
                                                <asp:BoundField DataField="sector" HeaderText="sector" />
                                                <asp:BoundField DataField="TotalAfterDis" HeaderText="Total Fare" />
                                                <asp:BoundField DataField="GdsPnr" HeaderText="PNR No." />
                                                <asp:BoundField DataField="PaxCount" HeaderText="Total Pax" />
                                                <%--<asp:BoundField DataField="ReferenceNo" HeaderText="ReferenceNo" />--%>
                                                <%--<asp:BoundField DataField="ReservationId" HeaderText="ReservationId" />--%>
                                                <%--<asp:BoundField DataField="tid" HeaderText="No of Pnr" />--%>



                                                <asp:BoundField DataField="CreateDate" HeaderText="CreatedDate" />
                                                <%--<asp:BoundField DataField="type" HeaderText="Agent Type"  />--%>
                                                <%-- <asp:BoundField HeaderText="Payment Mode" DataField="PaymentMode" ></asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="RollBack">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnlupdate" runat="server" Text="RollBack" CommandName="Update" CssClass="newbutton_2 updatebtn" OnClientClick="return confirmRefund();" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

