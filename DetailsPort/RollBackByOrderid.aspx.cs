﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class DetailsPort_RollBackByOrderid :  System.Web.UI.Page
{

    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        Label label = Master.FindControl("lblBC") as Label;
        label.Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Report</a><a class='current' href='#'>RollBack Amount By OrderID</a>";

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
    }
     protected void btn_search_Click(object sender, EventArgs e)
    {
        try
        {
            string pnr="";
            pnr = txtOrderid.Text.Trim();
            if (!string.IsNullOrEmpty(pnr))
            {
                SqlTransactionDom STDom = new SqlTransactionDom();

                string msg = string.Empty;
                DataTable dtSearch = STDom.RollbackbyOrderid(pnr);
                if (dtSearch != null && dtSearch.Rows.Count > 0)
                {
                    Grid_Ledger.DataSource = dtSearch;
                    Grid_Ledger.DataBind();
                }                
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
            throw;
        }
    }

    protected void Grid_Ledger_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    private int InsertCreditDebitIntoLedgerTable(string agentId, string userId, string totalAmount, string agencyName, string invoiceNo, string pnrNo)
    {
        SqlTransactionDom STDom = new SqlTransactionDom();
        Hashtable hsLedger = STDom.InsertCreditDebitIntoLedgerTable(Convert.ToDouble(totalAmount), userId, agencyName, invoiceNo, pnrNo, invoiceNo, "", "", Session["UID"].ToString(), "", "::1", 0, Convert.ToDouble(totalAmount), "Credit", "RollBack Ticket Amount", 0, "CR", "", 0, "Confirm", "", "RollBack", "", "", "Rollback_Flt_Ticket_Amount", 0, "CREDIT NOTE");
        if (hsLedger.Count == 2)
        {
            STDom.UpdateRollbackbyOrderid(invoiceNo);
            return 1;

        }
        return 0;
    }

    protected void Grid_Ledger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Refund")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to refund?')){ return false; };";
                }
            }
        }
    }

    protected void Grid_Ledger_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int flag = 0; decimal totalAmt = 0; string agencyName = string.Empty;
        try
        {
            Label lblRecordId = (Label)(Grid_Ledger.Rows[e.RowIndex].FindControl("lblRecordId"));
            Label lblUserId = (Label)(Grid_Ledger.Rows[e.RowIndex].FindControl("lblUserId"));
            Label lblAgencyId = (Label)(Grid_Ledger.Rows[e.RowIndex].FindControl("lblAgencyId"));
            Label lblTotalAmt = (Label)(Grid_Ledger.Rows[e.RowIndex].FindControl("lblTotalAmt"));
            Label lblAgencyName = (Label)(Grid_Ledger.Rows[e.RowIndex].FindControl("lblAgencyName"));
            Label lblInvoice = (Label)(Grid_Ledger.Rows[e.RowIndex].FindControl("lblInvoice"));
            Label lblPnrNo = (Label)(Grid_Ledger.Rows[e.RowIndex].FindControl("lblPnrNo"));

            totalAmt = Convert.ToDecimal(lblTotalAmt.Text.Trim().ToString());
            agencyName = lblAgencyName.Text.Trim().ToString();

            flag = InsertCreditDebitIntoLedgerTable(lblAgencyId.Text, lblUserId.Text, lblTotalAmt.Text, lblAgencyName.Text, lblInvoice.Text, lblPnrNo.Text);
        }
        catch (SqlException ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }


        if (flag > 0)
        {
            Grid_Ledger.DataSource = null;
            Grid_Ledger.DataBind();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Rs. " + totalAmt + " has been refund to " + agencyName + " successfully.');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
        }
    }

    protected void Grid_Ledger_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {

    }

    protected void Grid_Ledger_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }
}