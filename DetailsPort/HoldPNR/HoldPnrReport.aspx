﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HoldPnrReport.aspx.vb" Inherits="DetailsPort_HoldPNR_HoldPnrReport" MasterPageFile="~/MasterPageSignIn.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <%--<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"   rel="stylesheet" />--%>
     

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>

    <div class="row">
        <div class="container-fluid" style="padding-right:35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                
                    <div class="panel-body" style="margin-right:-165px">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" placeholder="FROM DATE" name="From" id="From" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar"></span>
                               </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                <%--<label for="exampleInputPassword1">To Date</label>--%>
                                <input type="text" placeholder="TO DATE" name="To" id="To" class="form-control input-text full-width" readonly="readonly" />
                                 <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar"></span>
                               </span>
                            </div>
                                </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                <%--<label for="exampleInputPassword1">PNR</label>--%>
                                <asp:TextBox ID="txt_PNR" placeholder="PNR" class="form-control input-text full-width" runat="server"></asp:TextBox>
                                 <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-plane"></span>
                                        </span>
                            </div>
                                </div>
                               <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Order Id</label>--%>
                                    <asp:TextBox ID="txt_OrderId" placeholder="ORDER ID" class="form-control input-text full-width" runat="server"></asp:TextBox>
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-first-order"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                <%--<label for="exampleInputPassword1">Pax Name</label>--%>
                                <asp:TextBox ID="txt_PaxName" placeholder="PAX NAME" class="form-control input-text full-width" runat="server"></asp:TextBox>
                                <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-users"></span>
                                        </span>
                            </div>
                        </div>
                        </div>
                   <%--     <div class="row">

                         
                          


                        </div>--%>

                        <br />

                        <div id="td_Agency" runat="server">
                            <div class="row">
                                  <div class="col-md-2">
                                      <div class="input-group">
                                <%--<label for="exampleInputPassword1">Airline</label>--%>
                                <asp:TextBox ID="txt_AirPNR" placeholder="AIRLINE" class="form-control input-text full-width" runat="server"></asp:TextBox>
                                      <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-plane"></span>
                                        </span>
                            </div>
                                      </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                       <%-- <label for="exampleInputPassword1">Exec ID</label>--%>
                                        <asp:DropDownList ID="ddl_ExecID" class="input-text full-width" runat="server">
                                            <%--<asp:ListItem>--EXEC ID--</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Agency</label>--%>
                                    <input type="text" placeholder="BRAND" id="txtAgencyName" class="form-control input-text full-width" name="txtAgencyName" onfocus="focusObjag(this);"
                                        onblur="blurObjag(this);" autocomplete="off" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-bold"></span>
                                        </span>
                                </div>
                                    </div>
                                
                                <div class="col-md-2" id="divPartnerName" runat="server">
                                    <label for="exampleInputEmail1">PartnerName :</label>
                                    <asp:DropDownList CssClass="input-text full-width" ID="txtPartnerName" runat="server">
                                    </asp:DropDownList>
                                </div>

                                  <div class="col-md-2">

                                <label for="exampleInputPassword1" id="tdTripNonExec1" runat="server"></label>
                                <div id="tdTripNonExec2" runat="server">
                                    <asp:DropDownList ID="ddlTripRefunDomIntl" class="input-text full-width" runat="server">
                                        <%--<asp:ListItem Value="">--SELECT FLIGHT TYPE--</asp:ListItem>--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT FLIGHT TYPE</asp:ListItem>
                                        <asp:ListItem Value="D">Domestic</asp:ListItem>
                                        <asp:ListItem Value="I">International</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                                
                            <div class="col-md-2">
                                <div class="input-group" id="">
                                    <%--<label for="exampleInputPassword1">Ticket No</label>--%>
                                    <asp:TextBox ID="txt_TktNo" placeholder="TICKET NO." runat="server" class="form-control input-text full-width"></asp:TextBox>
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-ticket"></span>
                                        </span>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="row">


                              <div class="col-md-4" id="divPaymentMode" runat="server">
                                    <label for="exampleInputEmail1">PaymentMode :</label>
                                    <asp:DropDownList CssClass="input-text full-width" ID="txtPaymentmode" runat="server">                                 
                                   <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                   <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                  <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>                                                                
                                    </asp:DropDownList>
                                </div>

                          
                                <div class="col-md-2">
                                    <label for="exampleInputPassword1" style="visibility: hidden">Status</label>
                                    <div id="tr_ExecID" runat="server">

                                        <div class="large-2 medium-2 small-9 large-push-1 medium-push-1 columns" style="display: none;">
                                            <asp:DropDownList ID="ddl_Status" class="input-text full-width" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                       

                          
                        </div>
                        <div class="row">
                                  <div class="col-md-2" style="margin-top: -48px;">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_result" runat="server" Text="Go" CssClass="btn btn-success" Width="74px" />
                               <%-- </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                     <br />--%>
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-success" />
                                </div>
                            </div>
                        </div>
                        <hr />
                            <div class="row" id="divReport" style="background-color: #fff; overflow: auto; max-height: 500px;" runat="server" visible="false">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    AutoGenerateColumns="False" CssClass="table " GridLines="None" PageSize="30" style="text-transform:uppercase;">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Order Id">
                                            <ItemTemplate>

                                                <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                    style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; font-weight: bold;" title="click to view">
                                                    <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PNR">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PNR" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sector">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("sector") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Status" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Duration">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Duration" runat="server" Text='<%#Eval("Duration") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trip Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TripType" runat="server" Text='<%#Eval("TripType") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trip">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tour Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TourCode" runat="server" Text='<%#Eval("TourCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total Booking Cost">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalBookingCost" runat="server" Text='<%#Eval("TotalBookingCost") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Total After Discount">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalAfterDis" runat="server" Text='<%#Eval("TotalAfterDis") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgFName" runat="server" Text='<%#Eval("PgFName") %>'></asp:Label>
                                                <asp:Label ID="lbl_PgLName" runat="server" Text='<%#Eval("PgLName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P Mobile">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgMobile" runat="server" Text='<%#Eval("PgMobile") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="P Email">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PgEmail" runat="server" Text='<%#Eval("PgEmail") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Created Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Partner Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("PartnerName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Mode">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Convenience Fee">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle"/>
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  
                <script type="text/javascript">
                    var UrlBase = '<%=ResolveUrl("~/") %>';
                </script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>