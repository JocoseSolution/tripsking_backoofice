﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="PreHold.aspx.vb" Inherits="DetailsPort_HoldPNR_PreHold" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css") %>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript">

        function fareRuleToolTip(id) {
            
            $.ajax({
                type: "POST",
                url: "PreHold.aspx/GetFairRule",
                data: '{paxid: "' + id + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {                    
                    if (msg.d != "") {
                        $("#ddd").html(msg.d);
                        $("#outerdiv").show();

                    }
                    else {
                        alert("Fare Rule Not Available")
                    }
                },
                Error: function (x, e) {
                    alert("error")
                }
            });
        }
    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow-y: scroll;
        }

        .tooltip1 {
            position: relative;
        }

        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            /* Position the tooltip */
            position: absolute;
            z-index: 1;
        }

        .tooltip1:hover .tooltiptext {
            visibility: visible;
        }

        .popupnew2 {
            position: absolute;
            top: 10px;
            left: 7%;
            width: 900PX;
            height: 500px !important;
            z-index: 1;
            box-shadow: 0px 5px 5px #f3f3f3;
            border: 2px solid #004b91;
            background-color: #fff;
            background-color: #ffffff !important;
            padding: 10px 20px;
            overflow-x: hidden;
        }

        .vew321 {
            background-color: #fff;
            width: 75%;
            float: right;
            padding: 5px 10px;
            text-align: justify;
            height: 300px;
            overflow-x: scroll !important;
            overflow-y: scroll !important;
            z-index: 1;
            position: fixed;
            top: 100px;
            left: 20%;
            border: 5px solid #d1d1d1;
        }
    </style>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-12">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="background: #fdb714">
                        <h3 class="panel-title">Flight > Flight Pre Hold Booking Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="input-text full-width" readonly="readonly" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" class="input-text full-width" readonly="readonly" />
                                </div>
                            </div>
                           <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">TransactionId</label>
                                    <asp:TextBox ID="txt_TransactionId" runat="server" CssClass="input-text full-width"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Pax Name</label>

                                    <asp:TextBox ID="txt_PaxName" runat="server" CssClass="input-text full-width"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Ticket No</label>

                                    <asp:TextBox ID="txt_TktNo" runat="server" CssClass="input-text full-width"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="td_Agency" runat="server">
                                    <label for="exampleInputPassword1">
                                        Agency Name
                                    </label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" class="form-control input-text full-width" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                                    <input type="hidden" id="hidtxtAgencyName" class="form-control" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        OrderId
                                    </label>
                                    <asp:TextBox ID="txt_OrderId" runat="server" CssClass="input-text full-width"></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group" id="Partnernameid" runat="server">
                                    <label for="exampleInputEmail1">PartnerName :</label>
                                    <asp:DropDownList CssClass="selector" ID="txtPartnerName" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputEmail1">PaymentMode :</label>
                                <asp:DropDownList CssClass="selector" ID="txtPaymentmode" runat="server">
                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                    <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                         
                        <div class="row">
                          
                          
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="btn btn-info full-width btn-medium" />
                          <hr />
                                    <asp:Button ID="btn_export" runat="server" CssClass="btn btn-info full-width btn-medium" Text="Export"  />
                             
                        </div>
                        <%--<div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Ticket Sale :
                                    </label>
                                    <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>--%>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">
                                        Total Count:
                                    </label>
                                    <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group" style="font-size: 15px; line-height: 20px; text-align: justify; color: Red;">
                                    * N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="ticket_grdview" runat="server" AllowPaging="True" AllowSorting="True"
                                            AutoGenerateColumns="False" CssClass="table table-bordered table-striped"
                                            GridLines="None" PageSize="30">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Booking">
                                                    <ItemTemplate>

                                                        <a href='../admin/Update_BookingOrder.aspx?OrderId=<%#Eval("OrderId")%> &TransID='
                                                            rel="lyteframe" rev="width: 1200px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">Booking&nbsp;Details
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pax&nbsp;Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="PaxType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pax&nbsp;ID">
                                                    <ItemTemplate>
                                                        <a href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=<%#Eval("PaxId")%>'
                                                            rel="lyteframe" rev="width: 900px; height: 500px; overflow:hidden;" target="_blank"
                                                            style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <asp:Label ID="TID" runat="server" Text='<%#Eval("PaxId")%>'></asp:Label>(TktDetail)
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Order&nbsp;ID">
                                                    <ItemTemplate>
                                                        <div class="tag">
                                                            <a href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' rel="lyteframe" class="gridViewToolTip"
                                                                rev="width: 900px; height: 500px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                                                            <div id="tooltip" style="display: none;">
                                                                <div style="float: left;">
                                                                    <table width="100%" cellpadding="11" cellspacing="11" border="0">
                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Refund_Request:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("SubmitDate")%>  
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Accepted:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("AcceptDate")%>  
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td style="width: 110px; font-weight: bold;">Refunded:
                                                                            </td>
                                                                            <td style="width: 166px;">
                                                                                <%#Eval("UpdateDate")%>  
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Pnr" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Ticket&nbsp;No">
                                                    <ItemTemplate>
                                                        <asp:Label ID="TktNo" runat="server" Text='<%#Eval("TicketNumber")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="AgencyID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgencyID" runat="server" Text='<%#Eval("AgencyId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="User&nbsp;ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AgentID" runat="server" Text='<%#Eval("AgentId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Agency&nbsp;Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Excutive&nbsp;ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="ExcutiveID" runat="server" Text='<%#Eval("ExecutiveId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AirLine">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Airline" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Sector" DataField="sector"></asp:BoundField>
                                                <asp:BoundField HeaderText="Net&nbsp;Fare" DataField="TotalAfterDis">
                                                    <ItemStyle HorizontalAlign="center"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Status" DataField="Status"></asp:BoundField>
                                                <asp:TemplateField HeaderText="Modify Status">
                                                    <ItemTemplate>
                                                        <div class="tooltip1">
                                                            <%#Eval("MORDIFYSTATUS")%>
                                                            <span class="tooltiptext"><%#Eval("msg")%></span>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Trip" DataField="Trip"></asp:BoundField>
                                                <asp:BoundField HeaderText="Booking&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" DataField="CreateDate"></asp:BoundField>
                                                <%--<asp:BoundField HeaderText="Partner Name" DataField="PartnerName"></asp:BoundField>--%>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LB_CahngeStatus" runat="server" CommandName="ChangeStatus" CommandArgument='<%#Eval("OrderId")%>' Visible='<%# IsVisible(Eval("Status"))%>'
                                                            Font-Bold="True" Font-Underline="False" ForeColor="Red" OnClientClick="javascript:return validate();">Change Status to Hold</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payment Mode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Convenience Fee">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPGreport" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               <%--  <asp:TemplateField HeaderText="Online Cancel Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCancelstatus" runat="server" Text='<%#Eval("CancelStatus")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                               <asp:TemplateField HeaderText="FareRule">
                                                    <ItemTemplate>
                                                        <span class="fareRuleToolTip">
                                                            <img src='<%#ResolveClientUrl("~/Images/air(ex).png")%>' class="cursorpointer" alt="Click to View Full Details" title="Click to View Full Details" style="height: 20px; cursor: pointer;" onclick="fareRuleToolTip('<%#Eval("PaxId") %>')" />
                                                        </span>
                                                        </div>             
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="Easy Transaction Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEasyTranId" runat="server" Text='<%#Eval("EasyTranscationid")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="PG Transaction Id">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPgTransaId" runat="server" Text='<%#Eval("PGTransactionid")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style="text-decoration-line: initial; display: none" id="outerdiv" class="vew321">
        <div onclick="closethis();" title="click to close" style="background: url(../../Images/closebox.png); cursor: pointer; float: right; z-index: 9999; min-height: 30px; min-width: 30px;"></div>
        <div id="ddd"></div>
    </div>

    <script type="text/javascript">
        function validate() {
            if (confirm("Are you sure you want to change status to hold!")) {
                return true;
            } else {
                return false;
            }
        }
        function closethis() {
            $("#outerdiv").hide();
        }

        $(function () {
            InitializeToolTip();
        });
    </script>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>

