﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="AgentProxyTicket.aspx.vb" Inherits="DetailsPort_Proxy_AgentProxyTicket" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

  <%--  <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>


    <style type="text/css">
        #wrapper {
            width: 100% !important;
            padding: 0 10px;
            margin: 38px auto !important;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#datepicker").datepicker();
            $('.cd').click(function () {
                $("#datepicker").focus();

            });
            $("#datepicker1").datepicker();
            $('.cd1').click(function () {
                $("#datepicker1").focus();

            });
        });
    </script>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>

     <div class="col-md-12 container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">

                        <div class="row " style="margin-left:12px">

                   
                   <div class="row">
                            
                        <div class="col-md-2">
                            <div class="input-group">
                                <input type="text" name="datepicker" id="datepicker" class="form-control input-text lg-width" readonly="readonly" placeholder="REG. FROM :" />
                                <span class="input-group-addon" style="background-color: #49cced">
                                    <span class="fa fa-calendar cd"></span></span>
                            </div>
                        </div>
                    
                    <div class="col-md-2">
                        <div class="input-group">
                            <input type="text" name="datepicker" id="datepicker1" class="form-control input-text lg-width" readonly="readonly" placeholder="REG. TO :" />
                            <span class="input-group-addon" style="background-color: #49cced">
                                <span class="fa fa-calendar cd1"></span>
                            </span>
                        </div>
                    </div>

                       <div id="Div1" class="col-md-2" runat="server">
                        <div class="form-group" id="spn_Projects1" runat="server">

                            <asp:DropDownList ID="ddl_TripType" runat="server" CssClass="input-text full-width">
                                <asp:ListItem Value="Select">--TRIP--</asp:ListItem>
                                <asp:ListItem Value="D">Domestic</asp:ListItem>
                                <asp:ListItem Value="I">International</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2" id="td_Agency" runat="server" style="margin-top: 37px;">
                        <div class="input-group" id="tr_AgencyName" runat="server" style="top: -39px">
                            <div id="tr_Agency" runat="server" class="input-group">
                                <input type="text" placeholder="Brand Name" id="txtAgencyName" name="txtAgencyName" class="form-control input-text full-width" />
                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                <span class="input-group-addon" style="background: #49cced">
                                    <span class="fa fa-black-tie"></span>
                                </span>
                            </div>
                        </div></div>
                        <asp:Button ID="btn_search" runat="server" Text="Submit" CssClass="btn btn-success" Style="margin-left: 15px;" ToolTip="Search by Date" />
                            <asp:Button ID="btn_export" runat="server" Text="Export"  CssClass="btn btn-success" Style="margin-left: 15px;"  ToolTip="Export by Date" />
                    
                    
                    <%--tr><td class="clear1" colspan="4"></td></tr>
                        <tr>
                            <td class="bodytext" style="font-family: arial, Helvetica, sans-serif; font-size: 12px;
                                font-weight: bold;" width="100px">
                                From Date:&nbsp;&nbsp;
                            </td>
                            <td width="120px">
                                <input type="text" name="From" id="From" class="txtCalander" readonly="readonly"
                                    style="width: 100px" />
                            </td>
                            <td class="bodytext" style="font-family: arial, Helvetica, sans-serif; font-size: 12px;
                                font-weight: bold;" width="100px" colspan="1">
                                To Date&nbsp;:&nbsp;&nbsp;
                            </td>
                            <td>
                                <input type="text" name="To" id="To" class="txtCalander" readonly="readonly" style="width: 100px" />
                            </td>
                        </tr>--%>

                    <tr>
                        <td class="clear1"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr id="tr_ExecID" runat="server">
                                    <td align="left" style="font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold"
                                        class="bodytext" width="102px">Exec ID :&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td width="120px">
                                        <asp:DropDownList ID="ddl_ExecID" runat="server" Width="105px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left" style="font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold"
                                        class="bodytext" width="102px">Status :
                                    </td>

                                    <td align="left">
                                        <asp:DropDownList ID="ddl_Status" runat="server" Width="105px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                       

                    
                    <%--<tr><td class="clear1"></td></tr>
            <tr>
            
              <td class="bodytext" 
                    style="font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;" 
                    width="100px">
            Trip
            </td>
            <td width="120px">
            <asp:DropDownList ID="ddl_TripType" runat="server" Width="105px" >
            <asp:ListItem  Value="Select">--Select--</asp:ListItem>
            <asp:ListItem  Value="D">Domestic</asp:ListItem>
            <asp:ListItem  Value="I">International</asp:ListItem>
            </asp:DropDownList>
            </td>
            <td id="td_Agency" runat="server" colspan="2">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="left" style="font-family: arial, Helvetica, sans-serif; font-size: 12px;
                                font-weight: bold" class="bodytext" width="100px">
                                &nbsp;Agency Name :
                            </td>
                            <td align="left">
                                <input type="text" id="txtAgencyName" name="txtAgencyName" style="width: 200px" onfocus="focusObj(this);"
                                    onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID" />
                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                            </td>
                        </tr>
                    </table>
                </td>
       
            </tr>--%>
                    <%--<tr>
                        <td align="right" style="vertical-align: middle; line-height: 29px;" colspan="4">&nbsp;&nbsp;&nbsp;
                  --%>    <%--   </td>
                    </tr>
                    <tr>
                       <td style="color: #FF0000" colspan="4">* N.B: To get Today's booking without above parameter,do not fill any field, &nbsp;
                    only click on search your booking.
                        </td>
                    </tr>
                    </table>--%>

                   </div></div>
   
    <div align="center" style="background-color: #fff; overflow: auto; max-height: 600px;">
        <div class="clear1"></div>
        <table cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <asp:GridView ID="GridProxyDetail" runat="server" AutoGenerateColumns="False" DataKeyNames="ProxyID"
                        OnPageIndexChanging="GridProxyDetail_Changing" Width="100%" CssClass="GridViewStyle"
                        PageSize="30" AllowPaging="True">
                        <Columns>
                            <asp:TemplateField HeaderText="ProxyID">
                                <ItemTemplate>
                                    <a id="ancher" href='PassengerDetail.aspx?ProxyID=<%#Eval("ProxyID")%>'
                                        target="_blank"
                                        style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #800000; font-weight: bold;">
                                        <asp:Label ID="lbl_ProxyID" runat="server" Text='<%#Eval("ProxyID") %>'></asp:Label>(View)</a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AgentID">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ag_Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Ag_Name" runat="server" Text='<%#Eval("Ag_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BookingType">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_BookingType" runat="server" Text='<%#Eval("BookingType") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TravelType">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TravelType" runat="server" Text='<%#Eval("TravelType") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="From">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ProxyFrom" runat="server" Text='<%#Eval("ProxyFrom") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ProxyTo" runat="server" Text='<%#Eval("ProxyTo") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DDate">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_DDate" runat="server" Text='<%#Eval("DepartDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="RDate">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_RDate" runat="server" Text='<%#Eval("ReturnDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Class">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Class" runat="server" Text='<%#Eval("Class") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Airlines">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Airlines" runat="server" Text='<%#Eval("Airlines") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Classes">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Classes" runat="server" Text='<%#Eval("Classes") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PaymentMode">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remark">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Remark" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Alert Message" ItemStyle-ForeColor="Red">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_UpdateRemark" runat="server" Text='<%#Eval("UpdateRemark") %>'
                                        ForeColor="Red" Font-Bold="True"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle ForeColor="Red"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Status" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Trip">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ProxyType" runat="server" Text='<%#Eval("ProxyType") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ExecutiveID">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_ExecutiveID" runat="server" Text='<%#Eval("Exec_ID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="RequestedDate">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_requestDateTime" runat="server" Text='<%#Eval("requestDateTime") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AcceptedDate">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_AcceptedDateTime" runat="server" Text='<%#Eval("AcceptedDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UpdatedDate">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_UpdatedDate" runat="server" Text='<%#Eval("UpdatedDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField HeaderText="Accept">
    <ItemTemplate>
   <asp:LinkButton ID="ITZ_Accept" runat="server" CommandName="Accept" CommandArgument='<%#Eval("ProxyID") %>' Font-Bold="True" Font-Underline="False">Accept</asp:LinkButton>
    </ItemTemplate>
    </asp:TemplateField>--%>
                        </Columns>
                        <RowStyle CssClass="RowStyle" />
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <PagerStyle CssClass="PagerStyle" BackColor="White" />
                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                        <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                        <EditRowStyle CssClass="EditRowStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>

                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>
