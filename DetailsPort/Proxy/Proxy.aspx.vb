﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Object
Imports System.Data

Partial Public Class DetailsPort_Proxy_Proxy

    Inherits System.Web.UI.Page
    Dim STDom As New SqlTransactionDom()
    Dim clsCorp As New ClsCorporate()
    Dim objDA As New SqlTransaction


    Protected Sub btn_Submit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btn_Submit.Click

        Try
            Dim AgencyDs As DataSet
            AgencyDs = objDA.GetAgencyDetails(Session("UID"))
            If AgencyDs.Tables(0).Rows(0)("Agent_Status").ToString.ToUpper.Trim <> "NOT ACTIVE" And AgencyDs.Tables(0).Rows(0)("Online_tkt").ToString.ToUpper.Trim <> "NOT ACTIVE" Then
                Dim FromDate As String = Request("txtDepDate").ToString
                Dim ToDate As String = Request("txtRetDate").ToString
                Dim split_str1 As String() = FromDate.Split("/"c)
                Dim split_str2 As String() = ToDate.Split("/"c)
                Dim FromDEST As String = Request("hidtxtDepCity1").ToString
                Dim ToDEST As String = Request("hidtxtArrCity1").ToString
                Dim split_str3 As String() = FromDEST.Split(","c)
                Dim split_str4 As String() = ToDEST.Split(","c)
                Dim FromCtry As String = split_str3(1)
                Dim ToCtry As String = split_str4(1)

                Session("ProjectId") = If(DropDownListProject.Visible = True, If(DropDownListProject.SelectedValue.ToLower() <> "select", DropDownListProject.SelectedValue, Nothing), Nothing)
                Session("BookedBy") = If(DropDownListBookedBy.Visible = True, If(DropDownListBookedBy.SelectedValue.ToLower() <> "select", DropDownListBookedBy.SelectedValue, Nothing), Nothing)


                If RB_OneWay.Text = "One Way" AndAlso RB_OneWay.Checked = True Then
                    Session("Adult") = ddl_Adult.SelectedItem.Text
                    Session("Child") = ddl_Child.SelectedItem.Text
                    Session("Infrant") = ddl_Infrant.SelectedItem.Text
                    Session("A") = True
                    Session("BookingType") = RBL_Booking.SelectedItem.Text
                    Session("OneWay") = RB_OneWay.Text
                    'Add only City Code
                    Session("From") = split_str3(0)
                    Session("To") = split_str4(0)
                    'Added only Country Codes to Session'
                    Session("FromCtry") = FromCtry
                    Session("ToCtry") = ToCtry
                    Session("DepartDD") = split_str1(0) 'Splitting Departure Date in one way
                    Session("DepartMM") = split_str1(1)
                    Session("DepartYYYY") = split_str1(2).Substring(2, 2) 'ddldepart_YYYY.SelectedValue
                    Session("DepartAnyTime") = ddl_DepartAnytime.SelectedItem.Text
                    Session("Class") = ddl_Class.SelectedItem.Text
                    Session("Airline") = Right((Request("hidtxtAirline")), 2)
                    Session("Classes") = ddl_Classes.SelectedItem.Text
                    Session("PaymentMode") = ddl_PaymentMode.SelectedValue
                    Session("Remark") = txt_Remark.Text

                    Response.Redirect("ProxyEmp.aspx")
                Else

                    Session("Adult") = ddl_Adult.SelectedItem.Text
                    Session("Child") = ddl_Child.SelectedItem.Text
                    Session("Infrant") = ddl_Infrant.SelectedItem.Text
                    Session("A") = False
                    Session("BookingType") = RBL_Booking.SelectedItem.Text
                    Session("RoundTrip") = RB_RoundTrip.Text
                    'Splitting Departure Date and Return in 2 way
                    Session("From") = split_str3(0)                 'ddl_From.SelectedValue
                    Session("To") = split_str4(0)                   'ddl_To.SelectedValue
                    Session("DepartDD") = split_str1(0)             'ddldepart_DD.SelectedValue
                    Session("DepartMM") = split_str1(1)             'ddldepart_MM.SelectedValue
                    Session("DepartYYYY") = split_str1(2).Substring(2, 2) 'ddldepart_YYYY.SelectedValue
                    Session("DepartAnyTime") = ddl_DepartAnytime.SelectedItem.Text
                    'Splitting Return Date
                    Session("ToDD") = split_str2(0)                     'ddlto_DD.SelectedValue
                    Session("ToMM") = split_str2(1)                     'ddlto_MM.SelectedValue
                    Session("ToYYYY") = split_str2(2).Substring(2, 2)   'ddlto_YYYY.SelectedValue
                    Session("ReturnAnyTime") = ddl_ReturnAnytime.SelectedItem.Text
                    Session("Class") = ddl_Class.SelectedItem.Text
                    Session("Airline") = Right((Request("hidtxtAirline")), 2)        'ddl_Airlines.SelectedValue
                    Session("Classes") = ddl_Classes.SelectedItem.Text
                    Session("PaymentMode") = ddl_PaymentMode.SelectedValue
                    Session("Remark") = txt_Remark.Text
                    'Added only Country Codes to Session'
                    Session("FromCtry") = FromCtry
                    Session("ToCtry") = ToCtry
                    Response.Redirect("ProxyEmp.aspx")
                End If

            Else
                ShowAlertMessage("You are not authorized. Please contact to your sales person")
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub


    Public Shared Sub ShowAlertMessage(ByVal [error] As String)
        Try


            Dim page As Page = TryCast(HttpContext.Current.Handler, Page)
            If page IsNot Nothing Then
                [error] = [error].Replace("'", "'")
                ScriptManager.RegisterStartupScript(page, page.[GetType](), "err_msg", "alert('" & [error] & "');window.location='Proxy.aspx';", True)
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If

            If Not IsPostBack Then
                Dim ds As DataSet = clsCorp.Get_Corp_Project_Details_By_AgentID(Session("UID").ToString(), Session("User_Type"))
                If ds Is Nothing Then

                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        DropDownListProject.Items.Clear()
                        Dim item As New ListItem("Select")
                        DropDownListProject.AppendDataBoundItems = True
                        DropDownListProject.Items.Insert(0, item)
                        DropDownListProject.DataSource = ds.Tables(0)
                        DropDownListProject.DataTextField = "ProjectName"
                        DropDownListProject.DataValueField = "ProjectId"
                        DropDownListProject.DataBind()
                        Dim dtbooked As New DataTable
                        dtbooked = clsCorp.Get_Corp_BookedBy(Session("UID").ToString(), "BB").Tables(0)
                        If ds.Tables(0).Rows.Count > 0 Then
                            DropDownListBookedBy.AppendDataBoundItems = True
                            DropDownListBookedBy.Items.Clear()
                            DropDownListBookedBy.Items.Insert(0, "Select")
                            DropDownListBookedBy.DataSource = dtbooked
                            DropDownListBookedBy.DataTextField = "BOOKEDBY"
                            DropDownListBookedBy.DataValueField = "BOOKEDBY"
                            DropDownListBookedBy.DataBind()
                        End If

                        TBL_Projects.Visible = True
                    Else
                        TBL_Projects.Visible = False

                    End If

                End If
                'Me.ddldepart_MM.SelectedValue = DateTime.Now.Month.ToString()
                'Me.ddldepart_DD.SelectedValue = DateTime.Now.Day.ToString()
                'Me.ddldepart_YYYY.SelectedValue = DateTime.Now.Year.ToString()
                'Me.ddlto_MM.SelectedValue = DateTime.Now.Month.ToString()
                'Me.ddlto_DD.SelectedValue = DateTime.Now.Day.ToString()
                'Me.ddlto_YYYY.SelectedValue = DateTime.Now.Year.ToString()
                'ddlto_MM.Enabled = False
                'ddlto_DD.Enabled = False
                'ddlto_YYYY.Enabled = False
                'ddl_ReturnAnytime.Enabled = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try


    End Sub
End Class