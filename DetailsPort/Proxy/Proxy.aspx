﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="Proxy.aspx.vb" Inherits="DetailsPort_Proxy_Proxy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />

    <script src="../../JS/JScript.js" type="text/javascript"></script>

    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />


    <script type="text/javascript">

        function Show(obj) {

            if (obj.checked) {
                document.getElementById("txtRetDate").style.display = "block";
                document.getElementById("td_ret").style.display = "block";
                document.getElementById("td_time").style.display = "block";
                document.getElementById("ctl00_ContentPlaceHolder1_ddl_ReturnAnytime").style.display = "block";
            }
        }
        function Hide(obj) {
            if (obj.checked) {
                document.getElementById("txtRetDate").style.display = "none";
                document.getElementById("td_ret").style.display = "none";
                document.getElementById("td_time").style.display = "none";
                document.getElementById("ctl00_ContentPlaceHolder1_ddl_ReturnAnytime").style.display = "none";
            }


        }


    </script>
    
    <div class="w70 auto boxshadow">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table class="w100">
                    <tr>

                        <td align="left" style="color: #fff; font-weight: bold; padding-top: 7px;" colspan="4">Domestic/International Proxy Request
                        </td>
                    </tr>
                    <tr>
                        <td class="clear1" colspan="4"></td>
                    </tr>
                    <tr>
                        <td align="center" class="text1" width="150px">Trip Type
                        </td>
                        <td width="300" align="left">
                            <asp:RadioButton ID="RB_OneWay" runat="server" Checked="true" GroupName="Trip" onclick="Hide(this)"
                                Text="One Way" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <asp:RadioButton ID="RB_RoundTrip" runat="server" GroupName="Trip" onclick="Show(this)"
                                                                                Text="Round Trip" />
                        </td>
                        <td class="text1">Booking :
                        </td>
                        <td class="Text">
                            <asp:RadioButtonList ID="RBL_Booking" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True">Proxy</asp:ListItem>
                                <asp:ListItem>LTC</asp:ListItem>
                                <asp:ListItem>Group</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td class="Text">&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td>&nbsp;
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td height="30px" width="100px" class="text1">From
                                    </td>
                                    <td class="TextBig" width="200px">
                                        <input id="txtDepCity1" name="txtDepCity1" type="text" />
                                        <input type="hidden" id="hidtxtDepCity1" name="hidtxtDepCity1" value="" />
                                    </td>
                                    <td width="60px" class="text1">To
                                    </td>
                                    <td class="TextBig">
                                        <input id="txtArrCity1" name="txtArrCity1" type="text" />
                                    </td>
                                    <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" value="" />
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="clear1"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="100px" class="text1">Departure
                                    </td>
                                    <td height="30px" class="TextBig" width="200px">
                                        <input type="text" name="txtDepDate" id="txtDepDate" class="txtCalander" value=""
                                            readonly="readonly" />
                                        <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDate" value="" />
                                    </td>
                                    <td width="60px" class="text1">
                                        <div id="td_ret">
                                            Return
                                        </div>
                                    </td>
                                    <td class="Text" style="font-weight: bold" align="left">
                                        <input type="text" name="txtRetDate" id="txtRetDate" class="txtCalander" value=""
                                            readonly="readonly" />
                                        <input type="hidden" name="hidtxtRetDate" id="hidtxtRetDate" value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="clear1"></td>
                                </tr>
                                <tr>
                                    <td class="text1">Time
                                    </td>
                                    <td height="30px" width="200px">
                                        <asp:DropDownList ID="ddl_DepartAnytime" runat="server" Width="160px" CssClass="Text">
                                            <asp:ListItem>Anytime</asp:ListItem>
                                            <asp:ListItem>Morning(0.00-12:00)</asp:ListItem>
                                            <asp:ListItem>Midday(10:00-14:00)</asp:ListItem>
                                            <asp:ListItem>Afternoon(12:00-17:00)</asp:ListItem>
                                            <asp:ListItem>Evening(17:00-20:00)</asp:ListItem>
                                            <asp:ListItem>Night(20:00-23:59)</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td width="60px" class="text1">
                                        <div id="td_time">
                                            Time
                                        </div>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_ReturnAnytime" runat="server" Width="160px" CssClass="Text">
                                            <asp:ListItem>Anytime</asp:ListItem>
                                            <asp:ListItem>Morning(0.00-12:00)</asp:ListItem>
                                            <asp:ListItem>Midday(10:00-14:00)</asp:ListItem>
                                            <asp:ListItem>Afternoon(12:00-17:00)</asp:ListItem>
                                            <asp:ListItem>Evening(17:00-20:00)</asp:ListItem>
                                            <asp:ListItem>Night(20:00-23:59)</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>


                    <tr>
                        <td colspan="4" class="clear1"></td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td height="30px" class="text1">Adult(>12 yrs)
                                    </td>
                                    <td class="text1">Child(2 - 11 yrs)
                                    </td>
                                    <td class="text1">Infant(Under 2 yrs)
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30px" width="200px">
                                        <asp:DropDownList ID="ddl_Adult" runat="server" Width="60px" CssClass="Text">
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td width="200px">
                                        <asp:DropDownList ID="ddl_Child" runat="server" Width="60px" CssClass="Text">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_Infrant" runat="server" Width="60px" CssClass="Text">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" class="clear1"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td height="30px" class="text1">Class
                                    </td>
                                    <td class="text1">Prefered Airlines
                                    </td>
                                    <td class="text1">Classes
                                    </td>
                                    <td class="text1">Payment Mode
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30px" width="200px">
                                        <asp:DropDownList ID="ddl_Class" runat="server" Width="160px" CssClass="Text">
                                            <asp:ListItem>Economy</asp:ListItem>
                                            <asp:ListItem>Non Refundable Economy</asp:ListItem>
                                            <asp:ListItem>Refundable Economy</asp:ListItem>
                                            <asp:ListItem>Full Fare Economy</asp:ListItem>
                                            <asp:ListItem>Business</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td width="200px">
                                        <input type="text" name="txtAirline" value="" id="txtAirline" />
                                        <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_Classes" runat="server" Width="60px" CssClass="Text">
                                            <asp:ListItem>All</asp:ListItem>
                                            <asp:ListItem>A</asp:ListItem>
                                            <asp:ListItem>B</asp:ListItem>
                                            <asp:ListItem>C</asp:ListItem>
                                            <asp:ListItem>D</asp:ListItem>
                                            <asp:ListItem>E</asp:ListItem>
                                            <asp:ListItem>F</asp:ListItem>
                                            <asp:ListItem>G</asp:ListItem>
                                            <asp:ListItem>H</asp:ListItem>
                                            <asp:ListItem>I</asp:ListItem>
                                            <asp:ListItem>J</asp:ListItem>
                                            <asp:ListItem>K</asp:ListItem>
                                            <asp:ListItem>L</asp:ListItem>
                                            <asp:ListItem>M</asp:ListItem>
                                            <asp:ListItem>N</asp:ListItem>
                                            <asp:ListItem>O</asp:ListItem>
                                            <asp:ListItem>P</asp:ListItem>
                                            <asp:ListItem>Q</asp:ListItem>
                                            <asp:ListItem>R</asp:ListItem>
                                            <asp:ListItem>S</asp:ListItem>
                                            <asp:ListItem>T</asp:ListItem>
                                            <asp:ListItem>U</asp:ListItem>
                                            <asp:ListItem>V</asp:ListItem>
                                            <asp:ListItem>W</asp:ListItem>
                                            <asp:ListItem>X</asp:ListItem>
                                            <asp:ListItem>Y</asp:ListItem>
                                            <asp:ListItem>Z</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddl_PaymentMode" runat="server" Width="100px" CssClass="Text">
                                            <asp:ListItem Value="CL">Cash Limit</asp:ListItem>
                                            <asp:ListItem Value="CL">Card Limit</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="clear1"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table id="TBL_Projects" runat="server" border="0" cellpadding="0" cellspacing="0"
                                width="100%">
                                <tr>
                                    <td width="200px" class="text1" height="30px">Project Id :
                                    </td>
                                    <td width="200px">
                                        <asp:DropDownList ID="DropDownListProject" runat="server" CssClass="drpBox">
                                        </asp:DropDownList>
                                    </td>
                                    <td width="180px" class="text1">Booked By :
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListBookedBy" runat="server" CssClass="drpBox">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="clear1"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <table width="100%">
                                <tr>
                                    <td height="30px" class="text1">Remark :
                                    </td>
                                    <td style="padding-left: 15px; padding-right: 10px;">
                                        <asp:TextBox ID="txt_Remark" runat="server" Height="40px" TextMode="MultiLine" Width="488px"></asp:TextBox>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" class="clear1"></td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Button ID="btn_Submit" runat="server" Text="Submit" OnClientClick="return Proxy();"
                                CssClass="button buttonBlue" /></td>
                    </tr>
                    <tr>
                        <td align="center" height="35px" colspan="4">
                            <span style="font-family: arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #FF3300; height: 20px;">Note: Cash agents will be charged Rs. 50 extra in
                                                                    case of proxy being issued without sufficient balance.</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" class="clear1"></td>
                    </tr>

                </table>


            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">

        var myDate = new Date();
        var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
        document.getElementById("txtDepDate").value = currDate;
        document.getElementById("hidtxtDepDate").value = currDate;
        document.getElementById("txtRetDate").value = currDate;
        document.getElementById("hidtxtRetDate").value = currDate;
        var UrlBase = '<%=ResolveUrl("~/") %>';
        document.getElementById("txtRetDate").style.display = "none";
        document.getElementById("td_ret").style.display = "none";
        document.getElementById("td_time").style.display = "none";
        document.getElementById("ctl00_ContentPlaceHolder1_ddl_ReturnAnytime").style.display = "none";

    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Common.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Search3.js") %>"></script>

</asp:Content>
