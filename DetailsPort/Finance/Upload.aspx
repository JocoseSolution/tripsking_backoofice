﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="Upload.aspx.vb" Inherits="DetailsPort_Finance_Upload" %>
<%--<%@ Register Src="~/UserControl/RequestControl.ascx" TagPrefix="uc1" TagName="RequestControl" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <script src="../../JS/JScript.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        myfunction = function () {
            debugger;
            $('#slidediv').toggle('slide', { direction: 'right' }, 0);
            $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);


        }
        $("#Div_close").click(function () {
            $('#slidediv').toggle('slide', { direction: 'left' }, 0);
            $("#backgroundPopup").fadeOut("normal");
            popupStatus = 0;  // and set value to 0
        });


</script>

    <style type="text/css">
        .buttonBlue {
            background: #d9534f;
        }

        .button {
            position: relative;
            display: inline-block;
            padding: 10px 24px;
            margin: .3em 0 1em 0;
            /* width: 100%; */
            vertical-align: middle;
            color: #fff;
            font-size: 16px;
            line-height: 20px;
            border-radius: 2px;
            -webkit-font-smoothing: antialiased;
            text-align: center;
            letter-spacing: 1px;
            /*background: transparent;*/
            background: #d9534f;
            border: 0;
            border-bottom: 2px solid #af3e3a;
            cursor: pointer;
            -webkit-transition: all 0.15s ease;
            transition: all 0.15s ease;
        }
    </style>

    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function validateAmt() {
            var val = document.getElementById("txt_crd_val").value;
            var r = document.getElementById("txt_rm").value;

            if (r == "") {
                alert("Please Enter Remark."); return false;
            }
            if (val == "") {
                alert("Please Enter Amount."); return false;

            }
            try {
                var mtype = document.getElementById("ddl_moduletype").value;
                if (mtype == "--Select--") {
                    alert("Please Select Module Type."); return false;

                }

            }
            catch (err) {
            }

            //if (val > 500000) {
            //    alert("Maximum update amount is 5 lacs."); return false;
            //}

            if (confirm("Are you sure??"))
                return true;
            return false;

        }
        function checkCreditTrasac(count, record) {

            var val = document.getElementById("txt_crd_val").value;
            var r = document.getElementById("txt_rm").value;
            //var mtype = document.getElementById("ddl_moduletype").value;
            //var mtype = document.getElementById("<%= ddl_moduletype.ClientID%>"); //document.getElementById("ddl_moduletype").value;
            if (r == "") {
                alert("Please Enter Remark."); return false;
            }
            //For Narration
            try {
                var utype = document.getElementById("ddl_uploadtype").value;
                alert(utype);

                if (utype == "CA") {
                    var Bank = document.getElementById("ddl_banklist").value;
                    var Narration = document.getElementById("txt_narration").value;
                    if (Bank == "--Select--") {
                        alert("Please Select Bank."); return false;

                    }
                    if (Narration == "") {
                        alert("Please Enter Narration."); return false;

                    }
                }



            }
            catch (err) {
                alert('err');
            }
            //End Narration
            if (val == "") {
                alert("Please Enter Amount."); return false;

            }
            try {
                var mtype = document.getElementById("ddl_moduletype").value;
                if (mtype == "--Select--") {
                    alert("Please Select Module Type."); return false;

                }


            }
            catch (err) {
            }

            //if (val > 500000) {
            //    alert("Maximum update amount is 5 lacs."); return false;
            //}

            if (confirm("Are you sure??")) {



                if (count > 0) {

                    if (confirm("Agent already on credit. Are you sure?")) {

                        if (record != "") {
                            if (confirm(record))

                                return true;
                            else
                                return false;

                        }

                    }
                    else {
                        return false;
                    }
                }
                else {

                    if (record != "") {
                        if (confirm(record))

                            return true;
                        else
                            return false;

                    }
                }
            }
            else {
                return false;
            }



        }

        function MyFunc() {

            debugger;
            var val = document.getElementById('<%= msg_show.ClientID%>').value;

            if (val.length > 5) {
                window.close();
                alert(val);

                document.getElementById('<%= msg_show.ClientID%>').value = "";
                window.open('ProcessAccount.aspx', '_parent');

            }
        }
        function MyFunc1() {
            debugger;
            var val = document.getElementById('<%= msg_show.ClientID%>').value;

            if (val.length > 5) {
                window.close();
                alert(val);
                document.getElementById('<%= msg_show.ClientID%>').value = "";
                window.open('Upload.aspx', '_parent');

            }
        }
    </script>
     <script type="text/javascript">
         function validateAmtminus() {
             var val = document.getElementById("txt_crd_val").value;
             var r = document.getElementById("txt_rm").value;

             if (r == "") {
                 alert("Please Enter Remark."); return false;
             }
             if (val == "") {
                 alert("Please Enter Amount."); return false;

             }
             try {
                 var mtype = document.getElementById("ddl_moduletype").value;
                 if (mtype == "--Select--") {
                     alert("Please Select Module Type."); return false;

                 }

             }
             catch (err) {
             }

             //if (val > 500000) {
             //    alert("Maximum update amount is 5 lacs."); return false;
             //}

             if (confirm("Are you sure??"))
                 return true;
             return false;

         }
         function checkCreditTrasac(count, record) {

             var val = document.getElementById("txt_crd_val").value;
             var r = document.getElementById("txt_rm").value;
             //var mtype = document.getElementById("ddl_moduletype").value;
             //var mtype = document.getElementById("<%= ddl_moduletype.ClientID%>"); //document.getElementById("ddl_moduletype").value;
             if (r == "") {
                 alert("Please Enter Remark."); return false;
             }

             if (val == "") {
                 alert("Please Enter Amount."); return false;

             }
             try {
                 var mtype = document.getElementById("ddl_moduletype").value;
                 if (mtype == "--Select--") {
                     alert("Please Select Module Type."); return false;

                 }


             }
             catch (err) {
             }

             //if (val > 500000) {
             //    alert("Maximum update amount is 5 lacs."); return false;
             //}

             if (confirm("Are you sure??")) {



                 if (count > 0) {

                     if (confirm("Agent already on credit. Are you sure?")) {

                         if (record != "") {
                             if (confirm(record))

                                 return true;
                             else
                                 return false;

                         }

                     }
                     else {
                         return false;
                     }
                 }
                 else {

                     if (record != "") {
                         if (confirm(record))

                             return true;
                         else
                             return false;

                     }
                 }
             }
             else {
                 return false;
             }



         }
    </script>
    <style type="text/css">
        .panel-body {
            height: 100%;
            overflow: inherit;
        }

        #slidediv {
            color: #F25022;
            background-color: #fff;
            border: 2px solid #00A4EF;
            display: none;
        }

            #slidediv p {
                margin: 15px;
                font-size: 0.917em;
            }

        #contentdiv {
            clear: both;
            margin: 0 auto;
            max-width: initial;
        }
    </style>

<style>
    .overlay {
        position: fixed;
        top: 136px;
        left: 398px;
        right: 0;
        bottom: 0;
        width: auto;
        height: 72%;
        background: #fff;
        opacity: 1.5;
        z-index: 1;
        display: inline-block;
    }

    #backgroundPopup {
        z-index: 3;
        position: fixed;
        display: none;
        height: 100%;
        width: 100%;
        background: #0000009e;
        top: 0px;
        left: 0px;
    }
</style>    
    
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <%--<link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />--%>
 <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
         <div id="backgroundPopup">
    </div>
      <div class="mtop80"></div>

    
    <%--<div class="large-2 medium-2 small-12 columns">
        <uc1:RequestControl runat="server" ID="RequestControl" />
            </div>--%>
    <div  id="slidediv"  class="model overlay" style="z-index: 5;">
        <div>
         <asp:HiddenField ID="msg_show" runat="server" Value=""></asp:HiddenField>
       <%-- <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UP" runat="server">
            <ContentTemplate>--%>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbl_Upload" runat="server">
                    <tr>
                        <td>
                            <div style="padding-top: 5px; padding-bottom: 5px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="padding-right: 1px">
                                                 <h2 style="background-color:aliceblue;text-align:center">   AGENCY Infromation</h2>
                                                <table border="0" cellpadding="2" cellspacing="2" width="100%" style="    margin-left: 10px;">
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="h2" height="25" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;" width="120">
                                                                        USER ID :
                                                                    </td>
                                                                    <td id="td_AgentID" runat="server" width="130" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;" width="100">
                                                                       AGENCY NAME :
                                                                    </td>
                                                                    <td id="td_AgencyName" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                    <td colspan="4" class="h2" height="25" style="font-family: arial, Helvetica, sans-serif;
                                                                        font-weight: bold; color: #000000;">
                                                                        ADDRESS :<asp:Label ID="lblAdd1" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;"></asp:Label> &nbsp
                                                                        <asp:Label ID="lblAdd2" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    
                                                                </tr>
                                                                <%--<tr>
                                                                    <td colspan="4" id="td_Address" runat="server" height="10" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" id="td_Address1" runat="server" height="10" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                </tr>--%>
                                                                 <tr>
                                                                    <td class="h2" height="25" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;width:129px">
                                                                        TOTAL BALANCE :
                                                                    </td>
                                                                       <td id="td_Aval_Bal" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                   
                                                                    <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;width:130px">
                                                                      DUE AMOUNT :
                                                                    </td>
                                                                  
                                                                      <td style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;"> 
                                                                          <asp:Label ID="lblDueAmount" runat="server"></asp:Label>&nbsp;&nbsp;
                                                                        </td>
                                                                     <td> <span style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;margin-left:-1px">CREDIT LIMIT :</span></td> 
                                                                          <td style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                           <asp:Label ID="LblCreditLimit" runat="server" ></asp:Label>
                                                                   </td>
                                                                </tr>

                                                                
                                                                <tr>
                                                                    <td class="h2" height="25" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        AGENCY TYPE :
                                                                    </td>
                                                                   <td style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                       <asp:Label ID="Label1" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        PAN NO :
                                                                    </td>
                                                                    <td id="td_pan" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                    <td class="h2" height="25" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        MOBILE NO :
                                                                    </td>
                                                                    <td id="td_Mobile" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <br />
                                                                    <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        EMAIL :
                                                                    </td>
                                                                    <td id="td_Email" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                      <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;"
                                                                        align="left" width="100">
                                                                        AMOUNT :
                                                                    </td>
                                                                    <td align="left" width="125" style="padding-top: 17px">
                                                                        <asp:TextBox ID="txt_crd_val" runat="server" MaxLength="8" oncopy="return false" onpaste="return false" onkeypress="return isNumberKey(event)"
                                                                            Width="120px" BackColor="#F0F0F0"></asp:TextBox>
                                                                        &nbsp;&nbsp;
                                                                    </td>
                                                                     
                                                                    <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;
                                                                        padding-left: 4px;" width="90px">
                                                                        UPLOAD TYPE :
                                                                    </td>
                                                                    <td width="105px">
                                                                        <asp:DropDownList ID="ddl_uploadtype" runat="server" Width="100px" AutoPostBack="True">
                                                                            <asp:ListItem Selected="True" Value="CA">Cash</asp:ListItem>
                                                                            <%--<asp:ListItem Value="CR">Credit</asp:ListItem>--%>
                                                                            <asp:ListItem Value="CC">Card</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td><br /><br />
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;"
                                                                        align="left" class="h2" width="100">
                                                                        REMARK :
                                                                    </td>
                                                                    <td align="left" width="320px">
                                                                        <asp:TextBox ID="txt_rm" runat="server" TextMode="MultiLine" Rows="3" Columns="25"
                                                                            Width="300px" BackColor="#F0F0F0"></asp:TextBox>
                                                                    </td>
                                                                    <td align="center" class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;" width="90px">
                                                                       
                                                                    </td>
                                                                    <td>
                                                                        <div style="display:none"><asp:TextBox ID="txt_Yatra" runat="server" Width="100px"></asp:TextBox></div>
                                                                    </td>
                                                                    <td id="td_BankDetails" runat="server"  style="display:none">
                                                                        <table width="100%" border="0" cellpadding="10" cellspacing="10">
                                                                            <tr>
                                                                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;display:none"
                                                                                    align="left" class="h2" width="105">
                                                                                    BANK NAME :
                                                                                </td>
                                                                                <td style="display:none">
                                                                                    <asp:DropDownList ID="ddl_banklist" runat="server" Width="100px">
                                                                                        <asp:ListItem Selected="True" Value="">--Select--</asp:ListItem>
                                                                                        
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;display:none"
                                                                                    align="left" class="h2" width="100">
                                                                                    NARRATION :
                                                                                </td>
                                                                                <td style="display:none">
                                                                                    <asp:TextBox ID="txt_narration" runat="server" Width="100px" Text="KT"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                 
                                                                    <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;
                                                                        padding-left: 10px;" width="250px" id="td_moduletype" runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;
                                                                                    padding-left: 10px;" width="90px">
                                                                                    MODULE TYPE :
                                                                                </td>
                                                                                <td width="105px">
                                                                                    <asp:DropDownList ID="ddl_moduletype" runat="server" Width="100px">
                                                                                        <%--<asp:ListItem Selected="True" Value="--Select--">--Select--</asp:ListItem>--%>
                                                                                        <asp:ListItem Value="251010/UPL">Upload</asp:ListItem>
                                                                                        <asp:ListItem Value="162001/CCR">Card</asp:ListItem>
                                                                                        <asp:ListItem Value="251011/UPL">Other</asp:ListItem>
                                                                                        
                                                                                       <%-- <asp:ListItem Value="450017/UPL">Incentive/Commission</asp:ListItem>
                                                                                        <asp:ListItem Value="162012/UPL">Invoice</asp:ListItem>
                                                                                        <asp:ListItem Value="162013/UPL">Rail</asp:ListItem>
                                                                                        <asp:ListItem Value="162014/UPL">Recharge</asp:ListItem>
                                                                                        <asp:ListItem Value="162014/UPL">Bus</asp:ListItem>                                                                                       
                                                                                        <asp:ListItem Value="450017/UPL">Stax/TDS</asp:ListItem>--%> 
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td id="td_spl" runat="server" width="160px">
                                                                        <table>
                                                                            <tr>
                                                                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;
                                                                                    padding-left: 10px; padding-right: 5px;">
                                                                                    SPL.UPLOAD
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkSpl" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                  
                                                                    <td valign="middle" style="float:right;margin-right: 55px">
                                                                        <asp:Button ID="plus" runat="server" Text="Credit" CssClass="btn btn-success" />
                                                                        <%--<asp:ImageButton ID="plus" runat="server" ImageUrl="../../Images/btn_plus.gif" />--%>
                                                                        <asp:ImageButton ID="minus_Series" runat="server" ImageUrl="../../Images/btn_minus.gif"
                                                                            OnClientClick="return validateAmt();" Visible="False" />
                                                                   
                                                                         <asp:Button ID="minus" runat="server" Text="Debit" OnClientClick="return validateAmtminus();" CssClass="btn btn-success" />
                                                                     &nbsp 
                              <asp:Button ID="cancle_update" runat="server"  Text="Cancel" CssClass="btn btn-danger" />
                       <%--  <asp:ImageButton ID="minus" runat="server" ImageUrl="../../Images/btn_minus.gif"
                                                                            OnClientClick="return validateAmtminus();" />--%>
                                                                    </td>
                                                                       
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                           
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td id="td_msg" runat="server" visible="false" align="center" height="300px" class="SubHeading"
                            valign="middle">
                        </td>
                    </tr>
                </table>
        <%--    </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
            <ProgressTemplate>
                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden;
                    padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5;
                    z-index: 1000;">
                </div>
                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center;
                    z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px;
                    font-weight: bold; color: #000000">
                    Please Wait....<br />
                    <br />
                    <img alt="loading" src="<%= ResolveUrl("~/images/loading_bar.gif")%>" />
                    <br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
    </div>
         <asp:HiddenField ID="hidTransType" runat="server" />
        </div>

   




    <div class="row">
        <div class="col-md-2"></div>

        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
    
                    <div class="panel-body">
    <div class="large-9 medium-9 small-12 columns end">
        <div class="large-12 medium-12 small-12 heading">             
        <div class="clear1"></div>
        
                
                    
                            
                                            <table border="0" cellpadding="0" cellspacing="0" style=" float:left;" id="tblUploadType" runat="server"> 
                                                <tr>
                                                    <td style="padding: 10px 5px 10px 5px; font-weight: bold; font-family: arial, Helvetica, sans-serif;
                                                        font-size: 13px;" width="125px" align="left">
                                                        UPLOAD TYPE :
                                                    </td>
                                                    <td align="left">
                                                        <fieldset style="width: 140px;">
                                                            <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                                                CellPadding="2" CellSpacing="2" Font-Size="12px" Font-Names="Arial" Width="140px">
                                                            </asp:RadioButtonList>
                                                        </fieldset>
                                                    </td>
                                                </tr>
                                                <tr id="tr_Cat" runat="server">
                                                    <td style="padding: 10px 5px 10px 5px; font-weight: bold; font-family: arial, Helvetica, sans-serif;
                                                        font-size: 13px;" align="left">
                                                        UPLOAD CATEGORY :
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="ddl_Category" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                  
              
         
        <table cellpadding="0" cellspacing="0" style="width:90%;">

        <tr id="tr_search" runat="server" visible="false">
      
            <td valign="top" style="width:100%;">
               
                    <table>
                    <tr>
                      <div class="col-md-3" id="txtSearch" runat="server" style="margin-top: 37px;text-align:center" >
                                                            <div class="input-group"  id="tr_AgencyName" runat="server" style="top:-39px;left:319px">
                                                                <div id="tr_Agency" runat="server" class="input-group">
                                                                <input type="text" placeholder="AGENCY NAME" id="txtAgencyName" autocomplete="off" name="txtAgencyName" class="form-control input-text full-width" />
                                                                <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                                                <span class="input-group-addon" style="background: #49cced">
                                                                    <span class="fa fa-black-tie"></span>
                                                                </span>
                                                            </div> </div>
                            </div>
                               <div style="text-align:center">                        
                        <asp:Button ID="btn_search" runat="server" CssClass="btn btn-success" Text="Go" style="Width:74px"/>
                     </div>
                  
                       </tr>

                    </table>
                
            </td>
         
        </tr>

            </table>

        <div class="clear"></div>

            </div>

          <div class="clear1"></div>
        <table class="gridwidth1 large-10 medium-10 small-10">
        <tr>
            <td style="padding-top: 10px">
                <asp:GridView ID="GridView1" runat="server"  AutoGenerateColumns="False" DataKeyNames="counter"
                    AllowPaging="True" AllowSorting="True" CssClass="table" OnPageIndexChanging="GridView1_PageIndexChanging"
                    PageSize="25"  Width="100%" style="background-color: #fff;text-transform: uppercase; overflow: auto; max-height: 500px;" OnRowCommand="GridView1_RowCommand" >
                    <Columns>
                        <asp:BoundField DataField="counter" HeaderText="Sr.No" ReadOnly="True" />


                         <asp:TemplateField HeaderText="DEBIT NOTE">
                            <ItemTemplate>
                              <asp:LinkButton ID ="lbl_debit" runat ="server"  CssClass="btn btn-info" CommandArgument='<%#Eval("user_id")%>' CommandName ="DEBIT"  Text ="DEBIT"></asp:LinkButton>     
                                 
                                  <%-- <a target="_blank" href="UploadCredit.aspx?AgentID=<%#Eval("user_id")%>&Action=DEBIT" rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;
                                                color: #161946">Debit</a>--%>
                                
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CREDIT NOTE">
                            <ItemTemplate>    
                                <asp:LinkButton ID ="lbl_credit" runat ="server" CommandArgument='<%#Eval("user_id")%>' CommandName ="CREDIT" Text ="CREDIT" CssClass="btn btn-info"></asp:LinkButton>                               
                                <%--    <a target="_blank" href="UploadCredit.aspx?AgentID=<%#Eval("user_id")%>&Action=CREDIT" rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;
                                                color: #161946">Credit</a>--%>
                                
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="AGENCY NAME">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstName" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User_Id">
                            <ItemTemplate>
                              <asp:Label ID="lbl_uid" runat="server" Text='<%#Eval("user_id")%>' Font-Bold="True" ForeColor="#161946"></asp:Label>
                                   <%-- <a target="_blank" href="UploadCredit.aspx?AgentID=<%#Eval("user_id")%>" rel="lyteframe" rev="width: 900px; height: 280px; overflow:hidden;" target="_blank"
                                                style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;
                                                color: #161946">
                                        <asp:Label ID="lbl_uid" runat="server" Text='<%#Eval("user_id")%>' Font-Bold="True" ForeColor="#161946"></asp:Label></a>
                                --%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--Crd_Limit-AgentLimit as Balance,--%>
                        <asp:BoundField DataField="AgencyId" HeaderText="Agency Id" />
                        <asp:BoundField DataField="Balance" HeaderText="Balance" />
                        <asp:BoundField DataField="DueAmount" HeaderText="Due_Amount" />
                        <asp:BoundField DataField="AgentLimit" HeaderText="CreditLimit" />                         
                        <asp:BoundField DataField="Mobile" HeaderText="Mobile" />
                        <asp:BoundField DataField="Email" HeaderText="Email" />                    
                        <asp:BoundField DataField="SalesExecID" HeaderText="SalesExecID" />
                        <asp:BoundField DataField="Crd_Trns_Date" HeaderText="Transaction Date" />
                    </Columns>
                    <RowStyle CssClass="RowStyle" />
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <EditRowStyle CssClass="EditRowStyle" />
                    <AlternatingRowStyle CssClass="AltRowStyle" />
                </asp:GridView>
            </td>
        </tr>

            </table>

        <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td align="center">
                <div id="register_info">
                </div>
            </td>
        </tr>
    </table>
        </div>

        <div class="clear"></div>
        
</div>
                </div>
            </div>
        </div>
    </div>
      <script type="text/javascript">
          var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
     


</asp:Content>

