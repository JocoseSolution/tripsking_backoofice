﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UploadCredit.aspx.vb" Inherits="UploadCredit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Update Credit and Debit</title>
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <script src="../../JS/JScript.js" type="text/javascript"></script>

    <style type="text/css">
        .buttonBlue {
    background: #d9534f;
}
        .button {
    position: relative;
    display: inline-block;
    padding: 10px 24px;
    margin: .3em 0 1em 0;
    /* width: 100%; */
    vertical-align: middle;
    color: #fff;
    font-size: 16px;
    line-height: 20px;
    border-radius: 2px;
    -webkit-font-smoothing: antialiased;
    text-align: center;
    letter-spacing: 1px;
    /*background: transparent;*/
     background: #d9534f;
    border: 0;
    border-bottom: 2px solid #af3e3a;
    cursor: pointer;
    -webkit-transition: all 0.15s ease;
    transition: all 0.15s ease;
}

    </style>

    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
                return true;
            }
            else {

                return false;
            }
        }
    </script>

    <script type="text/javascript">
        function validateAmt() {
            var val = document.getElementById("txt_crd_val").value;
            var r = document.getElementById("txt_rm").value;

            if (r == "") {
                alert("Please Enter Remark."); return false;
            }
            if (val == "") {
                alert("Please Enter Amount."); return false;

            }
            try {
                var mtype = document.getElementById("ddl_moduletype").value;
                if (mtype == "--Select--") {
                    alert("Please Select Module Type."); return false;

                }

            }
            catch (err) {
            }

            //if (val > 500000) {
            //    alert("Maximum update amount is 5 lacs."); return false;
            //}

            if (confirm("Are you sure??"))
                return true;
            return false;

        }
        function checkCreditTrasac(count, record) {

            var val = document.getElementById("txt_crd_val").value;
            var r = document.getElementById("txt_rm").value;
            //var mtype = document.getElementById("ddl_moduletype").value;
            //var mtype = document.getElementById("<%= ddl_moduletype.ClientID%>"); //document.getElementById("ddl_moduletype").value;
            if (r == "") {
                alert("Please Enter Remark."); return false;
            }
            //For Narration
            try {
                var utype = document.getElementById("ddl_uploadtype").value;
                alert(utype);
                
                if (utype == "CA") {
                    var Bank = document.getElementById("ddl_banklist").value;
                    var Narration = document.getElementById("txt_narration").value;
                    if (Bank == "--Select--") {
                        alert("Please Select Bank."); return false;

                    }
                    if (Narration == "") {
                        alert("Please Enter Narration."); return false;

                    }
                }



            }
            catch (err) {
                alert('err');
            }
            //End Narration
            if (val == "") {
                alert("Please Enter Amount."); return false;

            }
            try {
                var mtype = document.getElementById("ddl_moduletype").value;
                if (mtype == "--Select--") {
                    alert("Please Select Module Type."); return false;

                }


            }
            catch (err) {
            }

            //if (val > 500000) {
            //    alert("Maximum update amount is 5 lacs."); return false;
            //}

            if (confirm("Are you sure??")) {



                if (count > 0) {

                    if (confirm("Agent already on credit. Are you sure?")) {

                        if (record != "") {
                            if (confirm(record))

                                return true;
                            else
                                return false;

                        }

                    }
                    else {
                        return false;
                    }
                }
                else {

                    if (record != "") {
                        if (confirm(record))

                            return true;
                        else
                            return false;

                    }
                }
            }
            else {
                return false;
            }



        }

        function MyFunc() {

            debugger;
            var val = document.getElementById('<%= msg_show.ClientID%>').value;

            if (val.length > 5) {
                window.close();
                alert(val);
               
                document.getElementById('<%= msg_show.ClientID%>').value = "";
                window.open('ProcessAccount.aspx', '_parent');
                
            }
        }
        function MyFunc1() {
            debugger;
            var val = document.getElementById('<%= msg_show.ClientID%>').value;

            if (val.length > 5) {
                window.close();
                alert(val);
                document.getElementById('<%= msg_show.ClientID%>').value = "";
                window.open('Upload.aspx', '_parent');
               
            }
        }
    </script>
     <script type="text/javascript">
    function validateAmtminus() {
        var val = document.getElementById("txt_crd_val").value;
        var r = document.getElementById("txt_rm").value;

        if (r == "") {
            alert("Please Enter Remark."); return false;
        }
        if (val == "") {
            alert("Please Enter Amount."); return false;

        }
        try {
            var mtype = document.getElementById("ddl_moduletype").value;
            if (mtype == "--Select--") {
                alert("Please Select Module Type."); return false;

            }

        }
        catch (err) {
        }

        //if (val > 500000) {
        //    alert("Maximum update amount is 5 lacs."); return false;
        //}

        if (confirm("Are you sure??"))
            return true;
        return false;

    }
    function checkCreditTrasac(count, record) {

        var val = document.getElementById("txt_crd_val").value;
        var r = document.getElementById("txt_rm").value;
        //var mtype = document.getElementById("ddl_moduletype").value;
        //var mtype = document.getElementById("<%= ddl_moduletype.ClientID%>"); //document.getElementById("ddl_moduletype").value;
        if (r == "") {
            alert("Please Enter Remark."); return false;
        }
       
        if (val == "") {
            alert("Please Enter Amount."); return false;

        }
        try {
            var mtype = document.getElementById("ddl_moduletype").value;
            if (mtype == "--Select--") {
                alert("Please Select Module Type."); return false;

            }


        }
        catch (err) {
        }

        //if (val > 500000) {
        //    alert("Maximum update amount is 5 lacs."); return false;
        //}

        if (confirm("Are you sure??")) {



            if (count > 0) {

                if (confirm("Agent already on credit. Are you sure?")) {

                    if (record != "") {
                        if (confirm(record))

                            return true;
                        else
                            return false;

                    }

                }
                else {
                    return false;
                }
            }
            else {

                if (record != "") {
                    if (confirm(record))

                        return true;
                    else
                        return false;

                }
            }
        }
        else {
            return false;
        }



    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <asp:HiddenField ID="msg_show" runat="server" Value=""></asp:HiddenField>
       <%-- <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UP" runat="server">
            <ContentTemplate>--%>
                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="tbl_Upload" runat="server" style="background-color: burlywood;">
                    <tr>
                        <td>
                            <div style="padding-top: 5px; padding-bottom: 5px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td style="padding-right: 17px">
                                            <fieldset style="border: thin solid #161946; padding-left: 10px">
                                                <legend style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #161946;">
                                                    Brand Infromation</legend>
                                                <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td class="h2" height="25" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;" width="120">
                                                                        User ID :
                                                                    </td>
                                                                    <td id="td_AgentID" runat="server" width="130" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                        &nbsp;
                                                                    </td>
                                                                    <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;" width="100">
                                                                        Brand Name :
                                                                    </td>
                                                                    <td id="td_AgencyName" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" class="h2" height="25" style="font-family: arial, Helvetica, sans-serif;
                                                                        font-weight: bold; color: #000000;">
                                                                        Address :<asp:Label ID="lblAdd1" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;"></asp:Label> &nbsp
                                                                        <asp:Label ID="lblAdd2" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <%--<tr>
                                                                    <td colspan="4" id="td_Address" runat="server" height="10" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="4" id="td_Address1" runat="server" height="10" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                </tr>--%>
                                                                 <tr>
                                                                    <td class="h2" height="25" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        Total Balance :
                                                                    </td>
                                                                       <td id="td_Aval_Bal" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                   
                                                                    <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                      Due Amount :
                                                                    </td>
                                                                   <%-- <td id="td_DueAmount" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>--%>
                                                                      <td style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                          <asp:Label ID="lblDueAmount" runat="server"></asp:Label>&nbsp;&nbsp;
                                                                         <span style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">Credit Limit:</span> &nbsp;
                                                                           <asp:Label ID="LblCreditLimit" runat="server" ></asp:Label>
                                                                    </td>
                                                                </tr>


                                                                <tr>
                                                                    <td class="h2" height="25" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        Brand Type :
                                                                    </td>
                                                                   <td style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                       <asp:Label ID="Label1" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        Pan No :
                                                                    </td>
                                                                    <td id="td_pan" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="h2" height="25" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        Mobile No :
                                                                    </td>
                                                                    <td id="td_Mobile" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                    <td class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;">
                                                                        Email :
                                                                    </td>
                                                                    <td id="td_Email" runat="server" style="font-family: arial, Helvetica, sans-serif;
                                                                        color: #000000; font-size: 12px;">
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;"
                                                                        align="left" class="h2" width="100">
                                                                        Remark :
                                                                    </td>
                                                                    <td align="left" width="320px">
                                                                        <asp:TextBox ID="txt_rm" runat="server" TextMode="MultiLine" Rows="3" Columns="25"
                                                                            Width="300px" BackColor="#F0F0F0"></asp:TextBox>
                                                                    </td>
                                                                    <td align="center" class="h2" style="font-family: arial, Helvetica, sans-serif; font-weight: bold;
                                                                        color: #000000;" width="90px">
                                                                       
                                                                    </td>
                                                                    <td>
                                                                        <div style="display:none"><asp:TextBox ID="txt_Yatra" runat="server" Width="100px"></asp:TextBox></div>
                                                                    </td>
                                                                    <td id="td_BankDetails" runat="server"  style="display:none">
                                                                        <table width="100%" border="0" cellpadding="10" cellspacing="10">
                                                                            <tr>
                                                                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;display:none"
                                                                                    align="left" class="h2" width="105">
                                                                                    Bank Name:
                                                                                </td>
                                                                                <td style="display:none">
                                                                                    <asp:DropDownList ID="ddl_banklist" runat="server" Width="100px">
                                                                                        <asp:ListItem Selected="True" Value="">--Select--</asp:ListItem>
                                                                                        
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;display:none"
                                                                                    align="left" class="h2" width="100">
                                                                                    Narration:
                                                                                </td>
                                                                                <td style="display:none">
                                                                                    <asp:TextBox ID="txt_narration" runat="server" Width="100px" Text="KT"></asp:TextBox>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;"
                                                                        align="left" width="100">
                                                                        Amount :
                                                                    </td>
                                                                    <td align="left" width="125" style="padding-top: 17px">
                                                                        <asp:TextBox ID="txt_crd_val" runat="server" MaxLength="8" oncopy="return false" onpaste="return false" onkeypress="return isNumberKey(event)"
                                                                            Width="120px" BackColor="#F0F0F0"></asp:TextBox>
                                                                        &nbsp;&nbsp;
                                                                    </td>
                                                                    <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;
                                                                        padding-left: 10px;" width="90px">
                                                                        Upload Type:
                                                                    </td>
                                                                    <td width="105px">
                                                                        <asp:DropDownList ID="ddl_uploadtype" runat="server" Width="100px" AutoPostBack="True">
                                                                            <asp:ListItem Selected="True" Value="CA">Cash</asp:ListItem>
                                                                            <%--<asp:ListItem Value="CR">Credit</asp:ListItem>--%>
                                                                            <asp:ListItem Value="CC">Card</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;
                                                                        padding-left: 10px;" width="250px" id="td_moduletype" runat="server">
                                                                        <table>
                                                                            <tr>
                                                                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;
                                                                                    padding-left: 10px;" width="90px">
                                                                                    Module Type:
                                                                                </td>
                                                                                <td width="105px">
                                                                                    <asp:DropDownList ID="ddl_moduletype" runat="server" Width="100px">
                                                                                        <%--<asp:ListItem Selected="True" Value="--Select--">--Select--</asp:ListItem>--%>
                                                                                        <asp:ListItem Value="251010/UPL">Upload</asp:ListItem>
                                                                                        <asp:ListItem Value="162001/CCR">Card</asp:ListItem>
                                                                                        <asp:ListItem Value="251011/UPL">Other</asp:ListItem>
                                                                                        
                                                                                       <%-- <asp:ListItem Value="450017/UPL">Incentive/Commission</asp:ListItem>
                                                                                        <asp:ListItem Value="162012/UPL">Invoice</asp:ListItem>
                                                                                        <asp:ListItem Value="162013/UPL">Rail</asp:ListItem>
                                                                                        <asp:ListItem Value="162014/UPL">Recharge</asp:ListItem>
                                                                                        <asp:ListItem Value="162014/UPL">Bus</asp:ListItem>                                                                                       
                                                                                        <asp:ListItem Value="450017/UPL">Stax/TDS</asp:ListItem>--%> 
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td id="td_spl" runat="server" width="160px">
                                                                        <table>
                                                                            <tr>
                                                                                <td style="font-family: arial, Helvetica, sans-serif; font-weight: bold; color: #000000;
                                                                                    padding-left: 10px; padding-right: 5px;">
                                                                                    Spl. Upload
                                                                                </td>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkSpl" runat="server" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td width="60" align="center">
                                                                        <asp:Button ID="plus" runat="server" Text="Credit" CssClass="btn btn-success" />
                                                                        <%--<asp:ImageButton ID="plus" runat="server" ImageUrl="../../Images/btn_plus.gif" />--%>
                                                                        <asp:ImageButton ID="minus_Series" runat="server" ImageUrl="../../Images/btn_minus.gif"
                                                                            OnClientClick="return validateAmt();" Visible="False" />
                                                                    </td>
                                                                    <td valign="middle">
                                                                         <asp:Button ID="minus" runat="server" Text="Debit" OnClientClick="return validateAmtminus();" CssClass="btn btn-success" />
                                                                      <%--  <asp:ImageButton ID="minus" runat="server" ImageUrl="../../Images/btn_minus.gif"
                                                                            OnClientClick="return validateAmtminus();" />--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td id="td_msg" runat="server" visible="false" align="center" height="300px" class="SubHeading"
                            valign="middle">
                        </td>
                    </tr>
                </table>
        <%--    </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UP">
            <ProgressTemplate>
                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden;
                    padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5;
                    z-index: 1000;">
                </div>
                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center;
                    z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px;
                    font-weight: bold; color: #000000">
                    Please Wait....<br />
                    <br />
                    <img alt="loading" src="<%= ResolveUrl("~/images/loading_bar.gif")%>" />
                    <br />
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>--%>
    </div>
        <asp:HiddenField ID="hidTransType" runat="server" />
    </form>
</body>
</html>
