﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="CheckPaymentStaus.aspx.cs" Inherits="DetailsPort_Finance_CheckPaymentStaus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function CheckTime() {
            document.getElementById("signuputl").style.display = "block";
            var currentHrs = document.getElementById('currentHrs').value;//$('#currentHrs').val();
            var currentMints = document.getElementById('currentMints').value; // $('#currentMints').val();
            var hiddAmPm = document.getElementById('hidAmPm').value;
            //if ((currentHrs == 8 && currentMints <= 30) || (currentHrs == 10 && currentMints <= 30) || (currentHrs == 11 && currentMints <= 30))
            if ((((currentHrs == 8 && currentMints <= 30) || (currentHrs == 10 && currentMints <= 30) || (currentHrs == 11 && currentMints <= 30) || (currentHrs == 12 && currentMints <= 31)) && hiddAmPm == "AM") || (currentHrs == 11 && currentMints >= 44 && hiddAmPm == "PM")) {
                document.getElementById("signuputl").style.display = "none";
                alert('Action not allowed at this time.');
                return false;
            }
            else {
                return true;
            }
        }
    </script>
    <div id="signuputl" style="display: none; background: black none repeat scroll 0 0; height: 100%; left: 0; position: fixed; opacity: 0.90; top: 0; width: 100%; z-index: 999; font-family: arial; font-size: 14px;">
        <div id="signuputlcontent" style="left: 22%; background: #f1f1f1 none repeat scroll 0 0; border-radius: 4px; box-shadow: 0 0 15px #272727; padding: 2%; position: fixed; top: 20%; width: 50%; z-index: 999;">
            <div id="UtlSource" style="width: 100%; height: 65px;">
                <strong style="font-weight: bold;">Getting response from Server.</strong> Please donot refresh or back button.
                <img id="Img1" src="/images/loading1.gif">
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <input type='hidden' id='currentHrs' value='<%=currentHrs%>' />
    <input type='hidden' id='currentMints' value='<%=currentMints%>' />
    <input type='hidden' id='hidAmPm' value='<%=amPM%>' />
    <%--<div>
        <div class="row-fluid">
            <div class="span12">
                <div class="widget">
                    <div class="widget-title">
                        <h4><i class="icon-globe"></i>Check Pending Ticket Status</h4>

                    </div>
                    <div class="widget-body">
                        <table cellpadding="10" style="width: 80%; margin: auto;">
                            <tr>
                                <td>From Date:<br />
                                    <asp:textbox id="TN_From_Date" class="fromToDatePicker" style="width: 120px;" runat="server"></asp:textbox>
                                </td>
                                
                                <td>To Date:<br />
                                    <asp:textbox id="TN_To_Date" class="ToDatePicker" style="width: 120px;" runat="server"></asp:textbox>
                                </td>
                               <td><br />
                                    <asp:Button ID="btbSubmit" runat="server" cssclass="btn btn-primary" Text="Submit" OnClick="btbSubmit_Click" />
                                </td>
                            </tr>                            
                         
                         
                            <tr>
                                <td>&nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>&nbsp;
                                </td>
                                
                                </tr>
                        </table>                     
                    </div>

                </div>
            </div>
        </div>
    </div>--%>

    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > PGReport</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row" style="display:none;">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">OrderId</label>
                                    <asp:TextBox ID="txt_OrderId" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btbSubmit" runat="server" cssclass="btn btn-primary" Text="Submit" OnClick="btbSubmit_Click" />
                                   <%-- <label for="exampleInputEmail1">Status :</label>
                                    <asp:DropDownList CssClass="form-control" ID="drpPaymentStatus" runat="server">
                                    </asp:DropDownList>--%>
                                </div>
                            </div>
                        </div>

                        

                        <div class="row" style="display:none;">
                            <div style="color: #FF0000">
                                * N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div id="divReport" runat="server" visible="true" style="background-color: #fff; overflow-y: scroll;" class="large-12 medium-12 small-12">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                    <tr>
                                        <td>
                                            <asp:GridView ID="Gridbkgsts" runat="server" BackColor="White" BorderColor="#CCCCCC" CssClass="table" AutoGenerateColumns="false"
                                                        EmptyDataText="No Record Found"  OnRowCommand="Gridbkgsts_RowCommand">
                                                      
                                                        <Columns>
                                                            <asp:BoundField DataField="AgentId" HeaderText="User_Id" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="OrderId" HeaderText="OrderId" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="TId" HeaderText="TId" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="ServiceType" HeaderText="ServiceType" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="TotalCharges" HeaderText="Trans.Charges" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="OriginalAmount" HeaderText="OriginalAmount" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="Amount" HeaderText="Amount(OriginalAmount+Trans.Charges)" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" HeaderStyle-HorizontalAlign="Left" />                                                            
                                                            <asp:BoundField DataField="ApiStatus" HeaderText="ApiStatus" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="Status" HeaderText="Txn Status" HeaderStyle-HorizontalAlign="Left" />
                                                           <%-- <asp:TemplateField HeaderText="Check Status" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnRefund" Text="Check Status" runat="server" OnClientClick="return CheckTime();" CommandArgument='<%#Eval("OrderId") + ";" + Eval("TId")+ ";" +  Eval("CreatedDate")+ ";" +  Eval("AgentId")%>'
                                                                        CommandName="Search" OnDataBinding="btnSearch_DataBinding" ToolTip='<%#Eval("OrderId") + ";" + Eval("TId")+ ";" +  Eval("CreatedDate")+ ";" +  Eval("AgentId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>

                                                             <asp:TemplateField HeaderText="Check Status" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnRefund" Text="Check Status" runat="server" CommandArgument='<%#Eval("OrderId") + ";" + Eval("TId")+ ";" +  Eval("CreatedDate")+ ";" +  Eval("AgentId")%>'
                                                                        CommandName="Search" OnDataBinding="btnSearch_DataBinding" ToolTip='<%#Eval("OrderId") + ";" + Eval("TId")+ ";" +  Eval("CreatedDate")+ ";" +  Eval("AgentId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                         <RowStyle CssClass="RowStyle" />
                                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                        <PagerStyle CssClass="PagerStyle" />
                                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                        <HeaderStyle CssClass="HeaderStyle" />
                                                        <EditRowStyle CssClass="EditRowStyle" />
                                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                                    </asp:GridView>
                                            <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                <ContentTemplate>
                                                    
                                                </ContentTemplate>
                                            </asp:UpdatePanel>--%>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <div>
        <table>
            <tr align="center">
                <td></td>
            </tr>
        </table>
    </div>
</asp:Content>

