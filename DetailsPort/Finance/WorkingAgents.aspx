﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="WorkingAgents.aspx.vb" Inherits="DetailsPort_Finance_WorkingAgents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <style type="text/css">
        input[type="text"], input[type="password"], select
        {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
        .txtBox
        {
            width: 140px;
            height: 18px;
            line-height: 18px;
            border: 2px #D6D6D6 solid;
            padding: 0 3px;
            font-size: 11px;
        }
        .txtCalander
        {
            width: 100px;
            background-image: url(../../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
        }
    </style>
    <table cellspacing="10" cellpadding="0" border="0" align="center" class="tbltbl">
        <tr>
            <td style="color: #000000; padding-bottom: 2px; padding-top: 2px; font-size: 20px;
                font-weight: bold">
                Agent's Last Available Balance
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">
                Date
            </td>
            <td>
                <input type="text" name="From" id="From" class="txtCalander" readonly="readonly"
                    style="width: 100px" />
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold">
                Working Agent
            </td>
            <td>
                <asp:DropDownList ID="ddl_type" runat="server">
                    <asp:ListItem Selected="True" Value="ALL">All Working Agents</asp:ListItem>
                    <asp:ListItem Value="TODAYS">Today's Working Agents</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button" />&nbsp;&nbsp;<asp:Button
                    ID="btn_export" runat="server" CssClass="button" Text="Export" />
            </td>
        </tr>
    </table>
    <table cellspacing="10" cellpadding="0" border="0" align="center" style="margin: auto">
        <tr>
            <td>
                <div id="totcnt" runat="server">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="false"
                            PageSize="50" CssClass="GridViewStyle" AllowPaging="True">
                            <Columns>
                                <asp:TemplateField HeaderText="AgentID">
                                    <ItemTemplate>
                                        <asp:Label ID="AgentID" runat="server" Text='<%# Eval("AgentID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="AgencyName">
                                    <ItemTemplate>
                                        <asp:Label ID="AgencyName" runat="server" Text='<%# Eval("AgencyName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Available Balance">
                                    <ItemTemplate>
                                        <asp:Label ID="Aval_Balance" runat="server" Text='<%# Eval("Aval_Balance") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <AlternatingRowStyle CssClass="AltRowStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                    <ProgressTemplate>
                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden;
                            padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5;
                            z-index: 1000;">
                        </div>
                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center;
                            z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px;
                            font-weight: bold; color: #000000">
                            Please Wait....<br />
                            <br />
                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                            <br />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

</asp:Content>
