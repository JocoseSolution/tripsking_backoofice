﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="Ledger.aspx.vb" Inherits="DetailsPort_Finance_Ledger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
   <%-- <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .lft{
            float:left;
        }

        .rgt{
            float:right;
        }
    </style>


  <!--gridview-->

    <script type="text/javascript">
        $(document).ready(function () {
            $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd").click(function () {
                $("#From").focus();
            }
            );
            $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd1").click(function () {
                $("#To").focus();
            }
            );
        });
    </script>


       <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <div class="row">
       <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
        
                    <div class="panel-body">

                        <div class="row" >
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputEmail1">From Date</label>--%>
                                    <input type="text" placeholder="FROM DATE" name="From" id="From" readonly="readonly" class="form-control input-text full-width" id="dp1539064332191" />
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar cd" style="cursor: pointer;" ></span>
                               </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputEmail1">To Date</label>--%>
                                    <input type="text" placeholder="TO DATE" name="To" id="To" readonly="readonly" class="form-control input-text full-width" />
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar cd1"  style="cursor: pointer;" ></span>
                               </span>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="input-group" id="tr_Agency" runat="server">
                                    <%--<label for="exampleInputEmail1">Agency</label>--%>
                                    <span id="tr_AgencyName" runat="server">
                                        <input type="text" placeholder="AGENCY NAME" id="txtAgencyName" name="txtAgencyName" <%--onfocus="focusObj(this);"--%>
                                           <%-- onblur="blurObj(this);"--%> <%--defvalue="Agency Name or ID"--%> autocomplete="off" <%--value="Agency Name or ID"--%> class="form-control input-text full-width" />
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /></span>
                                      <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-black-tie"></span>
                               </span>
                                </div>
                            </div>
                                               <div class="form-group" id="tr_BookingType" runat="server">
                                <div class="col-md-2">
                                  <%--  <label for="exampleInputEmail1" id="lblBookType" runat="server">Booking Type</label>--%>
                                    
                                    <asp:DropDownList ID="ddl_BookingType"  runat="server" CssClass=" input-text full-width">
                                        <asp:ListItem  value="" disabled selected style="display: none;">BOOKING TYPE</asp:ListItem>
                                        <%--<asp:ListItem>--Booking Type--</asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>


                            </div>

                            <div class="col-md-2" style="margin-top: -16px;">
                                <%--<label for="exampleInputEmail1">Trans Type :</label>--%>
                                <asp:DropDownList CssClass="input-text full-width" ID="ddlTransType" runat="server">
                                    <asp:ListItem  value="" disabled selected style="display: none;">TRANS TYPE</asp:ListItem>
                                    <%--<asp:ListItem>--Trans Type--</asp:ListItem>   --%>                                 
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-2" style="margin-top: -12px;">
                                <div class="form-group">
                                    <%--<br />--%>
                           <asp:Button ID="btn_search" runat="server" Text="Find" CssClass="btn btn-success" />
                            <%--    </div>
                            </div>  
                                <div class="col-md-2">
                                <div class="form-group">--%>
                                    <%--<br />--%>
                                   <asp:Button ID="btn_export" runat="server" Text="Export To Excel" CssClass="btn btn-success"/>
                                </div>
                            </div> 


                        </div>



                        <div class="row">
                                 
                            <div class="col-md-4" >
                                <div class="form-group" id="tr_SearchType" runat="server" visible="false">    <!--Hidden Field-->
                                    <label for="exampleInputEmail1">Search Type</label>
                                    <asp:RadioButton CssClass="input-text full-width" ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                        Text="Agent" />
                                    <asp:RadioButton ID="RB_Distr" CssClass="input-text full-width" runat="server" GroupName="Trip" onclick="Hide(this)"
                                        Text="Own" />
                                </div>
                            </div>
                            <div class="col-md-4" >
                                <div class="form-group" id="tr_Cat" runat="server">
                                    <label for="exampleInputEmail1">Upload Category </label>
                                    <asp:DropDownList ID="ddl_Category" runat="server" CssClass="input-text full-width">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="input-group" id="tr_UploadType" runat="server">
                                    <label for="exampleInputEmail1" id="lblUpType" runat="server">Upload Type</label>
                                    <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                        CssClass="form-control input-text full-width">
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                       
                                
                        </div>
                        <div class="row">
         


                            <div class="col-md-4 " style="display: none;">
                                <label for="exampleInputEmail1">PaymentMode :</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="txtPaymentmode" runat="server">
                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                    <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                    <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>




                        <br />
                        <div class="row" style="background-color: #fff; overflow-y: scroll;" runat="server" visible="true">
                            <div class="col-md-12">

                                <asp:UpdatePanel ID="up" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover" GridLines="None" PageSize="30" style="text-transform: uppercase;">

                                            <Columns>
                                                <asp:BoundField DataField="AgencyId" HeaderText="AGENCY ID" />
                                                <asp:BoundField DataField="AgentID" HeaderText="USER ID" />
                                                <asp:BoundField DataField="AgencyName" HeaderText="AGENCY NAME" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger" />
                                                <asp:BoundField HeaderText="REF. NO" DataField="InvoiceNo"></asp:BoundField>
                                                <asp:BoundField HeaderText="PNR" DataField="PnrNo" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="AIR CODE" DataField="Aircode" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="TICKET NO." DataField="TicketNo" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <%--<asp:BoundField HeaderText="Easy ID" DataField="YatraAccountID" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>--%>
                                                <asp:BoundField HeaderText="DR." DataField="Debit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="CR." DataField="Credit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="BALANCE" DataField="Aval_Balance"></asp:BoundField>
                                                <asp:BoundField HeaderText="BOOKING TYPE" DataField="BookingType" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="CREATED DATE" DataField="CreatedDate" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="REMARK" DataField="Remark" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>

                                                <asp:BoundField HeaderText="DUE AMOUNT" DataField="DueAmount" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                <asp:BoundField HeaderText="CREDIT LIMIT" DataField="CreditLimit" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>
                                                
                                                <%-- <asp:BoundField HeaderText="Payment Mode" DataField="PaymentMode" ItemStyle-CssClass="passenger" HeaderStyle-CssClass="passenger"></asp:BoundField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>
