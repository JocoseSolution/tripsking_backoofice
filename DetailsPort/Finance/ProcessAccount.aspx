﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="ProcessAccount.aspx.vb" Inherits="DetailsPort_Finance_ProcessAccount" %>

<%--<%@ Register Src="~/UserControl/RequestControl.ascx" TagPrefix="uc1" TagName="RequestControl" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">   
    <link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />
    <div class="row">
        <%--<div class="col-md-2"></div>--%>

        <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
             
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-12">
                              <%--  <div class="large-12 medium-12 small-12 heading1">Search Record By Type And Category</div>--%>
                                <div class="clear1"></div>
                                <table border="0" cellpadding="0" cellspacing="0" style="float: left; width: 45%;" id="tableFilter" runat="server">
                                    <tr>
                                        <td style="padding: 10px 5px 10px 5px; font-weight: bold; font-family: arial, Helvetica, sans-serif; font-size: 13px;"
                                            width="125px" align="left">Upload Type :
                                        </td>
                                        <td align="left">
                                            <fieldset style="border: thin solid #004b91; width: 140px;">
                                                <asp:RadioButtonList ID="RBL_Type" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                                    CellPadding="2" CellSpacing="2" Font-Size="12px" Font-Names="Arial" Width="140px">
                                                </asp:RadioButtonList>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr id="tr_Cat" runat="server">
                                        <td style="padding: 10px 5px 10px 5px; font-weight: bold; font-family: arial, Helvetica, sans-serif; font-size: 13px;"
                                            align="left">Upload Category :
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddl_Category" runat="server" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>

                                <%-- <div class="col-md-2" runat="server" style="margin-top: 37px;" >
                                                            <div class="input-group"  id="" runat="server" style="top:-39px">
                                                                
                                                                <input type="text" placeholder="Agency Name" id="txtSearch"  name="txtAgencyName" class="form-control input-text full-width" />
                                                                <%--<input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />--%>
                                                               
                                                        <%--    </>
                                                        </div></div>--%>

                                <table cellpadding="0" cellspacing="0" style="float: left; width: 55%">
                                    <tr>
                                        <td width="150px" valign="top">
                                        </td>
                                        <td width="200px" style="padding-bottom: 3px;" class="input-group">
                                            <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" placeholder="AGENCY NAME" CssClass="form-control input-text full-width"></asp:TextBox>
                                        <span class="input-group-addon" style="background: #49cced">
                                                                    <span class="fa fa-black-tie"></span>
                                                                </span> </td>
                                   
                                        <td id="td_Reject" runat="server" visible="false" valign="top" colspan="2">

                                           <%-- <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>--%>
                                                    <td width="190px" valign="top"></td>

                                                    <td align="center" style="padding-top: -1px;padding-left:10px;">
                                                        <asp:TextBox ID="txt_Reject" runat="server" placeholder="Submit Comment" TextMode="MultiLine" Height="60px" Width="350px"
                                                            BackColor="#FFFFCC"></asp:TextBox><br />
                                                    </td>
                                              &nbsp;&nbsp;
                                                    <td colspan="2" style="float: right;margin-left:9px;margin-top:21px">
                                                        <asp:Button ID="btn_Submit" runat="server" Text="Submit" CssClass="btn btn-success" />&nbsp;&nbsp;
                                                        
                                                         </td>  
                                               <td>      
                                                                       <asp:Button ID="btn_Cancel" runat="server" Text="Cancle" class="btn btn-success"/>
                                                           </td> 
                                                  
                                             </td>   </tr>
                                            </table>

                                     

                               

                                <div class="clear"></div>

                            </div>
                            <div class="clear1"></div>
                            <div class="clear1"></div>
                             <div class="row">
                            <table class="gridwidth1 large-12 medium-12 small-12" style="background-color:#ffffff">

                                <tr>

                                    <td>
                                          <asp:GridView ID="grd_accdeposit" runat="server"  AutoGenerateColumns="False" DataKeyNames="counter"
                    AllowPaging="True" AllowSorting="True" CssClass="table" 
                    PageSize="25"  Width="100%" style="background-color: #fff;text-transform: uppercase; overflow: auto; max-height: 500px;" OnRowCommand="grd_accdeposit_RowCommand" >
                   
                                       <%-- <asp:GridView ID="grd_accdeposit" runat="server" AutoGenerateColumns="false" OnRowCommand="grd_accdeposit_RowCommand"
                                            OnRowDataBound="grd_accdeposit_RowDataBound" CssClass="table">--%>
                                            <Columns>
                                                <asp:TemplateField HeaderText="&nbsp;&nbsp;ID&nbsp;&nbsp">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblID" runat="server" Text='<%# Eval("Counter") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="200px" />
                                                </asp:TemplateField>
                                                <%--   <asp:BoundField HeaderText="&nbsp;&nbsp;ID&nbsp;&nbsp;" DataField="Counter" />--%>
                                                <asp:TemplateField HeaderText="Agency Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbl_agencyid" runat="server" Text='<%# Eval("AgencyName").ToString() %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:BoundField HeaderText="AgencyID" DataField="AgencyID" />--%>
                                                <asp:TemplateField HeaderText="User_Id">
                                                    <ItemTemplate>

                                                        <div>
                                                            <%--<a href="UploadCredit.aspx?AgentID=<%#Eval("AgencyID")%>&ID=<%#Eval("Counter")%>&Amount=<%#Eval("Amount")%>#lightbox">
                                                                            <asp:Label ID="lbl_uid" runat="server" Text='<%#Eval("AgencyID")%>' Font-Bold="True"
                                                                                ForeColor="#004b91"></asp:Label></a>--%>
                                                           <a href='UploadCredit.aspx?AgentID=<%#Eval("AgencyID")%>&ID=<%#Eval("Counter")%>&Amount=<%#Eval("Amount")%>' rel="lyteframe"
                                                                rev="width: 900px; height: 300px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif;margin-top:-41px; font-size: 12px; font-weight: bold; color: #004b91">
                                                                <asp:Label ID="OrderID" runat="server" Text='<%#Eval("AgencyID")%>'></asp:Label></a>
                                                      
                                                              <%--<asp:LinkButton ID ="OrderID" runat ="server"  CssClass="btn btn-info" CommandArgument='<%#Eval("AgencyID")%>' CommandName ="User_Id"  Text ="User_Id"></asp:LinkButton>   --%>  
                               
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="AgencyID" DataField="RegAgencyID" />
                                                <asp:BoundField HeaderText="Amount" DataField="Amount" />
                                                <asp:BoundField HeaderText="ModeOfPayment" DataField="ModeOfPayment" />
                                                <asp:BoundField HeaderText="Bank Name" DataField="BankName" />
                                                <asp:BoundField HeaderText="ChequeNo" DataField="ChequeNo" />
                                                <asp:BoundField HeaderText="ChequeDate" DataField="ChequeDate" />
                                                <asp:BoundField HeaderText="TransactionID" DataField="TransactionID" />
                                                <asp:BoundField HeaderText="ReferenceNo" DataField="BankAreaCode" />
                                                <asp:BoundField HeaderText="Deposit City" DataField="DepositCity" />
                                                <asp:BoundField HeaderText="Remark" DataField="Remark" />
                                                <asp:BoundField HeaderText="Status" DataField="Status" />
                                                <asp:BoundField HeaderText="Date" DataField="Date" />
                                                <asp:TemplateField HeaderText="Reject">
                                                    <ItemTemplate>
                                                        <%--<asp:LinkButton ID="lnkaccept" runat="server" Text="Accept" CommandName="accept"
                                                    CommandArgument='<%#Eval("AgencyID") %>'></asp:LinkButton>/--%>
                                                        <asp:LinkButton ID="lnkcancel" runat="server" CommandName="reject" CommandArgument='<%#Eval("Counter") %>'
                                                            ForeColor="Red" Font-Bold="True">Reject</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />--%>
                                            </Columns>
                                        </asp:GridView>
                                    </td>

                                </tr>

                            </table>
                                 </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
