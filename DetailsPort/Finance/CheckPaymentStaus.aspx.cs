﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Finance_CheckPaymentStaus : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    SqlDataAdapter da = new SqlDataAdapter();
    private string Data;
    SqlCommand cmd = null;
    private SqlParameter res;
    public int currentHrs;
    public int currentMints;
    public string amPM;
    protected void Page_Load(object sender, EventArgs e)
    {
        string cTime = System.DateTime.Now.ToString("hh : mm ");
        currentHrs = Convert.ToInt32(cTime.Split(':')[0].Trim());
        currentMints = Convert.ToInt32(cTime.Split(':')[1].Trim());
        amPM = System.DateTime.Now.ToString("tt");
        if (Session["UID"] == null)
        {
            Response.Redirect("login.aspx", false);
        }

        if (!IsPostBack)
                {
                    bind("", "");
                }
           }
    private void bind(string fDate, string tDate)
    {
        try
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();
            //fDate = fDate + " 00:00:00.000";
            //tDate = tDate + " 23:59:59.999";
            //string SDate = DateTime.Now.ToString("MM-dd-yyyy") + " 00:00:00.000";
            //string DDate = DateTime.Now.ToString("MM-dd-yyyy") + " 23:59:59.999";
            da = new SqlDataAdapter("spGetPgReport", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("@usertype", Convert.ToString(Session["User_Type"]));
            da.SelectCommand.Parameters.AddWithValue("@LoginID", "");
            da.SelectCommand.Parameters.AddWithValue("@FormDate", fDate);
            da.SelectCommand.Parameters.AddWithValue("@ToDate", tDate);
            da.SelectCommand.Parameters.AddWithValue("@OderId", "");            
            da.Fill(dt);
            Gridbkgsts.DataSource = dt;
            Gridbkgsts.DataBind();           
        }
        catch (Exception ex)
        {
            //return ex.Message.ToString() + "aaa";
        }
    }
    protected void Gridbkgsts_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Search")
        {
            SqlCommand cmd = new SqlCommand();
            string OrderId;
            string oprTxnId;
            string message;
            string date;
            string AGENTID = string.Empty;
            int index = ((GridViewRow)(((LinkButton)e.CommandSource).NamingContainer)).RowIndex;
            string action = e.CommandName.ToString();
            Data = (e.CommandArgument).ToString();
            string[] records = Data.Split(';');
            if (records.Length == 4)
            {
                OrderId = records[0];
                oprTxnId = records[1];
                date = records[2];
                AGENTID = records[3];
                PG.PaymentGateway obPg = new PG.PaymentGateway();                
                if (!string.IsNullOrEmpty(OrderId))
                {
                    message = obPg.CheckPaymmentStatusPayU(OrderId);                    
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('" + message + " ');", true);
                }
                else
                {
                    message = "Try again";
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('" + message + " ');", true);
                }               
            }
            else
            {
                message = "Some error occur.";
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('" + message + " ');", true);
                //Response.Redirect("Default.aspx");
            }
        }       
        bind("", "");
    }


    protected void btnSearch_DataBinding(object sender, System.EventArgs e)
    {
        LinkButton btn = (LinkButton)(sender);
        if (Eval("STATUS").ToString() == "Requested" || Eval("STATUS").ToString() == "failure" || Eval("STATUS").ToString() == "pending")
        {
            btn.Visible = true;//Eval("STATUS").ToString().Equals("Requested");
        }
        //btn.Visible = Eval("STATUS").ToString().Equals("Requested");
    }

    protected void btbSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null)
                Response.Redirect("login.aspx", false);

            try
            {
                string FromDate;
                string ToDate;
                //string PgStatus = drpPaymentStatus.Visible == true ? drpPaymentStatus.SelectedValue.ToLower() != "select" ? drpPaymentStatus.SelectedValue : null : null;
                if (String.IsNullOrEmpty(Request["From"]))
                {
                    FromDate = "";
                }
                else
                {
                   // FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4);
                    FromDate = (Request["From"]).Split('/')[1] + "-" + (Request["From"]).Split('/')[0] + "-" + (Request["From"]).Split('/')[2];
                    FromDate = FromDate + " " + "12:00:00 AM";
                }

                if (String.IsNullOrEmpty(Request["To"]))
                {
                    ToDate = "";
                }
                else
                {
                    //ToDate = Mid((Request["To"]).Split(" ")(0), 4, 2) + "/" + Left((Request("To")).Split(" ")(0), 2) + "/" + Right((Request("To")).Split(" ")(0), 4);
                    ToDate = (Request["To"]).Split('/')[1] + "-" + (Request["To"]).Split('/')[0] + "-" + (Request["To"]).Split('/')[2];
                    ToDate = ToDate + " " + "11:59:59 PM";
                    
                }
                bind(FromDate, ToDate);                
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }            
        }
        catch (Exception ex)
        {
            //ErrorLog.ErrorRoutine(ex, "", "", "");
            //throw ex;
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "Message", "alert('Try agin');", true);
        }
    }

    public string getBookingStatus(string refNo)
    {
        string bookingStatus = string.Empty;
        DataSet dsBkg = new DataSet();
        try
        {
            SqlDataAdapter adp = new SqlDataAdapter("select  STATUS FROM TBL_CREDITCARD_PAYMENT where ReferenceNo= '" + refNo + "'", con);
            adp.SelectCommand.CommandType = CommandType.Text;
            //adp.SelectCommand.Parameters.AddWithValue("@RefNo", refNo.Trim());                       
            adp.Fill(dsBkg);
            if (dsBkg != null && dsBkg.Tables.Count > 0)
            {
                if (dsBkg.Tables[0].Rows.Count > 0)
                {
                    bookingStatus = Convert.ToString(dsBkg.Tables[0].Rows[0]["STATUS"]);
                }
            }
        }
        catch (Exception ex)
        {
            //ErrorLog.ErrorRoutine(ex, refNo, "", "");
        }
        return bookingStatus;
    }

    public string SearchPendingBookingTicketAdmin(string tid, string oprTxnId, string bookingDate, string AgentId)
    {
        string msg = "";
        int inserflag = 0;
        return msg;

    }

}