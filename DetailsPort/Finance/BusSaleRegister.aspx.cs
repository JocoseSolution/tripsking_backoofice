﻿
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class BS_BusSaleRegister : System.Web.UI.Page
{
    private SqlTransaction ST = new SqlTransaction();
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private clsInsertSelectedFlight CllInsSelectFlt = new clsInsertSelectedFlight();
    DataSet AgencyDDLDS = new DataSet();
    DataSet grdds = new DataSet();
    DataSet fltds = new DataSet();
    private Status sttusobj = new Status();
    SqlConnection con = new SqlConnection();
    ClsCorporate clsCorp = new ClsCorporate();
    public void CheckEmptyValue()
    {
        try
        {
            string FromDate = null;
            string ToDate = null;
            int lenth = 0;
         

            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {

                FromDate = Request["From"].Substring(3, 2) + "/" + Request["From"].Substring(0, 2) + "/" + Request["From"].Substring(6, 4);
                FromDate = FromDate + " " + "12:00:00 AM";
            }
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {

                ToDate = Request["To"].Substring(3, 2) + "/" + Request["To"].Substring(0, 2) + "/" + Request["To"].Substring(6, 4);
                ToDate = ToDate + " " + "11:59:59 PM";
            }


          //  string AgentID = String.IsNullOrEmpty(Request["txtAgencyName"]) ? "" : Request["txtAgencyName"];
            string AgentID = String.IsNullOrEmpty(Request["hidtxtAgencyName"]) | Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"];
            string OrderID = String.IsNullOrEmpty(txt_OrderId.Text) ? "" : txt_OrderId.Text.Trim();
            string paymentmode  = txtPaymentmode.SelectedValue.ToString();
            string source =  TxtSource.Text.ToString();
            string destination = Txt_Destination.Text.ToString();
            string pnr =  txt_PNR.Text.ToString();


            if (AgentID != "")
            {
                //string str = AgentID;
                //int pos1 = str.IndexOf("(");
                //int pos2 = str.IndexOf(")");
                //lenth = pos2 - pos1;
                //string AgentID1 = str.Substring(pos1 + 1, lenth - 1);
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, paymentmode, pnr, source, destination);
                ViewState["grdds"] = grdds;
                GrdBusReport.DataSource = grdds;
                GrdBusReport.DataBind();
            }
            else
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, paymentmode, pnr, source, destination);
                ViewState["grdds"] = grdds;
                GrdBusReport.DataSource = grdds;
                GrdBusReport.DataBind();

            }

            grdds.Clear();
            grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, paymentmode, pnr, source,destination);
            ViewState["grdds"] = grdds;
            if ((grdds.Tables[0].Rows.Count > 0))
            {
                DivPrint.InnerHtml = "";              
              // PrintVisible.Visible = true;
             }

            GrdBusReport.DataSource = grdds;
            GrdBusReport.DataBind();


        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    protected void Page_Load(object sender, System.EventArgs e)
    {

        try
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (string.IsNullOrEmpty(Session["UID"].ToString()) | Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
         
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }



    protected void GrdBusReport_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        GrdBusReport.PageIndex = e.NewPageIndex;
        GrdBusReport.DataSource = ViewState["grdds"];
        GrdBusReport.DataBind();
    }


    public DataSet BUSDetails(string loginid, string usertype, string fromdate, string todate, string orderid, string agentid, string paymentStatus,string pnr,string source,string destination)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myCon"].ConnectionString);
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "[SP_BUS_SALEREGISTER]";
                sqlcmd.Parameters.AddWithValue("@usertype", usertype);
                sqlcmd.Parameters.AddWithValue("@LoginID", loginid);
                sqlcmd.Parameters.AddWithValue("@FormDate", fromdate);
                sqlcmd.Parameters.AddWithValue("@ToDate", todate);
                sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
                sqlcmd.Parameters.AddWithValue("@AgentId", agentid);
                sqlcmd.Parameters.AddWithValue("@PaymentStatus", paymentStatus);
                sqlcmd.Parameters.AddWithValue("@source", source);
                sqlcmd.Parameters.AddWithValue("@destination", destination);
                sqlcmd.Parameters.AddWithValue("@pnr", pnr); 
    
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
               
               
            }
        }
        catch (Exception ex)
        {
        }
        return DS;
    }
    protected void btn_export_Click(object sender, System.EventArgs e)
    {
        try
        {
            string FromDate = null;
            string ToDate = null;
            int lenth = 0;
           // string PgStatus = drpPaymentStatus.Visible == true ? drpPaymentStatus.SelectedValue.ToLower() != "select" ? drpPaymentStatus.SelectedValue : null : null;

            if (String.IsNullOrEmpty(Request["From"]))
            {
                FromDate = "";
            }
            else
            {

                FromDate = Request["From"].Substring(3, 2) + "/" + Request["From"].Substring(0, 2) + "/" + Request["From"].Substring(6, 4);
                FromDate = FromDate + " " + "12:00:00 AM";
            }
            if (String.IsNullOrEmpty(Request["To"]))
            {
                ToDate = "";
            }
            else
            {

                ToDate = Request["To"].Substring(3, 2) + "/" + Request["To"].Substring(0, 2) + "/" + Request["To"].Substring(6, 4);
                ToDate = ToDate + " " + "11:59:59 PM";
            }


            string AgentID = String.IsNullOrEmpty(Request["hidtxtAgencyName"]) | Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"];
            string OrderID = String.IsNullOrEmpty(txt_OrderId.Text) ? "" : txt_OrderId.Text.Trim();
            string paymentmode = txtPaymentmode.SelectedValue.ToString();
            string source = TxtSource.Text.ToString();
            string destination = Txt_Destination.Text.ToString();
            string pnr = txt_PNR.Text.ToString();
            if (AgentID != "")
            {
                string str = AgentID;
                int pos1 = str.IndexOf("(");
                int pos2 = str.IndexOf(")");
                lenth = pos2 - pos1;
                string AgentID1 = str.Substring(pos1 + 1, lenth - 1);
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, paymentmode, pnr, source, destination);

                STDom.ExportData(grdds);
            }
            else
            {
                grdds.Clear();
                grdds = BUSDetails(Session["UID"].ToString(), Session["User_Type"].ToString(), FromDate, ToDate, OrderID, AgentID, paymentmode, pnr, source, destination);



                STDom.ExportData(grdds);

            }


        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void btn_result_Click(object sender, EventArgs e)
       {
        CheckEmptyValue();
    }

    protected void ButtonPrint_Click(object sender, System.EventArgs e)
	{
		
		try {
			DataSet ds = (DataSet)ViewState["grdds"];

			if ((!string.IsNullOrEmpty(TextBoxPrintNo.Text))) {
				int dsCount = ds.Tables[0].Rows.Count;

				DataTable dtUniqRecordsByOerderID = new DataTable();
				dtUniqRecordsByOerderID = ds.Tables[0].DefaultView.ToTable(true, "OrderId");

				if (dtUniqRecordsByOerderID.Rows.Count > 0) {

                    string[] pageNo = TextBoxPrintNo.Text.Split('-');
				

					if (Convert.ToInt16(pageNo[1]) > Convert.ToInt16(pageNo[0])) {

						if ((Convert.ToInt16(pageNo[1]) - Convert.ToInt16(pageNo[0]) + 1) <= 50) {
							DivPrint.InnerHtml = GetInvoicePrintHtml(Convert.ToInt16(pageNo[0]), Convert.ToInt16(pageNo[1]), dtUniqRecordsByOerderID);
							ScriptManager.RegisterStartupScript(Page, Page.GetType(), "print", "callprint('" + DivPrint.ClientID.ToString() + "');", true);
						} else {
							ScriptManager.RegisterStartupScript(Page, Page.GetType(), "print", "alert('You can not print more than 50 invoice at a time.');", true);
						}

					} else {
						ScriptManager.RegisterStartupScript(Page, Page.GetType(), "print", "alert('Please Provide Page Range in proper Format.');", true);

					}

				}
			} else {
				ScriptManager.RegisterStartupScript(Page, Page.GetType(), "print", "alert('Please enter page range');", true);

			}

		} catch (Exception ex) {
			ScriptManager.RegisterStartupScript(Page, Page.GetType(), "print", "alert('Please enter page range');", true);
		}


	}
	
	public string GetInvoicePrintHtml(int FromPage, int ToPage, DataTable dt)
	{

		string resultHtml = "";



		if (dt.Rows.Count > 0) {

			for (int i = 0; i <= dt.Rows.Count - 1; i++) {


				if (i >= FromPage - 1 && i < ToPage) {
					int ri = i;
					resultHtml += "<div style='page-break-after:always;'>";
					resultHtml += clsCorp.ShowInvoice(dt.Rows[i]["OrderId"].ToString());
					resultHtml += "</div>";




				}

			}



		}

		return resultHtml;
	}

}	


