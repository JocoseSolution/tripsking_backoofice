﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="BusSaleRegister.aspx.cs" Inherits="BS_BusSaleRegister" %>
<%@ Register Src="~/UserControl/AccountsControl.ascx" TagPrefix="uc1" TagName="Account" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script type="text/javascript" language='javascript'>
        function callprint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'left=0,top=0,width=750,height=500,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write("<html><head><title>Ticket Details</title></head><body>" + prtContent.innerHTML + "</body></html>");
            prtContent.innerHTML = "";
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();

            prtContent.innerHTML = "";
            //prtContent.innerHTML = strOldOne;
        }
    </script>
      <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
    rel="stylesheet" />

       <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    
    <style type="text/css">
        .txtBox
        {
            width: 140px;
            height: 18px;
            line-height: 18px;
            border: 2px #D6D6D6 solid;
            padding: 0 3px;
            font-size: 11px;
        }
        .txtCalander
        {
            width: 100px;
            background-image: url(../images/cal.gif);
            background-repeat: no-repeat;
            background-position: right;
            cursor: pointer;
            border: 1px #D6D6D6 solid;
        }
        
    </style>
    
  <%--  <table cellspacing="10" cellpadding="0" border="0" align="center" class="tbltbl">
        <tr>
            <td>
                <table cellspacing="3" cellpadding="3" align="center" style="background: #fff;">
                    <tr>
                        <td align="left" style="color: #668aff; font-size: 13px; font-weight: bold">
                           Bus Refund Report
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="90" style="font-weight: bold" height="25">
                                        From Date
                                    </td>
                                    <td width="130">
                                        <input type="text" name="From" id="From" class="txtCalander" readonly="readonly"
                                            style="width: 100px" />
                                    </td>
                                    <td width="90" style="font-weight: bold">
                                        To Date
                                    </td>
                                    <td width="120px">
                                        <input type="text" name="To" id="To" class="txtCalander" readonly="readonly" style="width: 100px" />
                                    </td>
                                    <td width="80" style="font-weight: bold">
                                        PNR
                                    </td>
                                    <td width="110">
                                        <asp:TextBox ID="txtPnr" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td align="left" width="80" style="font-weight: bold">
                                        OrderId
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtOrderID" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td width="90" style="font-weight: bold" height="25">
                                        Source
                                    </td>
                                    <td width="130" class="style4">
                                        <asp:TextBox ID="TxtSource" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td width="90" style="font-weight: bold" height="25">
                                        Destination
                                    </td>
                                    <td width="120" class="style4">
                                        <asp:TextBox ID="TxtDestination" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td align="left" width="80" style="font-weight: bold" class="style4">
                                        Ticket No
                                    </td>
                                    <td class="style4" width="110px">
                                        <asp:TextBox ID="txtTicketNo" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                    <td align="left" width="80" style="font-weight: bold">
                                        BusOperator
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtBusOperator" runat="server" Width="100px"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                
                                    <td align="right">
                                        <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button" OnClick="btn_result_Click" />&nbsp;&nbsp;
                                        <asp:Button ID="btn_export" runat="server" CssClass="button" Text="Export"  OnClick="btn_export_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="color: #FF0000" colspan="4">
                            * N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%">
     
        <tr>
            <td align="center">
                <asp:GridView ID="GrdBusReport" runat="server" AutoGenerateColumns="False" PageSize="500"
                    CssClass="GridViewStyle" GridLines="None" Width="974px">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <a href='../BS/TicketSummary.aspx?tin=<%#Eval("TICKETNO")%>&oid=<%#Eval("ORDERID")%>'
                                    rel="lyteframe" rev="width: 1000px; height: 500px; overflow:hidden;" target="_blank"
                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;
                                    color: #668aff">
                                    <asp:Label ID="TKTNO" runat="server" Text='<%#Eval("TICKETNO")%>'></asp:Label>(Summary)
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <a href='../BS/CancelTicket.aspx?tin=<%#Eval("TICKETNO")%>&oid=<%#Eval("ORDERID")%>'
                                    rel="lyteframe" rev="width: 700px; height: 300px; overflow:hidden;" target="_blank"
                                    style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold;
                                    color: #668aff">(CancelTicket) </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AGENTID" HeaderText="Agent&nbsp;Id" />                    
                        <asp:BoundField DataField="ORDERID" HeaderText="Order&nbsp;Id" />
                        <asp:BoundField DataField="TRIPID" HeaderText="Trip&nbsp;Id" />
                        <asp:BoundField DataField="SOURCE" HeaderText="Source" />
                        <asp:BoundField DataField="DESTINATION" HeaderText="Destination" />
                        <asp:BoundField DataField="BUSOPERATOR" HeaderText="Bus&nbsp;Operator" />
                        <asp:BoundField DataField="SEATNO" HeaderText="Seat&nbsp;No" />
                        <asp:BoundField DataField="TA_NET_FARE" HeaderText="Fare" />
                        <asp:BoundField DataField="TICKETNO" HeaderText="Ticket&nbsp;No" />
                        <asp:BoundField DataField="PNR" HeaderText="PNR" />
                        <asp:BoundField DataField="BOOKINGSTATUS" HeaderText="Booking&nbsp;Status" />
                        <asp:BoundField DataField="JOURNEYDATE" HeaderText="Journey&nbsp;Date" />
                        <asp:BoundField DataField="CREATEDDATE" HeaderText="Booking&nbsp;Date" />
                    </Columns>
                    <RowStyle CssClass="RowStyle" />
                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <EditRowStyle CssClass="EditRowStyle" />
                    <AlternatingRowStyle CssClass="AltRowStyle" />
                </asp:GridView>
            </td>
        </tr>
    </table>--%>

     <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Account > Bus Sale Register</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To Date</label>
                                    <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PNR</label>
                                    <asp:TextBox ID="txt_PNR" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">OrderId</label>
                                    <asp:TextBox ID="txt_OrderId" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Source</label>
                                    <asp:TextBox ID="TxtSource" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Destination</label>
                                    <asp:TextBox ID="Txt_Destination" runat="server" CssClass="form-control" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Provider Name</label>
                                     <asp:DropDownList CssClass="form-control" ID="DropDownList1" runat="server">                                 
                                   <asp:ListItem Text="RB" Value="RB"></asp:ListItem>
                                   <asp:ListItem Text="GS" Value="GS"></asp:ListItem>                                                                                            
                                    </asp:DropDownList>
                                </div>
                            </div>                      
                            <div class="col-md-3" id="td_Agency" runat="server">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Agency Name</label>
                                    <input type="text" id="txtAgencyName" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" defvalue="Agency Name or ID" autocomplete="off" value="Agency Name or ID"  class="form-control"/>
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                                </div>
                            </div>
                            
                           <div class="col-md-3">
                                    <label for="exampleInputEmail1">PaymentMode :</label>
                                    <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">                                 
                                   <asp:ListItem Text="ALL" Value=""></asp:ListItem>
                                   <asp:ListItem Text="PG" Value="PG"></asp:ListItem>
                                  <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>                                                                
                                    </asp:DropDownList>
                                </div>
                           
                      
                            <div class="col-md-3">
                                <div class="form-group"> 
                                    <br />                                   
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue"  OnClick="btn_result_Click" />

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="button buttonBlue" OnClick="btn_export_Click" />
                                    </div>
                            </div>
                            </div>
                        <div style="color: #FF0000">
                            * N.B: To get Today's booking without above parameter,do not fill any field, only
                            click on search your booking.
                        </div>    
                        
                        <div class="row" style="background-color:#fff; overflow-y:scroll; " runat="server">                                                              
                        <div align="center">
                
                   <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:white;">
                    <tr>
                        <td align="left">
                            <table id="PrintVisible" runat="server" visible="false">
                                <tr>
                                    <td>
                                        <b>Print Invoice Pages :</b>&nbsp;&nbsp;
                                    </td>
                                    <td>
                                         <br />
                                        <asp:TextBox ID="TextBoxPrintNo"  Width="90px" Height="43px" runat="server" CssClass="form-control"></asp:TextBox>
                                    </td>
                                  
                                    <td  style="padding-left:20px;">
                                         <br /> <br />
                                        <asp:Button ID="ButtonPrint" runat="server" Text="Print" CssClass="button buttonBlue" OnClick="ButtonPrint_Click"  />
                                        &nbsp;&nbsp;&nbsp;(Ex: 1-3 or 3-10)&nbsp;&nbsp;                              
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                              
                            <asp:UpdatePanel ID="up" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GrdBusReport" runat="server" AllowPaging="true" AutoGenerateColumns="false" OnPageIndexChanging="GrdBusReport_PageIndexChanging"
                                       CssClass="table" GridLines="None" PageSize="30">
                                        <Columns>
                                            <asp:TemplateField HeaderText="ORDERID">
                                                <ItemTemplate>
                                                      <asp:Label ID="lbl_orderid" runat="server" Text='<%#Eval("ORDERID")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                       <%--     <asp:TemplateField HeaderText="EasyID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_sector" runat="server" Text='<%#Eval("EASY_ORDERID_ITZ")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="EasyTransNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_sector" runat="server" Text='<%#Eval("EASY_TRAN_CODE_ITZ")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="AGENTID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_sector" runat="server" Text='<%#Eval("AgentId") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AGENCY&nbsp;NAME">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_sector0" runat="server" Text='<%#Eval("Agency_Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                             <asp:TemplateField HeaderText="Sources">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Sources" runat="server" Text='<%#Eval("SOURCE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="Distination">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_Sources" runat="server" Text='<%#Eval("DESTINATION") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="PAXNAME">
                                                <ItemTemplate>
                                                 <asp:Label ID="lbl_Paxname" runat="server" Text='<%#Eval("PAXNAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Seat Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_paxtype" runat="server" Text='<%#Eval("LADIESSEAT") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="BASE FARE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_basefare" runat="server" Text='<%#Eval("ORIGINAL_FARE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                            
                                            <asp:TemplateField HeaderText="DISCOUNT">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_totdis" runat="server" Text='<%#Eval("ADMIN_COMM") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TDS">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_tds" runat="server" Text='<%#Eval("TA_TDS") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TOTALFARE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_totfare" runat="server" Text='<%#Eval("TOTAL_FARE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NETFARE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_totbookcost" runat="server" Text='<%#Eval("TA_NET_FARE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CREATEDDATE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_CDate" runat="server" Text='<%#Eval("CREATEDDATE") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                     <asp:TemplateField HeaderText="PAYMENT MODE">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("Paymentmode")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                           <%--   <asp:TemplateField HeaderText="Convenience Fee">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("PgCharges")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>

                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                        <PagerStyle CssClass="PagerStyle" />
                                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <EditRowStyle CssClass="EditRowStyle" />
                                        <AlternatingRowStyle CssClass="AltRowStyle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                   
                                 
                        </td>
                        
                    </tr>
                </table>
               </div>
            </div>
            <div id="DivPrint" runat="server" visible="true">
                </div>
            </div>
        </div>
                </div>
            </div>
        </div>

    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

  
</asp:Content>

