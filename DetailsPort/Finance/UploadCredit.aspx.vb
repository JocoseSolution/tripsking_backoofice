﻿Imports System.Data
Imports System.Data.SqlClient
Imports YatraBilling

Partial Class UploadCredit
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom
    Private ST As New SqlTransaction
    Dim AgentType As String
    Dim series As New SeriesDepart
    Private objSql As New SqlTransactionNew
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            If (Session("UID") = "" Or Session("UID") Is Nothing) Then
                Response.Redirect("~/Login.aspx")
            End If
            td_spl.Visible = False
            If Not IsPostBack Then

                'Dim Action As String = ""
                'Dim agent As String = ""
                'Dim Amount As String = ""
                'If Not String.IsNullOrEmpty(agent) AndAlso (Not String.IsNullOrEmpty(agent) OrElse Not String.IsNullOrEmpty(Amount)) Then
                '    Action = ""
                'End If

                'If String.IsNullOrEmpty(agent) AndAlso (String.IsNullOrEmpty(agent) OrElse String.IsNullOrEmpty(Amount)) Then
                '    Action = ""
                'End If
                Dim agent As String = Request("AgentID")
                Dim ID As String = Request("ID")
                Dim Action As String = Request("Action")

                If Not String.IsNullOrEmpty(agent) Then
                    Dim Dt As New DataTable
                Dim count As String
                count = ""
                Dim rec As String
                Dim crdt As DataTable
                crdt = CheckCRRecord(agent, count)

                rec = ""
                If crdt.Rows.Count > 0 Then
                    rec = "Todays Upload \n"
                    For i As Integer = 0 To crdt.Rows.Count - 1

                        Dim struptype As String = ""
                        If (crdt.Rows(i)("UploadType").ToString().ToUpper() = "CA") Then
                            struptype = crdt.Rows(i)("UploadType").ToString() + " (Cash)"
                        ElseIf (crdt.Rows(i)("UploadType").ToString().ToUpper() = "CR") Then
                            struptype = crdt.Rows(i)("UploadType").ToString() + " (Credit)"
                        ElseIf (crdt.Rows(i)("UploadType").ToString().ToUpper() = "CC") Then
                            struptype = crdt.Rows(i)("UploadType").ToString() + " (CreditCard)"
                        End If

                        rec = rec + (i + 1).ToString() + "." + struptype + "-" + " INR. " + crdt.Rows(i)("Credit").ToString() + "\n"



                    Next


                End If

                plus.Attributes.Add("OnClick", "return checkCreditTrasac('" + count + "','" + rec + "')")




                Dt = ST.GetAgencyDetails(agent).Tables(0)
                td_AgentID.InnerText = agent
                td_AgencyName.InnerText = Convert.ToString(Dt.Rows(0)("Agency_Name")) + "-" + Convert.ToString(Dt.Rows(0)("AgencyId"))
                'td_Address.InnerText = Dt.Rows(0)("Address").ToString
                'td_Address1.InnerText = Dt.Rows(0)("City").ToString & "," & Dt.Rows(0)("State").ToString & "," & Dt.Rows(0)("Country").ToString
                lblAdd1.Text = Dt.Rows(0)("Address").ToString
                lblAdd2.Text = Dt.Rows(0)("City").ToString & "," & Dt.Rows(0)("State").ToString & "," & Dt.Rows(0)("Country").ToString
                td_Aval_Bal.InnerText = Dt.Rows(0)("Crd_Limit").ToString
                td_Email.InnerText = Dt.Rows(0)("Email").ToString
                td_Mobile.InnerText = Dt.Rows(0)("Mobile").ToString
                td_pan.InnerText = Dt.Rows(0)("PanNo").ToString
                'AgentType = Dt.Rows(0)("Agent_Type").ToString
                Label1.Text = Dt.Rows(0)("Agent_Type").ToString
                'td_DueAmount.InnerText = Dt.Rows(0)("DueAmount").ToString
                lblDueAmount.Text = Convert.ToString(Dt.Rows(0)("DueAmount"))
                LblCreditLimit.Text = Convert.ToString(Dt.Rows(0)("AgentLimit"))

                    If ID <> "" Then
                        txt_crd_val.Text = Request("Amount")
                        minus.Visible = False
                        td_moduletype.Visible = False
                        hidTransType.Value = "Receipts"

                        'Else
                        '    plus.Visible = False
                    End If
                td_moduletype.Visible = False
                If Request("Counter") <> "" Then
                    minus.Visible = False
                    plus.Visible = False
                    minus_Series.Visible = True
                    txt_crd_val.Text = Request("Amt")
                    'Else
                    '    plus.Visible = False
                End If

                If Action = "DEBIT" Then
                    minus.Visible = True
                        plus.Visible = False
                        hidTransType.Value = "DEBIT NOTE"
                End If
                If Action = "CREDIT" Then
                    minus.Visible = False
                        plus.Visible = True
                        hidTransType.Value = "CREDIT NOTE"
                End If
                    '' Request("ID") Request("Amt") Request("Amount")
                    If (String.IsNullOrEmpty(agent) AndAlso String.IsNullOrEmpty(Action) AndAlso String.IsNullOrEmpty(Request("ID"))) Then
                        minus.Visible = False
                        plus.Visible = False
                        txt_crd_val.Visible = False
                        txt_crd_val.Text = 0
                    End If
                    td_spl.Visible = False
                Else
                    minus.Visible = False
                    plus.Visible = False
                    txt_crd_val.Visible = False
                End If
                '' ENNNNNN
            End If
        Catch ex As Exception

        End Try
    End Sub
    Public Function CheckCRRecord(ByVal agentId As String, ByRef count As String) As DataTable
        Dim con As New SqlConnection
        Dim adp As SqlDataAdapter
        Dim dt As New DataTable
        Dim cmd As New SqlCommand
        If con.State = ConnectionState.Open Then con.Close()
        con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString

        'cmd = New SqlCommand("SP_CKECKCRDETAILS", con)
        'cmd.Parameters.AddWithValue("@AGENTID", td_AgentID.InnerText)
        'cmd.Parameters.Add("@Count", SqlDbType.Int)
        'cmd.Parameters(


        adp = New SqlDataAdapter("SP_CKECKCRDETAILS", con)
        adp.SelectCommand.CommandType = CommandType.StoredProcedure
        adp.SelectCommand.Parameters.AddWithValue("@AGENTID", agentId)
        adp.SelectCommand.Parameters.Add("@Count", SqlDbType.Int)
        adp.SelectCommand.Parameters("@Count").Direction = ParameterDirection.Output
        adp.Fill(dt)

        count = adp.SelectCommand.Parameters("@Count").Value.ToString()

        Return dt

    End Function
    Public Function GetMailingDetails(ByVal department As String) As DataTable
        Dim con As New SqlConnection
        Dim adp As SqlDataAdapter
        Dim dt As New DataTable
        Dim cmd As New SqlCommand
        If con.State = ConnectionState.Open Then con.Close()
        con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        adp = New SqlDataAdapter("SP_GetMailDetailsBydepartmant", con)
        adp.SelectCommand.CommandType = CommandType.StoredProcedure
        adp.SelectCommand.Parameters.AddWithValue("@Department", department)
        adp.SelectCommand.Parameters.AddWithValue("@DistrId", "KT")
        adp.Fill(dt)

        Return dt

    End Function
    Public Function GetMailBody(ByVal AgentName As String, ByVal UploadType As String, ByVal Amount As String, ByVal UploadDate As String, ByVal AVLBalance As String, ByVal Regards As String) As String
        Dim Body As String = ""
        Body = "<table cellpadding='0' cellspacing='0'>"
        Body += " <tr><td>Dear " & AgentName & ", </td><td></td></tr>"

        Body += "<tr><td colspan='2'>Amount has been " & UploadType & " to your account with <b> INR. " & Amount & "</b>( " & UploadDate & ").Now your current balance is <b>INR." + AVLBalance + "</b>. </td></tr>"
        Body += "<tr><td >&nbsp;</td></tr>"
        Body += "<tr><td >Thanks & Regards</td></tr>"
        Body += "<tr><td >" & Regards & "</td></tr>"

        Return Body

    End Function
    'Public Function GetSubject() As String
    '    Dim subj As String = "test mail"
    '    Return subj
    'End Function
    Public Function SendMail(ByVal toEMail As String, ByVal from As String, ByVal bcc As String, ByVal cc As String, ByVal smtpClient As String, ByVal userID As String, ByVal pass As String, ByVal body As String, ByVal subject As String) As Integer


        Dim objMail As New System.Net.Mail.SmtpClient
        Dim msgMail As New System.Net.Mail.MailMessage
        msgMail.To.Clear()
        msgMail.To.Add(New System.Net.Mail.MailAddress(toEMail))
        msgMail.From = New System.Net.Mail.MailAddress(from)
        If bcc <> "" Then
            msgMail.Bcc.Add(New System.Net.Mail.MailAddress(bcc))
        End If
        If cc <> "" Then
            msgMail.CC.Add(New System.Net.Mail.MailAddress(cc))
        End If

        msgMail.Subject = subject
        msgMail.IsBodyHtml = True
        msgMail.Body = body


        Try
            objMail.Credentials = New System.Net.NetworkCredential(userID, pass)
            objMail.Host = smtpClient
            objMail.Send(msgMail)
            Return 1

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
            Return 0

        End Try
    End Function
    ''Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click
    ''Protected Sub plus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles plus.Click
    Protected Sub plus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles plus.Click
        Try
            Dim CheckBalStatus As Boolean = False
            Dim StatusFR As Boolean
            Dim Sts As Boolean
            Sts = True
            Dim body As String = ""
            Dim subject As String = ""
            Dim email_id As String = ""
            'Dim COUNTER As Integer
            Dim dtagdetails As New DataTable
            Dim MailDt As New DataTable
            Dim ObjIntDetails As New IntlDetails()
            Try
                MailDt = GetMailingDetails("ACCOUNT")
                dtagdetails = ObjIntDetails.AgentIDInfo(Request("AgentID"))
                email_id = dtagdetails.Rows(0)("Email").ToString()
            Catch ex As Exception

            End Try
            If (String.IsNullOrEmpty(hidTransType.Value)) Then
                hidTransType.Value = "CREDIT NOTE"
            End If

            'For Narration
            Dim BankName As String = ""
            Dim BankCode As String = ""
            Dim Narration As String = ""
            Try
                If (ddl_uploadtype.SelectedValue.Trim().ToUpper = "CA") Then
                    BankName = ddl_banklist.SelectedItem.Text
                    BankCode = ddl_banklist.SelectedValue
                    Narration = txt_narration.Text.Trim()
                End If
            Catch ex As Exception

            End Try

            'End Narration

            If (ChkSpl.Checked = True OrElse Sts = True) Then
                If (Request("ID") <> "" AndAlso Request("AgentID") IsNot Nothing) Then
                    Dim DtDDetails As New DataTable
                    DtDDetails = STDom.GetDepositDetailsByID(Request("ID")).Tables(0)
                    Dim Status As String = DtDDetails.Rows(0)("Status").ToString
                    Dim IDS As String = DtDDetails.Rows(0)("Counter").ToString
                    Dim RequestId As Integer = Convert.ToInt32(Request("ID"))
                    If IDS = RequestId AndAlso Status = "Confirm" Then
                        ''ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Deposite amount is already Credited');", True)
                        tbl_Upload.Visible = False
                        td_msg.Visible = True
                        td_msg.InnerText = "Deposite amount is already Credited"
                    Else
                        If (txt_crd_val.Text.Trim <= Request("Amount")) Then
                            'Transaction Begin
                            Dim IP As String = Request.UserHostAddress
                            Dim HSCREDIT As New Hashtable
                            HSCREDIT = GET_INSERTUPLOADDETAILS_TRANSACTION(Convert.ToDouble(txt_crd_val.Text), Request("AgentID"), td_AgencyName.InnerText, "", "", "", "", "", Session("UID").ToString(), "", IP, 0, Convert.ToDouble(txt_crd_val.Text.Trim), "Credit", txt_rm.Text.Trim, 0, ddl_uploadtype.SelectedValue, txt_Yatra.Text.Trim, Request("ID"), "Confirm", "Con", "", BankName, BankCode, Narration, 1, hidTransType.Value)
                            If HSCREDIT("RESULT") <> "" Then
                                If HSCREDIT("RESULT").ToString().ToUpper().Contains("UPLOAD") = False Then
                                    'NAV METHOD  CALL START
                                    Try
                                        'Dim objNav As New AirService.clsConnection()
                                        'objNav.PushDepositPaymentA(HSCREDIT("RESULT").ToString(), "CREDIT")
                                    Catch ex As Exception

                                    End Try
                                    'Nav METHOD END'
                                    'Try
                                    '    Dim P_upload As New Party
                                    '    StatusFR = GETFIRSTUPLOAD(td_AgentID.InnerText)
                                    '    If (StatusFR = False) Then
                                    '        P_upload.InsertParty_Details(Request("AgentID"))
                                    '    End If
                                    '    P_upload.InsertUpload_Details(HSCREDIT("RESULT"), Request("AgentID"), "CREDIT", Convert.ToDecimal(txt_crd_val.Text), txt_rm.Text.Trim)
                                    '    If (ddl_uploadtype.SelectedValue.Trim().ToUpper = "CA") And BankCode <> "CASH" Then
                                    '        P_upload.InsertUpload_Details(HSCREDIT("RESULT"), Request("AgentID"), "CREDIT", Convert.ToDecimal(txt_crd_val.Text), Narration, BankCode)
                                    '    End If
                                    'Catch ex As Exception

                                    'End Try
                                    'Try
                                    '    body = GetMailBody(dtagdetails.Rows(0)("FName").ToString() & " " & dtagdetails.Rows(0)("LName").ToString(), "Credited", txt_crd_val.Text, DateTime.Now.ToString("dd-MM-yyyy"), HSCREDIT("AVLBALANCE"), MailDt.Rows(0)("REGARDS").ToString())
                                    '    SendMail(email_id, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserID").ToString(), MailDt.Rows(0)("Pass").ToString(), body, "Regarding Upload")
                                    'Catch ex As Exception
                                    'End Try
                                    tbl_Upload.Visible = False
                                    td_msg.Visible = True
                                    td_msg.InnerText = "Amount Credited Sucessfully. " & td_AgencyName.InnerText & " current balance " & HSCREDIT("AVLBALANCE") & " INR"
                                    Dim smsMsg As String = ""
                                    Dim smsStatus As String = ""
                                    Try
                                        Dim objSMSAPI As New SMSAPI.SMS
                                        Dim dtagentmob As New DataTable()
                                        dtagentmob = STDom.GetAgencyDetails(Request("AgentId")).Tables(0)
                                        Dim Name As String = dtagentmob.Rows(0)("Title").ToString & " " & dtagentmob.Rows(0)("FName").ToString & " " & dtagentmob.Rows(0)("LName").ToString


                                        Dim SmsCrd As DataTable
                                        Dim objDA As New SqlTransaction
                                        SmsCrd = objDA.SmsCredential(SMS.UPLOADCREDIT.ToString()).Tables(0)
                                        If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                                            smsStatus = objSMSAPI.sendUploadSmsCreditDebit(Request("AgentId").ToString(), td_Mobile.InnerText.Trim(), txt_crd_val.Text.Trim(), HSCREDIT("AVLBALANCE"), td_AgencyName.InnerText.Trim(), ddl_uploadtype.SelectedValue.ToString(), Name, smsMsg, SmsCrd, "CREDIT")
                                            objSql.SmsLogDetails(Request("AgentId").ToString(), td_Mobile.InnerText.Trim(), smsMsg, smsStatus)
                                        End If
                                    Catch ex As Exception

                                    End Try



                                Else
                                    tbl_Upload.Visible = False
                                    td_msg.Visible = True
                                    td_msg.InnerText = HSCREDIT("RESULT").ToString()
                                    ' ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert(" & HSCREDIT("RESULT").ToString() & ");", True)
                                End If

                            Else
                                tbl_Upload.Visible = False
                                td_msg.Visible = True
                                td_msg.InnerText = "Unable to process upload. Please cross check and upload again."
                            End If
                            'Transaction End
                        Else
                            ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Amount is greater than requested amount');", True)
                        End If
                    End If
                Else

                    'Transaction Begin
                    Dim IP As String = Request.UserHostAddress

                    Dim HSCREDIT As New Hashtable
                    HSCREDIT = GET_INSERTUPLOADDETAILS_TRANSACTION(Convert.ToDouble(txt_crd_val.Text), td_AgentID.InnerText, td_AgencyName.InnerText, "", "", "", "", "", Session("UID").ToString(), "", IP, 0, Convert.ToDouble(txt_crd_val.Text.Trim), "Credit", txt_rm.Text.Trim, 0, ddl_uploadtype.SelectedValue, txt_Yatra.Text.Trim, 0, "Confirm", "", ddl_moduletype.SelectedItem.Text, BankName, BankCode, Narration, 0, hidTransType.Value)
                    If HSCREDIT("RESULT") <> "" Then
                        If HSCREDIT("RESULT").ToString().ToUpper().Contains("UPLOAD") = False Then
                            'NAV METHOD  CALL START
                            'Try
                            '    Dim objNav As New AirService.clsConnection()
                            '    objNav.PushDepositPaymentA(HSCREDIT("RESULT").ToString(), "CREDIT")
                            'Catch ex As Exception
                            'End Try
                            'Nav METHOD END'

                            'yatra Payment upload

                            'Try
                            '    Dim P_upload As New Party
                            '    StatusFR = GETFIRSTUPLOAD(td_AgentID.InnerText)
                            '    If (StatusFR = False) Then
                            '        P_upload.InsertParty_Details(td_AgentID.InnerText)
                            '    End If

                            '    P_upload.InsertUpload_Details(HSCREDIT("RESULT"), td_AgentID.InnerText, "CREDIT", Convert.ToDecimal(txt_crd_val.Text), txt_rm.Text.Trim, ddl_moduletype.SelectedValue)
                            '    If (ddl_uploadtype.SelectedValue.Trim().ToUpper = "CA") And BankCode <> "CASH" Then
                            '        P_upload.InsertUpload_Details(HSCREDIT("RESULT"), td_AgentID.InnerText, "CREDIT", Convert.ToDecimal(txt_crd_val.Text), Narration, BankCode)
                            '    End If
                            'Catch ex As Exception

                            ''End Try
                            'Try
                            '    body = GetMailBody(dtagdetails.Rows(0)("FName").ToString() & " " & dtagdetails.Rows(0)("LName").ToString(), "Credited", txt_crd_val.Text, DateTime.Now.ToString("dd-MM-yyyy"), HSCREDIT("AVLBALANCE"), MailDt.Rows(0)("REGARDS").ToString())
                            '    SendMail(email_id, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserID").ToString(), MailDt.Rows(0)("Pass").ToString(), body, "Regarding Amount Upload")
                            'Catch ex As Exception
                            'End Try
                            tbl_Upload.Visible = False
                            td_msg.Visible = True
                            td_msg.InnerText = "Amount Credited Sucessfully. " & td_AgencyName.InnerText & " current balance " & HSCREDIT("AVLBALANCE").ToString() & " INR"
                            Dim smsMsg As String = ""
                            Dim smsStatus As String = ""
                            Try
                                Dim objSMSAPI As New SMSAPI.SMS
                                Dim dtagentmob As New DataTable()
                                dtagentmob = STDom.GetAgencyDetails(Request("AgentId")).Tables(0)
                                Dim Name As String = dtagentmob.Rows(0)("Title").ToString & " " & dtagentmob.Rows(0)("FName").ToString & " " & dtagentmob.Rows(0)("LName").ToString


                                Dim SmsCrd As DataTable
                                Dim objDA As New SqlTransaction
                                SmsCrd = objDA.SmsCredential(SMS.UPLOADCREDIT.ToString()).Tables(0)
                                If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                                    smsStatus = objSMSAPI.sendUploadSmsCreditDebit(Request("AgentId").ToString(), td_Mobile.InnerText.Trim(), txt_crd_val.Text.Trim(), HSCREDIT("AVLBALANCE"), td_AgencyName.InnerText.Trim(), ddl_uploadtype.SelectedValue.ToString(), Name, smsMsg, SmsCrd, "CREDIT")
                                    objSql.SmsLogDetails(Request("AgentId").ToString(), td_Mobile.InnerText.Trim(), smsMsg, smsStatus)
                                End If
                            Catch ex As Exception

                            End Try

                        Else
                            tbl_Upload.Visible = False
                            td_msg.Visible = True
                            td_msg.InnerText = HSCREDIT("RESULT").ToString()
                            'ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert(" & HSCREDIT("RESULT").ToString() & ");", True)
                        End If

                    Else
                        tbl_Upload.Visible = False
                        td_msg.Visible = True
                        td_msg.InnerText = "Unable to process upload. Please cross check and upload again."
                    End If
                    'Transaction End
                End If
                ChkSpl.Checked = False

            Else
                Dim MSG As String = ""
                ' MSG = dt.Rows(0)("CRMsg").ToString()
                ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "TEMP", "alert('" & MSG & "')", True)
                ' ShowAlertMessage(MSG)
            End If
            msg_show.Value = td_msg.InnerText
            If (Request("ID") <> "") Then
                ScriptManager.RegisterStartupScript(Me, Page.[GetType](), "key", "MyFunc()", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Page.[GetType](), "key", "MyFunc1()", True)
            End If


        Catch ex As Exception

        End Try
    End Sub
    ''Protected Sub btn_Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_Search.Click  'Protected Sub minus_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles minus.Click
    Protected Sub minus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles minus.Click
        Try
            'Dim MailDt As DataTable = GetMailingDetails("ACCOUNT")
            'Dim dtemail As DataTable
            'Dim ObjIntDetails As New IntlDetails()
            'dtemail = ObjIntDetails.AgentIDInfo(Request("AgentID"))
            'Dim email_id As String = dtemail.Rows(0)("Email").ToString()
            'Dim body As String = "Dear sir"
            'Dim subject As String = "Amount Debit"
            Dim body As String = ""
            Dim subject As String = ""
            Dim email_id As String = ""
            Dim COUNTER As Integer
            Dim dtagdetails As New DataTable
            Dim MailDt As New DataTable
            Dim ObjIntDetails As New IntlDetails()
            Dim CheckBalStatus As Boolean = False
            Try
                MailDt = GetMailingDetails("ACCOUNT")
                dtagdetails = ObjIntDetails.AgentIDInfo(Request("AgentID"))
                email_id = dtagdetails.Rows(0)("Email").ToString()
            Catch ex As Exception

            End Try


            If (String.IsNullOrEmpty(hidTransType.Value)) Then
                 hidTransType.Value = "DEBIT NOTE"
            End If

            'Transaction Begin
            Dim IP As String = Request.UserHostAddress
            Dim HSDEBIT As New Hashtable
            HSDEBIT = GET_INSERTUPLOADDETAILS_TRANSACTION(Convert.ToDouble(txt_crd_val.Text), Request("AgentID"), td_AgencyName.InnerText, "", "", "", "", "", Session("UID").ToString(), "", IP.Trim, Convert.ToDouble(txt_crd_val.Text.Trim), 0, "Debit", txt_rm.Text.Trim, 0, ddl_uploadtype.SelectedValue, txt_Yatra.Text.Trim, 0, "", "", ddl_moduletype.SelectedItem.Text, "", "", "", 0, hidTransType.Value)
            If HSDEBIT("RESULT") <> "" Then
                'NAV METHOD  CALL START
                Try
                    'Dim objNav As New AirService.clsConnection()
                    'objNav.PushDepositPaymentA(HSDEBIT("RESULT").ToString(), "DEBIT")
                Catch ex As Exception

                End Try
                'Nav METHOD END'
                Try
                    Dim P_upload As New Party
                    P_upload.InsertUpload_Details(HSDEBIT("RESULT"), Request("AgentID"), "DEBIT", Convert.ToDecimal(txt_crd_val.Text), txt_rm.Text.Trim, ddl_moduletype.SelectedValue)
                Catch ex As Exception

                End Try
                ' STDom.SendMail(email_id, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserID").ToString(), MailDt.Rows(0)("Pass").ToString(), body, subject)
                'Try
                '    body = GetMailBody(dtagdetails.Rows(0)("FName").ToString() & " " & dtagdetails.Rows(0)("LName").ToString(), "Debited", txt_crd_val.Text, DateTime.Now.ToString("dd-MM-yyyy"), HSDEBIT("AVLBALANCE").ToString(), MailDt.Rows(0)("REGARDS").ToString())
                '    SendMail(email_id, MailDt.Rows(0)("MAILFROM").ToString(), MailDt.Rows(0)("BCC").ToString(), MailDt.Rows(0)("CC").ToString(), MailDt.Rows(0)("SMTPCLIENT").ToString(), MailDt.Rows(0)("UserID").ToString(), MailDt.Rows(0)("Pass").ToString(), body, "Regarding Amount Upload")

                'Catch ex As Exception

                'End Try

                Try
                    Dim smsMsg As String = ""
                    Dim smsStatus As String = ""
                    Dim objSMSAPI As New SMSAPI.SMS
                    Dim dtagentmob As New DataTable()
                    dtagentmob = STDom.GetAgencyDetails(Request("AgentId")).Tables(0)
                    Dim Name As String = dtagentmob.Rows(0)("Title").ToString & " " & dtagentmob.Rows(0)("FName").ToString & " " & dtagentmob.Rows(0)("LName").ToString


                    Dim SmsCrd As DataTable
                    Dim objDA As New SqlTransaction
                    SmsCrd = objDA.SmsCredential(SMS.UPLOADDEBIT.ToString()).Tables(0)
                    If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                        smsStatus = objSMSAPI.sendUploadSmsCreditDebit(Request("AgentId").ToString(), td_Mobile.InnerText.Trim(), txt_crd_val.Text.Trim(), HSDEBIT("AVLBALANCE").ToString(), td_AgencyName.InnerText.Trim(), ddl_uploadtype.SelectedValue.ToString(), Name, smsMsg, SmsCrd, "DEBIT")
                        objSql.SmsLogDetails(Request("AgentId").ToString(), td_Mobile.InnerText.Trim(), smsMsg, smsStatus)
                    End If
                Catch ex As Exception

                End Try


                tbl_Upload.Visible = False
                td_msg.Visible = True
                td_msg.InnerText = "Amount Debited Sucessfully. " & td_AgencyName.InnerText & " current balance " & HSDEBIT("AVLBALANCE").ToString() & " INR"

            Else
                tbl_Upload.Visible = False
                td_msg.Visible = True
                td_msg.InnerText = "Unable to process debit. Please cross check and debit again."
            End If
            msg_show.Value = td_msg.InnerText
            If (Request("ID") <> "") Then
                ScriptManager.RegisterStartupScript(Me, Page.[GetType](), "key", "MyFunc()", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Page.[GetType](), "key", "MyFunc1()", True)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub minus_SeriesClick(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles minus_Series.Click
        Try
            Dim CurrentAval_Bal As Double = 0
            CurrentAval_Bal = ST.UpdateCrdLimit(Request("AgentID"), Convert.ToDouble(txt_crd_val.Text))
            Dim IP1 As String = Request.UserHostAddress
            STDom.insertLedgerDetails(Request("AgentID"), td_AgencyName.InnerText, "", "", "", "", "", Session("UID").ToString(), "", IP1.Trim, txt_crd_val.Text.Trim, 0, CurrentAval_Bal, "Debit", txt_rm.Text.Trim, 0)
            Dim LastAval_Bal As Double = 0
            LastAval_Bal = CurrentAval_Bal + Convert.ToDouble(txt_crd_val.Text.Trim)
            Dim UploadTypeDt As New DataTable
            UploadTypeDt = STDom.GetUploadTypeByType(Label1.Text).Tables(0)

            STDom.insertUploadDetails(Request("AgentID"), td_AgencyName.InnerText, Session("UID").ToString(), IP1, txt_crd_val.Text.Trim, 0, txt_rm.Text.Trim, ddl_uploadtype.SelectedValue, LastAval_Bal, CurrentAval_Bal, txt_Yatra.Text.Trim)
            Dim i As Integer = series.UpdateSeriesPendingRequest(Session("UID").ToString, IP1, "Confirm", Request("Counter"), txt_rm.Text.Trim)
            tbl_Upload.Visible = False
            td_msg.Visible = True
            Try
                Dim smsMsg As String = ""
                Dim smsStatus As String = ""
                Dim objSMSAPI As New SMSAPI.SMS
                Dim dtagentmob As New DataTable()
                dtagentmob = STDom.GetAgencyDetails(Request("AgentId")).Tables(0)
                Dim Name As String = dtagentmob.Rows(0)("Title").ToString & " " & dtagentmob.Rows(0)("FName").ToString & " " & dtagentmob.Rows(0)("LName").ToString


                Dim SmsCrd As DataTable
                Dim objDA As New SqlTransaction
                SmsCrd = objDA.SmsCredential(SMS.UPLOADDEBIT.ToString()).Tables(0)
                If SmsCrd.Rows.Count > 0 AndAlso SmsCrd.Rows(0)("Status") = True Then
                    smsStatus = objSMSAPI.sendUploadSmsCreditDebit(Request("AgentId").ToString(), td_Mobile.InnerText.Trim(), txt_crd_val.Text.Trim(), CurrentAval_Bal, td_AgencyName.InnerText.Trim(), ddl_uploadtype.SelectedValue.ToString(), Name, smsMsg, SmsCrd, "DEBIT")
                    objSql.SmsLogDetails(Request("AgentId").ToString(), td_Mobile.InnerText.Trim(), smsMsg, smsStatus)
                End If
            Catch ex As Exception

            End Try

            td_msg.InnerText = "Amount Debited Sucessfully. " & td_AgencyName.InnerText & " current balance " & CurrentAval_Bal & " INR"
            msg_show.Value = td_msg.InnerText
            If (Request("ID") <> "") Then
                ScriptManager.RegisterStartupScript(Me, Page.[GetType](), "key", "MyFunc()", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Page.[GetType](), "key", "MyFunc1()", True)
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Function GET_INSERTUPLOADDETAILS_TRANSACTION(ByVal Amount As Double, ByVal AgentId As String, ByVal AgencyName As String, ByVal InvoiceNo As String, ByVal PnrNo As String, ByVal TicketNo As String, ByVal TicketingCarrier As String, ByVal YatraAccountID As String, ByVal AccountID As String, ByVal ExecutiveID As String, ByVal IPAddress As String, ByVal Debit As Double, ByVal Credit As Double, ByVal BookingType As String, ByVal Remark As String, ByVal PaxId As Integer, ByVal Uploadtype As String, ByVal YtrRcptNo As String, ByVal ID As Integer, ByVal Status As String, ByVal Type As String, ByVal Rmk As String, ByVal BankName As String, ByVal BankCode As String, ByVal Narration As String, ByVal SplStatus As Integer, ByVal TransType As String) As Hashtable
        Dim HS As New Hashtable
        Dim con As New SqlConnection
        Dim adp As SqlDataAdapter
        Dim dt As New DataTable
        Dim cmd As New SqlCommand
        If con.State = ConnectionState.Open Then con.Close()
        con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        adp = New SqlDataAdapter("SP_INSERTUPLOADDETAILS_TRANSACTION", con)
        adp.SelectCommand.CommandType = CommandType.StoredProcedure
        adp.SelectCommand.Parameters.AddWithValue("@Amount", Amount)
        adp.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId)
        adp.SelectCommand.Parameters.AddWithValue("@AgencyName", AgencyName)
        adp.SelectCommand.Parameters.AddWithValue("@InvoiceNo", InvoiceNo)
        adp.SelectCommand.Parameters.AddWithValue("@PnrNo", PnrNo)
        adp.SelectCommand.Parameters.AddWithValue("@TicketNo", TicketNo)
        adp.SelectCommand.Parameters.AddWithValue("@TicketingCarrier", TicketingCarrier)
        adp.SelectCommand.Parameters.AddWithValue("@YatraAccountID", YatraAccountID)
        adp.SelectCommand.Parameters.AddWithValue("@AccountID", AccountID)
        adp.SelectCommand.Parameters.AddWithValue("@ExecutiveID", ExecutiveID)
        adp.SelectCommand.Parameters.AddWithValue("@IPAddress", IPAddress)
        adp.SelectCommand.Parameters.AddWithValue("@Debit", Debit)
        adp.SelectCommand.Parameters.AddWithValue("@Credit", Credit)
        adp.SelectCommand.Parameters.AddWithValue("@BookingType", BookingType)
        adp.SelectCommand.Parameters.AddWithValue("@Remark", Remark)
        adp.SelectCommand.Parameters.AddWithValue("@PaxId", PaxId)
        adp.SelectCommand.Parameters.AddWithValue("@Uploadtype", Uploadtype)
        adp.SelectCommand.Parameters.AddWithValue("@YtrRcptNo", YtrRcptNo)
        adp.SelectCommand.Parameters.AddWithValue("@ID", ID)
        adp.SelectCommand.Parameters.AddWithValue("@Status", Status)
        adp.SelectCommand.Parameters.AddWithValue("@Type", Type)
        adp.SelectCommand.Parameters.AddWithValue("@Rmk", Rmk)
        adp.SelectCommand.Parameters.AddWithValue("@SplStatus", SplStatus)
        adp.SelectCommand.Parameters.AddWithValue("@BankName", BankName)
        adp.SelectCommand.Parameters.AddWithValue("@BankCode", BankCode)
        adp.SelectCommand.Parameters.AddWithValue("@Narration", Narration)
        adp.SelectCommand.Parameters.AddWithValue("@TransType", TransType)
        adp.SelectCommand.Parameters.Add("@Aval_Balance", SqlDbType.Decimal)
        adp.SelectCommand.Parameters("@Aval_Balance").Direction = ParameterDirection.Output
        adp.SelectCommand.Parameters.Add("@result", SqlDbType.NVarChar, 500)
        adp.SelectCommand.Parameters("@result").Direction = ParameterDirection.Output
        adp.Fill(dt)
        Dim Result As String = adp.SelectCommand.Parameters("@result").Value.ToString()
        Dim AvailableBalance As Decimal = Convert.ToDecimal(adp.SelectCommand.Parameters("@Aval_Balance").Value.ToString())
        HS.Add("RESULT", Result)
        HS.Add("AVLBALANCE", AvailableBalance)
        Return HS
    End Function
    Private Function GETFIRSTUPLOAD(ByVal AgentID As String) As Boolean
        Dim Status As Boolean
        Try
            Dim con As New SqlConnection
            If con.State = ConnectionState.Open Then
                con.Close()
            End If
            con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
            con.Open()
            Dim cmd As SqlCommand
            cmd = New SqlCommand("SP_GETFIRSTUPLOAD", con)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@AGENTID", AgentID)
            Status = cmd.ExecuteScalar()
            con.Close()
        Catch ex As Exception
            Status = True
        End Try
        Return Status
    End Function
    Protected Sub ddl_uploadtype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddl_uploadtype.SelectedIndexChanged
        If ddl_uploadtype.SelectedValue.Trim().ToUpper() <> "CA" Then
            td_BankDetails.Visible = False
        Else
            td_BankDetails.Visible = True
        End If
    End Sub

End Class

