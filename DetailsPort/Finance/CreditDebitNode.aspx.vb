﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class DetailsPort_Finance_CreditDebitNode
    Inherits System.Web.UI.Page
    Dim cmd As SqlCommand
    Dim adp As SqlDataAdapter
    Dim ST As New SqlTransaction
    Dim dsAgencyDetails As New DataSet()
    Dim dsFlightDetails As New DataSet()
    Dim objSelectedfltCls As New clsInsertSelectedFlight
    Dim sqlCon As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Dim AgentId As String = ""
    Dim OrderId As String = ""
    Dim AgencyId As String = ""
    Dim AgencyName As String = ""
    Dim DistributerId As String = ""
    Dim IP As String = ""
    Dim CreatedBy As String = ""
    Dim ReferenceId As String = ""
    Dim ServiceType As String = ""
    Dim InvoiceNo As String = ""
    Dim TicketNo As String = ""
    Dim TicketingCarier As String = ""
    Dim PnrNo As String = ""
    Dim CreditAmount As Double = 0
    Dim DebitAmount As Double = 0
    Dim Aval_Balence As Double = 0
    Dim Remark As String = ""
    Dim BookingType As String = ""


    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If

            AgentId = If([String].IsNullOrEmpty(Request.QueryString("AgentId")), "", Request.QueryString("AgentId"))
            OrderId = If([String].IsNullOrEmpty(Request.QueryString("OrderID")), "", Request.QueryString("OrderID"))
            PnrNo = If([String].IsNullOrEmpty(Request.QueryString("PNR")), "", Request.QueryString("PNR"))
            TicketNo = If([String].IsNullOrEmpty(Request.QueryString("TicketNo")), "", Request.QueryString("TicketNo"))
            TicketingCarier = If([String].IsNullOrEmpty(Request.QueryString("TicketingCarier")), "", Request.QueryString("TicketingCarier"))
            CreatedBy = If([String].IsNullOrEmpty(Session("UID").ToString()), "", Session("UID").ToString())

            dsAgencyDetails = ST.GetAgencyDetails(AgentId)

            Dim agencyInfo As String = ""
            If (dsAgencyDetails.Tables(0).Rows.Count > 0) Then

                If Not IsDBNull(dsAgencyDetails.Tables(0).Rows(0)("AgencyId")) Then
                    AgencyId = dsAgencyDetails.Tables(0).Rows(0)("AgencyId")
                End If

                If Not IsDBNull(dsAgencyDetails.Tables(0).Rows(0)("Agency_Name")) Then
                    AgencyName = dsAgencyDetails.Tables(0).Rows(0)("Agency_Name")
                End If

                If Not IsDBNull(dsAgencyDetails.Tables(0).Rows(0)("Distr")) Then
                    DistributerId = dsAgencyDetails.Tables(0).Rows(0)("Distr")
                End If

                If Not IsDBNull(Convert.ToDouble(dsAgencyDetails.Tables(0).Rows(0)("Crd_Limit"))) Then
                    Aval_Balence = Convert.ToDouble(dsAgencyDetails.Tables(0).Rows(0)("Crd_Limit"))
                End If


                agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif;font-size:26px;font-weight:bold;color:#000'>" + dsAgencyDetails.Tables(0).Rows(0)("Agency_Name") + "</span><br/>"
                agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'> " + dsAgencyDetails.Tables(0).Rows(0)("Address") + "</span>"
                agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'>, " + dsAgencyDetails.Tables(0).Rows(0)("City") + "</span>"
                agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'> - " + dsAgencyDetails.Tables(0).Rows(0)("zipcode") + "</span>"
                agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'>, " + dsAgencyDetails.Tables(0).Rows(0)("State") + "</span>"
                agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'><br/>Mob. " + dsAgencyDetails.Tables(0).Rows(0)("Mobile") + "</span>"
                agencyInfo += "<span style='font-family: arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;  color: #666666'>,Email: " + dsAgencyDetails.Tables(0).Rows(0)("Email") + "</span>"

            End If





            Dim totalDebit As Double
            Dim totalCredit As Double
            Dim DSGETSUM As DataSet = GetCreditDebitNodeDetails(OrderId)

            If DSGETSUM.Tables.Count > 0 Then
                Dim dtbl As New DataTable()
                dtbl = DSGETSUM.Tables(1)

                If Not IsDBNull(dtbl.Rows(0)("totalDebit")) Then
                    totalDebit = Convert.ToDouble(dtbl.Rows(0)("totalDebit"))
                End If
                If Not IsDBNull(dtbl.Rows(0)("totalCredit")) Then
                    totalCredit = Convert.ToDouble(dtbl.Rows(0)("totalCredit"))
                End If

            End If






            ReferenceId = "CD" + objSelectedfltCls.getRndNum
            ServiceType = "Flight"
            IP = Request.UserHostAddress

            If Not IsPostBack Then
                lbl_agentId.Text = AgentId.ToString()
                lbl_OrderId.Text = OrderId.ToString()
                If Aval_Balence <> 0 Then
                    lblCurrentBal.Text = Aval_Balence.ToString() + " INR"
                Else
                    lblCurrentBal.Text = "0"
                End If
                lblAgencyInfo.Text = agencyInfo.ToString()

                If totalCredit <> 0 Then
                    lblTotalCredit.Text = totalCredit.ToString()
                Else
                    lblTotalCredit.Text = "0"
                End If

                If totalDebit <> 0 Then
                    lblTotalDebit.Text = totalDebit.ToString()
                Else
                    lblTotalDebit.Text = "0"
                End If

                BindGrid()

            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btnAddCredit_Click(sender As Object, e As EventArgs) Handles btnAddCredit.Click
        Try
            Dim effect As Integer
            Dim CreditLimit As Double = 0
            If txtAmount.Text <> "" Then
                CreditAmount = Convert.ToDouble(txtAmount.Text)
            End If
            Remark = If([String].IsNullOrEmpty(txtRemarks.Text), "", txtRemarks.Text)


            Dim ds As New DataSet()
            Dim limit As Double
            ds = GetCredditLimit(OrderId, "CreditLimit")
            If ds.Tables(0).Columns.Count > 0 Then

                limit = Convert.ToDouble(ds.Tables(0).Rows(0)("totalCreditLimit"))
            End If

            If CreditAmount > 0 Then
                If CreditAmount <= limit Then
                    effect = InsertCreditDebitNodeTrans(OrderId, AgentId, AgencyId, AgencyName, DistributerId, DebitAmount, CreditAmount,
                                                    Remark, ServiceType, IP, CreatedBy, ReferenceId, PnrNo, TicketNo, TicketingCarier, "",
                                                    CreatedBy, BookingType, "CreditNode")
                End If
            End If

            If effect > 0 Then
                ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('Amount credited successfully.!');", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('Transaction failed.!');", True)
            End If

            txtAmount.Text = ""
            txtRemarks.Text = ""
            BindGrid()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub

    Protected Sub btnAddDebit_Click(sender As Object, e As EventArgs) Handles btnAddDebit.Click
        Try

            Dim effect As Integer
            If txtAmount.Text <> "" Then
                DebitAmount = Convert.ToDouble(txtAmount.Text)
                Remark = If([String].IsNullOrEmpty(txtRemarks.Text), "", txtRemarks.Text)

            End If


            If DebitAmount > 0 Then
                effect = InsertCreditDebitNodeTrans(OrderId, AgentId, AgencyId, AgencyName, DistributerId, DebitAmount, CreditAmount,
                                               Remark, ServiceType, IP, CreatedBy, ReferenceId, PnrNo, TicketNo, TicketingCarier, "",
                                               CreatedBy, BookingType, "DebitNode")
            End If


            If effect > 0 Then
                ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('Amount debited successfully.!');", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "redirect", "alert('Transaction failed.!');", True)
            End If

            txtAmount.Text = ""
            txtRemarks.Text = ""
            BindGrid()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub

    Private Sub BindGrid()
        Try
            Dim ds As New DataSet()
            ds = GetCreditDebitNodeDetails(OrderId)
            gridCreditDebitDetails.DataSource = ds.Tables(0)
            gridCreditDebitDetails.DataBind()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Function GetCreditDebitNodeDetails(ByVal OrderId As String) As DataSet
        Dim ds As New DataSet()
        Try
            adp = New SqlDataAdapter("USP_GetCreditDebitNodeDetails", sqlCon)
            adp.SelectCommand.CommandType = CommandType.StoredProcedure
            adp.SelectCommand.Parameters.AddWithValue("@OrderId", OrderId)

            adp.Fill(ds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
        Return ds
    End Function

    Protected Sub gridCreditDebitDetails_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gridCreditDebitDetails.PageIndexChanging
        Try
            gridCreditDebitDetails.PageIndex = e.NewPageIndex
            Dim ds As New DataSet()
            ds = GetCreditDebitNodeDetails(OrderId)
            gridCreditDebitDetails.DataSource = ds.Tables(0)
            gridCreditDebitDetails.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub

    Function InsertCreditDebitNodeTrans(
                                  ByVal OrderId As String, ByVal AgentId As String, ByVal AgencyId As String, ByVal AgencyName As String, ByVal DistributerId As String,
                                  ByVal DebitAmt As Double, ByVal CreditAmt As Double, ByVal Remark As String, ByVal ServiceType As String,
                                  ByVal IPAddress As String, ByVal CreatedBy As String, ByVal ReferenceId As String, ByVal PNR As String, ByVal TicketNo As String,
                                  ByVal TicketingCarier As String, ByVal YatraAccountId As String, ByVal ExecutiveId As String, ByVal BookingType As String, ByVal Action As String
                                  ) As Integer
        Try
            cmd = New SqlCommand("USP_CreditDebitNodeTrans", sqlCon)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.AddWithValue("@OrderId", OrderId)
            cmd.Parameters.AddWithValue("@AgentId", AgentId)
            cmd.Parameters.AddWithValue("@AgencyId", AgencyId)
            cmd.Parameters.AddWithValue("@AgencyName", AgencyName)
            cmd.Parameters.AddWithValue("@DistributerId", DistributerId)
            cmd.Parameters.AddWithValue("@DebitAmt", DebitAmt)
            cmd.Parameters.AddWithValue("@CreditAmt", CreditAmt)
            cmd.Parameters.AddWithValue("@Remark", Remark)
            cmd.Parameters.AddWithValue("@ServiceType", ServiceType)
            cmd.Parameters.AddWithValue("@IPAddress", IPAddress)
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy)
            cmd.Parameters.AddWithValue("@ReferenceId", ReferenceId)
            cmd.Parameters.AddWithValue("@PNR", PNR)
            cmd.Parameters.AddWithValue("@TicketNo", TicketNo)
            cmd.Parameters.AddWithValue("@TicketingCarier", TicketingCarier)
            cmd.Parameters.AddWithValue("@YatraAccountId", YatraAccountId)
            cmd.Parameters.AddWithValue("@ExecutiveId", ExecutiveId)
            cmd.Parameters.AddWithValue("@BookingType", BookingType)
            cmd.Parameters.AddWithValue("@Action", Action)
            If ConnectionState.Open Then
                sqlCon.Close()
            End If
            sqlCon.Open()
            cmd.ExecuteNonQuery()
            Return 1

        Catch ex As Exception
            Return 0
            clsErrorLog.LogInfo(ex)
        End Try
    End Function
    Function GetCredditLimit(ByVal OrderId As String, ByVal Action As String) As DataSet
        adp = New SqlDataAdapter("USP_GetCreditLimitTrans", sqlCon)
        adp.SelectCommand.CommandType = CommandType.StoredProcedure
        adp.SelectCommand.Parameters.AddWithValue("@OrderId", OrderId)
        adp.SelectCommand.Parameters.AddWithValue("@Action", Action)

        Dim ds As New DataSet()
        adp.Fill(ds)
        Return ds

    End Function
End Class
