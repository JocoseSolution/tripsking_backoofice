﻿<%@ Page Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="HotelBookingRpt.aspx.vb" Inherits="DetailsPort_Hotel_HtlBookingRpt" %>

<%@ Register Src="~/UserControl/HotelMenu.ascx" TagPrefix="uc1" TagName="HotelMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="<%=ResolveUrl("~/Hotel/css/B2Bhotelengine.css")%>" rel="stylesheet" type="text/css" />
    <%--    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />--%>
    <%--    <link href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" type="text/css" />--%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script src="<%=ResolveUrl("~/Hotel/JS/HotelRefund.js")%>" type="text/javascript"></script>



    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <style type="text/css">
        .page-wrapperss
        {
            background-color: #fff;
            margin-left: 15px;
        }

        .lft
        {
            float: left;
        }
    </style>

    <div id="htlRfndPopup">
        <div class="refundbox">

            <div style="font-weight: bold; font-size: 16px; text-align: center; width: 100%;">

                <div id="RemarkTitle" runat="server"></div>
                <div style="float: right; width: 20px; height: 20px; margin: -20px -13px 0 0;">
                    <a href="javascript:ShowHide('hide');">
                        <img src="<%=ResolveUrl("~/Images/close.png") %>" height="20px" /></a>
                </div>

            </div>

            <div>
                <div class="large-12 medium-12 small-12">

                    <div class="laege-2 medium-2 small-3 columns bld">
                        Hotel Name:
                    </div>
                    <div class="laege-10 medium-10 small-9 columns" id="HotelName" runat="server"></div>
                    <div class="clear"></div>
                    <div class="laege-1 medium-1 small-3 columns bld">
                        Net Cost:
                    </div>
                    <div class="laege-2 medium-2 small-9 columns" id="amt" runat="server"></div>
                    <div class="large-2 medium-2 small-3  medium-push-1 columns bld">
                        No. of Room:
                    </div>
                    <div class="laege-2 medium-2 small-9 large-push-1 medium-push-1 columns" id="room" runat="server"></div>

                    <div class="laege-1 medium-2 small-3 large-push-2 medium-push-2 columns bld">
                        No. of Night:
                    </div>
                    <div class="laege-2 medium-2 small-9 large-push-2 medium-push-2 columns end" id="night" runat="server"></div>
                    <div class="clear"></div>
                    <div class="laege-1 medium-1 small-3 columns bld">
                        No. of Adult:
                    </div>
                    <div class="laege-2 medium-2 small-9 columns" id="adt" runat="server"></div>

                    <div class="laege-1 medium-1 small-3 large-push-1 medium-push-1 columns bld">
                        No. of Child:
                    </div>
                    <div class="laege-2 medium-2 small-9 large-push-2 medium-push-2 columns end" id="chd" runat="server"></div>

                </div>
            </div>
            <div class="clear1"></div>
            <div id="policy" runat="server"></div>

            <div style="visibility: hidden;">

                <div>

                    <div style="font-weight: bold;">
                        Full Cancellation
                                                        <input id="ChkFullCan" type="radio" name="Can" checked="checked" />
                    </div>
                    <div style="font-weight: bold; padding-left: 20px;">
                        Partial Cancellation
                                                        <input id="ChkParcialCan" type="radio" name="Can" />
                    </div>



                </div>

            </div>


            <div>

                <div style="font-weight: bold;">
                    Cancellation Remark:
                </div>
                <div>
                    <textarea id="txtRemarkss" cols="40" rows="2" name="txtRemarkss"></textarea>
                </div>
                <div style="float: right; padding-left: 40px;">
                    <%--<asp:Button ID="btn_Refund" runat="server" Text="Hotel Cancelltion" CssClass="button" ToolTip="Auto Cancel of Hotel"
                                            OnClientClick="return RemarkValidation('cancellation')" />--%>
                </div>
                <div class="clear1"></div>
            </div>

            <div>
                <input id="StartDate" type="hidden" name="StartDate" />
                <input id="EndDate" type="hidden" name="EndDate" />
                <input id="Parcial" type="hidden" name="Parcial" value="false" />
                <input id="OrderIDS" type="hidden" name="OrderIDS" runat="server" />
            </div>

        </div>
    </div>




    <div class="row">
        <uc1:HotelMenu runat="server" ID="Settings"></uc1:HotelMenu>
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading" style="background: #fdb714">
                        <h3 class="panel-title">Hotel > Search Hotel Booking</h3>
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From date</label>
                                    <input type="text" name="From" id="From" readonly="readonly" class="input-text full-width" />

                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">To date</label>

                                    <input type="text" name="To" id="To" readonly="readonly" class="input-text full-width" />
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Checkin</label>
                                    <input placeholder="Checkin" class="input-text full-width" type="text" name="Checkin" id="Checkin" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Order ID</label>
                                    <asp:TextBox CssClass="input-text full-width" runat="server" ID="txt_OrderId"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Booking ID</label>
                                    <asp:TextBox CssClass="input-text full-width" runat="server" ID="txt_bookingID"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Staus</label>
                                    <asp:DropDownList ID="ddlStatus" CssClass="input-text full-width" runat="server">

                                        <asp:ListItem Text="Confirm" Value="Confirm"></asp:ListItem>
                                        <asp:ListItem Text="Hold" Value="Hold"></asp:ListItem>
                                        <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Hotel name</label>
                                    <asp:TextBox CssClass="input-text full-width" runat="server" ID="txt_htlcode"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Room name</label>
                                    <asp:TextBox CssClass="input-text full-width" runat="server" ID="txt_roomcode"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Trip type</label>
                                    <asp:DropDownList ID="Triptype" CssClass="input-text full-width" runat="server">
                                        <asp:ListItem Text="Select Trip Type" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Domestic" Value="Domestic"></asp:ListItem>
                                        <asp:ListItem Text="International" Value="International"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>



                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group" id="TDAgency" runat="server">
                                    <label for="exampleInputPassword1">Agency Name</label>
                                    <asp:TextBox CssClass="input-text full-width" ID="txtAgencyName" runat="server" name="txtAgencyName" onfocus="focusObj(this);"
                                        onblur="blurObj(this);" value="Agency Name or ID"></asp:TextBox>
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                            </div>
                        

                        </div>
                            <div >
                           

                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="btn btn-info full-width btn-medium" />
                            <hr />
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="btn btn-info full-width btn-medium" />
                              
                            </div>


                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <div class="form-group" style="font-size: 15px; line-height: 20px; text-align: justify; color: Red;">
                                        * To get today booking, do not fill any field, only click on Search Result Button.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Total Amount :</label>
                                    <asp:Label ID="lbl_Total" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Total Hotel Booked :</label>
                                    <asp:Label ID="lbl_counttkt" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GrdReport" runat="server" AutoGenerateColumns="False" PageSize="8"
                                        CssClass="table" GridLines="None" Width="100%">

                                        <Columns>
                                            <asp:TemplateField HeaderText="OrderID">
                                                <ItemTemplate>
                                                    <a href='../../Hotel/BookingSummaryHtl.aspx?OrderId=<%#Eval("OrderId")%>' rel="lyteframe"
                                                        rev="width: 830px; height: 400px; overflow:hidden;" target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                        <asp:Label ID="lblBID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="BookingID" HeaderText="BookingID" ReadOnly="true" />
                                            <asp:BoundField DataField="Status" HeaderText="Status" />
                                            <asp:BoundField DataField="AgentID" HeaderText="Agent ID " />
                                            <asp:BoundField DataField="HotelName" HeaderText="Hotel Name" />
                                            <asp:TemplateField HeaderText="Room Name">
                                                <ItemTemplate>
                                                    <div><%#Eval("RoomName")%> </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="StarRating" HeaderText="Star" />
                                            <asp:BoundField DataField="TotalCost" HeaderText="Total Cost" />
                                            <asp:BoundField DataField="NetCost" HeaderText="Cost to Agent" />
                                            <asp:BoundField DataField="BookingDate" HeaderText="Booking Date" />
                                            <asp:BoundField DataField="PgCharges" HeaderText="Convenience Fee" />
                                            <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode" />
                                        </Columns>
                                    </asp:GridView>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var myDate = new Date();
        var currDate = (myDate.getDate()) + '-' + (myDate.getMonth() + 1) + '-' + myDate.getFullYear();
        $("#From").val(currDate); $("#To").val(currDate);
        var UrlBase = '<%=ResolveUrl("~/") %>';
        var CheckindtPickerOptions = { numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "+1y", minDate: "-1y", showOtherMonths: true, selectOtherMonths: false };
        var FromPickerOptions = { numberOfMonths: 1, dateFormat: "dd-mm-yy", maxDate: "0", minDate: "-1y", showOtherMonths: true, selectOtherMonths: false };
        $("#Checkin").datepicker(CheckindtPickerOptions);
        $("#From").datepicker(FromPickerOptions);
        $("#To").datepicker(FromPickerOptions);
    </script>
</asp:Content>




