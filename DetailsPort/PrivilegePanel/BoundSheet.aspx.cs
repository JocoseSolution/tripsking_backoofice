﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Page : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null || string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect("~/Login.aspx");
        }

        //if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        //{
        //}
        //else
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        if (!this.IsPostBack)
        {
            this.BindGridview();
            BindRoleName();
        }
        //<a href=" & ResolveClientUrl("~" & dset.Tables(0).Rows(k)("Page_url")) & "> "
        //((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href='PrivilegePanel/Dashboard.aspx' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Privilege Panel </a><a class='current' href='#'>Bound Sheet Details</a>";

        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom tt' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Privilege Setting </a><a class='current' href='#'>Bound Sheet Details</a>";
    
      
    }


    public void BindRoleName()
    {

        string Is_Parent_page = "Y";
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);

       // SqlCommand cmd = new SqlCommand("SELECT DISTINCT Page_name,Page_id FROM Page_Details Where Is_Parent_Page='Y'");

        SqlCommand cmd = new SqlCommand("BindRoleNameSp_PP");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@IsParentPage", Is_Parent_page);
        cmd.Connection = con;
        con.Open();
        Root_page_Name.DataSource = cmd.ExecuteReader();

        Root_page_Name.DataTextField = "Page_name";
        Root_page_Name.DataValueField = "Page_id";


        Root_page_Name.DataBind();

        con.Close();
        Root_page_Name.Items.Insert(0, new ListItem("Root_page_Name", "0"));
    }


    protected void Submit_Click(object sender, EventArgs e)
    {
        
            Pro bo = new Pro();

            //int result = 0;
            string result = "";

            bo.Page_name = Pagetxt.Text;
            bo.Page_url = Pageurltxt.Text;
            bo.CheckBox = CheckBox1.Checked ? "Y" : "N";
            bo.Page_id =Convert.ToInt32( Root_page_Name.SelectedValue);
            Bal bl = new Bal();

            try
            {
                result = bl.insertpage(bo);
                //if (result == -1)
                if (result == "Insert")
                {
                    BindGridview();
                    Label1.InnerText = "Data submitted successfully";
                    Pagetxt.Text = "";
                    Pageurltxt.Text = "";
                    BindRoleName();
                   
                }
                else
                {


                    Label1.InnerText = "Data Already Exist";

                 Pagetxt.Text = "";
                 Pageurltxt.Text = "";
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                bo = null;
                bl = null;
            }

            Root_page_Name.SelectedIndex = 0;
    }

    //private static DataTable GetData(string query)
    //{

    //    string constr = ConfigurationManager.ConnectionStrings["reg_connection"].ConnectionString;
    //    SqlConnection con = new SqlConnection(constr);
    //    con.Open();

    //    SqlCommand cmd = new SqlCommand();
    //    cmd.CommandText = query;

    //    SqlDataAdapter sda = new SqlDataAdapter();

    //    cmd.Connection = con;
    //    sda.SelectCommand = cmd;
    //    DataTable dt = new DataTable();

    //    sda.Fill(dt);
    //    con.Close();

    //    return dt;
    //}
    protected void BindGridview()
    {
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("GVPageDetails_PP");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter sda = new SqlDataAdapter();

        cmd.Connection = con;
        sda.SelectCommand = cmd;
        DataTable dt = new DataTable();

        sda.Fill(dt);
        con.Close();
        GridView1.DataSource = dt;


       // GridView1.DataSource = GetData("Select Page_id, Page_name, Page_url, Root_page_id,Is_Parent_page from Page_Details");
        GridView1.DataBind();

    }


    //protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    //{

    //}
    //protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    BindGridview();
    //    GridView1.PageIndex = e.NewPageIndex;
    //    GridView1.DataBind();
    //}
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = GridView1.Rows[e.RowIndex];
        int Pageid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);

        string Page_name = (row.FindControl("txtPagename") as TextBox).Text;
        string Page_url = (row.FindControl("txtPageurl") as TextBox).Text;
        string PageOrder = (row.FindControl("txtPageorder") as TextBox).Text;


        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("PageUpdateSp_PP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Pageid", Pageid);
                cmd.Parameters.AddWithValue("@Pagename", Page_name);
                cmd.Parameters.AddWithValue("@Pageurl", Page_url);
                cmd.Parameters.AddWithValue("@Pageorder", PageOrder);
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        GridView1.EditIndex = -1;
        this.BindGridview();
        BindRoleName();
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        this.BindGridview();
        BindRoleName();

    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        this.BindGridview();
        BindRoleName();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int Pageid = Convert.ToInt32(GridView1.DataKeys[e.RowIndex].Values[0]);
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("PageDeleteSp_PP"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Pageid", Pageid);
                cmd.Connection = con;
                con.Open();
                int rows = cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();
        BindRoleName();
    }
}
