﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="NewExecutive.aspx.cs" Inherits="NewExecutive" %>




<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 64 && charCode < 91 || charCode > 96 && charCode < 123 || (charCode == 8 || charCode == 45))) {
                return false;
            }
            status = "";
            return true;
        }

        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
    </script>


    <script src="<%=ResolveUrl("~/Js/jquery-ui-1.8.8.custom.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Js/jquery-1.7.1.min.js") %>" type="text/javascript"></script>
    <script src="validation.js"></script>



    <script type="text/javascript">
        $(document).ready(function () {


            $('#<%=Submit.ClientID%>').click(function (event) {

                var returntypp = true;
                if ($.trim($("#<%=txtemail.ClientID%>").val()) == "") {

                    $("#<%=txtemail.ClientID%>").focus();
                    $('.error').show();
                    returntypp = false;
                }
                else {
                    $('.error').hide();
                }
                if ($.trim($("#<%=txtpassword.ClientID%>").val()) == "") {

                    $("#<%=txtpassword.ClientID%>").focus();
                    $('.error1').show();
                    returntypp = false;
                }
                else {
                    $('.error1').hide();
                }
                if ($.trim($("#<%=txtname.ClientID%>").val()) == "") {

                    $("#<%=txtname.ClientID%>").focus();
                    $('.error2').show();
                    returntypp = false;
                }
                else {
                    $('.error2').hide();
                }

                if ($.trim($("#<%=txtmobile.ClientID%>").val()) == "") {

                    $("#<%=txtmobile.ClientID%>").focus();
                    $('.error3').show();
                    returntypp = false;
                }
                else {
                    $('.error3').hide();
                }

                if ($.trim($("#<%=ddlRole_type.ClientID%>").val()) == "0") {

                    $("#<%=ddlRole_type.ClientID%>").focus();
                    $('.error4').show();
                    returntypp = false;
                }
                else {
                    $('.error4').hide();
                }
                return returntypp;
            });

        });
        //function confirmUpdate(thisObj) {



        //    if ($.trim($("#ctl00_ContentPlaceHolder1_GridView1_ctl02_lbtnEdit").val()) == "") {

        //        $("#ctl00_ContentPlaceHolder1_GridView1_ctl02_lbtnEdit").focus();
        //        return false;
        //    }
        //    else {
        //        alert("hi");
        //        var upd = confirm('Are you sure to update this configuration');
        //        if (upd == true) {
        //            return true;
        //        }
        //        else {
        //            return false;
        //        }
        //    }
        //}
        function confirmDelete() {
            var upd = confirm('Are you sure to delete this configuration');
            if (upd == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>

    <div>
        <div class="container-fluid" style="padding-right: 35px">

            <div>


                <div class="panel panel-primary">




                    <div class="panel-body">

                        <div class="widget-content nopadding">

                            <div id="form-wizard-1" class="step">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="input-group">
                                            <%--  <label  for="exampleInputPassword1" id="lblemail" runat="server">Email:</label>--%>
                                            <asp:TextBox CssClass="form-control input-text full-width" runat="server" ID="txtemail" placeholder="EMAIL:"></asp:TextBox>
                                            <span class=" input-group-addon" style="background-color: #49cced">
                                                <span class="glyphicon glyphicon-envelope"></span>
                                            </span>



                                        </div>
                                    </div>


                                    <div class="col-xs-2">
                                        <div class="input-group">
                                            <%--<label for="exampleInputPassword1" id="lblpassword" runat="server">Password :</label>--%>
                                            <asp:TextBox CssClass="form-control input-text full-width" runat="server" ID="txtpassword" MaxLength="40" TextMode="Password" placeholder="PASSWORD:"></asp:TextBox>
                                            <span class=" input-group-addon" style="background-color: #49cced">
                                                <span class="glyphicon glyphicon-lock"></span>
                                            </span>
                                        </div>
                                    </div>


                                    <div class="col-xs-2">
                                        <div class="input-group">
                                            <%-- <label for="exampleInputPassword1" id="lblname" runat="server">Name :</label>--%>
                                            <asp:TextBox CssClass="form-control input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwxyz');" runat="server" ID="txtname" MaxLength="20" placeholder="NAME"></asp:TextBox>
                                            <span class=" input-group-addon" style="background-color: #49cced">
                                                <span class="glyphicon glyphicon-user"></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="input-group">
                                            <%--<label for="exampleInputPassword1" id="lblmobile" runat="server">Mobile :</label>--%>
                                            <asp:TextBox CssClass="form-control input-text full-width" onkeypress="return checkitt(event)" runat="server" ID="txtmobile" MaxLength="10" placeholder="MOBILE"></asp:TextBox>
                                            <span class=" input-group-addon" style="background-color: #49cced">
                                                <span class="glyphicon glyphicon-phone"></span>
                                            </span>
                                        </div>
                                    </div>


                                    <div class="col-xs-2">
                                        <div class="form-group">
                                            <%--<label for="exampleInputEmail1" id="lblrole_type">Role :</label>--%>
                                            <asp:DropDownList CssClass="input-text full-width" ID="ddlRole_type" runat="server">
                                                <asp:ListItem>ROLE:</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <div class="col-xs-2" style="margin-top:5px">
                                        <div class="form-group">
                                            <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="Submit_Click" ValidationGroup="group1" />



                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="input-group">

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="form-control" runat="server" ControlToValidate="txtemail" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtemail" ValidationGroup="group1" ErrorMessage=" enter valid email "
                                                ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                            </asp:RegularExpressionValidator>


                                        </div>
                                    </div>

                                    <div class="col-xs-2">
                                        <div class="form-group">

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtpassword" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtpassword" ValidationGroup="group1" Display="dynamic" ValidationExpression="^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{5,}$" ErrorMessage="Minimum 5 characters atleast 1 Alphabet, 1 Number and 1 Special Character" ForeColor="Red" />
                                        </div>
                                    </div>


                                    <div class="col-xs-2">
                                        <div class="form-group">

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtname" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtname" ID="RegularExpressionValidator7" ValidationExpression="^[\s\S]{5,30}$" runat="server" ErrorMessage="Minimum 5 and Maximum 20 characters required."></asp:RegularExpressionValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtname" ValidationGroup="group1"
                                                ValidationExpression="[a-zA-Z ]*$" ErrorMessage="*Valid characters: Alphabets and space." />

                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="form-group">

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtmobile" ErrorMessage="*"
                                                Display="dynamic" ValidationGroup="group1"><span style="color:#FF0000">*</span></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ValidationGroup="group1"
                                                ControlToValidate="txtmobile" ErrorMessage="Please Fill valid 10 digit mobile no."
                                                ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <label for="exampleInputPassword1" id="Label1" runat="server" style="color: red;"></label>
                                </div>


                                <%-- <div class="form-group">
                            <div class="col-md-8"></div>
                            <div class="col-md-2">
                                <asp:Button ID="Cancel" runat="server" Text="Cancel" CssClass="button buttonBlue" OnClick="Cancel_Click" />

                            </div>
                        </div>--%>
                                <%--<div class="row">
                               <div class="col-md-8">
                        <div class="form-group">
                            <label for="exampleInputPassword1" id="Label1" runat="server"></label>
                        </div>
                                   </div>
                               </div>--%>
                                <hr />
                                <div>
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" DataKeyNames="user_id"
                                        OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                        OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                     CssClass="table table-striped table-bordered table-hover"   Width="100%" style="text-transform:uppercase;">

                                        <Columns>
                                            <asp:TemplateField HeaderText="Counter" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcounter" runat="server" Text='<%# Eval("counter") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="User_id" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbluserid" runat="server" Text='<%# Eval("user_id") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Password" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpwd" runat="server" Text='<%# Eval("password") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Role_Name" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblroleid" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="ddlRole_Name" runat="server" Width="150px"></asp:DropDownList>
                                                    <asp:Label ID="lblRoleNameHidden" runat="server" Text='<%# Eval("Role") %>' Visible="false"></asp:Label>
                                                </EditItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Role_Type" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblroletype" runat="server" Text='<%# Eval("role_type") %>'></asp:Label>
                                                </ItemTemplate>
                                                <%--<EditItemTemplate>
                                        <asp:DropDownList ID="ddlRole_Type" runat="server" Width="150px"></asp:DropDownList>
                                        <asp:Label ID="lblRoleTypeHidden" runat="server" Text='<%# Eval("role_type") %>' Visible="false"></asp:Label>
                                    </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblemailid" runat="server" Text='<%# Eval("Email_id") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile_no" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblmobileno" runat="server" Text='<%# Eval("mobile_no") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" ItemStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus" runat="server" Text='<%# GetStatusVal( Eval("status")) %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>

                                                    <asp:DropDownList ID="ddlstatus" runat="server" Width="150px" SelectedValue='<%# Eval("status").ToString() %>'>
                                                        <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                                        <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>
                                                    </asp:DropDownList>

                                                </EditItemTemplate>

                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("user_id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



</asp:Content>

