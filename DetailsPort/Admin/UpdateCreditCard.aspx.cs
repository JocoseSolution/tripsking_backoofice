﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_UpdateCreditCard : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx");
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }

            string Id = Request.QueryString["id"];
            HdnId.Value = Id;
            if (!string.IsNullOrEmpty(Convert.ToString(Id)))
            {
                if (!IsPostBack)
                {
                    BindCreditcard(Id);
                                    
                }
            }
            else
            {
                Response.Redirect("CommissionMaster.aspx", false);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetCreditCard(int ID)
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("USP_CREDITCARDDETAILS", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@Counter", ID);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETBYID");
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }
    private void BindCreditcard(string Id)
    {
        DataTable dt = new DataTable();
        try
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Id)))
            {
                dt = GetCreditCard(Convert.ToInt32(Id));
                if (dt.Rows.Count > 0)
                {

                    txtpcc.Text = Convert.ToString(dt.Rows[0]["PCC"]);
                    txtAirline.Text = Convert.ToString(dt.Rows[0]["Airline"]);
                    ddltrip.SelectedValue = Convert.ToString(dt.Rows[0]["Trip"]);
                    ddlCardType.SelectedValue = Convert.ToString(dt.Rows[0]["CardType"]);
                    txtname.Text = Convert.ToString(dt.Rows[0]["CardHolderName"]);
                    txtcardnumber.Text = Convert.ToString(dt.Rows[0]["CardNumber"]);
                    txtcardnumber1.Text = Convert.ToString(dt.Rows[0]["CardNumber"]);
                    txtexpiry.Text = Convert.ToString(dt.Rows[0]["CardExpDate"]);
                    string Status = Convert.ToString(dt.Rows[0]["Isactive"]).ToLower();
                    hidtxtAirline.Value = Convert.ToString(dt.Rows[0]["Airline"]);
                    if (Status == "true")
                    {
                        ddlstatus.SelectedValue = "True";
                    }
                    else if (Status == "false")
                    {
                        ddlstatus.SelectedValue = "False";
                    }
                    // int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);               
                }
                else
                {
                    BtnSubmit.Visible = false;
                }
            }

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    //protected void BtnSubmit_Click(object sender, EventArgs e)
    //{

    //}

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            string Airline="";
            string Pcc = Convert.ToString(txtpcc.Text);
            if(hidtxtAirline.Value.Contains(','))
             Airline = hidtxtAirline.Value.Split(',')[1];
            else
                Airline = hidtxtAirline.Value;
            string Trip = Convert.ToString(ddltrip.SelectedValue);
            string CardType = Convert.ToString(ddlCardType.SelectedValue);
            string HName = Convert.ToString(txtname.Text);
            string CardNumber = Convert.ToString(txtcardnumber.Text);
            string ExpiryDate = Convert.ToString(txtexpiry.Text);
            string Status = Convert.ToString(ddlstatus.SelectedValue);
            int flag = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(HdnId.Value)))
            {
                int Counter = Convert.ToInt32(HdnId.Value);
                //int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType);
                flag = UpdateCreditCard(Counter, Pcc, Airline, Trip, CardType, HName, CardNumber, ExpiryDate, Status);
                if (flag > 0)
                {
                    BindCreditcard(Convert.ToString(HdnId.Value));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');window.location='AddCreditCard.aspx?ID=1';", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Record successfully updated.');window.location='ServiceCredentials.aspx'; ", true);
                }
                else
                {
                    BindCreditcard(Convert.ToString(HdnId.Value));
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record not updated, try again!!');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');", true);
            }


        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');", true);
        }

    }
    private int UpdateCreditCard(int Counter, string Pcc, string Airline, string Trip, string CardType, string HName, string CardNumber, string ExpiryDate, string Status)
    {

        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "update";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_CREDITCARDDETAILS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Counter", Counter);
            cmd.Parameters.AddWithValue("@Pcc", Pcc);
            cmd.Parameters.AddWithValue("@Airline", Airline);
            cmd.Parameters.AddWithValue("@Trip", Trip);
            cmd.Parameters.AddWithValue("@CardType", CardType);
            cmd.Parameters.AddWithValue("@HName", HName);
            cmd.Parameters.AddWithValue("@CardNumber", CardNumber);
            cmd.Parameters.AddWithValue("@ExpiryDate", ExpiryDate);
            cmd.Parameters.AddWithValue("@Status", Status);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            //CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            con.Close();
        }
        return flag;

    }
}