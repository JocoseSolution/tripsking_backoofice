﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="DealTransfer.aspx.cs" Inherits="DetailsPort_Admin_DealTransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
    <script type="text/javascript">
        function Validate() {
            var ddl_ptype = document.getElementById("<%=ddl_ptype.ClientID %>");
            var ddl_ptype_To = document.getElementById("<%=ddl_ptype_To.ClientID %>");

            if (ddl_ptype.value == 0) {
            //If the "Please Select" option is selected display error.
            alert("Please select GroupType From!");
            return false;
            }

            if (ddl_ptype_To.value == 0) {
                //If the "Please Select" option is selected display error.
                alert("Please select GroupType To!");
                return false;
            }
    }
</script>





       <div class="row">
        <div class="col-md-2">
        </div>
        <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    
                    <div class="panel-body">

      <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Deal Transfer From :</label>--%>
                                     <asp:DropDownList ID="ddl_ptype" CssClass="input-text full-width" runat="server" TabIndex="2">
                                         
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Deal Transfer To :</label>--%>
                                    <asp:DropDownList ID="ddl_ptype_To" CssClass="input-text full-width" runat="server" TabIndex="2">
                                        
                                    </asp:DropDownList>
                               
                            <div class="col-md-3" style="margin-left: 340px;margin-top: -41px;">
                                <div class="form-group">
                                    <asp:Button ID="Submit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="Submit_Click" OnClientClick="return Validate()" />
                                </div>
                            </div>
                              
     
                        </div>

                         <%--<div class="clear"></div>
                          <div id="DD" style="color:red">*Note: Your DealTransferFrom as you Select which will be insert into DealTransferTo and Previous Deal of DealTransferTo Will be Deleted</div>
                          <div class="row">--%>
                            <div class="col-md-3">
                                <div class="form-group">
                                   <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
                                </div>
                            </div>


                       
      </div>   
                </div>
            </div>
        </div>
    </div>
           </div>
           </div>
</asp:Content>

