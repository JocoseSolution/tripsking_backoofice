﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="PNRCreationCrd.aspx.cs" Inherits="DetailsPort_Admin_PNRCreationCrd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss
        {
            background-color: #fff;
            margin-left: 35px;
            width: 100%;
        }
    </style>
    <div class="row">
        <div class="container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary" style="background-color:rgba(255, 255, 255, 0.08)">

                    <div class="panel-body" style="margin-right: -163px">
                        <div class="row">
                              <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">User ID:</label>--%>
                                   
                                    <asp:TextBox ID="TxtUserID" placeholder="USER ID" CssClass="form-control input-text full-width" runat="server" MaxLength="50" TabIndex="5" required=""></asp:TextBox>
                                      <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-user"></span>
                               </span>
                                </div>
                            </div>

                               <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Password :</label>--%>
                                    <asp:TextBox ID="TxtPassword" placeholder="PASSWORD" runat="server" CssClass="form-control input-text full-width" MaxLength="50" TabIndex="6" required=""></asp:TextBox>
                                       <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-lock"></span>
                               </span>
                                </div>
                            </div>

                                 <div class="col-md-2" id="divCorporateID">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">CorporateID :</label>--%>
                                    <asp:TextBox ID="TxtCorporateID" placeholder="CORPORATE ID" CssClass="form-control input-text full-width" runat="server" MaxLength="50" TabIndex="4" required=""></asp:TextBox>
                                          <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-briefcase"></span>
                               </span>
                                </div>
                            </div>

                                      <div class="col-md-2" id="divCarrierPCC">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1">Carrier PCC:</label>--%>
                                    <asp:TextBox ID="TxtCarrierAcc" placeholder="CARRIER PCC." CssClass="form-control input-text full-width" runat="server" MaxLength="7" required=""></asp:TextBox>
                                          <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-expeditedssl"></span>
                               </span>
                                </div>
                            </div>
                           
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">Airline :</label>--%>
                                    <input type="text" placeholder="SEARCH BY AIRLINE" class="form-control input-text full-width" name="txtAirline" value="" id="txtAirline" tabindex="3" required=""/>
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-plane"></span>
                               </span>
                                </div>
                            </div>


                               
                          


                        </div>

                        <div class="row">
                     <div class="col-md-2">
                                <div class="form-group" style="margin-top: 14px">
                                   <%-- <label for="exampleInputPassword1">Trip Type</label>--%>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="input-text full-width" TabIndex="1" required="">
                                        <%--<asp:ListItem Value="D" Text="--TRIP TYPE--" Selected="True"></asp:ListItem>--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">TRIP TYPE</asp:ListItem>
                                        <asp:ListItem Value="D" Text="Domestic"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 14px">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Provider</label>--%>
                                    <asp:DropDownList ID="DdlProvider" runat="server" CssClass="input-text full-width" onclick="ShowHide();" required="">
                                   <%-- <asp:DropDownList ID="DdlProvider" runat="server" CssClass="input-text full-width" TabIndex="2">--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT PROVIDER</asp:ListItem>
                                        <%--<asp:ListItem Value="0" Text="--SELECT PROVIDER--" Selected="True"></asp:ListItem>--%>
                                        <asp:ListItem Value="1G" Text="GAL"></asp:ListItem>
                                        <%--<asp:ListItem Value="1GINT" Text="GAL INT"></asp:ListItem>
                                        <asp:ListItem Value="6E" Text="Indigo"></asp:ListItem>
                                        <asp:ListItem Value="SG" Text="Spicejet"></asp:ListItem>                                       
                                        <asp:ListItem Value="G8" Text="GoAir"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>

                         
                            <div class="col-md-2" style="margin-top: 14px">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Fare Type:</label>--%>
                                    <asp:DropDownList ID="DdlCrdType" runat="server" CssClass=" input-text full-width" required="">
                                        <%--<asp:ListItem Value="NRM" Text="--FARE TYPE--" Selected="True"></asp:ListItem>--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">FARE TYPE</asp:ListItem>
                                        <asp:ListItem Value="NRM" Text="Normal Fare"></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
                                        <%--<asp:ListItem Value="CPN" Text="Coupon Fare"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>
                                <div class="col-md-2" style="margin-top: 14px">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Status:</label>--%>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="input-text full-width" required="">
                                        <asp:ListItem  value="" disabled selected style="display: none;">STATUS</asp:ListItem>
                                        <%--<asp:ListItem Value="true" Text="--STATUS--" Selected="True"></asp:ListItem>--%>
                                        <asp:ListItem Value="true" Text="ACTIVE"></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                        </div>

         <div class="col-md-3" style="margin-top: -7px">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"></label>
                                    <br />
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="btn btn-success" />
                                </div>
                            </div>

                            <%-- <div class="col-md-4" style="display:none;" id="DivLoginID">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">LoginID :</label>
                                    <asp:TextBox ID="TxtLoginID" CssClass="input-text full-width" runat="server" MaxLength="300"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4" style="display:none;" id="DivLoginPwd">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Login Password:</label>
                                    <asp:TextBox ID="TxtLoginPwd" CssClass="input-text full-width" runat="server" MaxLength="100"></asp:TextBox>
                                </div>
                            </div> 

                                             

                           <div class="col-md-4" id="divResultFrom" style="display:none;">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Result From:</label>                                   
                                     <asp:DropDownList ID="DdlResultFrom" runat="server" CssClass="input-text full-width">                                                                             
                                        <asp:ListItem Value="AP" Text="API" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="SP" Text="Scraping"></asp:ListItem>                                        
                                    </asp:DropDownList>                                   
                                </div>
                            </div>

                            <div class="col-md-4" id="divFareType:">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Fare Type:</label>                                    
                                     <asp:DropDownList ID="DdlCrdType" runat="server" CssClass="input-text full-width">                                        
                                        <asp:ListItem Value="NRM" Text="Normal Fare" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
                                        <asp:ListItem Value="CPN" Text="Coupon Fare"></asp:ListItem>
                                    </asp:DropDownList>                                  
                                </div>
                            </div>--%>
                        </div>



                        <div class="row">
                        
                            <div class="col-md-2">
                                <div class="form-group">
                                </div>
                            </div>

                          
                        </div>
                
                        <div class="row">
                            <div class="col-md-10 container-fluid" style="overflow: auto;">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px;">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grid1" runat="server" AutoGenerateColumns="false"
                                            CssClass="table" GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="Grid1_RowCancelingEdit"
                                            OnRowEditing="Grid1_RowEditing" OnRowUpdating="Grid1_RowUpdating" OnRowDeleting="OnRowDeleting" OnRowDataBound="OnRowDataBound" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>
                                                <%-- <asp:TemplateField HeaderText="Provider">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                        <a href='UpdateServiceCredentials.aspx?ID=<%#Eval("Counter")%>' rel="lyteframe" rev="width: 900px; height: 400px; overflow:hidden;"
                                                            target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                                            <u>
                                                                <asp:Label ID="lblProvider" runat="server" Text='<%#Eval("Provider") %>'></asp:Label><br />
                                                                Update</u>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Trip">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                        <asp:Label ID="lblTripType" runat="server" Text='<%#Eval("Trip") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Airline">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblResultFrom" runat="server" Text='<%#Eval("VC") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Provider">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProvider" runat="server" Text='<%#Eval("Provider") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <%--  <EditItemTemplate>
                                                        <asp:DropDownList ID="ddlGrdProvider" runat="server" Width="150px" DataValueField='<%#Eval("Provider")%>' SelectedValue='<%#Eval("Provider")%>'>
                                                            <asp:ListItem Value="1G" Text="GAL"></asp:ListItem>
                                                            <asp:ListItem Value="TBO" Text="TBO"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>--%>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Corporate_ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCorporateID" runat="server" Text='<%#Eval("CorporateID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdCorporateId" runat="server" Text='<%#Eval("CorporateID") %>' Width="100px" MaxLength="50" BackColor="#ffff66"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="UserID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUserID" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdUserId" runat="server" Text='<%#Eval("UserID") %>' Width="100px" BackColor="#ffff66" MaxLength="50"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Password">
                                                    <ItemTemplate>

                                                        <%--<asp:Label ID="lblPassword" runat="server" Text='<%#Eval("Password") %>'></asp:Label>--%>
                                                        <asp:Label ID="lblPassword" runat="server" Text='******'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdPWD" runat="server" Text='<%#Eval("Password") %>' Width="100px" BackColor="#ffff66" MaxLength="50"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderText="CarrierAcc">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCarrierAcc" runat="server" Text='<%#Eval("CarrierAcc") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtGrdPCC" runat="server" Text='<%#Eval("CarrierAcc") %>' Width="100px" BackColor="#ffff66" MaxLength="10"></asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="ddlGrdStatus" runat="server" Width="150px" DataValueField='<%#Eval("Status")%>' SelectedValue='<%#Eval("Status")%>'>
                                                            <%-- <asp:ListItem Value="1" Text="ACTIVE"></asp:ListItem>
                                                            <asp:ListItem Value="0" Text="DEACTIVE"></asp:ListItem>--%>
                                                            <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                            <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Fare_Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCrdType" runat="server" Text='<%#Eval("CrdType") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="CreatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>' Style="text-wrap: inherit;"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="EDIT">
                                                    <ItemTemplate>
                                                        <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                        <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                            CssClass="newbutton_2" />
                                                    </EditItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css"/>
    <link rel="stylesheet" href="/resources/demos/style.css"/>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript">
        function ShowHide() {
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlProvider").val();
            if (Provider == "1G" || Provider == "1GINT") {
                $("#divCorporateID").show();
                $("#divCarrierPCC").show();
                $("#divResultFrom").hide();

                $("#DivRow").hide();
                $("#DivLoginID").hide();
                $("#DivLoginPwd").hide();
            }
            else if (Provider == "G8") {
                $("#divCorporateID").show();
                $("#divCarrierPCC").hide();
                $("#divResultFrom").show();

                //$("#DivRow").show();
                $("#DivLoginID").show();
                $("#DivLoginPwd").show();

            }
            else {
                $("#divCorporateID").show();
                $("#divCarrierPCC").hide();
                $("#divResultFrom").show();
                $("#DivRow").hide();
                $("#DivLoginID").hide();
                $("#DivLoginPwd").hide();

            }
        }

        function Check() {
            if ($("#ctl00_ContentPlaceHolder1_DdlProvider").val() == "0") {
                alert("Select provider.");
                $("#ctl00_ContentPlaceHolder1_DdlProvider").focus();
                return false;
            }
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlProvider").val();
            if ($("#ctl00_ContentPlaceHolder1_TxtCorporateID").val() == "") {
                alert("Enter Corporate ID.");
                $("#ctl00_ContentPlaceHolder1_TxtCorporateID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtUserID").val() == "") {
                alert("Enter User ID.");
                $("#ctl00_ContentPlaceHolder1_TxtUserID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtPassword").val() == "") {
                alert("Enter Password.");
                $("#ctl00_ContentPlaceHolder1_TxtPassword").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtCarrierAcc").val() == "") {
                alert("Enter Carrier PCC.");
                $("#ctl00_ContentPlaceHolder1_TxtCarrierAcc").focus();
                return false;
            }
        }

    </script>

</asp:Content>
