﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Web.Services

Partial Class DetailsPort_Admin_RequestedRequest
    Inherits System.Web.UI.Page
    Dim fltds, airds As New DataSet
    Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim AgentID As String = ""
        Dim strType As String = ""
        CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight </a><a class='current' href='#'>Requested Report</a>"
        Try
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            If Not IsPostBack Then
                strType = Request.QueryString("type")
                If strType = "F" Then
                    ddlsertype.Items.FindByText("Flight").Selected = True
                    airid.Visible = True
                    tripid.Visible = True
                ElseIf strType = "H" Then
                    ddlsertype.Items.FindByText("Hotel").Selected = True
                    airid.Visible = False
                    tripid.Visible = False
                ElseIf strType = "B" Then
                    ddlsertype.Items.FindByText("Bus").Selected = True
                    airid.Visible = False
                    tripid.Visible = False
                End If
                If Session("User_Type") = "AGENT" Then
                    AgentID = Session("UID").ToString()
                End If
                If Session("User_Type") = "EXEC" Then
                    tdTripNonExec2.Visible = False
                End If
                'Dim curr_date = Now.Date() & " " & "12:00:00 AM"
                'Dim curr_date1 = Now()             
            End If

            Dim curr_date = ""
            Dim curr_date1 = ""
            Dim trip As String = ""
            'If Session("User_Type") = "EXEC" Then
            '    If [String].IsNullOrEmpty(Session("TripExec")) Then
            '        trip = ""
            '    Else
            '        trip = Session("TripExec").ToString().Trim()
            '    End If
            'Else
            '    trip = If([String].IsNullOrEmpty(ddlTripDomIntl.SelectedItem.Value), "", ddlTripDomIntl.SelectedItem.Value.Trim())
            'End If
            Dim strType1 As String = If([String].IsNullOrEmpty(ddlsertype.SelectedItem.Value), "", ddlsertype.SelectedItem.Value.Trim())
            fltds.Clear()
            fltds = RequestedRequest(Session("User_Type").ToString, Session("UID").ToString, curr_date, curr_date1, "", "", Session("UID").ToString(), trip, "", strType1)
            Session("fltdss") = fltds
            Dim head As String = ""

            If (strType = "F" Or strType1 = "F") Then
                totalcount.Text = Convert.ToString(fltds.Tables(0).Rows.Count) & "(Flight)"
                ' divcount.InnerText = "Total No Of Request" & Convert.ToString(fltds.Tables(0).Rows.Count)
                ticket_grdview.DataSource = fltds
                ticket_grdview.DataBind()
                divtkt.Visible = True
                divbus.Visible = False
                divhtl.Visible = False
            End If

            If (strType = "B" Or strType1 = "B") Then
                totalcount.Text = Convert.ToString(fltds.Tables(0).Rows.Count) & "(Bus)"
                GridBus.DataSource = fltds
                GridBus.DataBind()
                divbus.Visible = True
                divtkt.Visible = False
                divhtl.Visible = False

            End If

            If (strType = "H" Or strType1 = "H") Then
                totalcount.Text = Convert.ToString(fltds.Tables(0).Rows.Count) & "(Hotel)"
                GridViewHotel.DataSource = fltds
                GridViewHotel.DataBind()
                divhtl.Visible = True
                divtkt.Visible = False
                divbus.Visible = False

            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        Try
            CheckEmptyValue()
            txt_OrderId.Text = ""
            txt_PaxName.Text = ""
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Function RequestedRequest(ByVal usertype As String, ByVal LoginID As String, ByVal FormDate As String, ByVal ToDate As String, ByVal OderId As String,
     ByVal PaxName As String, ByVal AgentId As String, ByVal Trip As String, ByVal Airline As String, ByVal type As String) As DataSet
        Dim cmd As New SqlCommand()
        Dim ds As New DataSet()
        Try
            cmd.CommandText = "USP_REQUESTED_REQUEST"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@usertype", SqlDbType.VarChar).Value = usertype
            cmd.Parameters.Add("@LoginID", SqlDbType.VarChar).Value = LoginID
            cmd.Parameters.Add("@FormDate", SqlDbType.VarChar).Value = FormDate
            cmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = ToDate
            cmd.Parameters.Add("@OderId", SqlDbType.VarChar).Value = OderId
            cmd.Parameters.Add("@PaxName", SqlDbType.VarChar).Value = PaxName
            cmd.Parameters.Add("@AgentId", SqlDbType.VarChar).Value = AgentId
            cmd.Parameters.Add("@Trip", SqlDbType.VarChar).Value = Trip
            cmd.Parameters.Add("@Airline", SqlDbType.VarChar).Value = Airline
            cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = type
            cmd.Connection = con1
            con1.Open()
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            cmd.Dispose()
            con1.Close()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
        Return ds
    End Function
    Protected Sub Role_SelectedIndexChanged(sender As Object, e As EventArgs)
        If (ddlsertype.SelectedValue = "B" Or ddlsertype.SelectedValue = "H") Then
            airid.Visible = False
            tripid.Visible = False
        End If
        If (ddlsertype.SelectedValue = "F") Then
            airid.Visible = True
            tripid.Visible = True
        End If
    End Sub
    <WebMethod()> _
    Public Shared Function GetAutoCompleteData(ByVal username As String) As List(Of String)
        Dim result As New List(Of String)()
        Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
        Using cmd As New SqlCommand("select AL_Code,AL_Name FROM AirlineNames  where AL_Name LIKE '%'+@SearchText+'%'", con)
            con.Open()
            cmd.Parameters.AddWithValue("@SearchText", username)
            Dim dr As SqlDataReader = cmd.ExecuteReader()
            While dr.Read()
                result.Add(String.Format("{0}/{1}", dr("AL_Name"), dr("AL_Code")))
            End While
            Return result
        End Using
    End Function
    Public Sub CheckEmptyValue()
        Try
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "/" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If
            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))
            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim Airline As String = If([String].IsNullOrEmpty(Request("aircode")) Or Request("aircode") = "Airlines", "", Request("aircode"))
            Dim strType As String = If([String].IsNullOrEmpty(ddlsertype.SelectedItem.Value), "", ddlsertype.SelectedItem.Value.Trim())
            If Airline = "ALL" Then
                Airline = ""
            End If
            Dim trip As String = ""
            'If Session("User_Type") = "EXEC" Then
            '    If [String].IsNullOrEmpty(Session("TripExec")) Then
            '        trip = ""
            '    Else
            '        trip = Session("TripExec").ToString().Trim()
            '    End If
            'Else
            '    trip = If([String].IsNullOrEmpty(ddlTripDomIntl.SelectedItem.Value), "", ddlTripDomIntl.SelectedItem.Value.Trim())
            'End If

            trip = If([String].IsNullOrEmpty(ddlTripDomIntl.SelectedItem.Value), "", ddlTripDomIntl.SelectedItem.Value.Trim())
            fltds.Clear()
            fltds = RequestedRequest(Session("User_Type").ToString, Session("UID").ToString, FromDate, ToDate, OrderID, PaxName, Session("UID").ToString(), trip, Airline, strType)
            Session("fltdss") = ""
            Session("fltdss") = fltds
            If (strType = "F") Then
                totalcount.Text = Convert.ToString(fltds.Tables(0).Rows.Count) & "(Flight)"
                ticket_grdview.DataSource = fltds
                ticket_grdview.DataBind()
                divtkt.Visible = True
                divbus.Visible = False
                divhtl.Visible = False
            End If
            If (strType = "B") Then
                totalcount.Text = Convert.ToString(fltds.Tables(0).Rows.Count) & "(BUS)"
                GridBus.DataSource = fltds
                GridBus.DataBind()
                divbus.Visible = True
                divtkt.Visible = False
                divhtl.Visible = False
            End If
            If (strType = "H") Then
                totalcount.Text = Convert.ToString(fltds.Tables(0).Rows.Count) & "(Hotel)"
                GridViewHotel.DataSource = fltds
                GridViewHotel.DataBind()
                divhtl.Visible = True
                divtkt.Visible = False
                divbus.Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Protected Sub ticket_grdview_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles ticket_grdview.PageIndexChanging
        Try
            ticket_grdview.PageIndex = e.NewPageIndex
            fltds = Session("fltdss")
            ticket_grdview.DataSource = fltds
            ticket_grdview.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub lnkupdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim orderid As String = CType(sender, LinkButton).CommandArgument.ToString()
        Dim strType As String = If([String].IsNullOrEmpty(ddlsertype.SelectedItem.Value), "", ddlsertype.SelectedItem.Value.Trim())
        Updateas_Hold(orderid, strType, "Hold")
    End Sub


    Protected Sub lnkupdate_Click2(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim orderid As String = CType(sender, LinkButton).CommandArgument.ToString()
        Dim strType As String = If([String].IsNullOrEmpty(ddlsertype.SelectedItem.Value), "", ddlsertype.SelectedItem.Value.Trim())
        Updateas_Hold(orderid, strType, "Reject")
    End Sub

    Public Sub Updateas_Hold(ByVal OrderId As String, ByVal type As String, ByVal status As String)
        Dim cmd As New SqlCommand()
        Dim ds As New DataSet()
        Try
            cmd.CommandText = "USP_Update_StatusHold"
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@OrderId", SqlDbType.VarChar).Value = OrderId
            cmd.Parameters.Add("@type", SqlDbType.VarChar).Value = type
            cmd.Parameters.Add("@status", SqlDbType.VarChar).Value = status
            cmd.Connection = con1
            con1.Open()
            cmd.ExecuteNonQuery()
            con1.Close()
            ScriptManager.RegisterStartupScript(Me.Page, Me.[GetType](), "Alert", "alert('Status has been updated successfully!!');", True)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub


End Class
