<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Update_Agent.aspx.vb" Inherits="Update_Agent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agent Grade</title>

    <link href="<%= ResolveUrl("~/css/style.css")%>" rel="stylesheet" type="text/css" />

    <%--  <link href="CSS/bootstrap.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ResolveUrl("~/CSS/bootstrap.min.css")%>" rel="stylesheet" />
    <script src="<%= ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>" type="text/javascript"></script>

    <meta charset="utf-8" />
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Travelo - Travel, Tour Booking HTML5 Template" />
    <meta name="author" content="SoapTheme" />
    <%--       <script type="text/javascript">
           var UrlBase = '<%=ResolveUrl("~/")%>';
 
    </script>--%>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <%--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />--%>

    <!-- Theme Styles -->
    <link href="<%=ResolveUrl("~/css1/bootstrap.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css1/font-awesome.min.css")%>" rel="stylesheet" />

<%--    <link href="<%=ResolveUrl ("~/css1/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css1/style.css")%>" rel="stylesheet" />--%>

    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,200,300,500' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,500,700' rel='stylesheet' type='text/css' />
    <link href="<%=ResolveUrl("~/css1/animate.min.css")%>" rel="stylesheet" />

   

    <link href="<%=ResolveUrl("~/css1/soap-icon.css" ) %>" rel="stylesheet" />
<%--    <link href="<%=ResolveUrl("~/css1/style-dark-blue.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css1/style-light-blue.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css1/style-light-orange.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css1/style-light-yellow.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css1/style-orange.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css1/style-purple.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css1/style-red.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css1/style-sea-blue.css")%>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css1/style-sky-blue.css")%>" rel="stylesheet" />--%>




    <!-- Main Style -->
    <link id="main-style" href="<%=ResolveUrl("~/css1/style.css") %>" rel="stylesheet" />


    <!-- Updated Styles -->
   
    <link href="<%=ResolveUrl("~/css1/updates.css")%>" rel="stylesheet" />


    <!-- Custom Styles -->

    <link href="<%=ResolveUrl ("~/css1/custom.css")%>" rel="stylesheet" />

    <!-- Responsive Styles -->
 

    <link href="<%=ResolveUrl ("~/css1/responsive.css")%>" rel="stylesheet" />


    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= btn_update.ClientID%>').click(function () {
                <%--var old = $('#<%= txtOldPassword.ClientID%>').val();--%>
                var newPass = $('#<%= txtNewPassword.ClientID%>').val();
                var confirmPass = $('#<%= txtConfirmPassword.ClientID%>').val();

                if (newPass == '') {
                    if (confirm("Are you sure!")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    if (confirmPass == '') {
                        alert("Enter confirm password.");
                        return false;
                    }

                    var regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$@!%&*?])[A-Za-z\d#$@!%&*?]{8,16}$/;
                    if (!regex.test(newPass)) {
                        alert("Password must contain:8-16 characters atleast 1 UpperCase Alphabet, 1 LowerCase Alphabet, 1 Number and 1 Special Character'");
                        document.newPass.focus();
                        return false;
                    }



                    if (confirmPass != newPass) {
                        alert("Password not confirm.");
                        $('#txtConfirmPassword').focus();
                        return false;
                    }
                    if (confirm("Are you sure!")) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            })
        });
    </script>

    <script language="javascript">

     

        function validatetype() {
            if (document.getElementById("ddl_type").value == "--Select Type--") {
                alert('Please Select Agent Type');
                document.getElementById("ddl_type").focus();
                return false;
            }
            //if (confirm("Are You Sure!!"))
            //    return true;
            //return false;
        }
        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

        function MyFunc() {
            alert("Agency record updated successfully.")
            window.open('Agent_Details.aspx', '_parent');
            window.close();
        }


        function showBox() {
            $("#td_displayBox").show();
            $("#showHeadingBox").hide();
        }
        function hideBox() {
            $("#td_displayBox").hide();
            $("#showHeadingBox").show();
        }
    </script>

    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
        </script>
    <style type="text/css">
        .tablesss {
            margin-bottom: 10px;
        }

        .button {
            position: relative;
            display: inline-block;
            padding: 12px 24px;
            margin: .3em 0 1em 0;
            width: 100%;
            vertical-align: middle;
            color: #fff;
            font-size: 16px;
            line-height: 20px;
            border-radius: 2px;
            -webkit-font-smoothing: antialiased;
            text-align: center;
            letter-spacing: 1px;
            background: transparent;
            border: 0;
            border-bottom: 2px solid #af3e3a;
            cursor: pointer;
            -webkit-transition: all 0.15s ease;
            transition: all 0.15s ease;
        }

        .buttonBlue {
            background: #d9534f;
        }

            .buttonBlue:hover {
                background: #af3e3a;
            }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class=" col-lg-12 col-sm-12 col-xs-12">
            <div class="panel panel-primary">

                <div class="panel-body">
                   
                    <div class="row col-md-12" style="background-color:aliceblue;">
                             <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1"> User ID:</label>
                                        <asp:label ID="td_AgentID" CssClass="input-text full-width" runat="server"></asp:label>
                                    </div>
                                </div>
             
                    <div class="col-md-2">
                        <div  class="form-group">
                             <label for="exampleInputPassword1">Avilable Balance</label>
                      <asp:label ID="td_CrLimit" CssClass="input-text full-width" runat="server"></asp:label>
                        </div>
                    </div>
              
                    <div class="col-md-2">
                        <div class="form-group">
                        
                       <label for="exampleInputPassword1">Credit Limit</label>
                            <asp:label ID="lblAgentLimit" CssClass="input-text full-width" runat="server"></asp:label>
                     
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div  class="form-group">
                             <label for="exampleInputPassword1">Due Amount :</label>
                           <asp:label ID="lblDueAmount" CssClass="input-text full-width" runat="server"></asp:label>
                        </div>
                    </div>
                  

                    <div class="col-md-4" style="display:none;"><!--Hiddenfield-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputPassword1"> Tds:</label>
                            <asp:label ID="td_tds" CssClass="input-text full-width" runat="server"></asp:label>
                    </div>
                      
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Last Transaction Date</label>
                        <asp:label ID="td_LTDate" CssClass="input-text full-width" runat="server"></asp:label>
                        </div>
                   
                    </div>
                </div>
                          </div>
                    
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>Brand Details</h3>
                    </div>
               
                <div class="col-md-2">
                    <div class="form-group">
                         <label for="exampleInputPassword1">  Address</label>
                                        <asp:Textbox ID="txt_Address" CssClass="input-text full-width" runat="server"></asp:Textbox>
                    
                    
                    </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                                   <label for="exampleInputPassword1">Agency Name :</label>
                            <asp:TextBox ID="txt_AgencyName" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz0123456789');" MaxLength="50"></asp:TextBox>
                        </div>
                       
                    </div>
                

            
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>City</label>
                            <asp:TextBox ID="txt_City" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label> State</label>
                            <asp:TextBox ID="txt_State" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>

                    </div>
            


            
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Zip Code</label>
                            <asp:TextBox ID="txt_zip" runat="server" Enabled="False" MaxLength="8" CssClass="input-text full-width" onkeypress="return checkitt(event)"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="regexpcontactZipCode" runat="server" ControlToValidate="txt_zip"
                                ValidationGroup="contactValidation" ForeColor="Red" ErrorMessage="Enter Only Digit"
                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Country</label>
                            <asp:TextBox ID="txt_Country" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="row">
                 <%--   <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>Brand Details</h3>
                    </div>--%>
               

   
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Title</label>
                            <asp:TextBox ID="txt_title" runat="server" Enabled="False" Width="100px" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                           <div class="form-group">
                         <label>First Name</label>
                        <asp:TextBox ID="txt_Fname" runat="server" Enabled="False" Width="90%" CssClass="input-text full-width"></asp:TextBox>
                               </div>

                    </div>
                    <div class="col-md-2">
                           <div class="form-group">
                               <label>Last Name</label>
                        <asp:TextBox ID="txt_Lname" runat="server" Enabled="False" Width="90%" CssClass="input-text full-width"></asp:TextBox>
                    </div>
                </div>

              
                    <div class="col-md-2">
                        <div class="form-group">
                         <label> Mobile</label>
                            <asp:TextBox ID="txt_Mobile" runat="server" MaxLength="10" Enabled="False" onkeypress="return checkitt(event)" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>  Email</label>

                            <asp:TextBox ID="txt_Email" runat="server" Enabled="False" CssClass="input-text full-width" MaxLength="50"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txt_Email" ValidationGroup="group1" ErrorMessage=" enter valid email " ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                            </asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>PAN Name</label>
                            <asp:TextBox ID="TxtNameOnPancard" runat="server" Enabled="False" CssClass="input-text full-width" MaxLength="50" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz');"></asp:TextBox>
                        </div>
                    </div>
                </div>



                <div class="row">

                    
                    <div class="col-md-2">
                        <div class="form-group">
                        <label>Pan No</label>
                            <asp:TextBox ID="txt_Pan" runat="server" Enabled="False" MaxLength ="15"  CssClass="input-text full-width" onKeyPress="return keyRestrict(event,' abcdefghijklmnopqrstuvwsyz1234567890');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                           <label>Fax No</label>
                            <asp:TextBox ID="txt_Fax" runat="server" Enabled="False" CssClass="input-text full-width" onKeyPress="return keyRestrict(event,'1234567890');"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                          <label>OTP Login</label>
                            <asp:CheckBox ID="OPTLOGIN" runat="server" />
                        </div>
                    </div>

                          <div class="col-md-2">
                        <div class="from-group">
                            <label>Exp Password Login</label> 
                            <asp:CheckBox ID="EXPPASS" runat="server" />
                        </div>
                    </div>

                    <div class="col-md-2" style="display:none;" >
                           <div class="col-md-12" id="td_pwd" runat="server">
                        <div class="form-group">
                          <label>  Password</label>
               
                            <asp:TextBox ID="txt_pwd" runat="server" Enabled="False" CssClass="input-text full-width"></asp:TextBox>
                        </div>
                    </div>
                        </div>
                </div>


          
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12">
                        <h3>Update Details</h3>
                    </div>
           
             
                    <div class="col-md-2">
                        <div class="form-group">
                           <label>Type</label>
                            <asp:DropDownList ID="ddl_type" runat="server" CssClass="input-text full-width">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                           <label> Sales Ref</label>
                            <%--<asp:TextBox ID="txt_saleref" runat="server" CssClass="form-control"></asp:TextBox>--%>
                             <asp:DropDownList ID="Sales_DDL" runat="server"  CssClass="input-text full-width">
                                    </asp:DropDownList>
                        </div>
                    </div>
          
                    <div class="col-md-2">
                        <div class="form-group">
                           <label> Activation</label>
                            <asp:DropDownList ID="ddl_activation" runat="server" CssClass="input-text full-width">
                                <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                <asp:ListItem Value="NOT ACTIVE">NOT ACTIVE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                           <label>Ticketing Activation</label>
                            <asp:DropDownList ID="ddl_TicketingActiv" runat="server" CssClass="input-text full-width">
                                <asp:ListItem Value="ACTIVE">ACTIVE</asp:ListItem>
                                <asp:ListItem Value="NOT ACTIVE">NOT ACTIVE</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-2" id="Div1" runat="server">
                        <div class="form-group">
                         <label>Agent Credit</label>
                           <asp:TextBox ID="TxtAgentCredit" runat="server" CssClass="input-text full-width" ReadOnly="true"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                              <asp:Button ID="btn_update" runat="server" ValidationGroup="group1" Text="Update" CssClass="btn btn-lg btn-primary" OnClientClick="return validatetype()" />
                        </div>
                    </div>
                </div>
                </div>

                 <%--Update Password box--%>
                <div id="showHeadingBox">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12 col-xs-12">
                            <asp:HyperLink ID="btnHyper" Text="Click here to update password" runat="server" onclick="showBox();"></asp:HyperLink>
                        </div>
                    </div>
                </div>
                <div id="td_displayBox" style="display:none;width:95%;margin:auto;padding-bottom :10px;border:1px solid #94aee5;border-radius:5px;">
                    <br />
                    <h4 style="font-size:16px;font-weight:bold;">Update Agency Password</h4>
                    <br />
                    
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                              <asp:Label ID="lbl_currentPassMsg" runat="server" Text="Current Password:" Visible="false" Font-Size="12px"></asp:Label>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                               <asp:Label ID="lbl_currentPassword" runat ="server" Text="" Visible="false" Font-Size="12px" Font-Bold ="true"></asp:Label>
                            </div>
                        </div>
                    </div>
             
                    <div class="row tablesss">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                New Password
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txtNewPassword" runat="server" TextMode="Password" MaxLength="16" CssClass="input-text full-width"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-xs-5">
                                Confirm Password
                            </div>
                            <div class="col-lg-8 col-sm-8 col-xs-7">
                                <asp:TextBox ID="txtConfirmPassword" runat="server" TextMode="Password" MaxLength="16" CssClass="input-text full-width"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="large-2 medium-2 small-12">
                        <a onclick="hideBox();" href="#">
                            <img src="<%= ResolveUrl("~/Images/close.png")%>" align="right" alt="Close" title="Close this box" /></a>
                    </div>
                    <br />
                </div>
               
               
                <%--End password updation--%>

           
                   </div>

                    </div>
               
           
            </form>
</body>
</html>
