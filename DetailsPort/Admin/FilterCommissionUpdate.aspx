﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FilterCommissionUpdate.aspx.cs" Inherits="DetailsPort_Admin_FilterCommissionUpdate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/bootstrap.min.css" rel="stylesheet" />
    <style type="text/css">
        .tablesss {
            margin-bottom: 10px;
        }

        .button {
            position: relative;
            display: inline-block;
            padding: 12px 24px;
            margin: .3em 0 1em 0;
            width: 100%;
            vertical-align: middle;
            color: #fff;
            font-size: 16px;
            line-height: 20px;
            border-radius: 2px;
            -webkit-font-smoothing: antialiased;
            text-align: center;
            letter-spacing: 1px;
            background: transparent;
            border: 0;
            border-bottom: 2px solid #af3e3a;
            cursor: pointer;
            -webkit-transition: all 0.15s ease;
            transition: all 0.15s ease;
        }

        .buttonBlue {
            background: #d9534f;
        }

            .buttonBlue:hover {
                background: #af3e3a;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-10">
                    <div class="page-wrapperss">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Flight Setting > Commission Master Filter</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-3">Include</div>
                                    <div class="col-sm-3">Exclude</div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Booking Class:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtBookingClassIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => A,B,C etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="TxtBookingClassEx" runat="server" CssClass="form-control"></asp:TextBox>
                                        <span style="color: #38b4ee;font-size: 10px;">(Ex => A,B,C etc)</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Fare Basis:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtFareBasisIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => AAA,BBB etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtFareBasisEx" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => AAA,BBB etc)</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Orgin Airport:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtOrginAirportIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => DEL,BOM etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtOrginAirportEx" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => BLR,PAT etc)</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Destination Airport:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtDestAirportIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => DEL,BOM etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtDestAirportEx" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => DEL,BOM etc)</span> 
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Origin Country:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtOriginCountryIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => IN,US etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtOriginCountryEx" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => IN,US etc)</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Destination Country:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtDestCountryIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => IN,US etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtDestCountryEx" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex => IN,US etc)</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Flight No:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtFlightNoIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex =>  304,384 etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtFlightNoEx" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex =>  304,384 etc)</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Operating Carrier:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtOperatingCarrierIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex =>  UK,9W etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtOperatingCarrierEx" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex =>  6E,9W etc)</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Marketing Carrier:</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtMarketingCarrierIn" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex =>  UK,9W etc)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:TextBox ID="TxtMarketingCarrierEx" runat="server" CssClass="form-control"></asp:TextBox>
                                            <span style="color: #38b4ee;font-size: 10px;">(Ex =>  AI,9W etc)</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="exampleInputPassword1">Cabin Class</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <asp:ListBox ID="ListCabinClassIn" runat="server" SelectionMode="Multiple" CssClass="form-control">
                                                <asp:ListItem Value="All">--All--</asp:ListItem>
                                                <asp:ListItem Value="C">Business</asp:ListItem>
                                                <asp:ListItem Value="Y">Economy</asp:ListItem>
                                                <asp:ListItem Value="F">First</asp:ListItem>
                                                <asp:ListItem Value="W">Premium Economy</asp:ListItem>
                                                <asp:ListItem Value="P">Premium First</asp:ListItem>
                                            </asp:ListBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group hide">

                                            <asp:ListBox ID="ListCabinClassEx" runat="server" SelectionMode="Multiple">
                                                <asp:ListItem Value="All">--All--</asp:ListItem>
                                                <asp:ListItem Value="C">Business</asp:ListItem>
                                                <asp:ListItem Value="Y">Economy</asp:ListItem>
                                                <asp:ListItem Value="F">First</asp:ListItem>
                                                <asp:ListItem Value="W">Premium Economy</asp:ListItem>
                                                <asp:ListItem Value="P">Premium First</asp:ListItem>
                                            </asp:ListBox>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row" align="center">
                                <asp:Button ID="BtnSubmit" runat="server" Text="Update" OnClick="BtnSubmit_Click" style="width: 200px;" CssClass="button buttonBlue" />
                               <%-- <div class="col-md-3">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <asp:Button ID="BtnSubmit" runat="server" Text="Update" OnClick="BtnSubmit_Click" style="width: 200px;" CssClass="button buttonBlue" />
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="HdnId" runat="server" />

    </form>
</body>
</html>
