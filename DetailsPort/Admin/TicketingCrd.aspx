﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="TicketingCrd.aspx.cs" Inherits="DetailsPort_Admin_TicketingCrd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 35px;
            width:100%;
        }
    </style>
    <div class="row">
        <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                   
                    <div class="panel-body" style="margin-right: -370px">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1"></label>--%>
                                    <asp:DropDownList ID="DdlTripType" runat="server" CssClass="input-text full-width" TabIndex="1" required="">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT TRIP TYPE</asp:ListItem>
                                        
                                        <asp:ListItem Value="D" Text="Domestic" ></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1"></label>--%>
                                    <asp:DropDownList ID="DdlProvider" runat="server" CssClass="input-text full-width" TabIndex="2" required="">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT PROVIDER</asp:ListItem>
                                        
                                        <asp:ListItem Value="GAL" Text="GAL"></asp:ListItem>
                                        <%--<asp:ListItem Value="TBO" Text="TBO"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                   <%-- <label for="exampleInputPassword1"></label>--%>
                                    <input type="text" placeholder="SEARCH BY AIRLINES" class="form-control input-text full-width" name="txtAirline" value="" id="txtAirline" tabindex="3" required="" />
                                    <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-plane"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <label for="exampleInputPassword1"></label>
                                    <asp:TextBox ID="TxtCorporateID" placeholder="CORPORATE ID" CssClass="form-control input-text full-width" runat="server" MaxLength="50" TabIndex="4" required=""></asp:TextBox>
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-user"></span>
                                        </span>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                  <%--  <label for="exampleInputPassword1"></label>--%>
                                    <asp:TextBox ID="TxtUserID" placeholder="USER ID" CssClass="form-control input-text full-width" runat="server" MaxLength="50" TabIndex="5" required=""></asp:TextBox>
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-user"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                 <%--   <label for="exampleInputPassword1"></label>--%>
                                   <asp:TextBox ID="TxtPassword" placeholder="PASSWORD" runat="server" CssClass="form-control input-text full-width" MaxLength="50" TabIndex="6" required=""></asp:TextBox>
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-lock"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1"></label>--%>
                                    <asp:TextBox ID="TxtProviderPcc" placeholder="PROVIDER PCC" CssClass="form-control input-text full-width" runat="server" MaxLength="10" TabIndex="7" required=""></asp:TextBox>
                                      <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-product-hunt"></span>
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                  <%--  <label for="exampleInputPassword1"></label>--%>
                                    <asp:TextBox ID="TxtPnrQNo" placeholder="PNR QUEUE NO." CssClass="form-control input-text full-width" runat="server" MaxLength="10" TabIndex="8" required=""></asp:TextBox>
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-plane"></span>
                                        </span>
                                </div>
                            </div>
                        </div>
                        
                       
                            <div class="col-md-2" id="DivApi" style="display: none;">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">Queue PCC :</label>--%>
                                    <asp:TextBox ID="TxtQueuePCC" runat="server" CssClass="input-text full-width" MaxLength="50" TabIndex="10"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2" id="DivApi1" style="display: none;">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">QNO For API:</label>--%>
                                    <asp:TextBox ID="TxtQNOForAPI" CssClass="input-text full-width" runat="server" MaxLength="10" TabIndex="11" ></asp:TextBox>
                                </div>
                            </div>

                            <br />


                         <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"></label>
                                    <%--<asp:TextBox ID="TxtTicketThrough" CssClass="input-text full-width"" runat="server" MaxLength="50" TabIndex="9"></asp:TextBox>--%>
                                    <asp:DropDownList ID="DdlTicketThrough" runat="server" CssClass="input-text full-width" onclick="ShowHide();" TabIndex="9" required="">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT TICKETING API</asp:ListItem>
                                        
                                        <asp:ListItem Value="1G" Text="GAL"></asp:ListItem>
                                        <%--<asp:ListItem Value="TBO" Text="TBO"></asp:ListItem>--%>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-2" id="divFareType:">                           
                                <div class="form-group">
                                    <label for="exampleInputPassword1"></label>                                    
                                     <asp:DropDownList ID="DdlCrdType" runat="server" CssClass="input-text full-width" required=""> 
                                         <asp:ListItem  value="" disabled selected style="display: none;">SELECT FARE TYPE</asp:ListItem>
                                                                              
                                        <asp:ListItem Value="NRM" Text="Normal Fare" ></asp:ListItem>
                                        <asp:ListItem Value="CRP" Text="Corporate Fare"></asp:ListItem>
                                        <%--<asp:ListItem Value="CPN" Text="Coupon Fare"></asp:ListItem>--%>
                                    </asp:DropDownList>                                  
                                </div>
                            </div>
                                      <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"></label>
                                    <asp:DropDownList ID="DdlStatus" runat="server" CssClass="input-text full-width" TabIndex="12" required="">
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT STATUS</asp:ListItem>
                                       
                                        <asp:ListItem Value="true" Text="ACTIVE" ></asp:ListItem>
                                        <asp:ListItem Value="false" Text="DEACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"></label>
                                    <asp:DropDownList ID="DrpForceToHold" runat="server" CssClass="input-text full-width" TabIndex="13" required="">
                                        <asp:ListItem  value="" disabled selected style="display: none;">FORCE TO HOLD</asp:ListItem>
                                        
                                        <asp:ListItem Value="false" Text="DEACTIVE" ></asp:ListItem>
                                        <asp:ListItem Value="true" Text="ACTIVE"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                  

                        </div>
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="btn btn-success" TabIndex="14"/>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-8" style="overflow:auto;">
                                <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;">
                                    <ContentTemplate>--%>
                                <asp:GridView ID="Grid1" runat="server" AutoGenerateColumns="false"
                                    CssClass="table" GridLines="None" Width="100%" PageSize="30" OnRowCancelingEdit="Grid1_RowCancelingEdit"
                                    OnRowEditing="Grid1_RowEditing" OnRowUpdating="Grid1_RowUpdating" OnRowDeleting="OnRowDeleting" AllowPaging="true"
                                    OnPageIndexChanging="OnPageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Trip_Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                                <asp:Label ID="lblTripTypeName" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Provider">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProvider" runat="server" Text='<%#Eval("Provider") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlGrdProvider" Enabled="False"  runat="server" Width="150px" DataValueField='<%#Eval("Provider")%>' SelectedValue='<%#Eval("Provider")%>'>
                                                    <asp:ListItem Value="GAL" Text="GAL"></asp:ListItem>
                                                    
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Airline">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAirline" runat="server" Text='<%#Eval("AIRLINE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                                                             
                                        <asp:TemplateField HeaderText="CorporateId">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCorporateId" runat="server" Text='<%#Eval("HAP") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGrdCorporateId" runat="server" Text='<%#Eval("HAP") %>' Width="100px" MaxLength="50" BackColor="#ffff66"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="User_Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUSERID" runat="server" Text='<%#Eval("USERID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGrdUserId" runat="server" Text='<%#Eval("USERID") %>' Width="100px" BackColor="#ffff66" MaxLength="50"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Password">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblCodeType" runat="server" Text='<%#Eval("PWD") %>'></asp:Label>--%>
						<asp:Label ID="lblCodeType" runat="server" Text='*****'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGrdPWD" runat="server" Text='<%#Eval("PWD") %>' Width="100px" BackColor="#ffff66" MaxLength="50"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Provider_PCC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPCC" runat="server" Text='<%#Eval("PCC") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGrdPCC" runat="server" Text='<%#Eval("PCC") %>' Width="100px" BackColor="#ffff66" MaxLength="10"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PNR_Queue_No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQNO" runat="server" Text='<%#Eval("QNO") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGrdQNO" runat="server" Text='<%#Eval("QNO") %>' Width="100px" BackColor="#ffff66" MaxLength="10"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticketing_API">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketThrough" runat="server" Text='<%#Eval("TicketThrough") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlGrdTicketThrough" runat="server" Width="150px" DataValueField='<%#Eval("TicketThrough")%>' SelectedValue='<%#Eval("TicketThrough")%>'>
                                                    <%--<asp:DropDownList ID="ddlGrdTicketThrough" runat="server" Width="150px" DataValueField='<%#Eval("TicketThrough")%>'>--%>
                                                    <%--<asp:DropDownList ID="ddlGrdTicketThrough" runat="server" Width="150px">--%>
                                                    <asp:ListItem Value="1G" Text="GAL"></asp:ListItem>
                                                    
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Queue_PCC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQueuePCC" runat="server" Text='<%#Eval("QueuePCC") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGrdQueuePCC" runat="server" Text='<%#Eval("QueuePCC") %>' Width="100px" BackColor="#ffff66" MaxLength="10"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="QNO_For_API">
                                            <ItemTemplate>
                                                <asp:Label ID="lblQNOForAPI" runat="server" Text='<%#Eval("QNOForAPI") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtGrdQNOForAPI" runat="server" Text='<%#Eval("QNOForAPI") %>' Width="100px" BackColor="#ffff66" MaxLength="10"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%--Counter, HAP, USERID, PWD, PCC, URL, QNO, AIRLINE, OnlineTkt, Trip, Provider, TicketThrough, QueuePCC, QNOForAPI,ForceToHold--%>

                                        <asp:TemplateField HeaderText="Online_Tkt">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOnlineTkt" runat="server" Text='<%#Eval("OnlineTkt") %>' Style="text-wrap: inherit;"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlGrdOnlineTkt" runat="server" Width="150px" DataValueField='<%#Eval("OnlineTkt")%>' SelectedValue='<%#Eval("OnlineTkt")%>'>
                                                    <%--<asp:DropDownList ID="ddlGrdOnlineTkt" runat="server" Width="150px">--%>
                                                    <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                    <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ForceToHold">
                                            <ItemTemplate>
                                                <asp:Label ID="lblForceToHold" runat="server" Text='<%#Eval("ForceToHold") %>' Style="text-wrap: inherit;"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlGrdForceToHold" runat="server" Width="150px" DataValueField='<%#Eval("ForceToHold")%>' SelectedValue='<%#Eval("ForceToHold")%>'>
                                                    <%--<asp:DropDownList ID="ddlGrdForceToHold" runat="server" Width="150px">--%>
                                                    <%-- <asp:ListItem Value="0" Text="DEACTIVE"></asp:ListItem>
                                                            <asp:ListItem Value="1" Text="ACTIVE"></asp:ListItem> --%>
                                                    <asp:ListItem Value="True" Text="ACTIVE"></asp:ListItem>
                                                    <asp:ListItem Value="False" Text="DEACTIVE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fare Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCrdType" runat="server" Text='<%#Eval("CrdType") %>'></asp:Label>
                                                    </ItemTemplate>                                                  
                                                </asp:TemplateField>  
                                        <asp:TemplateField HeaderText="EDIT">
                                            <ItemTemplate>
                                                <asp:Button ID="lnledit" runat="server" Text="Edit" CommandName="Edit" Font-Bold="true"
                                                    CssClass="newbutton_2" />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Button ID="lnlupdate" runat="server" Text="Update" CommandName="Update" Font-Bold="true" CssClass="newbutton_2" />
                                                <asp:Button ID="lnlcancel" runat="server" Text="Cancel" CommandName="Cancel" Font-Bold="true"
                                                    CssClass="newbutton_2" />
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:Button ID="btn_delete" CssClass="newbutton_2" runat="server" Text="Delete" CommandName="Delete" OnClientClick="if(!confirm('Do you want to delete?')){ return false; };" Font-Bold="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                    <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                </asp:GridView>
                                <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>


                                <%-- <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript">


        function ShowHide() {
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlTicketThrough").val();
            if (Provider == "1G" || Provider == "0") {
                $("#DivApi").hide();
                $("#DivApi1").hide();
                // $("#ctl00_ContentPlaceHolder1_TxtTicketThrough").val("");
                $("#ctl00_ContentPlaceHolder1_TxtQueuePCC").val("");
                $("#ctl00_ContentPlaceHolder1_TxtQNOForAPI").val("");
            }
            else {
                $("#DivApi").show();
                $("#DivApi1").show();
            }
        }

        function Check() {
            var Provider = $("#ctl00_ContentPlaceHolder1_DdlTicketThrough").val();
            if ($("#ctl00_ContentPlaceHolder1_DdlProvider").val() == "0") {
                alert("Select provider.");
                $("#ctl00_ContentPlaceHolder1_DdlProvider").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_TxtCorporateID").val() == "") {
                alert("Enter Corporate Id .");
                $("#ctl00_ContentPlaceHolder1_TxtCorporateID").focus();
                return false;
            }

            if ($("#ctl00_ContentPlaceHolder1_TxtUserID").val() == "") {
                alert("Enter User Id .");
                $("#ctl00_ContentPlaceHolder1_TxtUserID").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_TxtPassword").val() == "") {
                alert("Enter Password .");
                $("#ctl00_ContentPlaceHolder1_TxtPassword").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_TxtProviderPcc").val() == "") {
                alert("Enter Provider PCC .");
                $("#ctl00_ContentPlaceHolder1_TxtProviderPcc").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_TxtPnrQNo").val() == "") {
                alert("Enter Pnr Queue No.");
                $("#ctl00_ContentPlaceHolder1_TxtPnrQNo").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_DdlTicketThrough").val() == "0") {
                alert("Select Ticketing API .");
                $("#ctl00_ContentPlaceHolder1_DdlTicketThrough").focus();
                return false;
            }

            if (Provider != "1G") {
                if ($("#ctl00_ContentPlaceHolder1_TxtQueuePCC").val() == "") {
                    alert("Enter Queue PCC.");
                    $("#ctl00_ContentPlaceHolder1_TxtQueuePCC").focus();
                    return false;
                }
                if ($("#ctl00_ContentPlaceHolder1_TxtQNOForAPI").val() == "") {
                    alert("Enter Queue NO For API .");
                    $("#ctl00_ContentPlaceHolder1_TxtQNOForAPI").focus();
                    return false;
                }
            }

        }

    </script>

</asp:Content>


