﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeFile="UpdateCreditCard.aspx.cs" Inherits="DetailsPort_Admin_UpdateCreditCard" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
   
    <%-- <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>--%>
    <script src="../../Scripts/AgencySearch.js"></script>
    <style type="text/css">
        .tablesss {
            margin-bottom: 10px;
        }

        .button {
            position: relative;
            display: inline-block;
            padding: 12px 32px;
            margin: .3em 0 1em 0;
            width: 100%;
            vertical-align: middle;
            color: #fff;
            font-size: 16px;
            line-height: 20px;
            border-radius: 2px;
            -webkit-font-smoothing: antialiased;
            text-align: center;
            letter-spacing: 1px;
            background: transparent;
            border: 0;
            border-bottom: 2px solid #af3e3a;
            cursor: pointer;
            -webkit-transition: all 0.15s ease;
            transition: all 0.15s ease;
        }

        .buttonBlue {
            background: #d9534f;
        }

            .buttonBlue:hover {
                background: #af3e3a;
            }
    </style>     

    
<%--<style>
    .overlay {
        position: fixed;
        top: 136px;
        left: 398px;
        right: 0;
        bottom: 0;
        width: auto;
        height: 72%;
        background: #fff;
        opacity: 1.5;
        z-index: 1;
        display: inline-block;
    }

    #backgroundPopup {
        z-index: 3;
        position: fixed;
        display: none;
        height: 100%;
        width: 100%;
        background: #0000009e;
        top: 0px;
        left: 0px;
    }
</style>  --%> 
    <script src="../../JS/JScript.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        myfunction = function () {
            debugger;
            $('#slidediv').toggle('slide', { direction: 'right' }, 0);
            $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);


        }
        $("#Div_close").click(function () {
            $('#slidediv').toggle('slide', { direction: 'left' }, 0);
            $("#backgroundPopup").fadeOut("normal");
            popupStatus = 0;  // and set value to 0
        });


</script>
 
</head>
<body>
    <form id="form1" runat="server">
        <div>
            
    <div  id="slidediv"  class="model overlay" style="z-index: 5;">
        
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8" style="margin-top:102px;margin-left:198px">
                    <div class="page-wrapperss">

                        <div class="panel panel-primary">
                            <div style="margin-top:-18px">
                                <h3 style="background-color:aliceblue;text-align:center;color: #004b91;">Update Credit Card</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PCC 
                                    <asp:TextBox ID="txtpcc" CssClass="form-control" runat="server" MaxLength="20" onkeypress="return isCharNumberKey(event)"></asp:TextBox>
                               </label> </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">AIRLINE</label>
                                    <asp:TextBox ID="txtAirline" name="txtAirline" CssClass="form-control" runat="server" MaxLength="150"></asp:TextBox>
                                    <%--<input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />--%>
                                    <asp:HiddenField ID="hidtxtAirline" runat="server"/>
                                </div>
                            </div>
                                    
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">TRIP</label>
                                    <asp:DropDownList ID="ddltrip" runat="server" CssClass="form-control">
                                         <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="D" Text="Domestic"></asp:ListItem>
                                        <asp:ListItem Value="I" Text="International"></asp:ListItem>                                    
                                    </asp:DropDownList>
                                </div>
                            </div>
                                    
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">CARD TYPE</label>
                                   <asp:DropDownList ID="ddlCardType" runat="server" CssClass="form-control">
                                         <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="AX" Text="Amex"></asp:ListItem>
                                        <asp:ListItem Value="VI" Text="Visa"></asp:ListItem>
					                    <asp:ListItem Value="CA" Text="Master"></asp:ListItem>
                                        <asp:ListItem Value="DC" Text="Diners Club"></asp:ListItem>                                 
                                    </asp:DropDownList>
                                </div>
                            </div> 
                       
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">HOLDER NAME</label>
                                    <asp:TextBox ID="txtname" CssClass="form-control" runat="server" MaxLength="150"></asp:TextBox>
                                </div>
                            </div>
                                   </div>
                                 <div class="row">     
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">CARD NAME</label>
                                    <asp:TextBox ID="txtcardnumber" CssClass="form-control" runat="server" MaxLength="16"></asp:TextBox>
                                </div>
                            </div>
                                   
                           
                         
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1"> NAME</label>
                                    <asp:TextBox ID="txtcardnumber1" CssClass="form-control" runat="server" MaxLength="16"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">EXPIRY DATE</label>
                                    <asp:TextBox ID="txtexpiry" CssClass="form-control" runat="server" MaxLength="16" placeholder="date formate like mm/yyyy"></asp:TextBox>
                                </div>
                            </div>  
                        
                        
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">STATUS</label>
                                   <asp:DropDownList ID="ddlstatus" runat="server" CssClass="form-control">
                                         <asp:ListItem Value="0" Text="--Select--" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="True" Text="Active"></asp:ListItem>
                                        <asp:ListItem Value="False" Text="Inactive"></asp:ListItem>                                    
                                    </asp:DropDownList>
                                </div></div>                             <div style="margin-top:24px">
                                <asp:Button ID="BtnSubmit" runat="server" Text="Update" OnClick="BtnSubmit_Click" Style="width: 74px;" CssClass="btn btn-success" OnClientClick="return ValidateCreditCard();" />
                         
                              <asp:Button ID="cancle_update" runat="server"  Text="Cancel" CssClass="btn btn-danger" />
                           </div> </div></div>

                          <%--  <div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Card Number:</label>--%>
                                     <%--<asp:Button ID="Button1" runat="server" Text="Submit" OnClick="BtnSubmit_Click" CssClass="button buttonBlue" OnClientClick="return Check();" />--%>
                               
                            <%--</div>--%>
                            <%--<div class="col-md-2">
                                <div class="form-group">
                                    <%--<label for="exampleInputPassword1">Confirm Card Number:</label>
                                    <asp:TextBox ID="TextBox3" CssClass="form-control" runat="server" MaxLength="16"></asp:TextBox>--%>
                                </div></div>
                           <%-- </div>--%>
                           <%-- <div class="col-md-3">
                                <div class="form-group">--%>
                                   <%-- <label for="exampleInputPassword1">Card Expiry Date:</label>
                                    <asp:TextBox ID="TextBox4" CssClass="form-control" runat="server" MaxLength="16"></asp:TextBox>--%>
                                <%--</div>
                            </div>
                        </div>--%>
                                <%--<div class="row">

                                    <div class="col-sm-4">
                                    </div>
                                </div>--%>
                            

                            <div class="row" align="center">
                                <%--<asp:Button ID="BtnSubmit" runat="server" Text="Update" OnClick="BtnSubmit_Click" Style="width: 74px;" CssClass="btn btn-success" OnClientClick="return ValidateCreditCard();" />
                         --%>   </div>
                        </div>
                    </div>
                </div></div>
           <%-- </div>
        </div>--%>
        <asp:HiddenField ID="HdnId" runat="server" />

    </form>
</body>
</html>
 <script type="text/javascript">

     function ValidateCreditCard() {
         debugger;
         if ($("#txtpcc").val() == "")
         {
             alert("Plese enter pcc");
             $("#txtpcc").focus();
             return false;
         }
         if ($("#hidtxtAirline").val() == "") {
             alert("Plese select airline");
             $("#txtAirline").focus();
             return false;
         }
         if ($("#ddltrip").val() == "0") {
             alert("Plese select trip");
             $("#ddltrip").focus();
             return false;
         }
         if ($("#ddlCardType").val() == "0") {
             alert("Plese select card type");
             $("#ddlCardType").focus();
             return false;
         }
         if ($("#txtcardnumber").val() == "") {
             alert("Plese enter card number");
             $("#txtcardnumber").focus();
             return false;
         }
         if ($("#txtcardnumber1").val() == "") {
             alert("Plese enter confirm card number");
             $("#txtcardnumber1").focus();
             return false;
         }
         debugger;
         if ($.trim($("#ddlCardType").val()).toUpperCase() == "AX") {
             if ($.trim($("#txtcardnumber").val().length) != "15") {
                 alert('Credit Card no seems to be incorrect.Amex card length must be 15 digits long');
                $("#txtcardnumber").focus();
                 return false;
             }
             if ($.trim($("#txtcardnumber").val()[0]) != "3") {
                 alert('Amex credit card no. must start with 3');
                 $("#txtcardnumber").focus();
                 return false;
             }
         }

         if ($.trim($("#ddlCardType").val()).toUpperCase() == "VI") {
             if ($.trim($("#txtcardnumber").val().length) != "16") {
                 alert('Credit Card no seems to be incorrect.Visa card length must be 16 digits long');
                 $("#txtcardnumber").focus();
                 return false;
             }
             if ($.trim($("#txtcardnumber").val()[0]) != "4") {
                 alert('Visa credit card no. must start with 4');
                 $("#txtcardnumber").focus();
                 return false;
             }
         }
         if ($.trim($("#ddlCardType").val()).toUpperCase() == "CA") {
             if ($.trim($("#txtcardnumber").val().length) != "16") {
                 alert('Credit Card no seems to be incorrect.Master card length must be 16 digits long');
                 $("#txtcardnumber").focus();
                 return false;
             }
             if ($.trim($("#txtcardnumber").val()[0]) != "5") {
                 alert('Master credit card no. must start with 5');
                 $("#txtcardnumber").focus();
                 return false;
             }
         }




         if ($("#txtcardnumber1").val() != $("#txtcardnumber").val())
         {
             alert("Check card number and confirm card number");
             $("#txtcardnumber").focus();
             return false;
         }
         if ($("#txtexpiry").val() == "") {
             alert("Please enter expiry date");
             $("#txtexpiry").focus();
             return false;
         }
         var status = ValidateCCExpDate();
         if (status == false)
             return false;
         if ($("#ddlstatus").val() == "0") {
             alert("Please select status");
             $("#ddlstatus").focus();
             return false;
         }


         return true;
     }

     function isExpCCDate(evt) {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 47) {
             return true;
         }
         else {

             return false;
         }
     }
     function isNumberKey(evt) {
         //debugger;
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode >= 48 && charCode <= 57 || charCode == 08 || charCode == 46) {
             return true;
         }
         else {

             return false;
         }
     }
     function isCharKey(evt) {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode >= 65 && charCode <= 90 || charCode >= 97 && charCode <= 122 || charCode == 32 || charCode == 08) {
             return true;
         }
         else {
             //alert("please enter Alphabets  only");
             return false;
         }
     }
     function isCharNumberKey(evt) {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode >= 65 && charCode <= 90 || charCode >= 97 && charCode <= 122 || charCode >= 48 && charCode <= 57 || charCode == 32 || charCode == 08) {
             return true;
         }
         else {
             //alert("please enter Alphabets  only");
             return false;
         }
     }
     function ValidateCCExpDate() {
         debugger;
         var ExpCCdate = $("#txtexpiry").val();
         if (ExpCCdate.length > 0) {
             if (ExpCCdate.length == 7) {
                 ExpCCdate = ExpCCdate.substr(2, 1);
                 if (ExpCCdate != '/') {
                     alert("Please write correct date formate like mm/yyyy ")
                     $("#txtexpiry").focus();
                     return false;
                 }
             }
             else {
                 alert("Please write correct date formate like mm/yyyy ")
                 $("#txtexpiry").focus();
                 return false;
             }
         }

         var today = new Date();
         var dd = today.getDate();
         var mm = today.getMonth() + 1;

         var yyyy = today.getFullYear();
         if (dd < 10) {
             dd = '0' + dd
         }
         if (mm < 10) {
             mm = '0' + mm
         }
         var today = yyyy + '/' + mm + '/' + dd;
         var CCExp = $.trim($("#txtexpiry").val());
         CCExp = CCExp.split('/')[1] + '/' + CCExp.split('/')[0] + '/' + dd;

         var CCExpDate = Date.parse(CCExp); //new Date(CCExp);
         var todayDate = Date.parse(today); //new Date(today);
         if (CCExpDate < todayDate) {
             alert('Your credit card date is expired. Please update credit card information.');
             return false;
         }
         return true;
     }
     var UrlBase = '<%=ResolveUrl("~/")%>';
    </script>