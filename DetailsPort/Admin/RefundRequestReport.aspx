﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="RefundRequestReport.aspx.cs" Inherits="DetailsPort_Admin_RefundRequestReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="<%=ResolveUrl("../../Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("../../Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
   

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow-y: scroll;
        }
    </style>
    
    <%-- <div class="large-8 medium-8 small-12 columns">
        <div class="large-12 medium-12 small-12 heading">
            <div class="large-12 medium-12 small-12 heading1">
                PG Refund Requested Report
            </div>
            <div class="clear1">
            </div>
            <div class="large-2 medium-3 small-3 columns">
                From Date
            </div>
            <div class="large-3 medium-3 small-9  columns">
                <input type="text" name="From" id="From" class="date" readonly="readonly" />
            </div>
            <div class="large-3 medium-3 small-3 large-push-1 medium-push-1 columns">
                To Date
            </div>
            <div class="large-3 medium-3 small-9  columns">
                <input type="text" name="To" id="To" class="date" readonly="readonly" />
            </div>
            <div class="clear">
            </div>
            <div class="large-2 medium-3 small-3 columns">
                PNR
            </div>
            <div class="large-3 medium-3 small-9  columns">
                <asp:TextBox ID="txt_PNR" runat="server"></asp:TextBox>
            </div>
            <div class="large-3 medium-3 small-3 large-push-1 medium-push-1 columns">
                OrderId
            </div>
            <div class="large-3 medium-3 small-9  columns">
                <asp:TextBox ID="txt_OrderId" runat="server"></asp:TextBox>
            </div>
            <div class="clear">
            </div>
            <div class="large-2 medium-3 small-3 columns">
                RequestType
            </div>
            <div class="large-3 medium-3 small-9  columns">
                <asp:DropDownList ID="ddl_RequestType" runat="server">
                    <asp:ListItem Value="H">HoldPNR Request</asp:ListItem>
                    <asp:ListItem Value="C">Cancel Request</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="large-3 medium-3 small-3 large-push-1 medium-push-1 columns">
                Ticket No
            </div>
            <div class="large-3 medium-3 small-9  columns">
                <asp:TextBox ID="txt_TktNo" runat="server"></asp:TextBox>
            </div>
            <div class="clear1">
            </div>
            <div class="large-4 medium-6 small-12 large-push-5  medium-push-5">
                <div class="large-6 medium-6 small-6 columns">
                    <asp:Button ID="btn_result" runat="server" Text="Search Result" OnClick="btn_result_Click" />
                </div>
            </div>
            <div class="clear1">
            </div>
            <div class="clear1">
            </div>
        </div>
        <div class="clear1">
        </div>
    </div>
    <div align="center">
        <table cellspacing="10" cellpadding="0" border="0" class="tbltbl" width="950px">
            <tr>
                <asp:Label ID="lbl_Norecord" runat="server"></asp:Label>
            </tr>
          
            <tr>
                <td align="center" runat="server" id="td_holdpnr" visible="false">
                    <asp:GridView ID="GridHoldPNRAccept" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" GridLines="None" PageSize="10" OnRowCommand="GridHoldPNRAccept_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="OrderId">
                                <ItemTemplate>
                                 
                                    <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AgentId">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                    <asp:Label ID="lbl_Counter" runat="server" Visible="false" Text='<%#Eval("Counter") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PNR">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PNR" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sector">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("sector") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Status" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duration">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Duration" runat="server" Text='<%#Eval("Duration") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TripType">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TripType" runat="server" Text='<%#Eval("TripType") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Trip">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TourCode">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TourCode" runat="server" Text='<%#Eval("TourCode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TotalBookingCost">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TotalBookingCost" runat="server" Text='<%#Eval("TotalBookingCost") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TotalAfterDis">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TotalAfterDis" runat="server" Text='<%#Eval("TotalAfterDis") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PgFName" runat="server" Text='<%#Eval("PgFName") %>'></asp:Label>
                                    <asp:Label ID="lbl_PgLName" runat="server" Text='<%#Eval("PgLName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PgMobile">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PgMobile" runat="server" Text='<%#Eval("PgMobile") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PgEmail">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PgEmail" runat="server" Text='<%#Eval("PgEmail") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CreateDate">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Partner Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PartnerName" runat="server" Text='<%#Eval("PartnerName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Update Status">
                                <ItemTemplate>
                                    <asp:LinkButton ID="ITZ_Accept" runat="server" CommandName="Accept" CommandArgument='<%#Eval("OrderId") %>'
                                        Font-Bold="True" Font-Underline="False">Accept</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_remarks" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PaymentMode">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PG Charges">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Charges" runat="server" Text='<%#Eval("PgCharges") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reject Remark">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtRemark" runat="server" Height="47px" TextMode="MultiLine" Width="175px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSubmit" runat="server" OnClientClick="return Validate();"
                                        CommandArgument='<%# Eval("Counter") %>' CommandName="UPDATED"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                    <asp:LinkButton ID="lnkHides" runat="server"
                                        CommandName="CANCEL" CommandArgument='<%# Eval("Counter") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="RowStyle" />
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <PagerStyle />
                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                        <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                        <EditRowStyle CssClass="EditRowStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center" runat="server" id="td_cancellation" visible="false">
                    <asp:GridView ID="InProccess_grdview" runat="server" AllowPaging="False" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="GridViewStyle" Width="100%" OnRowCommand="InProccess_grdview_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Agent ID">
                                <ItemTemplate>
                                    <asp:Label ID="AgentID" runat="server" Text='<%#Eval("UserID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Agency Name" DataField="Agency_Name"></asp:BoundField>
                            <asp:TemplateField HeaderText="Order Id">
                                <ItemTemplate>
                                    <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax ID">
                                <ItemTemplate>
                                    <asp:Label ID="TID" runat="server" Text='<%#Eval("Counter")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax Type">
                                <ItemTemplate>
                                    <asp:Label ID="PaxType" runat="server" Text='<%#Eval("pax_type")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax Name">
                                <ItemTemplate>
                                    <asp:Label ID="PaxName" runat="server" Text='<%#(Eval("Title").ToString()+" " + Eval("pax_fname").ToString()+" " + Eval("pax_lname").ToString()).ToUpper()%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PNR">
                                <ItemTemplate>
                                    <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("pnr_locator")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ticket No">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkupdate" runat="server" Text='<%#Eval("Tkt_No") %>' ForeColor="Red"
                                        Font-Bold="true" Font-Size="11px" CommandName="lnkupdate" CommandArgument='<%#Eval("Counter")%>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Airline" DataField="VC"></asp:BoundField>
                            <asp:BoundField HeaderText="SECTOR" DataField="Sector"></asp:BoundField>
                            <asp:TemplateField HeaderText="Total Fare">
                                <ItemTemplate>
                               
                                    <asp:Label ID="lbltotalfare" runat="server" Text='<%#Eval("TotalFare") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Net Fare" DataField="TotalFareAfterDiscount">
                                <ItemStyle HorizontalAlign="center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Journey Date" DataField="Journey_Date"></asp:BoundField>
                            <asp:BoundField HeaderText="Partner Name" DataField="PartnerName"></asp:BoundField>
                            <asp:BoundField HeaderText="Status" DataField="Status"></asp:BoundField>
                            <asp:BoundField HeaderText="Request Date" DataField="SubmitDate"></asp:BoundField>
                            <asp:TemplateField HeaderText="Customer Remarks" ControlStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegardingCancel" runat="server" Text='<%#Eval("RegardingCancel")%>'></asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PaymentMode">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PG Charges">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Charges" runat="server" Text='<%#Eval("PgCharges") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reject Remark">
                                <ItemTemplate>
                                    <asp:TextBox ID="txt_Remark" runat="server" Height="47px" TextMode="MultiLine" Width="175px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSubmit" runat="server" OnClientClick="return Validate();"
                                        CommandArgument='<%# Eval("Counter") %>' CommandName="UPDATED"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                    <asp:LinkButton ID="lnkHides" runat="server"
                                        CommandName="CANCEL" CommandArgument='<%# Eval("Counter") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <RowStyle CssClass="RowStyle" />
                        <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                        <PagerStyle CssClass="PagerStyle" />
                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                        <HeaderStyle CssClass="HeaderStyle" />
                        <EditRowStyle CssClass="EditRowStyle" />
                        <AlternatingRowStyle CssClass="AltRowStyle" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>--%>

    
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-10">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">PG Refund Requested Report</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly"  />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">To Date</label>
                                <input type="text" name="To" id="To" class="form-control" readonly="readonly"  />
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">PNR</label>
                                <asp:TextBox ID="txt_PNR" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Order Id</label>
                                    <asp:TextBox ID="txt_OrderId" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                           <%-- <div class="col-md-4">
                                <label for="exampleInputPassword1">Pax Name</label>
                                <asp:TextBox ID="txt_PaxName" class="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">Airline</label>
                                <asp:TextBox ID="txt_AirPNR" class="form-control" runat="server"></asp:TextBox>
                            </div>--%>
                           <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">RequestType</label>
                                    <asp:DropDownList ID="ddl_RequestType" runat="server"  class="form-control">
                                        <asp:ListItem Value="H">HoldPNR Request</asp:ListItem>
                                        <asp:ListItem Value="C">Cancel Request</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">Ticket No</label>
                                <asp:TextBox ID="txt_TktNo" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>

                        

                       

                            <%--<div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">RequestType</label>
                                    <asp:DropDownList ID="ddl_RequestType" runat="server"  class="form-control">
                                        <asp:ListItem Value="H">HoldPNR Request</asp:ListItem>
                                        <asp:ListItem Value="C">Cancel Request</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">Ticket No</label>
                                <asp:TextBox ID="txt_TktNo" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>--%>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" OnClick="btn_result_Click" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Label ID="lbl_Norecord" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>

                     
                           
                              <div class="row" id="td_holdpnr" style="background-color:#fff; overflow-y:scroll; overflow-x:scroll; max-height:500px; " runat="server" visible="false">                               
                            <div class="col-md-28">
                           
                                <asp:GridView ID="GridHoldPNRAccept" runat="server"  AllowPaging="False" AllowSorting="True"
                                   AutoGenerateColumns="False" CssClass="table" GridLines="None" OnRowCommand="GridHoldPNRAccept_RowCommand" PageSize="5" OnPageIndexChanging="GridHoldPNRAccept_PageIndexChanging">
                                    <Columns>
                                        <asp:TemplateField HeaderText="OrderId">
                                <ItemTemplate>
                                    <%--     <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                        style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; font-weight: bold;"></a>--%>
                                    <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AgentId">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_AgentID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                    <asp:Label ID="lbl_Counter" runat="server" Visible="false" Text='<%#Eval("HeaderId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PNR">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PNR" runat="server" Text='<%#Eval("GdsPnr") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sector">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("sector") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Status" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duration">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Duration" runat="server" Text='<%#Eval("Duration") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TripType">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TripType" runat="server" Text='<%#Eval("TripType") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Trip">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TourCode">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TourCode" runat="server" Text='<%#Eval("TourCode") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TotalBookingCost">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TotalBookingCost" runat="server" Text='<%#Eval("TotalBookingCost") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TotalAfterDis">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_TotalAfterDis" runat="server" Text='<%#Eval("TotalAfterDis") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PgFName" runat="server" Text='<%#Eval("PgFName") %>'></asp:Label>
                                    <asp:Label ID="lbl_PgLName" runat="server" Text='<%#Eval("PgLName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PgMobile">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PgMobile" runat="server" Text='<%#Eval("PgMobile") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PgEmail">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PgEmail" runat="server" Text='<%#Eval("PgEmail") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CreateDate">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Partner Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PartnerName" runat="server" Text='<%#Eval("PartnerName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Update Status">
                                <ItemTemplate>
                                    <asp:LinkButton ID="ITZ_Accept" runat="server" CommandName="AcceptHold" CommandArgument='<%#Eval("OrderId") %>'
                                        Font-Bold="True" Font-Underline="False">Accept</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_remarks"  runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PaymentMode">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PG Charges">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Charges" runat="server" Text='<%#Eval("PgCharges") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reject Remark">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtRemark"  Visible="false" runat="server" Height="47px" TextMode="MultiLine" Width="175px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSubmit" runat="server"   Visible="false"
                                        CommandArgument='<%# Eval("HeaderId") %>' CommandName="UpdateHold"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                    <asp:LinkButton ID="lnkHides" runat="server"  Visible="false"
                                        CommandName="RejectHold" CommandArgument='<%# Eval("HeaderId") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                          
                        </div>
                        </div>

                        <div class="row" id="td_cancellation" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server" visible="false">
                           <div class="col-md-28">
                                <asp:GridView ID="InProccess_grdview" runat="server" AllowPaging="False" AllowSorting="True" OnRowCommand="InProccess_grdview_RowCommand" OnPageIndexChanging="GridHoldPNRAccept_PageIndexChanging"
                                    AutoGenerateColumns="False" CssClass="table " GridLines="None" border="1">
                                    <Columns>
                                       <asp:TemplateField HeaderText="Agent ID">
                                <ItemTemplate>
                                    <asp:Label ID="AgentID" runat="server" Text='<%#Eval("UserID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Agency Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblAgency_Name" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Order Id">
                                <ItemTemplate>
                                    <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax ID">
                                <ItemTemplate>
                                    <asp:Label ID="TID" runat="server" Text='<%#Eval("Counter")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax Type">
                                <ItemTemplate>
                                    <asp:Label ID="PaxType" runat="server" Text='<%#Eval("pax_type")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax Name">
                                <ItemTemplate>
                                    <asp:Label ID="PaxName" runat="server" Text='<%#(Eval("Title").ToString()+" " + Eval("pax_fname").ToString()+" " + Eval("pax_lname").ToString()).ToUpper()%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PNR">
                                <ItemTemplate>
                                    <asp:Label ID="GdsPNR" runat="server" Text='<%#Eval("pnr_locator")%>'></asp:Label>
                                    <asp:Label ID="lbl_Paxid" runat="server" Visible="false" Text='<%#Eval("PaxID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ticket No">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_tktno" runat="server" Text='<%#Eval("Tkt_No")%>'></asp:Label>
                                    <%--<asp:LinkButton ID="lnkupdate" runat="server" Text='<%#Eval("Tkt_No") %>' ForeColor="Red"
                                        Font-Bold="true" Font-Size="11px" CommandName="lnkupdate" CommandArgument='<%#Eval("Counter")%>'></asp:LinkButton>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Airline">
                                <ItemTemplate>
                                    <asp:Label ID="Airline" runat="server" Text='<%#Eval("VC")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SECTOR">
                                <ItemTemplate>
                                    <asp:Label ID="SECTOR" runat="server" Text='<%#Eval("Sector")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Fare">
                                <ItemTemplate>
                                    <asp:Label ID="lbltotalfare" runat="server" Text='<%#Eval("TotalFare") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Net Fare">
                                <ItemTemplate>
                                    <asp:Label ID="Net_Fare" runat="server" Text='<%#Eval("TotalFareAfterDiscount")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Journey Date">
                                <ItemTemplate>
                                    <asp:Label ID="Journey_Date" runat="server" Text='<%#Eval("Journey_Date")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Partner Name">
                                <ItemTemplate>
                                    <asp:Label ID="Partner_Name" runat="server" Text='<%#Eval("PartnerName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="Status" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Request Date">
                                <ItemTemplate>
                                    <asp:Label ID="Request_Date" runat="server" Text='<%#Eval("SubmitDate")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer Remarks" ControlStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegardingCancel" runat="server" Text='<%#Eval("RegardingCancel")%>'></asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PaymentMode">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PG Charges">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_Charges" runat="server" Text='<%#Eval("PgCharges") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Update Status">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk_updatestatus" runat="server" CommandName="AcceptCancel" CommandArgument='<%#Eval("OrderId") %>'
                                        Font-Bold="True" Font-Underline="False">Accept</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reject Remark">
                                <ItemTemplate>
                                    <asp:TextBox ID="txt_Remark"  Visible="false" runat="server" Height="47px" TextMode="MultiLine" Width="175px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSubmit" runat="server"  Visible="false"
                                        CommandArgument='<%# Eval("Counter") %>' CommandName="UpdateCancel"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                    <asp:LinkButton ID="lnkHides" runat="server"  Visible="false"
                                        CommandName="RejectCancel" CommandArgument='<%# Eval("Counter") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                                    </Columns>

                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" Height="50px" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
           
   



   <%-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".date").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>--%>
    
</asp:Content>
