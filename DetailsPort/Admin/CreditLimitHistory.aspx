﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="CreditLimitHistory.aspx.cs" Inherits="DetailsPort_Admin_CreditLimitHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../chosen/jquery-1.6.1.min.js" type="text/javascript"></script>
    <script src="../../chosen/chosen.jquery.js" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <%--<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />--%>
    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
                $(".cd").click(function () {
                    $("#From").focus();
                }
                );
                $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
                $(".cd1").click(function () {
                    $("#To").focus();
                }
                );
            });
    </script>
    <script language="javascript" type="text/javascript">
        function getKeyCode(e) {
            if (window.event)
                return window.event.keyCode;
            else if (e)
                return e.which;
            else
                return null;
        }
        function keyRestrict(e, validchars) {
            var key = '', keychar = '';
            key = getKeyCode(e);
            if (key == null) return true;
            keychar = String.fromCharCode(key);
            keychar = keychar.toLowerCase();
            validchars = validchars.toLowerCase();
            if (validchars.indexOf(keychar) != -1)
                return true;
            if (key == null || key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
                return true;
            return false;
        }
        
        function Validate() {
            //if ($("#txtAgencyName").val() == "") {
            //    alert("Please select agent id");
            //    $("#txtAgencyName").focus();
            //    return false;
            //}
            //if ($("#hidtxtAgencyName").val() == "") {
            //    alert("Please select agent id");
            //    $("#txtAgencyName").focus();
            //    return false;
            //}

            
        }
        </script>
    <div class="row">
        <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
  
                    <div class="panel-body">                        
                        <div class="row" style="margin-left: 240px;">                            
                            <div class="col-md-3">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">User Id/Agency Id :</label>--%>
                                    <input type="text" placeholder="AGENCY NAME" id="txtAgencyName" name="txtAgencyName" <%--onfocus="focusObj(this);"
                                        onblur="blurObj(this);"--%> <%--defvalue="Agency Name or ID"--%> autocomplete="off" value=""
                                        class="form-control input-text full-width" tabindex="3" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /> 
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-user"></span>
                                        </span>                                  
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="input-group">                                    
                                    <%--<label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" placeholder="FROM DATE" name="From" id="From" readonly="readonly" class="form-control input-text full-width" />
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar cd" style="cursor: pointer;"></span>
                                        </span>
                                </div>
                            </div>

                           
                            <div class="col-md-3">
                                <div class="input-group">                                    
                                    <%--<label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text" placeholder="TO DATE" name="To" id="To" readonly="readonly" class="form-control input-text full-width" />
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar cd1" style="cursor: pointer;"></span>
                                        </span>
                                </div>
                            </div>
                             <div class="row">
                               <div class="col-md-2" style="margin-top:-15px">
                                <div class="form-group">
                                    <br />                                   
                                    <asp:Button ID="BtnSearch" runat="server" Text="Find" CssClass="btn btn-success" OnClick="BtnSearch_Click"  OnClientClick="return Validate();" style="background-color: #4CAF50;"/>
                                </div>
                            </div>
                        </div>
                            

                        </div>
                    
                        <div class="clear"></div>
                        <div class="row">

                            <div class="col-md-12">
                                <div id="DivMsg" runat="server" style="color: red;"></div>                               
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="background-color: #fff; overflow: auto; max-height: 500px; text-transform:uppercase;" >
                                    <ContentTemplate>
                                        <asp:GridView ID="grd_P_IntlDiscount" runat="server" AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover" GridLines="None" Width="100%" PageSize="100" AllowPaging="true"
                                            OnPageIndexChanging="OnPageIndexChanging">
                                            <Columns>
                                               <%-- AgentID, AgencyId, AgencyName, DistrId,  ExecutiveID, IPAddress, AvalBalDebit, AvalBalCredit, Aval_Balance, CrdLimitDebit, 
                         CrdLimitCredit, CurrentCrdLimit, DueAmountDebit, DueAmountCredit, TotalDueAmount, CreatedDate, UpdatedDate, BookingType, Remark, BookedBy, PaymentMode, 
                         SetCreditLimit--%>
                                                <asp:TemplateField HeaderText="User_ID">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUser_ID" runat="server" Text='<%#Eval("AgentID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Agency ID" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgency_ID" runat="server" Text='<%#Eval("AgencyId") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Agency Name" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAgency_Name" runat="server" Text='<%#Eval("AgencyName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                              
                                                <asp:TemplateField HeaderText="CreditLimit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAvilable_Balance" runat="server" Text='<%#Eval("CurrentCrdLimit") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Remark">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRemark" runat="server" Text='<%#Eval("Remark") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="UpdatedBy">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExecutiveID" runat="server" Text='<%#Eval("ExecutiveID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="UpdatedDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle CssClass="RowStyle" />
                                            <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                            <PagerStyle CssClass="PagerStyle" />
                                            <SelectedRowStyle CssClass="SelectedRowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <EditRowStyle CssClass="EditRowStyle" />
                                            <AlternatingRowStyle CssClass="AltRowStyle" />
                                            <EmptyDataTemplate>No records Found</EmptyDataTemplate>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                                <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                    <ProgressTemplate>
                                        <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                        </div>
                                        <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                            Please Wait....<br />
                                            <br />
                                            <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                            <br />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidActionType" runat="server" Value="select" />
    </div>

   <%-- <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">--%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
    <%--<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search4.js") %>"></script>     --%>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    


    
</asp:Content>


