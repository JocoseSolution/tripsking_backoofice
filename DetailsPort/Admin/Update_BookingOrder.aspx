﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="Update_BookingOrder.aspx.vb" Inherits="Update_BookingOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">--%>
    <%--<link href="../../css/StyleSheet.css" rel="stylesheet" type="text/css" />--%>
    <%--    <link href="../../css/itz.css" rel="stylesheet" type="text/css" />--%>
    <%--<link href="../../css/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../Hotel/css/HotelStyleSheet.css?V=2" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/ReissueRefund.js?V=1" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
    <%-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />--%>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

    <script type="text/javascript">
        $(function () {
            $(".date").datepicker({
                dateFormat: 'ddmmy',
                minDate: 0,
            });
        });
    </script>
    <script type="text/javascript">
        function checkit(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58 || charCode > 64 && charCode < 91 || charCode > 96 && charCode < 123 || (charCode == 8 || charCode == 45))) {
                return false;
            }
            status = "";
            return true;

        }
    </script>


    <%--    <link href="../../CSS/foundation.min.css" rel="stylesheet" />
    <link href="../../CSS/foundation.css" rel="stylesheet" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>

   
    <style>
        input[type="text"], input[type="password"], select, textarea {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>
    <style type="text/css">
        .griddata th
        {
            background-color: #efefef;
            color: #666666;
            font-size:smaller;
        }

        .panel-body
        {
            height: 100%;
            overflow: inherit;
        }

    </style>
    





    

    
    

    
    <div class="container-fluid">
        <div class="page-wrapperss">
            <div class="panel panel-primary">
                <%--     <table width="90%">
                <tr>
                    <td align="right" style="font-weight: bold; font-size: 15px; color: #004b91;">Booking Reference No.
                    </td>
                    <td id="tdRefNo" runat="server" align="left" style="font-weight: bold;"></td>
                </tr>
             
            </table>--%>


               
                <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10" style="margin-left: 60px">
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 14px;">Brand Details
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="Grd_AgencyDetails" runat="server" AutoGenerateColumns="false" Width="100%" GridLines="None" CssClass="griddata" style="text-transform:uppercase;">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Brand Id">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUser_Id" runat="server" Text='<%#Eval("User_Id")%>'></asp:Label>
                                                </ItemTemplate>
                                             <%--   <EditItemTemplate>
                                                    <asp:TextBox ID="txtUser_Id" Width="80px" runat="server" Text='<%#Eval("User_Id")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Brand Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAgency_Name" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:Label>
                                                </ItemTemplate>
                                              <%--  <EditItemTemplate>
                                                    <asp:TextBox ID="txtAgency_Name" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("Agency_Name")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email")%>'></asp:Label>
                                                </ItemTemplate>
                                               <%-- <EditItemTemplate>
                                                    <asp:TextBox ID="txtEmail" Width="80px" runat="server" Text='<%#Eval("Email")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMobile" runat="server" Text='<%#Eval("Mobile")%>'></asp:Label>
                                                </ItemTemplate>
                                               <%-- <EditItemTemplate>
                                                    <asp:TextBox ID="txtMobile" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("Mobile")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address")%>'></asp:Label>
                                                </ItemTemplate>
                                             <%--   <EditItemTemplate>
                                                    <asp:TextBox ID="txtAddress" Width="80px" runat="server" Text='<%#Eval("Address")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address1">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress1" runat="server" Text='<%#Eval("Address1")%>'></asp:Label>
                                                </ItemTemplate>
                                             <%--   <EditItemTemplate>
                                                    <asp:TextBox ID="txAddress1" Width="60px" runat="server" Text='<%#Eval("Address1")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Crd_Limit">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAval_Crd_Limit" runat="server" Text='<%#Eval("Crd_Limit")%>'></asp:Label>
                                                </ItemTemplate>
                                             <%--   <EditItemTemplate>
                                                    <asp:TextBox ID="txtCrd_Limit" Width="50px" runat="server" Text='<%#Eval("Crd_Limit")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                        </Columns>
                                        <HeaderStyle CssClass="HeaderStyle" />
                                    </asp:GridView>
                                    <b>
                                        <asp:Label ID="Label2" runat="server" Visible="false"></asp:Label></b>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%-- <asp:UpdateProgress ID="updateprogress4" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>

                        </td>
                    </tr>

                </table>

                <div>
                    &nbsp;
                </div>

                <div class="row">
                    <div class="col-md-2" style="margin-left: 71px; margin-bottom: 18px;">
                        <div>
                            <asp:Button ID="btnaccept" runat="server" Text="Accept" OnClick="btnaccept_Click" CssClass="btn btn-success" Visible="false"  formnovalidate/>
                            <asp:Button ID="btnreject" runat="server" Text="Reject" OnClick="btnreject_Click" CssClass="btn btn-success" Visible="false"  formnovalidate/>
                        </div>
                    </div>
                    <div class="col-md-4" id="divremark" runat="server" style="display:none">
                        <div style="float: right; margin-right: 59px">
                            <label for="exampleInputEmail1">Remark</label>
                            <asp:TextBox ID="txt_Reject" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            <asp:Button ID="btn_Comment" runat="server" Text="Proceed Rejection" OnClientClick="return Validate();"
                             formnovalidate=""    CssClass="btn btn-success" />

                            <asp:Button ID="btn_Cancel" runat="server" Text="Cancel" CssClass="btn btn-success" formnovalidate="" />
                        </div>
                    </div>
                </div>
                <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10" style="margin-left: 55px;">
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 14px;">PNR Details
                        </td>
                    </tr>

                    <tr>


                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GvFlightHeader" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="griddata" style="text-transform:uppercase;" GridLines="None">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Booking Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBookingDate" runat="server" Text='<%#Eval("CreateDate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
  <asp:TemplateField HeaderText="Mobile NO.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblmbl" runat="server" Text='<%#Eval("PgMobile")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Gds Pnr">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGdsPnr"  CssClass="HideMode" runat="server" Text='<%#Eval("GDsPnr")%>'></asp:Label>
                                                   
                                                   
                                                     <asp:TextBox ID="txtGdsPnr" CssClass="EditMode" runat="server" Width="70px" Text='<%#Eval("GDsPnr")%>' required="" Style="display:none"></asp:TextBox>
                                              
                                                </ItemTemplate>
                                              <%--  <EditItemTemplate>
                                                    <asp:TextBox ID="txtGdsPnr" CssClass="EditMode" runat="server" Width="70px" Text='<%#Eval("GDsPnr")%>' required=""></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Airline Pnr">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAirlinePnr" CssClass="HideMode" runat="server" Text='<%#Eval("AirlinePnr")%>'></asp:Label>
                                              
                                                     <asp:TextBox ID="txtAirlinePnr" CssClass="EditMode" runat="server" Width="70px" Text='<%#Eval("AirlinePnr")%>' required="" Style="display:none"></asp:TextBox>
                                           
                                                     </ItemTemplate>
                                               <%--   <EditItemTemplate>
                                                  <asp:TextBox ID="txtAirlinePnr" CssClass="EditMode" runat="server" Width="70px" Text='<%#Eval("AirlinePnr")%>' required=""></asp:TextBox>
                                               </EditItemTemplate>--%> 
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server" CssClass="HideMode" Text='<%#Eval("Status")%>'></asp:Label>
                                               
                                                
                                                   <%-- <asp:TextBox ID="txtStatus" runat="server" Width="80px" Text='<%#Eval("Status")%>'></asp:TextBox>
                                                   --%> 
                                                    <asp:DropDownList ID="ddlStatus" runat="server" Width="69px" required="" Style="display:none"  CssClass="EditMode">
                                                        <asp:ListItem Value="Confirm">Confirm</asp:ListItem>
                                                        <asp:ListItem Value="Ticketed">Ticketed</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--<asp:TextBox ID="txtStatus" runat="server" Width="80px" Text='<%#Eval("Status")%>' CssClass="HideMode"></asp:TextBox>
                                               --%> </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Modify Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblmStatus" runat="server" Text='<%#Eval("MordifyStatus")%>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SearchId">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSearchid" CssClass="HideMode" runat="server" Text='<%#Eval("SearchId")%>'></asp:Label>
                                                  <asp:TextBox ID="txtSearchId" CssClass="EditMode" runat="server" Width="80px" Text='<%#Eval("SearchId")%>'  required="" Style="display:none"></asp:TextBox>
                                               
                                                     <%-- <input type="hidden" id="AirSearch" />--%>
                                                </ItemTemplate>
                                                <%--<EditItemTemplate>
                                                    <asp:TextBox ID="txtSearchId" CssClass="EditMode" runat="server" Width="80px" Text='<%#Eval("SearchId")%>' required=""></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BookingId">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBookingId" runat="server" CssClass="HideMode" Text='<%#Eval("PNRId")%>'></asp:Label>
                                              <asp:TextBox ID="BookingId" runat="server" CssClass="EditMode" Width="80px" Text='<%#Eval("PNRId")%>' required="" Style="display:none"></asp:TextBox>
                                                
                                                      </ItemTemplate>
                                                <%--<EditItemTemplate>
                                                    <asp:TextBox ID="BookingId" runat="server" CssClass="EditMode" Width="80px" Text='<%#Eval("PNRId")%>' required=""></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TicketId">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTicketId" CssClass="HideMode" runat="server" Text='<%#Eval("TicketId")%>'></asp:Label>
                                                <%-- <input type="hidden" id="TktIdStatus" />--%>
                                                     <asp:TextBox ID="txtTicketIdr" CssClass="EditMode" runat="server" Width="70px" Text='<%#Eval("TicketId")%>' required="" Style="display:none"></asp:TextBox>
                                               
                                                     </ItemTemplate>
                                               <%-- <EditItemTemplate>
                                                    <asp:TextBox ID="txtTicketIdr" CssClass="EditMode" runat="server" Width="70px" Text='<%#Eval("TicketId")%>' required=""></asp:TextBox>
                                                </EditItemTemplate>--%>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SearchIdName">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSearchIdName" runat="server" Text='<%#Eval("SearchIdName")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Hand_Bag_Fare">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblHand_Bag_Fare" runat="server" Text='<%#Eval("IsBagfares")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RESULTTYPE">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRESULTTYPE" runat="server" Text='<%#Eval("RESULTTYPE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField HeaderText="Modify Status" DataField="MordifyStatus"></asp:BoundField>--%>
                                            <%--<asp:BoundField HeaderText="SearchId" DataField="SearchId"></asp:BoundField>--%>
                                            <%--<asp:BoundField HeaderText="BookingId" DataField="PNRId"></asp:BoundField>--%>
                                            <%-- <asp:BoundField HeaderText="TicketId" DataField="TicketId"></asp:BoundField>--%>
                                            <%--<asp:BoundField HeaderText="SearchIdName" DataField="SearchIdName"></asp:BoundField>--%>
                                            <%--  <asp:BoundField HeaderText="Hand_Bag_Fare" DataField="IsBagfares"></asp:BoundField>--%>
                                            <%-- <asp:BoundField HeaderText="RESULTTYPE" DataField="RESULTTYPE"></asp:BoundField>--%>
                                            <asp:TemplateField HeaderText="Message">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblmsg" runat="server" ForeColor="Red" Text='<%#Eval("msg")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PaymentMode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPayment_Access" runat="server" Text='<%#Eval("Payment_Access")%>'></asp:Label>
                                                   <%--   <input type="hidden" id="Pymtmd" />--%>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtPayment_Access" runat="server" Width="80px" Text='<%#Eval("Payment_Access")%>' required=""></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Update">
                                                <ItemTemplate>
                                                    <%--   <asp:LinkButton ID="ITZ_Accept" runat="server" CommandArgument='<%#Eval("OrderId") %>'
                                                        Font-Bold="True" Font-Underline="False" OnClick="ITZ_Accept_Click">UpdatePNR</asp:LinkButton>--%>

                                                   <%-- <asp:LinkButton ID="btnCancel" OnClientClick="ButtonHide()" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>--%>
                                                    <%--<span  onclick="ButtonHide()"  class="hideCancle" style="display:none">Cancle</span>--%>
                                                </ItemTemplate>
                                                <ItemTemplate>
                                                  
                                                    <span class="hideCancle" onclick="ButtonShow()">Edit</span>
                                                   
                                                   <%-- <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Remark" Visible="false">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <%--      <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="newbutton_2" CommandName="Update" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="newbutton_2" CommandName="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                        </Columns>
                                        <HeaderStyle CssClass="HeaderStyle" />
                                    </asp:GridView>
                                    <b>

                                        <%--<asp:LinkButton ID="outbtnEdit" OnClientClick="ButtonShow()" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>--%>

                                        <asp:Label ID="lblUpdateFltHeader" runat="server" Visible="false"></asp:Label></b>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%--   <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>
                        </td>


                    </tr>

                </table>
                <%--<asp:button ID="edittt" runat="server" OnClick="edittt_Click"/>--%>
                <div>
                    &nbsp;
                </div>
                <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10" style="margin-left: 60px">
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 14px;">Passenger Information
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <asp:GridView ID="GvTravellerInformation" runat="server" AutoGenerateColumns="False"
                                Width="100%" GridLines="None" CssClass="griddata" style="text-transform:uppercase;">
                                <Columns>
                                    <asp:TemplateField Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PaxId")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Title">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("Title")%>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<EditItemTemplate>
                                                    <asp:TextBox ID="txtTitle" Width="50px" runat="server" Text='<%#Eval("Title")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="First Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFName" runat="server" Text='<%#Eval("FName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <%-- <EditItemTemplate>
                                                    <asp:TextBox ID="txtFname" Width="100px" runat="server" Text='<%#Eval("FName")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                    </asp:TemplateField>
  <asp:TemplateField HeaderText="Middle Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblMName" runat="server" Text='<%#Eval("MName")%>'></asp:Label>
                                        </ItemTemplate>
                                      
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblLName" runat="server" Text='<%#Eval("LName")%>'></asp:Label>
                                        </ItemTemplate>
                                        <%--  <EditItemTemplate>
                                                    <asp:TextBox ID="txtLname" Width="100px" runat="server" Text='<%#Eval("LName")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                    </asp:TemplateField>
  <asp:TemplateField HeaderText="DOB">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldob" runat="server" Text='<%#Eval("DOB")%>'></asp:Label>
                                        </ItemTemplate>
                                        <%--  <EditItemTemplate>
                                                    <asp:TextBox ID="txtLname" Width="100px" runat="server" Text='<%#Eval("DOB")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                    </asp:TemplateField>
 
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%#Eval("PaxType")%>'></asp:Label>
                                        </ItemTemplate>
                                        <%-- <EditItemTemplate>
                                                    <asp:TextBox ID="txtType" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("PaxType")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Passport No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassport" runat="server" Text='<%#Eval("PassportNo")%>'></asp:Label>
                                        </ItemTemplate>
                                        <%--  <EditItemTemplate>
                                                    <asp:TextBox ID="txtLname" Width="100px" runat="server" Text='<%#Eval("DOB")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                    </asp:TemplateField>
 
  <asp:TemplateField HeaderText="Passport Expiry">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPassportexp" runat="server" Text='<%#Eval("PassportExpireDate")%>'></asp:Label>
                                        </ItemTemplate>
                                        <%--  <EditItemTemplate>
                                                    <asp:TextBox ID="txtLname" Width="100px" runat="server" Text='<%#Eval("DOB")%>'></asp:TextBox>
                                                </EditItemTemplate>--%>
                                    </asp:TemplateField>
 
  
                                    <asp:TemplateField HeaderText="Ticket No">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTktNo" runat="server" CssClass="HideMode" Text='<%#Eval("TicketNumber")%>'></asp:Label>
                                            
                                            <asp:TextBox ID="txtTktNo" runat="server" CssClass="EditMode" Width="80px" Text='<%#Eval("TicketNumber")%>' required="" Style="display:none"></asp:TextBox>
                                      
                                        </ItemTemplate>
                                       <%-- <EditItemTemplate>
                                            <asp:TextBox ID="txtTktNo" runat="server" CssClass="EditMode" Width="80px" Text='<%#Eval("TicketNumber")%>' required=""></asp:TextBox>
                                        </EditItemTemplate>--%>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                              <asp:Label ID="lblsts" runat="server" Text='<%#Eval("MORDIFYSTATUS")%>'></asp:Label>
                                         <%--   <asp:LinkButton  ID="lblsts" runat="server" CommandArgument='<%#Eval("TicketNumber")%>' Text='<%#Eval("MORDIFYSTATUS")%>'></asp:LinkButton>--%>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Message">
                                        <ItemTemplate>
                                            <asp:Label ID="lblsts1" runat="server" ForeColor="Red" Text='<%#Eval("msg")%>'></asp:Label>
                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Remark" Visible="false">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <%-- <asp:TemplateField HeaderText="Edit/Update" Visible="true">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2" OnClientClick="btnEdit_Click"/>
                                </ItemTemplate>--%>
                                    <%--  <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>--%>
                                    <%--                          <asp:TemplateField HeaderText="Update" Visible="false">
                                                <EditItemTemplate>
                                              <asp:LinkButton ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>

                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                  
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <%--      <asp:TemplateField HeaderText="Edit/Update" Visible="false">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    <%--  <asp:ButtonField CommandName="Edit" Text="Edit" />--%>
                                </Columns>
                                <HeaderStyle CssClass="HeaderStyle" />
                            </asp:GridView>
                            <b>
                                <asp:Label ID="lblUpdatePax" runat="server" Visible="false"></asp:Label></b>
                            <asp:TextBox ID="txtBtnOut" runat="server" Visible="false"></asp:TextBox>
                            <%--   <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>

                        </td>
                    </tr>

                </table>
                <div class="col-md-6 col-md-push-7">
                    <div style="float: right; display: none" id="div_update" runat="server" class="col-md-8">
                        <%--                     <input id="btn_update1" type="button" style="display:none" runat="server" name="Update PNR"  class="btn btn-success"/>
                     <input id="Btn_reject" type="button" style="display:none" runat="server" name="Reject PNR" class="btn btn-success"  />--%>
                        <%--                        <input type="submit" name="ctl00$ContentPlaceHolder1$btn_update1" value="Update PNR" id="ctl00_ContentPlaceHolder1_btn_update1" class="btn btn-success">
                     <input type="submit" name="ctl00$ContentPlaceHolder1$Btn_reject" value="Reject PNR" id="ctl00_ContentPlaceHolder1_Btn_reject" class="btn btn-success" />--%>
                      <div class="col-md-4">
                         <asp:Button ID="btn_update1" runat="server" Text="Update PNR"  CssClass="btn btn-success col-md-4" />
                        <%--<label class="RejectPnr" ></label>--%>
                          </div>
                       
                          <div class="col-md-4">
                         <asp:Button ID="Btn_reject" runat="server" Text="Reject PNR" CssClass="btn btn-success col-md-4"  />
                              </div>
                    </div>
                </div>
                <div>
                    &nbsp;
                </div>
                <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10" style="margin-left: 60px">
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 14px;">Gst Details
                        </td>
                    </tr>

                    <tr> <td>
                            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="grd_GST" runat="server" CssClass="griddata" style="text-transform:uppercase;" AutoGenerateColumns="false" Width="100%" GridLines="None">
                                        <Columns>
                                            <asp:TemplateField HeaderText="GSTNO">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGSTNO" runat="server" Text='<%#Eval("GSTNO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GST_Company_Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_Company_Name" runat="server" Text='<%#Eval("GST_Company_Name")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtGST_Company_Name" runat="server" Width="70px" Text='<%#Eval("GST_Company_Name")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GST_Company_Address">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_Company_Address" runat="server" Text='<%#Eval("GST_Company_Address")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtGST_Company_Address" runat="server" Width="70px" Text='<%#Eval("GST_Company_Address")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="GST_PhoneNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGST_PhoneNo" runat="server" Text='<%#Eval("GST_PhoneNo")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtGST_PhoneNo" runat="server" Width="80px" Text='<%#Eval("GST_PhoneNo")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="GST_Email" DataField="GST_Email"></asp:BoundField>
                                            <asp:BoundField HeaderText="GSTRemark" DataField="GSTRemark"></asp:BoundField>
                                            <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="newbutton_2" CommandName="Update" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="newbutton_2" CommandName="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                        </Columns>
                                        <HeaderStyle CssClass="HeaderStyle" />
                                    </asp:GridView>
                                    <b>
                                        <asp:Label ID="Label4" runat="server" Visible="false"></asp:Label></b>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%--  <asp:UpdateProgress ID="updateprogress6" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>
                        </td>

                    </tr>
                </table>
                <div>
                    &nbsp;
                </div>
                <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10" style="margin-left: 60px">
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 14px;">Flight Information
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GvFlightDetails" runat="server" CssClass="griddata" style="text-transform:uppercase;" AutoGenerateColumns="false" Width="100%" GridLines="None">
                                        <Columns>
                                            <asp:TemplateField Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFltId" runat="server" Text='<%#Eval("FltId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DepCity">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDepcityName" runat="server" Text='<%#Eval("DepCityOrAirportName")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDepcityName" Width="80px" runat="server" Text='<%#Eval("DepCityOrAirportName")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DCityCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDepCityOrAirportCode" runat="server" Text='<%#Eval("DepCityOrAirportCode")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDepcityCode" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("DepCityOrAirportCode")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ArrCity">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblArrCityOrAirportName" runat="server" Text='<%#Eval("ArrCityOrAirportName")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtArrcityName" Width="80px" runat="server" Text='<%#Eval("ArrCityOrAirportName")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ACityCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblArrCityOrAirportCode" runat="server" Text='<%#Eval("ArrCityOrAirportCode")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtArrcityCode" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("ArrCityOrAirportCode")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Airline">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAirlineName" runat="server" Text='<%#Eval("AirlineName")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAirlineName" Width="80px" runat="server" Text='<%#Eval("AirlineName")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AirCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAirlineCode" runat="server" Text='<%#Eval("AirlineCode")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAirlineCode" Width="60px" runat="server" Text='<%#Eval("AirlineCode")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="FltNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFltNumber" runat="server" Text='<%#Eval("FltNumber")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtFltNo" Width="50px" runat="server" Text='<%#Eval("FltNumber")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DepDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDepDate" runat="server" Text='<%#Eval("DepDate")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDepDate" MaxLength="6" onkeypress="return isNumberKey(event)" Width="60px" runat="server" Text='<%#Eval("DepDate")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="DepTime">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDepTime" runat="server" Text='<%#Eval("DepTime")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDepTime" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("DepTime")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ArrTime">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblArrTime" runat="server" Text='<%#Eval("ArrTime")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtArrTime" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("ArrTime")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AirCraft">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAirCraft" runat="server" Text='<%#Eval("AirCraft")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAirCraft" Width="50px" runat="server" Text='<%#Eval("AirCraft")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="FareBasis">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFareBasis" runat="server" Text='<%#Eval("AdtFareBasis")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtFareBasis" Width="50px" runat="server" Text='<%#Eval("AdtFareBasis")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RBD">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRBD" runat="server" Text='<%#Eval("AdtRbd")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtRBD" Width="50px" runat="server" Text='<%#Eval("AdtRbd")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cabin">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCabin" runat="server" Text='<%#Eval("Cabin")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtCabin" Width="50px" runat="server" Text='<%#Eval("Cabin")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Remark" Visible="false">
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtRemark" runat="server" Width="100px" TextMode="MultiLine"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                        </Columns>
                                        <HeaderStyle CssClass="HeaderStyle" />
                                    </asp:GridView>
                                    <b>
                                        <asp:Label ID="lblUpdateFlight" runat="server" Visible="false"></asp:Label></b>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%-- <asp:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>

                        </td>
                    </tr>

                </table>
                <div>
                    &nbsp;
                </div>
                <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10" style="margin-left: 60px">
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 14px;">Meal & Baggage
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="Grd_Mealbagg" runat="server" CssClass="griddata" style="text-transform:uppercase;" AutoGenerateColumns="false" Width="100%" GridLines="None">
                                        <Columns>
                                            <asp:TemplateField HeaderText="MealCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMealCode" runat="server" Text='<%#Eval("MealCode")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMealCode" Width="80px" runat="server" Text='<%#Eval("MealCode")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MealPrice">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMealPrice" runat="server" Text='<%#Eval("MealPrice")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMealPrice" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("MealPrice")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BaggageCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBaggageCode" runat="server" Text='<%#Eval("BaggageCode")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBaggageCode" Width="80px" runat="server" Text='<%#Eval("BaggageCode")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BaggagePrice">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBaggagePrice" runat="server" Text='<%#Eval("BaggagePrice")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBaggagePrice" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("BaggagePrice")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="AirLineCode">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAirLineCode" runat="server" Text='<%#Eval("AirLineCode")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAirLineCode" Width="80px" runat="server" Text='<%#Eval("AirLineCode")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BaggageDesc">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBaggageDesc" runat="server" Text='<%#Eval("BaggageDesc")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBaggageDesc" Width="60px" runat="server" Text='<%#Eval("BaggageDesc")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BaggageCategory">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBaggageCategory" runat="server" Text='<%#Eval("BaggageCategory")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBaggageCategory" Width="50px" runat="server" Text='<%#Eval("BaggageCategory")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BaggagePriceWithNoTax">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBaggagePriceWithNoTax" runat="server" Text='<%#Eval("BaggagePriceWithNoTax")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBaggagePriceWithNoTax" MaxLength="6" onkeypress="return isNumberKey(event)" Width="60px" runat="server" Text='<%#Eval("BaggagePriceWithNoTax")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MealDesc">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMealDesc" runat="server" Text='<%#Eval("MealDesc")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMealDesc" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("MealDesc")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MealCategory">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMealCategory" runat="server" Text='<%#Eval("MealCategory")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMealCategory" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("MealCategory")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MealPriceWithNoTax">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMealPriceWithNoTax" runat="server" Text='<%#Eval("MealPriceWithNoTax")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtMealPriceWithNoTax" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("MealPriceWithNoTax")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                        </Columns>
                                        <HeaderStyle CssClass="HeaderStyle" />
                                    </asp:GridView>
                                    <b>
                                        <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label></b>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%--  <asp:UpdateProgress ID="updateprogress5" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>

                        </td>
                    </tr>

                </table>
                <div>
                    &nbsp;
                </div>
                <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10" style="margin-left: 60px">
                    <tr>
                        <td align="left" style="font-weight: bold; font-size: 14px;">LedgerDetails
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="Grd_LedgerDetails" runat="server" CssClass="griddata" style="text-transform:uppercase;" AutoGenerateColumns="false" Width="100%" GridLines="None">
                                        <Columns>
                                            <asp:TemplateField HeaderText="PnrNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPnrNo" runat="server" Text='<%#Eval("PnrNo")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtPnrNo" Width="80px" runat="server" Text='<%#Eval("PnrNo")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TicketNo">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTicketNo" runat="server" Text='<%#Eval("TicketNo")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtTicketNo" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("TicketNo")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="TicketingCarrier">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTicketingCarrier" runat="server" Text='<%#Eval("TicketingCarrier")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtTicketingCarrier" Width="80px" runat="server" Text='<%#Eval("TicketingCarrier")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ExecutiveID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblExecutiveID" runat="server" Text='<%#Eval("ExecutiveID")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtExecutiveID" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("ExecutiveID")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Debit">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDebit" runat="server" Text='<%#Eval("Debit")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDebit" Width="80px" runat="server" Text='<%#Eval("Debit")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Credit">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCredit" runat="server" Text='<%#Eval("Credit")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtCredit" Width="60px" runat="server" Text='<%#Eval("Credit")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Aval_Balance">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAval_Balance" runat="server" Text='<%#Eval("Aval_Balance")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtAval_Balance" Width="50px" runat="server" Text='<%#Eval("Aval_Balance")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BookingType">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblBookingType" runat="server" Text='<%#Eval("BookingType")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtBookingType" MaxLength="6" onkeypress="return isNumberKey(event)" Width="60px" runat="server" Text='<%#Eval("BookingType")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Remark">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRemark" runat="server" Text='<%#Eval("Remark")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtRemark" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("Remark")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="MERCHANDISERID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDISTRID" runat="server" Text='<%#Eval("DISTRID")%>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtDISTRID" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("DISTRID")%>'></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                        </Columns>
                                        <HeaderStyle CssClass="HeaderStyle" />
                                    </asp:GridView>
                                    <b>
                                        <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label></b>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <%-- <asp:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                                <ProgressTemplate>
                                    <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                    </div>
                                    <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                        Please Wait....<br />
                                        <br />
                                        <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                        <br />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>--%>

                        </td>
                    </tr>

                </table>





                <div>
                    &nbsp;
                </div>
                <div>
                    <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10" style="margin-left: 60px; background-color: #efefef;">
                        <tr>
                            <td align="left" style="font-weight: bold; font-size: 14px;">Fare Information
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblFareInformation" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    &nbsp;
                </div>
                <div>
                    <div id="ddtypediv" runat="server">
                        <div>
                            <b>Status:</b><asp:DropDownList ID="typedd" runat="server" CssClass="combobox">
                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                <asp:ListItem Value="Confirm" Text="Confirm"></asp:ListItem>
                            </asp:DropDownList>
                            <br />

                        </div>
                        <asp:Button ID="Status" runat="server" OnClientClick="return validateptk();" Text="Booking on Hold" Style="width: 150px; height: 50px; padding: 4px; margin-left: 75px;" formnovalidate=""/>
                        <br />
                        <br />
                        <span id="MessageHold" runat="server"><b>Note:</b><br />
                            1.Change booking status Request to Hold(Booking show on Hold Request)
                     <br />
                            2.Balance deduct from agent A/C, in case Balance not deducted.
                     <br />

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <%--  <script src="Script/jquery-1.3.2.min.js" type="text/javascript"></script>--%>

   <script type="text/javascript">

        function ButtonShow() {
            debugger;
            //alert('ffhh');
            var diva =document.getElementsByClassName("EditMode");
          
            var div1 =  document.getElementsByClassName("HideMode");
            var Update = document.getElementsByClassName("UpdatePnr");
            var Reject = document.getElementsByClassName("RejectPnr");

            for(var i = 0; i < div1.length; i++){
                //if(div1[i].id.indexOf("GdsPnr")>0 || div1[i].id.indexOf("txtTicketNo")>0)
              //  {
                    //{div1[i].}
                 diva[i].style.display = "block";
                 div1[i].style.display = "none";
                // Update.style.display = "block";
               // document.getElementsByTagName("INPUT")[i].setAttribute("type", "text"); 
                   // div1.find('input[type="text"]').show().focus();
               // }
            }
            $(".hideCancle").text("Cancle")
            document.getElementsByClassName("hideCancle")[0].setAttribute("onclick", "ButtonHide()"); 
            
            document.getElementById('ctl00_ContentPlaceHolder1_div_update').style.display = "block";
            //document.getElementById("ctl00_ContentPlaceHolder1_divbtn_update").style.display = "block";
            //document.getElementById("ctl00_ContentPlaceHolder1_Btn_reject").style.display="block";
            //document.getElementById("ctl00_ContentPlaceHolder1_GvFlightHeader_ctl02_ddlStatus").style.display="block";
           
        }
       
        //$(".hideCancle").text("Cancle")
        //document.getElementsByClassName("hideCancle")[0].setAttribute("onclick", "ButtonHide()"); 
        //document.getElementById("div_update").style.display="block";

        //}
        function ButtonHide() {
            debugger;
            var div2 =document.getElementsByClassName("EditMode");
          
            var div3 =  document.getElementsByClassName("HideMode");
           
            for(var i = 0; i < div2.length; i++){
                div3[i].style.display = "block";
                div2[i].style.display = "none";
                
            }
            $(".hideCancle").text("Edit")
            document.getElementsByClassName("hideCancle")[0].setAttribute("onclick", "ButtonShow()"); 
            //document.getElementById("ctl00_ContentPlaceHolder1_divbtn_update").style.display = "none";
            //document.getElementById("ctl00_ContentPlaceHolder1_Btn_reject").style.display = "none";
            document.getElementById("ctl00_ContentPlaceHolder1_div_update").style.display = "none";
            
          
            //$(".updatecancle").text("Update_PNR")
            //document.getElementsByClassName("UpdatePnr1")[0].setAttribute("onclick", "ButtonShow()"); 
            //document.getElementById("div_update").style.display="none";
          
            ////}
            //else {
            //    div.style.display = "block";
            //}
        }
    </script>
<%--    <script>
        function myFunction() {
            document.getElementsByTagName("INPUT")[0].setAttribute("type", "button"); 
        }
</script>--%>

   <%-- <script>
        $(document).ready(function(){
            $(".btn1").click(function(){
                $("p").hide();
            });
            $(".btn2").click(function(){
                $("p").show();
            });
        });
</script>--%>
</asp:Content>




