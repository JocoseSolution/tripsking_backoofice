﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_HoldBookingSetting : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    public string msgout = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        //if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        //{
        //    Response.Redirect("~/Login.aspx");
        //}

        if (!string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            try
            {
                if (!IsPostBack)
                {
                    ddl_ptype.AppendDataBoundItems = true;
                    ddl_ptype.Items.Clear();
                    //Dim item As New ListItem("All Type", "0")
                    //ddl_ptype.Items.Insert(0, item)
                    ddl_ptype.DataSource = STDom.GetAllGroupType().Tables[0];
                    ddl_ptype.DataTextField = "GroupType";
                    ddl_ptype.DataValueField = "GroupType";
                    ddl_ptype.DataBind();
                    ddl_ptype.Items.Insert(0, new ListItem("-- Select Type --", "ALL"));
                    BindGrid();
                }


            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }
    public DataTable GetAirline()
    {
        DataTable dt = new DataTable();
        try
        {
            adap = new SqlDataAdapter("SP_GetAirlinenames", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }

    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            #region Insert
            string GroupType = ddl_ptype.SelectedValue;
            string AgentId = Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string Airline = Convert.ToString(Request["hidtxtAirline"]);
            string FlightName = Convert.ToString(Request["txtAirline"]);
            string AirCode = "";
            string AirlineName = "";

            if (!string.IsNullOrEmpty(FlightName))
            {
                if (!string.IsNullOrEmpty(Airline))
                {
                    AirlineName = Airline.Split(',')[0];
                    if (Airline.Split(',').Length > 1)
                    {
                        AirCode = Airline.Split(',')[1];
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select again airline!!');", true);
                        return;
                    }
                }
            }
            else
            {
                AirlineName = "ALL";
                AirCode = "ALL";
            }
            //Convert.ToString(Request["hidtxtAirline"]).Split(',').Length  2            
            //string FltNo = Convert.ToString(TxtFlightNo.Text);
            msgout = "";
            string TripType = Convert.ToString(DdlTripType.SelectedValue);
            string TripTypeName = Convert.ToString(DdlTripType.SelectedItem.Text);           
            string IsActive = Convert.ToString(DdlStatus.SelectedValue);
            int flag = Insert(GroupType, AgentId, AirCode, AirlineName, TripType, TripTypeName, IsActive);
            if (flag > 0)
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');window.location='DealCodeMaster.aspx'; ", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.');", true);
                BindGrid();
            }
            else
            {
                if (msgout == "EXISTS")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('already exists.');", true);
                    BindGrid();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');window.location='HoldBookingSetting.aspx'; ", true);
                }        
            }
            #endregion

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='HoldBookingSetting.aspx'; ", true);
            return;
        }

    }
    private int Insert(string GroupType, string AgentId, string AirCode, string AirlineName, string TripType, string TripTypeName, string IsActive)
    {
        int flag = 0;
        string CreatedBy = Convert.ToString(Session["UID"]);
        string ActionType = "insert";
        try
        {
            SqlCommand cmd = new SqlCommand("SpFlightHoldBookingSetting", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@GroupType", GroupType);
            cmd.Parameters.AddWithValue("@AgentId", AgentId);
            cmd.Parameters.AddWithValue("@AirCode", AirCode); 
            cmd.Parameters.AddWithValue("@AirlineName", AirlineName);            
            cmd.Parameters.AddWithValue("@TripType", TripType);
            cmd.Parameters.AddWithValue("@TripTypeName", TripTypeName);           
            cmd.Parameters.AddWithValue("@IsActive", Convert.ToBoolean(IsActive));
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@ActionType", ActionType);
            cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
            if (con.State == ConnectionState.Closed)
                con.Open();
            flag = cmd.ExecuteNonQuery();
            con.Close();
            msgout = cmd.Parameters["@Msg"].Value.ToString();
        }
        catch (Exception ex)
        {
            con.Close();
            clsErrorLog.LogInfo(ex);
        }
        return flag;

    }
    public void BindGrid()
    {
        try
        {
            grd_P_IntlDiscount.DataSource = GetRecord();
            grd_P_IntlDiscount.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }
    public DataTable GetRecord()
    {
        DataTable dt = new DataTable();
        try
        {
            string AgentId = "";// Request["hidtxtAgencyName"] == "Agency Name or ID" ? "" : Request["hidtxtAgencyName"].Trim();
            string Airline = Convert.ToString(Request["hidtxtAirline"]);
            string AirCode = "";
            string AirlineName = "";
            if (!string.IsNullOrEmpty(Airline))
            {
                AirlineName = Airline.Split(',')[0];
                if (Airline.Split(',').Length > 1)
                {
                    AirCode = Airline.Split(',')[1];
                }
            }
            string TripType = Convert.ToString(DdlTripType.SelectedValue);
            adap = new SqlDataAdapter("SpFlightHoldBookingSetting", con);
            adap.SelectCommand.CommandType = CommandType.StoredProcedure;
            adap.SelectCommand.Parameters.AddWithValue("@GroupType", ddl_ptype.SelectedValue);
            adap.SelectCommand.Parameters.AddWithValue("@AgentId", AgentId);
            adap.SelectCommand.Parameters.AddWithValue("@AirCode", AirCode);
            adap.SelectCommand.Parameters.AddWithValue("@TripType", TripType);
            adap.SelectCommand.Parameters.AddWithValue("@ActionType", "select");
            adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
            adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
            adap.Fill(dt);
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
        finally
        {
            adap.Dispose();
        }
        return dt;
    }


    protected void grd_P_IntlDiscount_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            grd_P_IntlDiscount.EditIndex = e.NewEditIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }

    protected void grd_P_IntlDiscount_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            grd_P_IntlDiscount.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }

    protected void grd_P_IntlDiscount_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int result = 0;
            Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
            int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());

            DropDownList ddl_CType = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_CodeType");
            string CodeType = ddl_CType.SelectedValue;

            TextBox txt_DT_Code = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txt_D_T_Code"));
            string D_T_Code = Convert.ToString(txt_DT_Code.Text);
            if (string.IsNullOrEmpty(D_T_Code))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter Deal/Tour Code!');", true);
                return;
            }
            DropDownList ddl_Applied_On = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_AppliedOn");
            string AppliedOn = ddl_Applied_On.SelectedValue;

            if ((CodeType == "PC" || CodeType == "PF") && AppliedOn == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select  Applied On!');", true);
                return;
            }

            if (CodeType == "PC" || CodeType == "PF")
            {
                AppliedOn = ddl_Applied_On.SelectedValue;
            }
            else
            {
                AppliedOn = "";
            }
            DropDownList ddl_FareType = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_FareType");
            string IdType = ddl_FareType.SelectedValue;

            if (CodeType == "PC" && IdType == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select  fare type!');", true);
                return;
            }

            if (CodeType == "PC")
            {
                IdType = ddl_FareType.SelectedValue;
            }
            else
            {
                IdType = "";
            }


            DropDownList ddl_Active = (DropDownList)grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("ddl_IsActive");
            //string Active = ddl_Active.SelectedValue;
            string IsActive = ddl_Active.SelectedValue;//"true";
            //string IsActive = "true";
            //if (Active == "0")
            //{
            //    IsActive = "false";
            //}

            //string FltNo,string OrginAirport,string DestAirport,string OrginCountry,string DestCountry
            TextBox Txt_FltNo = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdFltNo"));
            string FltNo = Convert.ToString(Txt_FltNo.Text);

            TextBox TxtGrdOrginCity = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdOrginAirport"));
            string OrginAirport = Convert.ToString(TxtGrdOrginCity.Text);

            TextBox TxtGrdDestAirport = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdDestAirport"));
            string DestAirport = Convert.ToString(TxtGrdDestAirport.Text);

            TextBox TxtGrdOrginCountry = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdOrginCountry"));
            string OrginCountry = Convert.ToString(TxtGrdOrginCountry.Text);

            TextBox TxtGrdDestCountry = (TextBox)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("txtGrdDestCountry"));
            string DestCountry = Convert.ToString(TxtGrdDestCountry.Text);


           // result = UpdateRecords(Id, CodeType, D_T_Code, AppliedOn, IsActive, FltNo, OrginAirport, DestAirport, OrginCountry, DestCountry, IdType);
            grd_P_IntlDiscount.EditIndex = -1;
            BindGrid();

            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully updated.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('try again.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('" + ex.Message + "');", true);
        }
    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int flag = 0;
            try
            {
                Label lblSNo = (Label)(grd_P_IntlDiscount.Rows[e.RowIndex].FindControl("lblId"));
                int Id = Convert.ToInt16(lblSNo.Text.Trim().ToString());
                SqlCommand cmd = new SqlCommand("SpFlightHoldBookingSetting", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", Id);
                cmd.Parameters.AddWithValue("@ActionType", "delete");
                cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 30);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                flag = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (SqlException ex)
            {
                con.Close();
                clsErrorLog.LogInfo(ex);
            }
            BindGrid();
            if (flag > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }
    }


    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_P_IntlDiscount.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }

   
}