﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="User_Control.aspx.cs" Inherits="User_Control" %>


<%--<%@ Register Src="~/DetailsPort/PrivilegePanel/Splashboard.aspx" TagPrefix="uc1" TagName="DashBoard" %>--%>

<%@ Register Src="~/Reports/Set_Credentials/PNRCreationCrd.ascx" TagPrefix="Search" TagName="PNRCreationCrd" %>
<%@ Register Src="~/Reports/Set_Credentials/TicketingCrd.ascx" TagPrefix="Search1" TagName="TicketingCrd" %>
<%@ Register Src="~/Reports/Set_Credentials/ServiceCredentials.ascx" TagPrefix="Search2" TagName="ServiceCredentials" %>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <style>
        .panel panel-primary{
            background-color: rgba(255, 255, 255, 0.13);
        }
    </style>

    
    <div class="wrapper">
 
                <div class="container">
                    <div class="row" style="margin-bottom: 0px;">
                        <div style="clear: both;"></div>
                        <div class="col-xs-12 col-sm-12" style="padding-right: 0px;">
                            <ul class="nav nav-tabs tabstab">
                                <li class="active "><a href="#home" data-toggle="tab">
                                    <span class="fa fa-book"></span>
                                    Book Credential
                                </a></li>
                                <li class=" "><a href="#profile" data-toggle="tab">
                                    <span class="fa fa-plane" aria-hidden="true"></span>
                                    Ticketing Credential
                                </a></li>
                                <li class=" "><a href="#bus" data-toggle="tab">
                                    <span class="fa fa-search" aria-hidden="true"></span>
                                    Search Credential
                                </a></li>
                                <%--<li class=" "><a href="#bussss" data-toggle="tab">
                                    <span class="  sprte" aria-hidden="true"></span>
                                    <span class="hidden-xs"> Current</span> Booking
                                </a></li>--%>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="secndblak">
                    <div class="container"  >
                        <div class="col-md-12">
                            <!-- Tab panes -->
                            <div class="tab-content tabsbg" style="overflow:hidden">
                            
                                <div class="tab-pane active" id="home">
                                    <Search:PNRCreationCrd ID="PNRCreationCrd" runat="server" />
                                </div>
                                <div class="tab-pane" id="profile">
                                    <Search1:TicketingCrd runat="server" ID="TicketingCrd" />
                                </div>
                                <div class="tab-pane" id="bus">
                                    <Search2:ServiceCredentials runat="server" ID="ServiceCredentials" />
                                </div>
                                <div class="tab-pane" id="bussss">
                                <%--    <uc1:DashBoard runat="server" ID="DashBoard" />--%>
                                </div>
                                <%--<uc1:HotelSearch runat="server" ID="HotelSearch" />--%>
                            </div>
                            <!-- Tab panes -->
                        </div>
                    </div>
                </div>
            </div>
   
     

</asp:Content>

