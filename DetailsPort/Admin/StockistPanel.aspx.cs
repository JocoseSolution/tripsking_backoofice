﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;

public partial class DetailsPort_Admin_StockiestPannel : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)  
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Profile</a><a class='current' href='#'>Change Merchandiser</a>";
        if (!IsPostBack)
        {
           
            ChangeAgentStatus.Visible = false;            
            Div_ToStockist.Visible = false;
            Bindgrid();

        }
        else
        {   
                div_gridview.Visible = true;
                txtStockistName.Attributes.Add("readonly", "true");
        }
       
    }
 
    protected void BtnSearchStockist_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(txtStockistName.Value) || string.IsNullOrEmpty(hidtxtStockistName.Value))
            {
                string message = "Please Enter Valid AgenyName/Id or UserID";
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script type = 'text/javascript'>");
                sb.Append("window.onload=function(){");
                sb.Append("alert('");
                sb.Append(message);
                sb.Append("')};");
                sb.Append("</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
            }
            else
            {

                Bindgrid();
            }
        }
        catch(Exception ex)
        {

        }
        
    }
    public void Bindgrid()  
       {
        try
        {   
            AgencySearch obj = new AgencySearch();
            DataSet ds = new DataSet();
            if (txtStockistName.Value.LastIndexOf('(') != -1)
            {
               // ds = obj.FetchStokistds("", txtStockistName.Value.Substring(0, txtStockistName.Value.LastIndexOf('(')));
                ds = obj.FetchStokistds("", txtStockistName.Value.Split('(')[1].Split(')')[0]);
                if (ds.Tables.Count!=0)
                {
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        gv1.DataSource = ds;
                        gv1.DataBind();
                        ChangeAgentStatus.Visible = true;
                        Div_ToStockist.Visible = true;
                        BtnSearchStockist.Visible = false;

                    }
                    else
                    {
                        string message = "Record Not Found On this AgencyName/Id or UserID";
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.Append("<script type = 'text/javascript'>");
                        sb.Append("window.onload=function(){");
                        sb.Append("alert('");
                        sb.Append(message);
                        sb.Append("')};");
                        sb.Append("</script>");
                        ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                       
                        if (IsPostBack == true)
                        {
                            div_gridview.Visible = true;
                           
                            //Clear();
                           
                        }
                        else
                        {
                            div_gridview.Visible = false;
                        //    Clear();
                        }
                        
                       
                     
                    }
                }
              
            }
        }

        catch(Exception ex)
        {
            throw;
        }

    }
    protected void ChangeAgentStatus_Click(object sender, EventArgs e)
    {
        try
        {
            if (string.IsNullOrEmpty(TotxtStockistName.Value))
            {
                string message = "Must Be filled *Change To*";
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script type = 'text/javascript'>");
                sb.Append("window.onload=function(){");
                sb.Append("alert('");
                sb.Append(message);
                sb.Append("')};");
                sb.Append("</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
               

            }

            else if (txtStockistName.Value == TotxtStockistName.Value)
            {
                string message = "You Entered Same Distributor Which is not allowed";
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<script type = 'text/javascript'>");
                sb.Append("window.onload=function(){");
                sb.Append("alert('");
                sb.Append(message);
                sb.Append("')};");
                sb.Append("</script>");
                ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());

                          
            }
            string confirmValue = Request.Form["confirm_value"];
             if (confirmValue == "No")
            {
                foreach (GridViewRow gvrow in gv1.Rows)
                {
                    var checkbox1 = gvrow.FindControl("CheckBox1") as CheckBox;
                    if (checkbox1.Checked == true)
                    {
                        checkbox1.Checked = false;

                    }

                   

                }

                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You Cancelled Request!')", true);
                
                Clear();
                div_gridview.Visible = false;
                ChangeAgentStatus.Visible = false;
                Div_ToStockist.Visible = false;
                Div_BtnSearch.Visible = true;
                txtStockistName.Value = "";
                txtStockistName.Attributes.Remove("readonly");
                
            }


            else
            {
              int i = 0; int j = 0; 
              foreach (GridViewRow gvrow in gv1.Rows)
              {
                  var checkbox1 = gvrow.FindControl("CheckBox1") as CheckBox;
                  var UseriD = gvrow.FindControl("Label2_User_Id") as Label;
                  var Agent_Type = gvrow.FindControl("Label_Agent_Type") as Label;

                  if (checkbox1.Checked == true)
                  {
                      if (!string.IsNullOrEmpty(TotxtStockistName.Value) && !string.IsNullOrEmpty(txtStockistName.Value))
                      {   
                          if(txtStockistName.Value != TotxtStockistName.Value)
                          {
                              SqlCommand cmd = new SqlCommand("ChangeStockList_DI", con);
                              cmd.CommandType = CommandType.StoredProcedure;
                              cmd.Parameters.AddWithValue("@UserId", UseriD.Text);
                              cmd.Parameters.AddWithValue("@UpdateBy", Session["UID"].ToString());
                              cmd.Parameters.AddWithValue("@Agent_Type", Agent_Type.Text);
                              cmd.Parameters.AddWithValue("@FromDistr", txtStockistName.Value.Split('(')[1].Split(')')[0]);
                              cmd.Parameters.AddWithValue("@ToDistr", TotxtStockistName.Value.Split('(')[1].Split(')')[0]);
                              cmd.Parameters.AddWithValue("@Case", "Update");
                              con.Open();
                              i += cmd.ExecuteNonQuery();
                              con.Close();
                              j++;
                          }
                          
                      }
                     

                  }
                 
              }
                
              if (j == 0)
              {
                  string message = "Please Checked the below CheckBox";
                  System.Text.StringBuilder sb = new System.Text.StringBuilder();
                  sb.Append("<script type = 'text/javascript'>");
                  sb.Append("window.onload=function(){");
                  sb.Append("alert('");
                  sb.Append(message);
                  sb.Append("')};");
                  sb.Append("</script>");
                  ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());
                 
              }
              if (i >= 1)
              {   
                  if(txtStockistName.Value != TotxtStockistName.Value)
                  {
                      string confirmValue1 = Request.Form["confirm_value"];
                      if (confirmValue1 == "Yes")
                      {
                          this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Transfer Succesfully!')", true);
                         
                          Bindgrid();
                  

                  }
                    
                  }
                  
                 
              }
             
                
            }
           
        }
        catch (Exception ex)
        {
            throw;

        }
        
    }
    public void Clear()
    {


        int s = 0;
        foreach (GridViewRow gvrow in gv1.Rows)
        {
            var checkbox1 = gvrow.FindControl("CheckBox1") as CheckBox;
          
            if (checkbox1.Checked==true)
            {
                checkbox1.Checked = false;
            }
            s++;
        }
        if (txtStockistName.Value != null && s != 0)
        {
            txtStockistName.Value = "";
        }
        if (TotxtStockistName.Value != null)
        {
            TotxtStockistName.Value = "";
        }
        ChangeAgentStatus.Visible = false;
        Div_ToStockist.Visible = false;
        BtnSearchStockist.Visible = true;
    }
}

