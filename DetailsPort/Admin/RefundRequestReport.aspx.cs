﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class DetailsPort_Admin_RefundRequestReport : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
   
    DataSet DSHold = new DataSet();
    DataSet DSCancel = new DataSet();
    private SqlTransaction ST = new SqlTransaction();
    private SqlTransactionDom STDom = new SqlTransactionDom();
    double Aval_Bal = 0;
    string RejectionMsg = "", RejectionRemarks = "";
    string UserId = "", UserType = "", RequestType = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == null || Session["UID"].ToString() == "")
        {
            Response.Redirect("~/Login.aspx");
        }
        UserId = Session["UID"].ToString();
        UserType = Session["User_Type"].ToString();
        if (IsPostBack != true)
        {
            lbl_Norecord.Text = "";
           
                DSHold = HOLDPNRBIND("", "");
                DSCancel = CANCELLATIONBIND("", "");
                RequestType = ddl_RequestType.SelectedValue.ToString();
                PagelodBind(DSHold, DSCancel, RequestType);
          
        }
    }
    protected void btn_result_Click(object sender, EventArgs e)
    {
        try
        {
            UserId = Session["UID"].ToString();
            UserType = Session["User_Type"].ToString();
            string Fromdate = "", Todate = "", str_PNR = "", str_OrderId = "", ReqType = "", str_TktNo = "";
            Fromdate = Request["Form"];
            Todate = Request["To"];
            str_PNR = txt_PNR.Text.Trim().ToString();
            str_OrderId = txt_OrderId.Text.Trim().ToString();
            ReqType = ddl_RequestType.SelectedValue.ToString();
            str_TktNo = txt_TktNo.Text.Trim().ToString();

            DataSet DSSearch = new DataSet();
            DSSearch = SeachBind(Fromdate, Todate, str_PNR, str_OrderId, ReqType, str_TktNo, UserId, UserType, "");
            if (DSSearch.Tables[0].Rows.Count > 0 && ReqType == "H")
            {
                td_holdpnr.Visible = true;
                td_cancellation.Visible = false;
                GridHoldPNRAccept.DataSource = DSSearch.Tables[0];
                GridHoldPNRAccept.DataBind();
            }
            else if (DSSearch.Tables[0].Rows.Count <= 0 && ReqType == "H")
            {
                td_holdpnr.Visible = true;
                td_cancellation.Visible = false;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('No record found!!');", true);
            }
            else if (DSSearch.Tables[0].Rows.Count > 0 && ReqType == "C")
            {
                td_cancellation.Visible = true;
                td_holdpnr.Visible = false;
                InProccess_grdview.DataSource = DSSearch.Tables[0];
                InProccess_grdview.DataBind();
            }
            else if (DSSearch.Tables[0].Rows.Count <= 0 && ReqType == "C")
            {
                td_cancellation.Visible = true;
                td_holdpnr.Visible = false;
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('No record found!!');", true);
            }
       
        }
        catch (Exception ex)
        {
            
            Response.Write(ex.Message+"Stack trace"+ex.StackTrace);
        }
        ClearInputs(Page.Controls);
        
    }

    //protected void GridHoldPNRAccept_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "AcceptHold")
    //    {
    //        LinkButton lb1 = e.CommandSource as LinkButton;
    //        GridViewRow gvr1 = lb1.Parent.Parent as GridViewRow;
    //        int RowIndex = gvr1.RowIndex;
    //        ViewState["RowIndex"] = RowIndex;
    //        TextBox GridHoldPNRAccept_Remark = (TextBox)GridHoldPNRAccept.Rows[RowIndex].FindControl("txtRemark");
    //        LinkButton GridHoldPNRAccept_lnkSubmit = (LinkButton)GridHoldPNRAccept.Rows[RowIndex].FindControl("lnkSubmit");
    //        LinkButton GridHoldPNRAccept_lnkHides = (LinkButton)GridHoldPNRAccept.Rows[RowIndex].FindControl("lnkHides");
    //        LinkButton GridHoldPNRAccept_ITZ_Accept = (LinkButton)GridHoldPNRAccept.Rows[RowIndex].FindControl("ITZ_Accept");
    //        GridHoldPNRAccept_Remark.Visible = true;
    //        GridHoldPNRAccept_lnkSubmit.Visible = true;
    //        GridHoldPNRAccept_lnkHides.Visible = true;
    //        GridHoldPNRAccept_ITZ_Accept.Visible = false;
    //    }
    //    if (e.CommandName == "UpdateHold")
    //    {
    //        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
    //        Label lbl_OrderId = (Label)row.FindControl("lbl_OrderId");
    //        TextBox txtRemark = (TextBox)row.FindControl("txtRemark");
    //        if(txtRemark.Text == "")
    //        {
    //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('Remark Cannot be blank!!');", true);
    //        }
    //        else
    //        {
              
    //                DSHold = HOLDPNRBIND("", "");
    //                DSCancel = CANCELLATIONBIND("", "");
    //                RequestType = ddl_RequestType.SelectedValue.ToString();
    //                PagelodBind(DSHold, DSCancel, RequestType);
    //                UpdateHoldPNR(lbl_OrderId.Text.ToString(), UserId, txtRemark.Text.ToString());
    //                //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('Refunded Successfully!!');", true);

    //                ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Ticket Refunded Successfully!!.');javascript:window.close();window.opener.location.href = window.opener.location.href;", true);
    //                Response.Redirect("RefundRequestReport.aspx");
    //            //Response.Write("<script language='javascript'>alert('Refunded Successfully!!.');window.location.reload(false);</script>");
    //            }
              
            
    //    }
    //    if (e.CommandName == "RejectHold")
    //    {
           
    //            DSHold = HOLDPNRBIND("", "");
    //            DSCancel = CANCELLATIONBIND("", "");
    //            RequestType = ddl_RequestType.SelectedValue.ToString();
    //            PagelodBind(DSHold, DSCancel, RequestType);
           
    //    }
    //}
    //protected void InProccess_grdview_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "AcceptCancel")
    //    {
    //        try
    //        {
    //            LinkButton lb = e.CommandSource as LinkButton;
    //            GridViewRow gvr = lb.Parent.Parent as GridViewRow;
    //            int RowIndex = gvr.RowIndex;
    //            ViewState["RowIndex"] = RowIndex;
    //            TextBox InProccess_txt_Remark = (TextBox)InProccess_grdview.Rows[RowIndex].FindControl("txt_Remark");
    //            LinkButton InProccess_lnkSubmit = (LinkButton)InProccess_grdview.Rows[RowIndex].FindControl("lnkSubmit");
    //            LinkButton InProccess_lnkHides = (LinkButton)InProccess_grdview.Rows[RowIndex].FindControl("lnkHides");
    //            LinkButton InProccess_lnk_updatestatus = (LinkButton)InProccess_grdview.Rows[RowIndex].FindControl("lnk_updatestatus");
    //            InProccess_txt_Remark.Visible = true;
    //            InProccess_lnkSubmit.Visible = true;
    //            InProccess_lnkHides.Visible = true;
    //            InProccess_lnk_updatestatus.Visible = false;
    //            gvr.BackColor = System.Drawing.Color.Yellow;
    //        }
    //        catch (Exception ex)
    //        {
    //            clsErrorLog.LogInfo(ex);
    //        }
    //    }
    //    if (e.CommandName == "UpdateCancel")
    //    {
    //        GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
    //        Label OrderId = (Label)row.FindControl("OrderID");
    //        TextBox GVRemark = (TextBox)row.FindControl("txt_Remark");
    //        Label GdsPNR = (Label)row.FindControl("GdsPNR");
    //        Label lbl_Counter = (Label)row.FindControl("TID");
    //        Label PaxID = (Label)row.FindControl("lbl_Paxid");
    //        if (GVRemark.Text == "")
    //        {
    //            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('Remark Cannot be blank!!');", true);
    //        }
    //        else
    //        {
              
    //                UpdateCancelledPNR(GVRemark.Text.ToString(), UserId, OrderId.Text.Trim().ToString(), GdsPNR.Text.Trim().ToString(), Convert.ToInt32(lbl_Counter.Text.ToString()), Convert.ToInt32(PaxID.Text.ToString()));
    //                DSHold = HOLDPNRBIND("", "");
    //                DSCancel = CANCELLATIONBIND("", "");
    //                RequestType = ddl_RequestType.SelectedValue.ToString();
    //                PagelodBind(DSHold, DSCancel, RequestType);
    //                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('Refunded Successfully!!');", true);
    //                // Response.Write("<script language='javascript'>alert('Refunded Successfully!!.');window.location.reload();</script>");
               
    //        }         
    //    }
    //    if (e.CommandName == "RejectCancel")
    //    {
           
    //            DSHold = HOLDPNRBIND("", "");
    //            DSCancel = CANCELLATIONBIND("", "");
    //            RequestType = ddl_RequestType.SelectedValue.ToString();
    //            PagelodBind(DSHold, DSCancel, RequestType);
            
    //    }
    //}
    protected void GridHoldPNRAccept_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "AcceptHold")
        {
            LinkButton lb1 = e.CommandSource as LinkButton;
            GridViewRow gvr1 = lb1.Parent.Parent as GridViewRow;
            int RowIndex = gvr1.RowIndex;
            ViewState["RowIndex"] = RowIndex;
            TextBox GridHoldPNRAccept_Remark = (TextBox)GridHoldPNRAccept.Rows[RowIndex].FindControl("txtRemark");
            LinkButton GridHoldPNRAccept_lnkSubmit = (LinkButton)GridHoldPNRAccept.Rows[RowIndex].FindControl("lnkSubmit");
            LinkButton GridHoldPNRAccept_lnkHides = (LinkButton)GridHoldPNRAccept.Rows[RowIndex].FindControl("lnkHides");
            LinkButton GridHoldPNRAccept_ITZ_Accept = (LinkButton)GridHoldPNRAccept.Rows[RowIndex].FindControl("ITZ_Accept");
            GridHoldPNRAccept_Remark.Visible = true;
            GridHoldPNRAccept_lnkSubmit.Visible = true;
            GridHoldPNRAccept_lnkHides.Visible = true;
            GridHoldPNRAccept_ITZ_Accept.Visible = false;
        }
        if (e.CommandName == "UpdateHold")
        {
            DataSet ds = new DataSet();
            DataSet PGds = new DataSet();
            DataTable dtID = new DataTable();
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lbl_OrderId = (Label)row.FindControl("lbl_OrderId");
            TextBox txtRemark = (TextBox)row.FindControl("txtRemark");
            Label lbl_Trip = (Label)row.FindControl("lbl_Trip");
            ds = ST.GetFltHeaderDetail(lbl_OrderId.Text.Trim().ToString());
            dtID = ds.Tables[0];
            PGds = PgAmount(lbl_OrderId.Text.Trim().ToString());
            //if (PGds.Tables[0].Rows.Count > 0)
            //{
            //    Aval_Bal = ST.AddCrdLimit(dtID.Rows[0]["AgentId"].ToString(), Convert.ToDouble(PGds.Tables[0].Rows[0]["Amount"].ToString()));
            //}
            //else
            //{
            //    Aval_Bal = ST.AddCrdLimit(dtID.Rows[0]["AgentId"].ToString(), Convert.ToDouble(PGds.Tables[0].Rows[0]["TotalAfterDis"].ToString()));
            //}

            //Aval_Bal = ST.AddCrdLimit(dtID.Rows[0]["AgentId"].ToString(), Convert.ToDouble(PGds.Tables[0].Rows[0]["Amount"].ToString()));
            if (lbl_OrderId.Text.Trim().ToString() == "")
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('Remark can not be blank,Please Fill Remark');", true);
            }
            else
            {
                if (lbl_Trip.Text == "I")
                {
                    RejectionMsg = "Intl Rejection";
                    RejectionRemarks = "Intl PNR Rejected Against OrderID=" + lbl_OrderId.Text.Trim().ToString();
                }
                else
                {
                    RejectionMsg = "Dom. Rejection";
                    RejectionRemarks = "Dom. PNR Rejected Against OrderID=" + lbl_OrderId.Text.Trim().ToString();
                }
                DSHold = HOLDPNRBIND("", "");
                DSCancel = CANCELLATIONBIND("", "");
                RequestType = ddl_RequestType.SelectedValue.ToString();
                PagelodBind(DSHold, DSCancel, RequestType);
                UpdateHoldPNR(lbl_OrderId.Text.ToString(), UserId, txtRemark.Text.ToString());
                STDom.insertLedgerDetails(dtID.Rows[0]["AgentId"].ToString(), dtID.Rows[0]["AgencyName"].ToString(), lbl_OrderId.Text.Trim().ToString(), dtID.Rows[0]["GdsPnr"].ToString(), "", "", "", "", Session["UID"].ToString(), Request.UserHostAddress, 0,
                   Convert.ToDouble(PGds.Tables[0].Rows[0]["Amount"].ToString()), 0, RejectionMsg, RejectionRemarks, 0,0);
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('Refunded Successfully!!');", true);
               // ClientScript.RegisterStartupScript(Page.GetType(), "Alert", "alert('Refunded Successfully!!.');javascript: window.close();window.opener.location=window.opener.location.href;", true);
            }
        }
        if (e.CommandName == "RejectHold")
        {
            DSHold = HOLDPNRBIND("", "");
            DSCancel = CANCELLATIONBIND("", "");
            RequestType = ddl_RequestType.SelectedValue.ToString();
            PagelodBind(DSHold, DSCancel, RequestType);
        }
    }
    protected void InProccess_grdview_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "AcceptCancel")
        {
            try
            {
                LinkButton lb = e.CommandSource as LinkButton;
                GridViewRow gvr = lb.Parent.Parent as GridViewRow;
                int RowIndex = gvr.RowIndex;
                ViewState["RowIndex"] = RowIndex;
                TextBox InProccess_txt_Remark = (TextBox)InProccess_grdview.Rows[RowIndex].FindControl("txt_Remark");
                LinkButton InProccess_lnkSubmit = (LinkButton)InProccess_grdview.Rows[RowIndex].FindControl("lnkSubmit");
                LinkButton InProccess_lnkHides = (LinkButton)InProccess_grdview.Rows[RowIndex].FindControl("lnkHides");
                LinkButton InProccess_lnk_updatestatus = (LinkButton)InProccess_grdview.Rows[RowIndex].FindControl("lnk_updatestatus");
                InProccess_txt_Remark.Visible = true;
                InProccess_lnkSubmit.Visible = true;
                InProccess_lnkHides.Visible = true;
                InProccess_lnk_updatestatus.Visible = false;
                gvr.BackColor = System.Drawing.Color.Yellow;
            }
            catch (Exception ex)
            {
                clsErrorLog.LogInfo(ex);
            }
        }
        if (e.CommandName == "UpdateCancel")
        {
            DataSet ds = new DataSet();
            DataSet PGds = new DataSet();
            DataTable dtID = new DataTable();
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label OrderId = (Label)row.FindControl("OrderID");
            TextBox GVRemark = (TextBox)row.FindControl("txt_Remark");
            Label GdsPNR = (Label)row.FindControl("GdsPNR");
            Label lbl_Counter = (Label)row.FindControl("TID");
            Label PaxID = (Label)row.FindControl("lbl_Paxid");
            Label lbl_tktno = (Label)row.FindControl("lbl_tktno");
            RejectionRemarks = "ReFund Charge Against Ticket No " + lbl_tktno.Text.Trim().ToString();
            ds = ST.GetFltHeaderDetail(OrderId.Text.Trim().ToString());
            dtID = ds.Tables[0];
            PGds = PgAmount(OrderId.Text.Trim().ToString());
            //if (PGds.Tables[0].Rows.Count>0)
            //{
            //    Aval_Bal = ST.AddCrdLimit(dtID.Rows[0]["AgentId"].ToString(), Convert.ToDouble(PGds.Tables[0].Rows[0]["Amount"].ToString()));
            //}
            //else
            //{
            //    Aval_Bal = ST.AddCrdLimit(dtID.Rows[0]["AgentId"].ToString(), Convert.ToDouble(PGds.Tables[0].Rows[0]["TotalAfterDis"].ToString()));
            //}
          
            if (GVRemark.Text.Trim().ToString() == "")
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('Remark can not be blank,Please Fill Remark');", true);
            }
            else
            {
                UpdateCancelledPNR(GVRemark.Text.ToString(), UserId, OrderId.Text.Trim().ToString(), GdsPNR.Text.Trim().ToString(), Convert.ToInt32(lbl_Counter.Text.ToString()), Convert.ToInt32(PaxID.Text.ToString()));
                STDom.insertLedgerDetails(dtID.Rows[0]["AgentId"].ToString(), dtID.Rows[0]["AgencyName"].ToString(), OrderId.Text.Trim().ToString(), GdsPNR.Text.Trim().ToString(), lbl_tktno.Text.Trim().ToString(), "", "", "", Session["UID"].ToString(), Request.UserHostAddress, 0, Convert.ToDouble(PGds.Tables[0].Rows[0]["Amount"].ToString()), 0
                    , "Ticket Refund", RejectionRemarks, 0,0);
                DSHold = HOLDPNRBIND("", "");
                DSCancel = CANCELLATIONBIND("", "");
                RequestType = ddl_RequestType.SelectedValue.ToString();
               
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('Refunded Successfully!!');", true);
                PagelodBind(DSHold, DSCancel, RequestType);
            }

        }
        if (e.CommandName == "RejectCancel")
        {
            DSHold = HOLDPNRBIND("", "");
            DSCancel = CANCELLATIONBIND("", "");
            RequestType = ddl_RequestType.SelectedValue.ToString();
            PagelodBind(DSHold, DSCancel, RequestType);
        }

    }
    protected void btn_Cancel_Click(object sender, EventArgs e)
    {
        
            DSHold = HOLDPNRBIND("", "");
            DSCancel = CANCELLATIONBIND("", "");
            if (ddl_RequestType.SelectedValue == "C")
            {
                td_cancellation.Visible = true;
            }
            else
            {
                td_holdpnr.Visible = true;
            }
            GridHoldPNRAccept.DataSource = DSHold;
            GridHoldPNRAccept.DataBind();
            InProccess_grdview.DataSource = DSHold;
            InProccess_grdview.DataBind();
      
    }
    protected void PagelodBind(DataSet HoldPnr, DataSet Cancellation, string TD)
    {
        if (TD == "C")
        {
            td_cancellation.Visible = true;
            td_holdpnr.Visible = false;
        }
        else
        {
           
            td_holdpnr.Visible = true;
            td_cancellation.Visible = false;
        }
        if (HoldPnr.Tables[0].Rows.Count > 0 && TD == "H")
        {
            GridHoldPNRAccept.DataSource = HoldPnr;
            GridHoldPNRAccept.DataBind();
        }
        else if (HoldPnr.Tables[0].Rows.Count <= 0 && TD == "H")
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('No record found!!');", true);
        }
        if (Cancellation.Tables[0].Rows.Count > 0 && TD == "C")
        {
            InProccess_grdview.DataSource = Cancellation;
            InProccess_grdview.DataBind();
        }
        else if (Cancellation.Tables[0].Rows.Count <= 0 && TD == "C")
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "err_msg", "alert('No record found!!');", true);
        }
    }
    protected void UpdateHoldPNR(string OrderID, string ExecutiveId, string RejectedRemark)
    {
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_REFUNDREQUESTED";
                sqlcmd.Parameters.Add("@ExecutiveID", SqlDbType.VarChar).Value = ExecutiveId;
                sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = "UpdatedHOLD";
                sqlcmd.Parameters.Add("@Remark", SqlDbType.VarChar).Value = RejectedRemark;
                sqlcmd.Parameters.Add("@ORDERID", SqlDbType.VarChar).Value = OrderID;
                sqlcmd.ExecuteNonQuery();
                con.Close();
            }
        }
        catch (Exception ex)
        {
            //ErrorLogTrace.WriteErrorLog(ex, "GetFltDtlsGroupBooking");
        }
    }
    protected void UpdateCancelledPNR(string Remark, string ExecutiveID, string OrderId, string GDSPNR, int Counter, int PAXID)
    {
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_REFUNDREQUESTED_PP";
                sqlcmd.Parameters.Add("@ExecutiveID", SqlDbType.VarChar).Value = ExecutiveID;
                sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = "UpdatedCancel";
                sqlcmd.Parameters.Add("@Remark", SqlDbType.VarChar).Value = Remark;
                sqlcmd.Parameters.Add("@PNRNO", SqlDbType.VarChar).Value = GDSPNR;
                sqlcmd.Parameters.Add("@counter", SqlDbType.Int).Value = Counter;
                sqlcmd.Parameters.Add("@PAXID", SqlDbType.Int).Value = PAXID;
                sqlcmd.Parameters.Add("@ORDERID", SqlDbType.VarChar).Value = OrderId;
                sqlcmd.ExecuteNonQuery();
                con.Close();
            }
        }
        catch (Exception ex)
        {
        }
    }
    public DataSet SeachBind(string From, string To, string PNR, string OrderID, string RequestTyp, string TicketNo, string UserID, string UserType, string Trip)
    {
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_SEARCHREFUNDREQUESTED";
                sqlcmd.Parameters.Add("@From", SqlDbType.VarChar, 4000).Value = From;
                sqlcmd.Parameters.Add("@To", SqlDbType.VarChar, 4000).Value = To;
                sqlcmd.Parameters.Add("@PNR", SqlDbType.VarChar, 4000).Value = PNR;
                sqlcmd.Parameters.Add("@OrderID", SqlDbType.VarChar, 4000).Value = OrderID;
                sqlcmd.Parameters.Add("@RequestTyp", SqlDbType.VarChar, 4000).Value = RequestTyp;
                sqlcmd.Parameters.Add("@TicketNo", SqlDbType.VarChar, 4000).Value = TicketNo;
                sqlcmd.Parameters.Add("@UserID", SqlDbType.VarChar, 4000).Value = UserID;
                sqlcmd.Parameters.Add("@UserType", SqlDbType.VarChar, 4000).Value = UserType;
                sqlcmd.Parameters.Add("@Trip", SqlDbType.VarChar, 4000).Value = Trip;
                sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar, 4000).Value = "Search";
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
            }
        }
        catch (Exception ex)
        {
        }
        return DS;
    }
    public DataSet ExecuType(string ExecID)
    {
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_REFUNDREQUESTED_PP";
                sqlcmd.Parameters.Add("@ExecutiveID", SqlDbType.VarChar).Value = ExecID;
                sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = "EXECU";

                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
            }
        }
        catch (Exception ex)
        {
            //ErrorLogTrace.WriteErrorLog(ex, "GetFltDtlsGroupBooking");
        }
        return DS;
    }
    public DataSet HOLDPNRBIND(string ExecuID, string Trip)
    {
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_REFUNDREQUESTED_PP";
                sqlcmd.Parameters.Add("@ExecutiveID", SqlDbType.VarChar).Value = ExecuID;
                sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = "H";
                sqlcmd.Parameters.Add("@TRIP", SqlDbType.VarChar).Value = Trip;

                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
            }
        }
        catch (Exception ex)
        {
        }
        return DS;
    }
    public DataSet CANCELLATIONBIND(string ExecuID, string Trip)
    {
        DataSet DS = new DataSet();
        try
        {
            using (SqlCommand sqlcmd = new SqlCommand())
            {
                sqlcmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                else
                {
                    con.Open();
                }
                sqlcmd.CommandTimeout = 900;
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "USP_REFUNDREQUESTED_PP";
                sqlcmd.Parameters.Add("@ExecutiveID", SqlDbType.VarChar).Value = ExecuID;
                sqlcmd.Parameters.Add("@CMD_TYPE", SqlDbType.VarChar).Value = "C";
                sqlcmd.Parameters.Add("@TRIP", SqlDbType.VarChar).Value = Trip;
                SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
                da.Fill(DS);
                con.Close();
                DS.Dispose();
            }
        }
        catch (Exception ex)
        {
        }
        return DS;
    }
    protected void GridHoldPNRAccept_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    {
        try
        {
            GridHoldPNRAccept.PageIndex = e.NewPageIndex;
            PagelodBind(DSHold, DSCancel, RequestType);

        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);

        }
    }
     public DataSet PgAmount(string orderID)
    {
        SqlDataAdapter adap = new SqlDataAdapter("USP_RefundPGAmount", con);
        adap.SelectCommand.CommandType = CommandType.StoredProcedure;
        adap.SelectCommand.Parameters.AddWithValue("@orderid", orderID);
        DataSet ds = new DataSet();
        adap.Fill(ds);
        return ds;
    }
     void ClearInputs(ControlCollection ctrls)
     {
         try
         {
             foreach (Control ctrl in ctrls)
             {
                 if (ctrl is TextBox)
                     ((TextBox)ctrl).Text = string.Empty;
                 ClearInputs(ctrl.Controls);
             }
         }
         catch (Exception ex)
         {
         }

     }

}
  

