﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_DealTransfer : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    private SqlTransactionDom STDom = new SqlTransactionDom();
    private SqlDataAdapter adap;
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href='PrivilegePanel/Dashboard.aspx' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight Setting </a><a class='current' href='#'>Deal Transfer</a>";
        if (Session["UID"] == null)
        {
            Response.Redirect("~/Login.aspx");
        }
        if (Session["User_Type"].ToString().ToUpper() != "ADMIN")
        {
            Response.Redirect("~/Login.aspx");
        }
        try
        {
            if (!IsPostBack)
            {
                ddl_ptype_To.AppendDataBoundItems = true;
                ddl_ptype_To.Items.Clear();

                //Dim item As New ListItem("All Type", "0")
                //ddl_ptype.Items.Insert(0, item)
                ddl_ptype_To.DataSource = STDom.GetAllGroupType().Tables[0];
                ddl_ptype_To.DataTextField = "GroupType";
                ddl_ptype_To.DataValueField = "GroupType";
                ddl_ptype_To.DataBind();
                ddl_ptype_To.Items.Insert(0, new ListItem("--DEAL TRANSFER FROM--", "0"));

                dropDownFrom();
            }


        }
        catch (Exception ex)
        {
        }


    }
    public void dropDownFrom()
    {
        ddl_ptype.AppendDataBoundItems = true;
        ddl_ptype.Items.Clear();
        SqlCommand cmd = new SqlCommand("DealFrom", con);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);
        ddl_ptype.DataSource = dt;
        ddl_ptype.DataTextField = "GroupType";
        ddl_ptype.DataValueField = "GroupType";
        ddl_ptype.Items.Insert(0, new ListItem("--DEAL TRANSFER TO--", "0"));
        ddl_ptype.DataBind();       
    }
    protected void Submit_Click(object sender, EventArgs e)
    {
        try
        {
            SqlCommand cmd = new SqlCommand("Insert_FM_TODealTransfer", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@From", ddl_ptype.SelectedValue);
            cmd.Parameters.AddWithValue("@TO", ddl_ptype_To.SelectedValue);
            cmd.Parameters.AddWithValue("@UserID", Session["UID"]);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }    
            int k = cmd.ExecuteNonQuery();
            if (k != 0)
            {
                lblmsg.Text = "Deal Inserted Succesfully";
                lblmsg.ForeColor = System.Drawing.Color.CornflowerBlue;
            }
            con.Close();
        }
        catch(Exception ex)
        {
            lblmsg.Text = "Something went Wrong!!Try Aagin";
            lblmsg.ForeColor = System.Drawing.Color.CornflowerBlue;

        
        }

    }
}