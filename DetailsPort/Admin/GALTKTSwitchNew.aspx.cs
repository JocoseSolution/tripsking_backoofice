﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;


public partial class DetailsPort_Admin_GALTKTSwitchNew : System.Web.UI.Page
{
    string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    string Ddl_Value = "";
 
protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack == true)
        {
            if (Session["UID"] == null || Session["UID"].ToString() == "")
            {
                Response.Redirect("../../Login.aspx");
            }
            else
            {
                Ddl_Value = ddl_gds_tktsearch.SelectedValue;
                BindGridView(Ddl_Value);
                BindPCC();
            }                
        }
    }

[WebMethod]
public static List<string> GetAutoCompleteData(string username)
    {
        List<string> result = new List<string>();
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        using (SqlCommand cmd = new SqlCommand("select AL_Code,AL_Name FROM AirlineNames  where AL_Name LIKE '%'+@SearchText+'%'", con))
        {
            con.Open();
            cmd.Parameters.AddWithValue("@SearchText", username);
            SqlDataReader dr = cmd.ExecuteReader();           
            while (dr.Read())
            {
                result.Add(string.Format("{0}/{1}", dr["AL_Name"], dr["AL_Code"]));
            }
            
            return result;
        }
    }

public void BindPCC()
{
   // string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    SqlConnection con = new SqlConnection(constr);
    SqlCommand cmd = new SqlCommand("select distinct provider,provider+'('+PCC+')' as providerpcc from tkt_by_Agency_GAL where Provider is not null");
    cmd.CommandType = CommandType.Text;
    cmd.Connection = con;
    con.Open();
    txtPCC.DataSource = cmd.ExecuteReader();
    txtPCC.DataTextField = "providerpcc";
    txtPCC.DataValueField = "provider";
    txtPCC.DataBind();
    con.Close();
    txtPCC.Items.Insert(0, new ListItem("--Select Provider--", "0"));
}

protected void ddl_gds_tktsearch_SelectedIndexChanged(object sender, EventArgs e)
{
    Ddl_Value = ddl_gds_tktsearch.SelectedValue.ToString();
    BindGridView(Ddl_Value);
}

protected void BindGridView(string DDL_Val)
{
    try
    {
        using (SqlCommand sqlcmd = new SqlCommand())
        {
            SqlConnection con = new SqlConnection(constr);
            sqlcmd.Connection = con;
            con.Open();
            sqlcmd.CommandTimeout = 900;
            sqlcmd.CommandType = CommandType.StoredProcedure;
            //  sqlcmd.CommandText = "USP_GDS_TKTSEARCH";
            sqlcmd.CommandText = "USP_GDS_TKTSEARCH_PP2";
            sqlcmd.Parameters.Add("@cmd", SqlDbType.VarChar).Value = "SELECT";
            sqlcmd.Parameters.Add("@trip", SqlDbType.VarChar).Value = DDL_Val.Trim().ToUpper();                        
            SqlDataAdapter da = new SqlDataAdapter(sqlcmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            con.Close();           
            GV_GDS_TKT_Search.DataSource = ds;
            GV_GDS_TKT_Search.DataBind();
         
        }
    }
    catch (Exception ex)
    {
        clsErrorLog.LogInfo(ex);
    }
}

protected void GV_GDS_TKT_Search_PageIndexChanging(object sender, GridViewPageEventArgs e)
{
    GV_GDS_TKT_Search.PageIndex = e.NewPageIndex;
    BindGridView(ddl_gds_tktsearch.SelectedValue);
}

protected void Submit_Click(object sender, EventArgs e)
{
   try
    {
    string trip = ddl_gds_tktsearch.SelectedValue;
    string Airline = String.IsNullOrEmpty(Request["aircode"]) | Request["aircode"] == "Airlines" ? "" : Request["aircode"];
    string Airlinename = String.IsNullOrEmpty(Request["airlinename"]) | Request["airlinename"] == "Airlines" ? "" : Request["airlinename"];
    string CheckBox = CheckBox1.Checked ? "True" : "False";
    string supplier = txtPCC.SelectedValue;


        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "USP_GDS_TKTSEARCH_PP2";
        cmd.CommandType = CommandType.StoredProcedure;  
        cmd.Parameters.AddWithValue("@airlinecode", Airline);
        cmd.Parameters.AddWithValue("@trip", trip);
        cmd.Parameters.AddWithValue("@cmd","CHECK");      
        cmd.Connection = con;
        con.Open();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        con.Close(); 
       if(ds.Tables[0].Rows.Count>0)
       {
           ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Data already exist!!!');", true);
       }
       else
       {
           SqlConnection con1 = new SqlConnection(constr);
           SqlCommand cmd1 = new SqlCommand();
           cmd1.CommandText = "USP_GDS_TKTSEARCH_PP2";
           cmd1.CommandType = CommandType.StoredProcedure;
           cmd1.Parameters.AddWithValue("@airlinename", Airlinename);
           cmd1.Parameters.AddWithValue("@airlinecode", Airline);
           cmd1.Parameters.AddWithValue("@trip", trip);
           cmd1.Parameters.AddWithValue("@cmd", "ADD");
           cmd1.Parameters.AddWithValue("@supplier", supplier);
           cmd1.Parameters.AddWithValue("@isactive", CheckBox);
           cmd1.Connection = con1;
           con1.Open();
           cmd1.ExecuteNonQuery();
           con1.Close();
           ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Data  has been added successfully!!!');", true);
           ClearInputs(Page.Controls);
           txtPCC.SelectedIndex = 0;
           Ddl_Value = ddl_gds_tktsearch.SelectedValue;
           BindGridView(Ddl_Value);
       }
    }

    catch (Exception ex)
    {
        clsErrorLog.LogInfo(ex);
    }  
}

protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
{
    try
    {
        GV_GDS_TKT_Search.EditIndex = e.NewEditIndex;
        Ddl_Value = ddl_gds_tktsearch.SelectedValue;
        BindGridView(Ddl_Value);
    }
    catch (Exception ex)
    {
        clsErrorLog.LogInfo(ex);
    }
}

protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
{
    try
    {
        GV_GDS_TKT_Search.EditIndex = -1;
        Ddl_Value = ddl_gds_tktsearch.SelectedValue;
        BindGridView(Ddl_Value);
    }
    catch (Exception ex)
    {
        clsErrorLog.LogInfo(ex);
    }
}

protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
{
    try
    {
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand();

            Label lbl_counter = (Label)GV_GDS_TKT_Search.Rows[e.RowIndex].FindControl("lbl_counter");
            DropDownList txtstatus = (DropDownList)GV_GDS_TKT_Search.Rows[e.RowIndex].FindControl("ddl_status");
            cmd.CommandText = "USP_GDS_TKTSEARCH_PP2";
            cmd.CommandType = CommandType.StoredProcedure;          
            cmd.Parameters.Add("@cmd", SqlDbType.VarChar).Value = "UPDATE";
            cmd.Parameters.Add("@counter", SqlDbType.Int).Value = Convert.ToInt64(lbl_counter.Text);
            cmd.Parameters.Add("@isactive", SqlDbType.VarChar).Value = txtstatus.SelectedValue;
            cmd.Connection = con;
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Data has been updated successfully!!');", true);
            GV_GDS_TKT_Search.EditIndex = -1;
            Ddl_Value = ddl_gds_tktsearch.SelectedValue;
            BindGridView(Ddl_Value);
           
    }

    catch (Exception ex)
    {
        clsErrorLog.LogInfo(ex);
    }
}

protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
{
    try
    {
        SqlConnection con = new SqlConnection(constr);
        SqlCommand cmd = new SqlCommand();    
        Label lbl_counter = (Label)GV_GDS_TKT_Search.Rows[e.RowIndex].FindControl("lbl_counter");
        cmd.CommandText = "USP_GDS_TKTSEARCH_PP2";
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@cmd", SqlDbType.VarChar).Value = "DELETE";
        cmd.Parameters.Add("@counter", SqlDbType.Int).Value = Convert.ToInt64(lbl_counter.Text);
      
        cmd.Connection = con;
        con.Open();
        cmd.ExecuteNonQuery();
        con.Close();
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert('Data has been deleted successfully !!!');", true);
        Ddl_Value = ddl_gds_tktsearch.SelectedValue;
        BindGridView(Ddl_Value);     
    }
    catch (Exception ex)
    {
        clsErrorLog.LogInfo(ex);
    }
}

void ClearInputs(ControlCollection ctrls)
{
    try
    {
        foreach (Control ctrl in ctrls)
        {
            if (ctrl is TextBox)
                ((TextBox)ctrl).Text = string.Empty;
           
            ClearInputs(ctrl.Controls);
        }
    }
    catch (Exception ex)
    {
        clsErrorLog.LogInfo(ex);
    }

}


}