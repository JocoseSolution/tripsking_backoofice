<%@ Page Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="UploadAmountDetails.aspx.vb" Inherits="DetailsPort_Agent_UploadAmountDetails"
    Title="Upload Amount Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        /*.luna-navs li a {
    color: #fff;
    padding: 15px 15px!important;
    cursor: pointer;
    font-size: 14px!important;*/
    </style>
    

    <script type="text/javascript">
        $(document).ready(function () {
            $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd").click(function () {
                $("#From").focus();
            }
            );
            $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
            $(".cd1").click(function () {
                $("#To").focus();
            }
            );

        });
    </script>


   <%-- <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />--%>
    <%--<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />--%>
    <div class="row">
        

        <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary" <%--style="margin-top: 37px"--%>>
           
                    <div class="panel-body" <%--style="overflow:scroll;"--%>>
                         <div class="row">
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputPassword1">From Date</label>--%>
                                    <input type="text" name="From" id="From" placeholder="FROM DATE" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd" style="cursor:pointer;"></span>
                                    </span>
                                </div>
                            </div>
                             

                              <div class="col-md-2">
                                <div class="input-group" >
                                    <%--<label for="exampleInputPassword1">To Date</label>--%>
                                    <input type="text" name="To" placeholder="TO DATE" id="To" class="form-control input-text full-width" readonly="readonly" />
                                    <span class="input-group-addon" style="background-color: #49cced">
                                        <span class="glyphicon glyphicon-calendar cd1" style="cursor:pointer;"></span>
                                    </span>
                                </div>
                            </div>


                             <div class="col-md-2" >
                                    <div class="input-group" id="td_Agency" runat="server">
                                        
                                        <input type="text" placeholder="AGENCY NAME" id="txtAgencyName" name="txtAgencyName" class="form-control input-text full-width" onfocus="focusObj(this);"
                                            onblur="blurObj(this);"
                                             autocomplete="off" value="" />
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                        <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-briefcase"></span>
                               </span>
                                    </div>
                                </div>

                               <div class="col-md-2">
                                <div class="form-group">
                                   <%-- <label for="exampleInputPassword1">Trip Type</label>--%>
                                    <asp:DropDownList ID="ddl_PType" runat="server" CssClass="input-text full-width" TabIndex="1">
                                        <%--<asp:ListItem Text="--Select--" Value=""></asp:ListItem>--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT PAYMENT MODE</asp:ListItem>
                                        <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                        <asp:ListItem Text="Cash Deposite In Bank" Value="Cash Deposite In Bank"></asp:ListItem>
                                        <asp:ListItem Text="NetBanking" Value="NetBanking"></asp:ListItem>
                                        <asp:ListItem Text="RTGS" Value="RTGS"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                                    <div class="col-md-2">
                                <div class="form-group" >
                                   <%-- <label for="exampleInputPassword1">Trip Type</label>--%>
                                    <asp:DropDownList ID="ddl_status" runat="server" CssClass="input-text full-width" TabIndex="1">
                                        <%--<asp:ListItem Text="--Select--" Value=""></asp:ListItem>--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">SELECT STATUS</asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                        <asp:ListItem Text="InProcess" Value="InProcess"></asp:ListItem>
                                        <asp:ListItem Text="Confirm" Value="Confirm"></asp:ListItem>
                                        <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>

                                    </asp:DropDownList>
                                </div>
                            </div>   
                             

                     <%--   <table class="w90 auto boxshadow" >
                            <tr>
                                <td class="clear1" colspan="4"></td>
                            </tr>
                            <tr>
                                <td align="left" class="fltdtls" width="100" style="height: 30px">From Date:
                                </td>
                                <td align="left" style="height: 30px">
                                    <input type="text" placeholder="FROM DATE" name="From" id="From" class="form-control input-text full-width" readonly="readonly" style="width: 300px" />
                                    <br />
                                </td>
                                <td align="left" class="fltdtls" style="height: 30px" width="100">To Date:
                                </td>
                                <td align="left" style="height: 30px">
                                    <input type="text" name="To" id="To" class="input-text full-width" readonly="readonly" style="width: 300px" />
                                    <br />
                                </td>
                            </tr>--%>
                            <%--<tr>
                                <td align="left" class="fltdtls" width="100" style="height: 30px">Payment Mode :
                                </td>
                                <td align="left" style="height: 30px">

                                    <asp:DropDownList ID="ddl_PType" runat="server" Width="300px" CssClass="input-text full-width">
                                        <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                        <asp:ListItem Text="Cash Deposite In Bank" Value="Cash Deposite In Bank"></asp:ListItem>
                                        <asp:ListItem Text="NetBanking" Value="NetBanking"></asp:ListItem>
                                        <asp:ListItem Text="RTGS" Value="RTGS"></asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                </td>--%>
                             
                              <%--  <td class="fltdtls">Status</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddl_status" runat="server" Width="300px" CssClass="input-text full-width">
                                        <asp:ListItem Text="--Select--" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                        <asp:ListItem Text="InProcess" Value="InProcess"></asp:ListItem>
                                        <asp:ListItem Text="Confirm" Value="Confirm"></asp:ListItem>
                                        <asp:ListItem Text="Rejected" Value="Rejected"></asp:ListItem>

                                    </asp:DropDownList>
                                    <br />
                                </td>--%>
                        
                        
                         
                            <%--</tr>
                            <tr>
                                <td colspan="4" id="td_Agency" runat="server">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td style="width:50%;">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" id="td_ag">
                                                    <tr>
                                                        <td align="left" class="fltdtls" width="100" style="height: 30px">Agency Name:
                                                        </td>
                                                        <td align="left" style="height: 30px" colspan="1">

                                                            <input type="text" id="txtAgencyName" name="txtAgencyName" style="width: 300px" onfocus="focusObj(this);"
                                                                onblur="blurObj(this);" defvalue="Agency Name or ID"
                                                                value="Agency Name or ID" class="input-text full-width"/>
                                                            <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />

                                                        </td>
                                                    </tr>

                                                </table>
                                            </td>--%>
                         <div class="col-md-2">
                                <div class="form-group" >
                                   <%-- <label for="exampleInputPassword1">Trip Type</label>--%>
                                    <asp:DropDownList ID="DropDownListADJ" runat="server" CssClass="input-text full-width" TabIndex="1">
                                       <%--<asp:ListItem Text="--Select--" Value="Select" Selected="True"></asp:ListItem>--%>
                                        <asp:ListItem  value="" disabled selected style="display: none;">UPLOAD TYPE</asp:ListItem>
                                                    <asp:ListItem Text="Fresh Upload" Value="FU"></asp:ListItem>
                                                    <asp:ListItem Text="Adjustment" Value="AD"></asp:ListItem>
                                                </asp:DropDownList>
                                </div>
                            </div>

                                            <%--<td class="fltdtls" width="100">Upload Type</td>
                                            <td>
                                                <asp:DropDownList ID="DropDownListADJ" runat="server" Width="290px" CssClass="input-text full-width">
                                                    <asp:ListItem Text="--Select--" Value="Select" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Fresh Upload" Value="FU"></asp:ListItem>
                                                    <asp:ListItem Text="Adjustment" Value="AD"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>--%>

                            <tr>
                                <%--<td height="30px" style="padding-top: 10px; padding-bottom: 10px;">
                                &nbsp;
                            </td>--%>
                              
                                  <tr>
                                <td id="tr_SearchType" runat="server" visible="false" colspan="4">   <!--hidden field-->
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="fltdtls"></td>
                                            <td style="width: 150px">
                                                <asp:RadioButton ID="RB_Agent" runat="server" Checked="true" GroupName="Trip" onclick="Show(this)"
                                                    Text="Agent" />
                                               
                                            <asp:RadioButton ID="RB_Distr" runat="server" GroupName="Trip" onclick="Hide(this)"
                                                                              Text="Own" />
                                            </td>
                                        </tr>

                                    </table>

                                </td>
                                        </tr>
                                   
                               

                           
       
                       
                             
                            <div class="col-md-2">
                                <div class="input-group">
                                     <asp:Button ID="btn_showdetails" runat="server" CssClass="btn btn-success" Text="Go" Width="74px" />
                                </div>
                            </div>
                        </div></div>
                    
                        <table class="w70 auto">
                            <tr>
                                <td style="padding-top: 10px" align="center">
                                    <asp:GridView ID="grd_deposit" style="text-transform: uppercase;" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="false"  
                                        >
                                        <Columns>
                                            <asp:BoundField HeaderText="Transaction&nbsp;Date" DataField="Date" />
                                            <asp:BoundField HeaderText="Agency&nbsp;Name" DataField="AgencyName" />
                                            <asp:BoundField HeaderText="Agency&nbsp;Id" DataField="AgencyID" />
                                            <asp:BoundField HeaderText="Amount" DataField="Amount" />
                                            <asp:BoundField HeaderText="ModeOfPayment" DataField="ModeOfPayment" />
                                            <asp:BoundField HeaderText="Bank&nbsp;Name" DataField="BankName" />
                                            <asp:BoundField HeaderText="ChequeNo" DataField="ChequeNo" />
                                            <asp:BoundField HeaderText="Cheque&nbsp;Date" DataField="ChequeDate" />
                                            <asp:BoundField HeaderText="TransactionId" DataField="TransactionID" />
                                            <asp:BoundField HeaderText="BankAreaCode" DataField="BankAreaCode" />
                                            <asp:BoundField HeaderText="Deposit&nbsp;City" DataField="DepositCity" />
                                            <asp:BoundField HeaderText="Deposite&nbsp;Office" DataField="DepositeOffice" />
                                            <asp:BoundField HeaderText="Concern&nbsp;Person" DataField="ConcernPerson" />
                                            <asp:BoundField HeaderText="Reciept&nbsp;No" DataField="RecieptNo" />
                                            <asp:BoundField HeaderText="Remark" DataField="Remark" />
                                            <asp:BoundField HeaderText="Status" DataField="Status" />
                                            <asp:BoundField HeaderText="Remark&nbsp;By&nbsp;Account" DataField="RemarkByAccounts" />
                                            <asp:BoundField HeaderText="Account&nbsp;Id" DataField="AccountID" />
                                            <%--<asp:BoundField HeaderText="Date" DataField="Date" />
    <asp:BoundField HeaderText="Date" DataField="Date" />--%>
                                        </Columns>

                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
   
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>

</asp:Content>
