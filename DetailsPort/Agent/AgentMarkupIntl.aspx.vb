﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class DetailsPort_Agent_AgentMarkupIntl
    Inherits System.Web.UI.Page


    Dim con As New SqlConnection
    Dim adp As SqlDataAdapter
    Public ds As New DataSet
    Public Agent

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)

        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        If Not Page.IsPostBack Then
            Try
                Bind_Data()
                If con.State = ConnectionState.Open Then con.Close()
                con.ConnectionString = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
                txt_AgentID.Text = Session("UID").ToString()
                Dim ds1 As New DataSet
                adp = New SqlDataAdapter("SELECT AL_Code,AL_Name FROM AirlineNames order by AL_Name asc", con)
                adp.Fill(ds1)
                If ds1.Tables(0).Rows.Count > 0 Then
                    airline_code.DataSource = ds1
                    airline_code.DataTextField = "AL_Name"
                    airline_code.DataValueField = "AL_Code"
                    airline_code.DataBind()

                Else
                End If

                lbl.Visible = False
                mk.Text = "0"
            Catch ex As Exception
                clsErrorLog.LogInfo(ex)
            End Try
        End If

    End Sub
    Private Sub Bind_Data()
        Try
            Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim da As New SqlDataAdapter("SELECT counter,UserId,AirlineCode,MarkupValue,MarkupType,Trip FROM AgentMarkup where Userid='" & Session("UID").ToString() & "' order by AirlineCode", con)
            Dim dt As New DataTable()
            da.Fill(dt)
            GridView1.DataSource = dt
            GridView1.DataBind()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub


    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        Try
            GridView1.EditIndex = -1
            Bind_Data()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs)
        Try

            Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim cmd As New SqlCommand()
            Dim lblSrNo As Label = DirectCast(GridView1.Rows(e.RowIndex).FindControl("lbl_ID"), Label)
            cmd.CommandText = "DELETE FROM AgentMarkup WHERE counter=@cat_id"
            cmd.Parameters.Add("@cat_id", SqlDbType.Int).Value = lblSrNo.Text
            cmd.Connection = con
            con.Open()
            cmd.ExecuteNonQuery()
            con.Close()
            Bind_Data()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles GridView1.RowEditing
        Try
            GridView1.EditIndex = e.NewEditIndex
            Bind_Data()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)
        Try
            Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim cmd As New SqlCommand()

            If DirectCast(GridView1.Rows(0).Cells(0).Controls(0), LinkButton).Text = "Insert" Then

            Else

                Dim lblSrNo As Label = DirectCast(GridView1.Rows(e.RowIndex).FindControl("lbl_ID"), Label)
                'Dim mrkvalue As Label = DirectCast(GridView1.Rows(e.RowIndex).FindControl("lbl_MrkValue"), Label)
                cmd.CommandText = "UPDATE AgentMarkup SET MarkupValue=@MRK WHERE Counter=@cat_id"
                cmd.Parameters.Add("@MRK", SqlDbType.VarChar).Value = UCase(DirectCast(GridView1.Rows(e.RowIndex).Cells(4).Controls(0), TextBox).Text.ToUpper)
                cmd.Parameters.Add("@cat_id", SqlDbType.Int).Value = lblSrNo.Text
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End If
            GridView1.EditIndex = -1
            Bind_Data()
            lbl.Visible = False

            lbl_msg.Visible = False

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try


    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try

            Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim da As New SqlDataAdapter("Select user_id from agent_register where user_id='" & Session("UID").ToString() & "' ", con)
            Dim dt As New DataTable()
            con.Open()
            da.Fill(dt)
            Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
            Dim da1 As New SqlDataAdapter("Select * from AgentMarkup where (Userid='" & Session("UID").ToString() & "') and (AirlineCode='" & airline_code.SelectedValue & "') ", con)
            Dim dt1 As New DataTable()
            con1.Open()
            da1.Fill(dt1)
            con1.Close()
            If (dt1.Rows.Count > 0) Then
                lbl.Visible = True
                lbl.Text = "MarkUp For This AirLine Already Exist."
                lbl_msg.Text = ""
            Else

                Dim txtlenght As Integer = mk.Text.Length
                If (RadioButtonList1.SelectedValue = "D" AndAlso txtlenght > 3 OrElse RadioButtonList1.SelectedValue = "I" AndAlso txtlenght > 4) Then
                    Response.Write("<script>alert('Markup is not valid')</script>")
                    GoTo finish
                End If

                Dim adap As New SqlDataAdapter("SP_InsertAgentMarkup", con)
                adap.SelectCommand.CommandType = CommandType.StoredProcedure
                adap.SelectCommand.Parameters.AddWithValue("@uid", Session("UID").ToString())
                adap.SelectCommand.Parameters.AddWithValue("@AL_Code", airline_code.SelectedValue)
                adap.SelectCommand.Parameters.AddWithValue("@AL_Name", airline_code.SelectedItem.Text)
                adap.SelectCommand.Parameters.AddWithValue("@Mrk_Type", ddl_MarkupType.SelectedValue)
                adap.SelectCommand.Parameters.AddWithValue("@Trip", RadioButtonList1.SelectedValue)
                adap.SelectCommand.Parameters.AddWithValue("@Mrk_Value", mk.Text.Trim)
                adap.SelectCommand.Parameters.AddWithValue("@Dis", "SPRING")
                adap.SelectCommand.Parameters.AddWithValue("@Show", "TRN")

                Dim dt2 As New DataTable()
                adap.Fill(dt2)
                Bind_Data()
                lbl_msg.Visible = True
                lbl_msg.Text = "Markup Submitted Sucessfully"
                lbl.Text = ""
                airline_code.SelectedIndex = 0
finish:
                Response.Write("")

                mk.Text = 0
                lbl.Text = ""

            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub


    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Try
            GridView1.PageIndex = e.NewPageIndex
            Bind_Data()

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
End Class

