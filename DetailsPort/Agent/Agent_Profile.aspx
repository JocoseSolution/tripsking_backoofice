<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false"
    CodeFile="Agent_Profile.aspx.vb" Inherits="DetailsPort_Agent_Agent_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   
    <script language="javascript" type="text/javascript">
        function checkpwd() {
            if (document.getElementById("").value != document.getElementById("").value) {
                alert('Please Enter Same Password');

            }
        }
    </script>

    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="margin-top:10px;">
            <h2 style="color:#000; text-align:center"><asp:LinkButton ID="lnk_tds" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#20313f" Font-Underline="True">Click here to download your TDS Certificate(2013-2014)
                        </asp:LinkButton></h2></div>
            <table cellspacing="0" cellpadding="0" border="0" align="center" class="boxshadow w70 auto">
                <tr>
                     <td align="left" style="color:#fff; font-weight:bold; padding-top:7px;">
                      Profile
                    </td>
                   
                </tr>
                <tr>
                            <td class="clear1">
                                
                            </td>
                        </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td class="ProfileHead">
                                    Login Details
                                </td>
                                <td align="right" class="ProfileHead">
                                    <asp:LinkButton ID="LinkEdit" runat="server" Font-Bold="True"><img src="../../Images/edit.png" alt="Edit" /></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td valign="top" width="300px">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td height="25" width="140px" class="Text">
                                                            Username
                                                        </td>
                                                        <td id="td_username" runat="server" class="Textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr id="td_login" runat="server">
                                                        <td height="25" width="120px" class="Text">
                                                            Password
                                                        </td>
                                                        <td>
                                                            ******
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="td_login1" runat="server" visible="false">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td height="25px" width="140px" class="text1">
                                                            Password
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt_password" runat="server" TextMode="Password"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25px" class="text1">
                                                            Confirm Password
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt_cpassword" runat="server" TextMode="Password"></asp:TextBox>
                                                            <asp:Label ID="lbl_msg" runat="server" ForeColor="Red"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="25px">
                                                            <asp:Button ID="btn_Save" runat="server" Text="Save" />
                                                            &nbsp;Or&nbsp;<asp:LinkButton ID="lnk_Cancel" runat="server" CssClass="cancelprofile"
                                                                Font-Bold="False" Font-Underline="True">Cancel</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td width="140px" colspan="2" style="height: 18px" align="left">
                                                &nbsp;<asp:Image ID="Image111" runat="server" Height="70px" Width="90px" />
                                            </td>
                                        </tr>
                                        <tr valign="middle">
                                            <td valign="middle" height="25" class="text1" align="left" style="padding-left: 10px;">
                                                Upload Logo
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="FileUpload1" runat="server" />
                                                &nbsp;<asp:Button ID="button_upload" runat="server" Text="Upload" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" height="25px" style="padding-left: 10px; color: #FF3300; font-size: 11px;"
                                                align="left">
                                                <span class="text1" style="font-size: 13px; font-family: arial, Helvetica, sans-serif;
                                                    font-weight: bold;">Note : </span>Image formate must be in JPEG and Size should
                                                be (90*70) pixels
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td class="ProfileHead">
                                    Personal Details
                                </td>
                                <td align="right" class="ProfileHead">
                                    <asp:LinkButton ID="LinkPersonalEdit" runat="server" Font-Bold="True"><img src="../../Images/edit.png" alt="Edit" /></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td id="td_PDetails" runat="server" valign="top">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td width="140px" style="height: 25px" class="Text">
                                                            Name
                                                        </td>
                                                        <td id="td_Name" runat="server" style="height: 25px" class="Textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="120px" class="Text">
                                                            Email ID
                                                        </td>
                                                        <td id="td_EmailID" runat="server" class="Textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="120px" class="Text">
                                                            Alternate EmailID
                                                        </td>
                                                        <td id="td_AlternateEmailID" runat="server" class="Textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="120px" class="Text">
                                                            Mobile No
                                                        </td>
                                                        <td id="td_Mobile" runat="server" class="Textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="120px" class="Text">
                                                            Landline
                                                        </td>
                                                        <td id="td_Landline" runat="server" class="Textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="120px" class="Text">
                                                            PanCard No
                                                        </td>
                                                        <td id="td_Pan" runat="server" class="Textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="120px">
                                                            Fax
                                                        </td>
                                                        <td id="td_Fax" runat="server" class="Textsmall">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td id="td_PDetails1" runat="server" visible="false" valign="top">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                    <tr>
                                                        <td height="25" width="140px" class="text1">
                                                            Name
                                                        </td>
                                                        <td id="td_Name1" runat="server" class="textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="140px" class="text1">
                                                            Email
                                                        </td>
                                                        <td id="td_Email1" runat="server" class="textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="140px" class="text1">
                                                            Alternate Email
                                                        </td>
                                                        <td id="td4" runat="server">
                                                            <asp:TextBox ID="txt_aemail" runat="server" Width="200px"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 20px" class="text1">
                                                            Mobile No
                                                        </td>
                                                        <td id="td_Mobile1" runat="server" height="25px" class="textsmall">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 25px" class="text1" width="140px">
                                                            Landline
                                                        </td>
                                                        <td style="height: 20px">
                                                            <asp:TextBox ID="txt_landline" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text1">
                                                            PanCard No
                                                        </td>
                                                        <td height="25px" class="textsmall" id="td_Pan1" runat="server">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="25" width="140px" class="text1">
                                                            Fax
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txt_fax" runat="server"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr valign="middle">
                                                        <td valign="middle" height="30" colspan="2">
                                                            <asp:Button ID="btn_SavePDetails" runat="server" Text="Save" />
                                                            &nbsp;Or&nbsp;<asp:LinkButton ID="lnk_CancelPDetails" runat="server" CssClass="cancelprofile"
                                                                Font-Bold="False" Font-Underline="True">Cancel</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <%--<tr>
                                            <td valign="middle" height="25" class="text1" align="left" style="padding-left: 10px;">
                                                Update Pancard
                                            </td>
                                            <td>
                                                <asp:FileUpload ID="FileUpload_Pan" runat="server" />
                                                &nbsp;<asp:Button ID="btn_uploadpan" runat="server" Text="Upload" />
                                            </td>
                                        </tr>--%>
                                        <%--<tr>
                                            <td colspan="2" height="25px" style="padding-left: 10px; color: #FF3300; font-size: 11px;"
                                                align="left">
                                                <span class="text1" style="font-size: 13px; font-family: arial, Helvetica, sans-serif;
                                                    font-weight: bold;">Note : </span>Image formate must be in JPEG
                                            </td>
                                        </tr>--%>
                                        <tr>
                                            <td valign="middle" height="25" class="text1" align="left" style="padding-left: 10px;">
                                              
                                           
                                            </td>
                                            <td>
                                                <%--<asp:FileUpload ID="FileUpload_Pan" runat="server" />
                                                &nbsp;<asp:Button ID="btn_uploadpan" runat="server" Text="Upload" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td class="ProfileHead">
                                    Address
                                </td>
                                <td align="right" class="ProfileHead">
                                    <asp:LinkButton ID="LinkEditAdd" runat="server" Font-Bold="True"><img src="../../Images/edit.png" alt="Edit" /></asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td id="td_Address" runat="server" valign="top">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td height="25" width="140px" class="Text">
                                                Address
                                            </td>
                                            <td id="td_Add" runat="server" class="Textsmall">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" width="120px" class="Text">
                                                City
                                            </td>
                                            <td id="td_City" runat="server" class="Textsmall">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" width="120px" class="Text">
                                                State
                                            </td>
                                            <td id="td_State" runat="server" class="Textsmall">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" width="120px" class="Text">
                                                Country
                                            </td>
                                            <td id="td_Country" runat="server" class="Textsmall">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td id="td_Address1" runat="server" visible="false">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td height="25" width="140px" class="text1">
                                                Address
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_address" runat="server" Height="50px" TextMode="MultiLine" Width="220px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 25px" height="25" class="text1" width="140px">
                                                City
                                            </td>
                                            <td style="height: 20px">
                                                <asp:TextBox ID="txt_city" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25" width="140px" class="text1">
                                                State
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_state" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr valign="middle">
                                            <td valign="middle" height="25" class="text1">
                                                Country
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txt_country" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="30px">
                                                <asp:Button ID="btn_Saveadd" runat="server" Text="Save" />
                                                &nbsp;Or&nbsp;<asp:LinkButton ID="lnk_CancelAdd" runat="server" CssClass="cancelprofile"
                                                    Font-Bold="False" Font-Underline="True">Cancel</asp:LinkButton>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="button_upload" />
              <asp:PostBackTrigger ControlID="lnk_tds" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden;
                padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5;
                z-index: 1000;">
            </div>
            <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center;
                z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px;
                font-weight: bold; color: #000000">
                Please Wait....<br />
                <br />
                <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                <br />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
