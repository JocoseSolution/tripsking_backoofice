﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="ControlItems.aspx.cs" Inherits="ControlItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            height: 40px;
        }

        .auto-style3 {
            height: 22px;
        }
    </style>

    <!--TextBox-Validation-->
   <%-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"></link>--%>
        <script src="https://code.jquery.com/jquery-1.12.4.js">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js">
        </script>


    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8) || (charCode == 46)) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <%--<script type="text/javascript">
        function ValidateFunction() {
            if (document.getElementById("<%=txt_name.ClientID%>").value == "") {
                alert('Name can not be blank');
                document.getElementById("<%=txt_name.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_width.ClientID%>").value == "") {
                alert('Width can not be blank');
                document.getElementById("<%=txt_width.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_height.ClientID%>").value == "") {
                alert('Height can not be blank');
                document.getElementById("<%=txt_height.ClientID%>").focus();
            return false;
        }
        if (document.getElementById("<%=txt_displaytime.ClientID%>").value == "") {
                alert('Display time can not be blank');
                document.getElementById("<%=txt_displaytime.ClientID%>").focus();
            return false;
        }
    }
    </script>--%>

    <form data-toggle="validator" role="form">
       <div class="row">
        <%--<div class="col-md-2"></div>--%>

        <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
            
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-2" style="margin-left:124px">
                                <div class="input-group">
                                     <%--<label for="exampleInputPassword1"></label>--%>
                                     <asp:TextBox ID="txt_name" runat="server" placeholder="CONTROL NAME" Class="form-control input-text full-width"  type="text" required=""></asp:TextBox>
                                    <span class="input-group-addon" style="background-color: #49cced">
                                    <span class="glyphicon glyphicon-user"></span>
                                        </span>
                                    <div class="help-block with-errors"></div>
                                    </div>

                                 </div>
                                

                                        <div class="col-xs-2">
                                              <div class="input-group">
                                     <%--<label for="exampleInputPassword1"></label>--%>
                                   <asp:TextBox ID="txt_width" onkeypress="return isNumberKey(event)" placeholder="WIDTH" MaxLength="6" runat="server"  Class="form-control input-text full-width" type="text" required=""></asp:TextBox>
                                     <span class="input-group-addon" style="background-color: #49cced">
                                                  <span class="glyphicon glyphicon-text-width"></span>
                                         </span>
                                                  <div class="help-block with-errors"></div>
                                              </div>
                                          </div>
                      
                       

                                    
                                     
                                <div class="col-xs-2">
                                     <div class="input-group">
                                     <%--<label for="exampleInputPassword1"></label>--%>
                                   <asp:TextBox ID="txt_height" placeholder="HEIGHT" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server" Class="form-control input-text full-width" type="text" required=""></asp:TextBox>
                                     
                                          <span class="input-group-addon" style="background-color: #49cced"> 
                                         <span class="glyphicon glyphicon-sort-by-attributes"></span> 
                                         </span>
                                         <div class="help-block with-errors"></div>
                                     </div>
                                      </div>

                                            <div class="col-xs-2">
                                                 <div class="input-group">
                                     <%--<label for="exampleInputPassword1"></label>--%>
                                  <asp:TextBox ID="txt_displaytime" placeholder="ITEM DISPLAY AT TIME" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server" Class="form-control input-text full-width" type="text" required=""></asp:TextBox>
                                     <span class="input-group-addon" style="background-color: #49cced">
                                    <span class="fa fa-object-group"></span>
                                     </span>
                                                     <div class="help-block with-errors"></div>
                                                 </div>
                                                  </div>
                                          
                                     
                                      
                                           <div class="form-group">
                                               <%--<button class="btn btn-success" type="submit" runat="server" >
                                                   Submit
                                               </button>--%>
                                <div class="col-xs-2" style="top:3px; border-radius:40px;">
                                     <asp:Button ID="submit" runat="server" type="submit" OnClick="btn_submit_Click"  Text="Submit" CssClass="btn btn-success" />
                                      </div>
                                          </div></div> 
                                           

    <%--<table class="auto-style1">
        <tr>
            <td>Control Name :
            <asp:TextBox ID="txt_name" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Width :
            <asp:TextBox ID="txt_width" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Height :<asp:TextBox ID="txt_height" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">Item Display At Time :<asp:TextBox ID="txt_displaytime" onkeypress="return isNumberKey(event)" MaxLength="6" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" OnClientClick="return ValidateFunction();" Text="Submit" />
            </td>
        </tr>
    </table>--%>
                                       </div>
                        </div>
                            </div>
                        </div>
                    </div>
        </form>
              </asp:Content>

