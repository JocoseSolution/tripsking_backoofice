﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="DmtComm.aspx.cs" Inherits="DMT_DmtComm" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    </script>

    <div class="row">
        <div class="container-fluid" style="padding-right: 35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-2">
                                <label>Type :</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="type_group" runat="server" required="required">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <label>Charges :</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="dmt_charg" runat="server">
                                    <asp:ListItem Value="Fixed">Fixed</asp:ListItem>
                                    <asp:ListItem Value="Percentage">Percentage</asp:ListItem>

                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <label>Agent Charge:</label>

                                <input type="text" placeholder="Agent Charge" name="Agent_Charge" id="agntCharge" runat="server" class="form-control input-text full-width" />

                            </div>
                            <div class="col-md-2" style="margin-top: 28px;">
                                <div class="input-group">

                                    <asp:Button ID="btnSubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                                </div>
                            </div>



                        </div>
                        <div>
                            <asp:GridView ID="dmt_grid" runat="server" AutoGenerateColumns="false" DataKeyNames="Id"
                                OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                                OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" PageSize="8"
                                CssClass="table table-striped table-bordered table-hover" Width="100%" Style="text-transform: uppercase;">

                                <Columns>
                                    <asp:TemplateField HeaderText="Group Type" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lbltype_group" runat="server" Text='<%# Eval("Group_Type") %>'></asp:Label>
                                        </ItemTemplate>
                          <%--              <EditItemTemplate>

                                            <asp:TextBox ID="txttype_group" runat="server" Text='<%# Eval("Group_Type") %>'></asp:TextBox>
                                        </EditItemTemplate>--%>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Charge type" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldmt_charg" runat="server" Text='<%# Eval("Charges") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                      <asp:DropDownList CssClass="input-text full-width" ID="ddldmt_charg" runat="server">
                                    <asp:ListItem Value="Fixed">Fixed</asp:ListItem>
                                    <asp:ListItem Value="Percentage">Percentage</asp:ListItem>

                                </asp:DropDownList>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Agent Charge" ItemStyle-Width="150">
                                        <ItemTemplate>
                                            <asp:Label ID="lblagntCharge" runat="server" Text='<%# Eval("Agent_Charges") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:TextBox ID="txtAgent_Charges" runat="server" Text='<%# Eval("Agent_Charges") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>





                                    <asp:TemplateField HeaderText="Edit/Delete" ItemStyle-CssClass="nowrapgrdview">
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lbtnUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbtnCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>/
                                    <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="Delete" Text="Delete" CommandArgument='<%#Eval("Id")%>' OnClientClick="return confirmDelete();"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>

                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

