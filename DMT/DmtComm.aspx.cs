﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DMT_DmtComm : System.Web.UI.Page
{
    string Con =ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    SqlDataAdapter adp = null;
    DataTable dt = null;
    SqlCommand cmd = null;
    SqlTransactionDom  STDom = new SqlTransactionDom();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["UID"])))
        {
            Response.Redirect("~/Login.aspx");
        }
        try
        {
            if (!IsPostBack)
            {
                type_group.AppendDataBoundItems = true;
                type_group.Items.Clear();

                type_group.DataSource = STDom.GetAllGroupType().Tables[0];
                type_group.DataTextField = "GroupType";
                type_group.DataValueField = "GroupType";
                type_group.DataBind();
                type_group.Items.Insert(0, new ListItem("GROUP TYPE", ""));






                BindGridview();
            }
        }
        catch (Exception ex)
        {

        }


    }

    protected void BindGridview()
    {
        SqlDataAdapter sda = new SqlDataAdapter("select * from  Tripsking..DMT_Charges", Con);
        DataTable dt = new DataTable();
        sda.Fill(dt);
        dmt_grid.DataSource = dt;
        dmt_grid.DataBind();

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(Con);
        SqlCommand cmd = new SqlCommand("DMT_Charges_Pro", con);   
        cmd.CommandType = CommandType.StoredProcedure;  
        cmd.Parameters.AddWithValue("Group_Type", type_group.Text);  
        cmd.Parameters.AddWithValue("Charges", dmt_charg.Text);
        cmd.Parameters.AddWithValue("Agent_Charges", agntCharge.Value);
        cmd.Parameters.AddWithValue("type","Insert");
       
        con.Open();  
        int k = cmd.ExecuteNonQuery();

        if (k > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record submited successfully.');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record already Exists.');", true);
        }
        con.Close();
        BindGridview();
    }

    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        dmt_grid.EditIndex = -1;
        this.BindGridview();

    }

    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        dmt_grid.EditIndex = e.NewEditIndex;
        this.BindGridview();

    }

    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string user_id = dmt_grid.DataKeys[e.RowIndex].Values[0].ToString();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("DMT_Charges_Pro"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Id", user_id);
                cmd.Parameters.AddWithValue("@type", "Delete");
                cmd.Connection = con;
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }
        this.BindGridview();

    }


    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow row = dmt_grid.Rows[e.RowIndex];
        string user_id = dmt_grid.DataKeys[e.RowIndex].Values[0].ToString();

        TextBox txtCharges = row.FindControl("txtAgent_Charges") as TextBox;
        string status = (row.FindControl("ddldmt_charg") as DropDownList).SelectedItem.ToString();

        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand("DMT_Charges_Pro"))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Id", Convert.ToInt16(user_id));
                cmd.Parameters.AddWithValue("@Charges", status);
                cmd.Parameters.AddWithValue("@Agent_Charges", txtCharges.Text);
                cmd.Parameters.AddWithValue("@type","update");
                cmd.Parameters.AddWithValue("@UpdatedBy", (Convert.ToString(Session["UID"])));


                cmd.Connection = con;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                
                if (i > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record updated successfully.');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Record not updated.');", true);
                }
                con.Close();
            }
        }
        dmt_grid.EditIndex = -1;
        this.BindGridview();

    }


    }
   
    