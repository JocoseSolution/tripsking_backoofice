﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DetailsPort_Admin_Control : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ((Label)Page.Master.FindControl("lblBC")).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Flight Setting </a><a class='current' href='#'>Set Credentials</a>";

        if (Page.Request.QueryString.Keys.Count > 0)
        {


            if (Page.Request.QueryString["r"] == "tkt_cr")
                addUC("~/Reports/Set_Credentials/TicketingCrd.ascx");
            if (Page.Request.QueryString["r"] == "bkt_cr")
                addUC("~/Reports/Set_Credentials/ServiceCredentials.ascx");
         

            else if (Page.Request.QueryString["r"] == "src_cr")
                addUC("~/Reports/Set_Credentials/PNRCreationCrd.ascx");
        }
        else
        {
            addUC("~/Reports/Set_Credentials/PNRCreationCrd.ascx");
        }


    }
    private void addUC(string path)
    {
        UserControl myControl = (UserControl)Page.LoadControl(path);

        //UserControlHolder is a place holder on the aspx page where I want to load the
        //user control to.
        phModule.Controls.Add(myControl);
    }
}