﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Update_BookingOrder.aspx.vb"
    Inherits="Update_BookingOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 08 || charCode == 46 || charCode == 32)) {
                return true;
            }
            else {
                return false;
            }
        }


    </script>
     <link href="../../CSS/foundation.min.css" rel="stylesheet" />
    <link href="../../CSS/foundation.css" rel="stylesheet" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <style>
        input[type="text"], input[type="password"], select, textarea {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>

</head>


<body>
    <form id="form1" runat="server">
        <div>
            &nbsp;
        </div>
        <div align="center">
            <table width="90%">
                <tr>
                    <td align="right" style="font-weight: bold; font-size: 15px; color: #004b91;">Booking Reference No.
                    </td>
                    <td id="tdRefNo" runat="server" align="left" style="font-weight: bold;"></td>
                </tr>
            </table>

            <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">PNR Details
                    </td>
                </tr>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
                <tr>


                    <td>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvFlightHeader" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Booking Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBookingDate" runat="server" Text='<%#Eval("CREATEDDATE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pnr">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPnr" runat="server" Text='<%#Eval("PNR")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPnr" runat="server" Width="70px" Text='<%#Eval("PNR")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket no">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTICKETNO" runat="server" Text='<%#Eval("TICKETNO")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTICKETNO" runat="server" Width="70px" Text='<%#Eval("TICKETNO")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("BOOKINGSTATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtStatus" runat="server" Width="80px" Text='<%#Eval("BOOKINGSTATUS")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Modify Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblModifyStatus" runat="server" Text='<%#Eval("ModifyStatus")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                         <asp:BoundField HeaderText="Partner Name" DataField="PROVIDER_NAME"></asp:BoundField>                                     
                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="newbutton_2" CommandName="Update" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="newbutton_2" CommandName="Cancel" />
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdateFltHeader" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>


                </tr>

            </table>

            <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Passenger Information
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvTravellerInformation" runat="server" AutoGenerateColumns="false"
                                    Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PAX_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Title">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%#Eval("PAX_TITLE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTitle" Width="50px" runat="server" Text='<%#Eval("PAX_TITLE")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFName" runat="server" Text='<%#Eval("PAXNAME")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtFname" Width="100px" runat="server" Text='<%#Eval("PAXNAME")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblType" runat="server" Text='<%#Eval("GENDER")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtType" MaxLength="3" runat="server" Width="60px" Text='<%#Eval("GENDER")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket No">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTktNo" runat="server" Text='<%#Eval("TICKETNO")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTktNo" runat="server" Width="80px" Text='<%#Eval("TICKETNO")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText ="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsts" runat="server" Text='<%#Eval("BOOKINGSTATUS")%>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                       <%--  <asp:TemplateField HeaderText ="Message">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsts" runat="server"  ForeColor="Red" Text='<%#Eval("msg")%>'></asp:Label>
                                            </ItemTemplate>

                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdatePax" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="UpdatePanel2">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>
                </tr>

            </table>

            <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">BUS Information
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvFlightDetails" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                         <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PAX_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                            <asp:TemplateField HeaderText="BUSOPERATOR">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBUSOPERATOR" runat="server" Text='<%#Eval("BUSOPERATOR")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBUSOPERATOR" Width="80px" runat="server" Text='<%#Eval("BUSOPERATOR")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SOURCE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSOURCE" runat="server" Text='<%#Eval("SOURCE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtSOURCE" Width="80px" runat="server" Text='<%#Eval("SOURCE")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DESTINATION">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDESTINATION" runat="server" Text='<%#Eval("DESTINATION")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDESTINATION" Width="80px" runat="server" Text='<%#Eval("DESTINATION")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>  
                                    
                                        <asp:TemplateField HeaderText="BOARDINGPOINT">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBOARDINGPOINT" runat="server" Text='<%#Eval("BOARDINGPOINT")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBOARDINGPOINT" Width="80px" runat="server" Text='<%#Eval("BOARDINGPOINT")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField> 
                                        
                                            <asp:TemplateField HeaderText="DROPPINGPOINT">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDROPPINGPOINT" runat="server" Text='<%#Eval("DROPPINGPOINT")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDROPPINGPOINT" Width="80px" runat="server" Text='<%#Eval("DROPPINGPOINT")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>   
                                                                               
                                        <asp:TemplateField HeaderText="JOURNEYDATE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDepDate" runat="server" Text='<%#Eval("JOURNEYDATE")%>'></asp:Label>
                                            </ItemTemplate>                                          
                                        </asp:TemplateField>                                    
                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" Width="100px" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>                                      
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="lblUpdateFlight" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress2" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>

                    </td>
                </tr>

            </table>

            <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Fare Information
                    </td>
                </tr>
                <tr>
                    <td>                      
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvFareDetails" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                         <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaxId" runat="server" Text='<%#Eval("PAX_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                           <asp:TemplateField HeaderText="TA_TOT_FARE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTA_TOT_FARE" runat="server" Text='<%#Eval("TA_TOT_FARE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTA_TOT_FARE" Width="80px" runat="server" Text='<%#Eval("TA_TOT_FARE")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>       
                                         <asp:TemplateField HeaderText="ADMIN_MARKUP">
                                            <ItemTemplate>
                                                <asp:Label ID="lblADMIN_MARKUP" runat="server" Text='<%#Eval("ADMIN_MARKUP")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtADMIN_MARKUP" Width="80px" runat="server" Text='<%#Eval("ADMIN_MARKUP")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>   
                                          <asp:TemplateField HeaderText="AGENT_MARKUP">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAGENT_MARKUP" runat="server" Text='<%#Eval("AGENT_MARKUP")%>'></asp:Label>
                                            </ItemTemplate>                                          
                                        </asp:TemplateField>  
                                           <asp:TemplateField HeaderText="TDS">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTA_TDS" runat="server" Text='<%#Eval("TA_TDS")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTA_TDS" Width="80px" runat="server" Text='<%#Eval("TA_TDS")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>                                                                 
                                            <asp:TemplateField HeaderText="TA_NET_FARE">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTA_NET_FARE" runat="server" Text='<%#Eval("TA_NET_FARE")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTA_NET_FARE" Width="80px" runat="server" Text='<%#Eval("TA_NET_FARE")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>                                                                                                      
                                        <asp:TemplateField HeaderText="Remark" Visible="false">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" Width="100px" TextMode="MultiLine"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>                                      
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label></b>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <asp:UpdateProgress ID="updateprogress3" runat="server" AssociatedUpdatePanelID="UpdatePanel3">
                            <ProgressTemplate>
                                <div style="position: fixed; top: 0px; bottom: 0px; left: 0px; right: 0px; overflow: hidden; padding: 0; margin: 0; background-color: #000; filter: alpha(opacity=50); opacity: 0.5; z-index: 1000;">
                                </div>
                                <div style="position: fixed; top: 30%; left: 43%; padding: 10px; width: 20%; text-align: center; z-index: 1001; background-color: #fff; border: solid 1px #000; font-size: 12px; font-weight: bold; color: #000000">
                                    Please Wait....<br />
                                    <br />
                                    <img alt="loading" src="<%= ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </td>                    
                </tr>
            </table>       
        </div>
    </form>
</body>
</html>





