﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="Bus_RefundRequest.aspx.cs" Inherits="BS_Bus_RefundRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

       <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function Validate() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_Acept_grdview_ctl02_txtRemark").value == "") {

                alert('Remark can not be blank,Please Fill Remark');
                document.getElementById("ctl00_ContentPlaceHolder1_Acept_grdview_ctl02_txtRemark").focus();
                return false;
            }
            if (confirm("Are you sure you want to Reject!"))
                return true;
            return false;
        }
    </script>
      <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
         .overfl {
             overflow-y:scroll;
         }
    </style>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="container-fluid" style="padding-right: 285px">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
       
                    <div class="panel-body">
                          <div class="row">
                         <div class="col-md-4">           
                                <div class="form-group">
                                    <label for="exampleInputPassword1">PNR</label>
                                    <asp:TextBox ID="txt_PNR" runat="server" CssClass="input-text full-width"></asp:TextBox>
                                </div>
                                 </div>                            
                                  <div class="col-md-3">
                                       <br/>
                                <div class="form-group">
                                    <asp:Button ID="btn_result" runat="server" OnClick="btn_result_Click" CssClass="btn btn-lg btn-primary" Text="Search Result" />
                                     </div>
                                      </div>          
                             </div>

                            <hr />
                          <div class="row" id="divReport" style="background-color:#fff; overflow-y:scroll; overflow-x:scroll; max-height:500px; " runat="server" visible="True">                         
                            <div class="col-md-28">
                    <asp:GridView ID="Acept_grdview" runat="server" AllowPaging="False" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="table" OnRowCommand="RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Order Id">
                                <ItemTemplate>
                                    <%--<asp:Label ID="OrderID" runat="server" Text='<%#Eval("ORDERID")%>'></asp:Label>  --%>
                                    <a href='../BS/TicketSummary.aspx?tin=<%#Eval("TICKETNO")%>&oid=<%#Eval("ORDERID")%>'
                                        rel="lyteframe" rev="width: 1000px; height: 500px; overflow:hidden;" target="_blank"
                                        style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #668aff">
                                        <asp:Label ID="TKTNO" runat="server" Text='<%#Eval("TICKETNO")%>'></asp:Label>(Summary)
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PNR">
                                <ItemTemplate>
                                    <asp:Label ID="lblPnr" runat="server" Text='<%#Eval("PNR")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pax ID" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="TID" runat="server" Text='<%#Eval("Counter")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Pax Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaxName" runat="server" Text='<%#Eval("PAXNAME")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                           
                              <asp:TemplateField HeaderText="Gender">
                                <ItemTemplate>
                                    <asp:Label ID="lblGender" runat="server" Text='<%#Eval("GENDER")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="AgentId">
                                <ItemTemplate>
                                    <asp:Label ID="lblAgentId" runat="server" Text='<%#Eval("AGENTID")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                               <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("REFUND_STATUS")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                             <asp:TemplateField HeaderText="API_Cancellation_Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus1" runat="server" Text='<%#Eval("API_CANCELLATION_STATUS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                               <asp:TemplateField HeaderText="Refund Amt">
                                <ItemTemplate>
                                    <asp:Label ID="lblRefund_Amt" runat="server" Text='<%#Eval("REFUND_AMT")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CancelCharge">
                                <ItemTemplate>
                                    <asp:Label ID="lblCancelCharge" runat="server" Text='<%#Eval("CancelCharge")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                             <asp:TemplateField HeaderText="Refund Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblRefunddate" runat="server" Text='<%#Eval("REFUNDEDDATE")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="PAYMENT_MODE">
                                <ItemTemplate>
                                    <asp:Label ID="lblPAYMENT_MODE" runat="server" Text='<%#Eval("PAYMENT_MODE")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                              <asp:TemplateField HeaderText="Refund Service_chrg">
                                <ItemTemplate>
                                    <asp:Label ID="lblRefundService_chrg" runat="server" Text='<%#Eval("REFUND_SERVICECHRG")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                         
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkreissue" runat="server" ForeColor="#004b91" Font-Strikeout="False"
                                        Font-Overline="False" Font-Size="11px" CommandArgument='<%#Eval("Counter") %>'
                                        CommandName="Accept" Text="Accept"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkreject" runat="server" ForeColor="Red" Font-Strikeout="False"
                                        Font-Overline="False" Font-Size="11px" CommandArgument='<%#Eval("Counter") %>'
                                        CommandName="Reject" Text="Reject"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reject Remark">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtRemark" runat="server" Height="47px" TextMode="MultiLine" Width="175px"
                                        Visible="false"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ControlStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkSubmit" runat="server" OnClick="btnCanFee_Click" OnClientClick="return Validate();" Visible="false"
                                        CommandArgument='<%# Eval("Counter") %>' CommandName="submit"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                    <asp:LinkButton ID="lnkHides" runat="server" OnClick="lnkHides_Click" Visible="false"
                                        CommandName="lnkHides" CommandArgument='<%# Eval("Counter") %>'><img src="../../Images/Cancel.png" alt="Cancel" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                           <%-- <asp:TemplateField HeaderText="Customer Remarks" ControlStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lbltotalfare1" runat="server" Text='<%#Eval("RegardingCancel")%>'></asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateField>  --%>                       
                        </Columns>                      
                   </asp:GridView>
                </div></div></div></div></div></div>
    </div>
</asp:Content>

