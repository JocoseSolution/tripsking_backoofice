﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Partial Class DetailsPort_BusReport
    Inherits System.Web.UI.Page
    Private STDom As New SqlTransactionDom()
    Dim DS As New DataSet()
    Dim splitfromDate As String()
    Dim splittoDate As String()
    Dim FromDate As String
    Dim ToDate As String
    Dim Status As String
    Dim grdds As New DataSet()
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("myCon").ConnectionString)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href='#' data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Bus</a><a class='current' href='#'>Bus Ticket Report</a>"
            Dim AgentID As String = ""
            Dim Status As String = "Booked"
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            If Not IsPostBack Then
                If Session("User_Type") = "AGENT" Then
                    td_Agency.Visible = False
                    td_Agency1.Visible = False
                    GrdBusReport.Columns(1).Visible = True
                End If
                Dim curr_date = Now.Date().ToString("yyyy-MM-dd") + " " + "00:00:01.000"
                Dim curr_date1 = Now().ToString("yyyy-MM-dd hh:mm:ss.mmm")
                grdds.Clear()
                grdds = USP_GetTicketDetail(curr_date, curr_date1, AgentID, "", "", "", "", "", "", Status, Session("UID").ToString, Session("User_Type").ToString, "")
                BindSales(grdds)
                GrdBusReport.DataSource = grdds
                GrdBusReport.DataBind()
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "-" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "-" + Strings.Left((Request("From")).Split(" ")(0), 2)
                FromDate = FromDate + " " + "00:00:01.000"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Right((Request("To")).Split(" ")(0), 4) & "-" & Left((Request("To")).Split(" ")(0), 2) & "-" & Mid((Request("To")).Split(" ")(0), 4, 2)
                ToDate = ToDate & " " & "23:59:59.999"
            End If
            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", Nothing, Request("hidtxtAgencyName"))
            Dim Source As String = If([String].IsNullOrEmpty(TxtSource.Text), Nothing, TxtSource.Text.Trim)
            Dim Destination As String = If([String].IsNullOrEmpty(TxtDestination.Text), Nothing, TxtDestination.Text.Trim)
            Dim orderID As String = If([String].IsNullOrEmpty(txtOrderID.Text), Nothing, txtOrderID.Text.Trim)
            Dim BusOperator As String = If([String].IsNullOrEmpty(txtBusOperator.Text), Nothing, txtBusOperator.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txtTicketNo.Text), Nothing, txtTicketNo.Text.Trim)
            Dim Pnr As String = If([String].IsNullOrEmpty(txtPnr.Text), Nothing, txtPnr.Text.Trim)
            Dim userid As String = Session("User_Type").ToString()
            Dim loginid As String = Session("UID").ToString()           
            grdds = USP_GetTicketDetail(FromDate, ToDate, AgentID, Source, Destination, orderID, BusOperator, TicketNo, Pnr, Status, userid, loginid, "")
            Session("grdds") = grdds
            BindSales(grdds)
            GrdBusReport.DataSource = grdds
            GrdBusReport.DataBind()
            ClearInputs(Page.Controls)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Private Sub BindSales(ByVal ds As DataSet)
        Try
            Dim dt As DataTable
            Dim Db As String = ""
            Dim sum As Double = 0
            dt = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    Db = dr("TA_NET_FARE").ToString()
                    If Db Is Nothing OrElse Db = "" Then
                        Db = 0
                    Else
                        sum += Db
                    End If
                Next
            End If
            lbl_Total.Text = "0"
            If sum <> 0 Then
                lbl_Total.Text = sum.ToString
            End If
            lbl_Sales.Text = dt.Rows.Count
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try            
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                FromDate = Strings.Right((Request("From")).Split(" ")(0), 4) + "-" + Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "-" + Strings.Left((Request("From")).Split(" ")(0), 2)
                FromDate = FromDate + " " + "00:00:01.000"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Right((Request("To")).Split(" ")(0), 4) & "-" & Left((Request("To")).Split(" ")(0), 2) & "-" & Mid((Request("To")).Split(" ")(0), 4, 2)
                ToDate = ToDate & " " & "23:59:59.999"
            End If
            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", Nothing, Request("hidtxtAgencyName"))
            Dim Source As String = If([String].IsNullOrEmpty(TxtSource.Text), Nothing, TxtSource.Text.Trim)
            Dim Destination As String = If([String].IsNullOrEmpty(TxtDestination.Text), Nothing, TxtDestination.Text.Trim)
            Dim orderID As String = If([String].IsNullOrEmpty(txtOrderID.Text), Nothing, txtOrderID.Text.Trim)
            Dim BusOperator As String = If([String].IsNullOrEmpty(txtBusOperator.Text), Nothing, txtBusOperator.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txtTicketNo.Text), Nothing, txtTicketNo.Text.Trim)
            Dim Pnr As String = If([String].IsNullOrEmpty(txtPnr.Text), Nothing, txtPnr.Text.Trim)
            Dim userid As String = Session("User_Type").ToString()
            Dim loginid As String = Session("UID").ToString()
            'If ddl_Status.SelectedValue = "select" Then
            '    Status = Nothing
            'Else
            '    Status = ddl_Status.SelectedValue
            'End If
            grdds = USP_GetTicketDetail(FromDate, ToDate, AgentID, Source, Destination, orderID, BusOperator, TicketNo, Pnr, Status, userid, loginid, "")
            BindSales(grdds)
            STDom.ExportData(grdds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Public Function USP_GetTicketDetail(ByVal FromDate As String, ByVal ToDate As String, ByVal AgentID As String, ByVal Source As String, ByVal Destination As String, ByVal OrderID As String, ByVal BusOperator As String, ByVal TicketNo As String, ByVal pnr As String, ByVal Status As String, ByVal usertype As String, ByVal LoginId As String, ByVal PaymentStatus As String) As DataSet
        Dim cmd As New SqlCommand()
        Dim ds As New DataSet()
        Try
            Dim con1 As New SqlConnection(ConfigurationManager.ConnectionStrings("myCon").ConnectionString)
            Dim adap As SqlDataAdapter
            adap = New SqlDataAdapter("SP_BUS_REPORT", con)
            adap.SelectCommand.CommandType = CommandType.StoredProcedure
            adap.SelectCommand.Parameters.AddWithValue("@FromDate", FromDate)
            adap.SelectCommand.Parameters.AddWithValue("@ToDate", ToDate)
            adap.SelectCommand.Parameters.AddWithValue("@ORDERID", OrderID)
            adap.SelectCommand.Parameters.AddWithValue("@SOURCE", Source)
            adap.SelectCommand.Parameters.AddWithValue("@DESTINATION", Destination)
            adap.SelectCommand.Parameters.AddWithValue("@BUSOPERATOR", BusOperator)
            adap.SelectCommand.Parameters.AddWithValue("@TICKETNO", TicketNo)
            adap.SelectCommand.Parameters.AddWithValue("@PNR", pnr)
            adap.SelectCommand.Parameters.AddWithValue("@Status", Status)
            adap.SelectCommand.Parameters.AddWithValue("@AgentID", AgentID)
            adap.SelectCommand.Parameters.AddWithValue("@usertype", Session("User_Type").ToString())
            adap.SelectCommand.Parameters.AddWithValue("@LoginID", Session("UID").ToString())
            adap.SelectCommand.Parameters.AddWithValue("@PaymentStatus", PaymentStatus)
            adap.Fill(ds)
            cmd.Connection = con1
            con1.Open()
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds)
            cmd.Dispose()
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
        Return ds
    End Function
    Private Sub ClearInputs(ctrls As ControlCollection)
        Try
            For Each ctrl As Control In ctrls
                If TypeOf ctrl Is TextBox Then
                    DirectCast(ctrl, TextBox).Text = String.Empty
                End If
                ClearInputs(ctrl.Controls)
            Next
        Catch ex As Exception
        End Try

    End Sub
    Protected Sub GrdBusReport_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdBusReport.PageIndexChanging
        Try
            GrdBusReport.PageIndex = e.NewPageIndex
            GrdBusReport.DataSource = Session("grdds")
            GrdBusReport.DataBind()
            If DirectCast(GrdBusReport.DataSource, DataSet).Tables(0).Rows.Count > 0 Then
                GrdBusReport.Visible = True
            Else
                GrdBusReport.Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub 
End Class
