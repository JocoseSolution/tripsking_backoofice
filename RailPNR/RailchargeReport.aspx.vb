﻿Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Data
Imports System.Data.SqlClient
Partial Class RailPNR_RailchargeReport
    Inherits System.Web.UI.Page

    Private STDom As New SqlTransactionDom
    Private ST As New SqlTransaction

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            CType(Page.Master.FindControl("lblBC"), Label).Text = "<a title='' class='tip-bottom' href=" + ResolveClientUrl("~/DetailsPort/PrivilegePanel/Splashboard.aspx") + " data-original-title='Go to Home'><i class='icon-home'></i> Home</a><a title='' class='tip-bottom' href='#' data-original-title=''>Rail Import</a><a class='current' href='#'>Rail Details</a>"
            Response.Cache.SetCacheability(HttpCacheability.NoCache)

            If Session("UID") = "" Or Session("UID") Is Nothing Then
                Response.Redirect("~/Login.aspx")
            End If
            
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub btn_search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_search.Click
        Try
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If

            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))
            Dim DistrID As String = If([String].IsNullOrEmpty(Request("hidtxtStockistName")) Or Request("hidtxtStockistName") = "DISTRIBUTOR", "", Request("hidtxtStockistName"))
            Dim BookingType As String = ""
            Dim SearchType As String = ""
            Dim dtsaerch As New DataTable
            
            dtsaerch = STDom.GetchargeDetails(FromDate, ToDate, AgentID, DistrID).Tables(0)
            Dim sum As Integer = 0
            Dim sum1 As Integer = 0
            Dim sum2 As Integer = 0
            Dim sum3 As Integer = 0
            Dim sum4 As Integer = 0
            Dim i As Integer
            For i = 0 To dtsaerch.Rows.Count - 1
                sum += Convert.ToInt32(dtsaerch.Rows(i)("Amount"))
                sum1 += Convert.ToInt32(dtsaerch.Rows(i)("pg"))
                sum2 += Convert.ToInt32(dtsaerch.Rows(i)("agcharge"))
                sum3 += Convert.ToInt32(dtsaerch.Rows(i)("Totalfare"))
                sum4 += Convert.ToInt32(dtsaerch.Rows(i)("tid"))
            Next

            ViewState("dtsaerch") = dtsaerch
            Grid_Ledger.DataSource = dtsaerch
            Grid_Ledger.DataBind()
            Dim rowIdx As Integer = dtsaerch.Rows.Count - 1


            'Dim Amount As Label = DirectCast(Grid_Ledger.Rows(rowIdx).Cells(2).FindControl("Amount"), Label)
            'Dim pg As Label = DirectCast(Grid_Ledger.Rows(rowIdx).Cells(3).FindControl("pg"), Label)
            'Dim agcharge As Label = DirectCast(Grid_Ledger.Rows(rowIdx).Cells(4).FindControl("agcharge"), Label)
            'Dim Totalfare As Label = DirectCast(Grid_Ledger.Rows(rowIdx).Cells(5).FindControl("Totalfare"), Label)
            'lbltktare.Text = "Gross Total"
            lblpnr.Text = sum4.ToString()
            lbltktare.Text = sum.ToString()
            lblpg.Text = sum1.ToString()
            lblAg.Text = sum2.ToString()
            lbltotamount.Text = sum3.ToString()



        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub

    Protected Sub Grid_Ledger_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles Grid_Ledger.PageIndexChanging
        Grid_Ledger.PageIndex = e.NewPageIndex
        Grid_Ledger.DataSource = ViewState("dtsaerch")
        Grid_Ledger.DataBind()

    End Sub


    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If
           Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))
            Dim DistrID As String = If([String].IsNullOrEmpty(Request("hidtxtStockistName")) Or Request("hidtxtStockistName") = "DISTRIBUTOR", "", Request("hidtxtStockistName"))
            Dim dtsaerch As New DataSet
            dtsaerch = STDom.GetchargeDetails(FromDate, ToDate, AgentID, DistrID)

            STDom.ExportData(dtsaerch)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub

End Class
