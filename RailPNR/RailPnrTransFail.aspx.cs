﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RailPNR_RailPnrTransFail : System.Web.UI.Page
{
    string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null)
        {
            if (!IsPostBack)
            {
                BindGridview();
            }
        }
        else
        {
            Response.Redirect("~/Login.aspx");
        }
    }
    public static string excelCS;
    protected void btnremove_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("Sp_Getraildata", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@type", "CLEARFAIL");
            con.Open();
            cmd.ExecuteNonQuery();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Cleared Please ReImport')", true);
            con.Close();
            BindGridview();

        }

    }
    protected void btnledger_Click(object sender, EventArgs e)
    {
        try
        {
            SqlConnection con = new SqlConnection(CS);
            con.Open();
            SqlCommand cmd = new SqlCommand("sp_refundFailrailpnr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 120;
            // cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.Output;
            int issuccess = cmd.ExecuteNonQuery();
            // int id = (int)cmd.Parameters["@return"].Value;          
            con.Close();

            if (issuccess > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ledger Inserted Successfully')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Error : Ledger Not Inserted Successfully')", true);
            }
        }

        catch (Exception ex)
        {

            Response.Write("<script language=javascript>alert('" + ex.Message + "');</script>");
        }
        BindGridview();

    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        lblmsg.Text = string.Empty;
        if (FileUpload1.PostedFile != null)
        {
            try
            {
                string path = string.Concat(Server.MapPath("~/FailUploadFile/" + FileUpload1.FileName));
                FileUpload1.SaveAs(path);
                excelCS = @"provider=Microsoft.ACE.OLEDB.12.0;data source=" + path + ";extended properties=" + "\"excel 5.0;hdr=yes;\"";
                using (OleDbConnection con = new OleDbConnection(excelCS))
                {
                    OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", con);
                    con.Open();
                    DbDataReader dr = cmd.ExecuteReader();
                    SqlBulkCopy bulkInsert = new SqlBulkCopy(CS);
                    bulkInsert.DestinationTableName = "Tbl_RailFailtransaction";
                    bulkInsert.WriteToServer(dr);
                    BindGridview();
                    lblMessage.Text = "Your file uploaded successfully";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = "Your file not uploaded";
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblmsg.Text = ex.Message;
            }
        }
    }


    private void BindGridview()
    {
        string message = "";
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("Sp_Getraildata", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@type", "REFUNDTRANS");
            SqlDataAdapter sdr = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sdr.Fill(ds);
            con.Open();

            GridView1.DataSource = ds.Tables[0];
            GridView1.DataBind();

            if (ds.Tables[0].Rows.Count != 0)
            {
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    message += ds.Tables[1].Rows[i]["TRANSACTION_ID"].ToString() + '(' + ds.Tables[1].Rows[i]["TRANSACTION_IDCOUNT"].ToString() + ')' + ',';
                }
            }

            if (!string.IsNullOrEmpty(message))
            {
                lblmsg.Text = message + "Please Check these TransactionId!Then Insert Into Ledger ";
            }

        }
    }
}





