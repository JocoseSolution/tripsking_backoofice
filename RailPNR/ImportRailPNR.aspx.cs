﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RailPNR_ImportRailPNR : System.Web.UI.Page
{
    string CS = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGridview();
        } 
    }
    public static string excelCS; 
    protected void btnUpload_Click(object sender, EventArgs e)
    {
string path ="";
        if (FileUpload1.PostedFile != null)
        {
            try
            {
                
               path  = string.Concat(Server.MapPath("~/UploadFile/" + FileUpload1.FileName));
                FileUpload1.SaveAs(path);
                //excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 12.0;";
             
              // excelCS = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
                excelCS = @"provider=microsoft.jet.oledb.4.0;data source=" + path + ";extended properties=" + "\"excel 5.0;hdr=yes;\""; 


                using (OleDbConnection con = new OleDbConnection(excelCS))
                {
                    OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", con);
                    con.Open();
                    // Create DbDataReader to Data Worksheet  
                    DbDataReader dr = cmd.ExecuteReader();
                    // SQL Server Connection String  
                   
                    // Bulk Copy to SQL Server   
                    SqlBulkCopy bulkInsert = new SqlBulkCopy(CS);
                    bulkInsert.DestinationTableName = "Tbl_RailPnrDetail";
                    bulkInsert.WriteToServer(dr);
                    BindGridview();
                    lblMessage.Text = "Your file uploaded successfully";
                    lblMessage.ForeColor = System.Drawing.Color.Green;
                }
            }
            catch (Exception ex)
            {
                Response.Write("<script language=javascript>alert('"+ex.Message+"');</script>");
                //lblMessage.Text = "Your file not uploaded";
                //lblMessage.ForeColor = System.Drawing.Color.Red;
lblmsg.Text = ex.Message+path;
            }
        }
    }


    private void BindGridview()
    {
        string message = "";
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("Sp_Getraildata", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@type", "BOOKED");
            SqlDataAdapter sdr = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            sdr.Fill(ds);
            con.Open();

                GridView1.DataSource = ds.Tables[0];
                GridView1.DataBind();
                lblmsg.Text = string.Empty;
            if (ds.Tables[0].Rows.Count != 0)
            {

                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    message += ds.Tables[1].Rows[i]["TRANSACTION_ID"].ToString() + '(' + ds.Tables[1].Rows[i]["TRANSACTION_IDCOUNT"].ToString() + ')' + ',';
                }
            }
            if (!string.IsNullOrEmpty(message))
            {
                lblmsg.Text = message + "Please Check these TransactionId!Then Insert Into Ledger ";
            }

        }
    }
    protected void btnledger_Click(object sender, EventArgs e)
    {
        //SqlDataAdapter sdr = new SqlDataAdapter("select * from Tbl_RailPnrDetail where Ledgerentry=false", CS);
        //DataTable dt = new DataTable();
        //sdr.Fill(dt);
        //SqlConnection con = new SqlConnection(CS);
        //con.Open();
        //SqlCommand cmd = new SqlCommand("sp_insertledgerRailpnr",con);
        //cmd.CommandType = CommandType.StoredProcedure;
        //for (int i = 0; i < dt.Rows.Count; i++)
        //{

        //    cmd.Parameters.AddWithValue("@TRANSACTION_ID",dt.Rows[i]["TRANSACTION_ID"].ToString());
        //    cmd.Parameters.AddWithValue("@PNR_NO", dt.Rows[i]["PNR_NO"].ToString());
        //    cmd.Parameters.AddWithValue("@BOOKING_AMOUNT",Convert.ToDecimal(dt.Rows[i]["BOOKING_AMOUNT"]));
        //    cmd.Parameters.AddWithValue("@PGNAME", dt.Rows[i]["PG NAME"].ToString());
        //    cmd.Parameters.AddWithValue("@CLASS", dt.Rows[i]["CLASS"].ToString());
        //    cmd.Parameters.AddWithValue("@USER_ID", dt.Rows[i]["USER_ID"].ToString());
        //    cmd.ExecuteNonQuery();


        //}
        //con.Close();
        try
        {
           // string message = "";
            SqlConnection con = new SqlConnection(CS);
            con.Open();
            SqlCommand cmd = new SqlCommand("Sp_raildatainsertledger", con);
            cmd.CommandType = CommandType.StoredProcedure;
           // cmd.Parameters.AddWithValue("@IP", Request.UserHostAddress.ToString());
            //cmd.Parameters.Add("@ERROR", SqlDbType.Char, 500);
            //cmd.Parameters["@ERROR"].Direction = ParameterDirection.Output;
            int i=cmd.ExecuteNonQuery();
            //message = (string)cmd.Parameters["@ERROR"].Value;
            con.Close();
            if (i > 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ledger Inserted Successfully')", true);
            }
        }
        catch (Exception ex)
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No Records Found')", true);
        }

    }
    protected void btnremove_Click(object sender, EventArgs e)
    {
        using (SqlConnection con = new SqlConnection(CS))
        {
            SqlCommand cmd = new SqlCommand("Sp_Getraildata", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@type", "CLEARBOOKED");
            con.Open();
            cmd.ExecuteNonQuery();
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Cleared Please ReImport')", true);
            con.Close();
            BindGridview();
        }
    }
}

