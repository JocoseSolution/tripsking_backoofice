﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="IrctcMarkup.aspx.cs" Inherits="RailPNR_IrctcMarkup" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
function checkitt(evt) {
            evt = (evt) ? evt : window.event
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (!(charCode > 47 && charCode < 58)) {
                return false;
            }
            status = "";
            return true;
        }

function isNumberKey(evt)
       {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
       }
 
</script>

      <div class="row">
       <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
        
                    <div class="panel-body">

                        <div class="row" >
                                 <div class="col-md-2">
                                <label >Type :</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="ddl_type" runat="server" required="required">
                                </asp:DropDownList>
                            </div>
                               <div class="col-md-2">
                                <label >Class :</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="ddl_class" runat="server">
                                    <asp:ListItem Value="AC">AC</asp:ListItem>
                                    <asp:ListItem Value="SL">SL</asp:ListItem>
<asp:ListItem Value="CC">CC</asp:ListItem>
<asp:ListItem Value="2S">2S</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <label >Agent Charge:</label>
                                <div class="input-group">
                                    <input type="text" placeholder="Agent Charge" name="Agent_Charge" onkeypress="return checkitt(event)" id="txt_agCharge" runat="server" MaxLength="2"  class="form-control input-text full-width" required="required" />
                                </div>
                            </div>
                                  <div class="col-md-2">
                                      <label >Pg Charge:</label>
                                    <input type="text" placeholder="PG Charge" name="Pg_Charge" onkeypress="return isNumberKey(event)" id="txt_pgcharge" MaxLength="4" runat="server" class="form-control input-text full-width" required="required"/>
                            </div>
                            <div class="col-md-2">
                                      <label >Pg Charge On Fair>2000:</label>
                                <asp:DropDownList CssClass="input-text full-width" ID="ddlac_Charge" runat="server">
                                    <asp:ListItem Value="0" Selected="True">0</asp:ListItem>
                                    <asp:ListItem Value="1">1</asp:ListItem>
                                    </asp:DropDownList>
                            </div>

                            <div class="col-md-2" style="margin-top: 28px;">
                            <div class="input-group">

                                <asp:Button ID="btnSubmit" CssClass="btn btn-success" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                            </div>
                        </div>
                            
                            </div>
                               <asp:GridView ID="cityGridview" runat="server" CssClass="table table-striped table-bordered table-hover" EmptyDataText="Sorry,no city found..!" AutoGenerateColumns="False"  OnRowDeleting="cityGridview_RowDeleting">
                        <Columns>
                                  <asp:TemplateField HeaderText="Id">
                                <ItemTemplate>
                               
                                    <asp:Label ID="lblid" runat="server" Text='<%# Eval("ROW") %>'></asp:Label>
                                </ItemTemplate>
                           
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                <ItemTemplate>
                               
                                    <asp:Label ID="lbltype" runat="server" Text='<%# Eval("Type") %>'></asp:Label>
                                </ItemTemplate>
                           
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Class">
                                <ItemTemplate>
                                    <asp:Label ID="lblclass" runat="server" Text='<%# Eval("Class") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Agent Charge">
                                <ItemTemplate>
                                    <asp:Label ID="lblagcharge" runat="server" Text='<%# Eval("Agent_Charge") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                               <asp:TemplateField HeaderText="Pg Charge">
                                <ItemTemplate>
                                    <asp:Label ID="lblpgcharge" runat="server" Text='<%# Eval("PG_Charge") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Pg Charge On Fair>2000">
                                <ItemTemplate>
                                    <asp:Label ID="lblpgchargeONAC" runat="server" Text='<%# Eval("PG_ChargeONAC") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                     
                     
                            <asp:TemplateField HeaderText="Action" ShowHeader="False">
                         
                                <ItemTemplate>
                                   
                                    &nbsp;&nbsp;&nbsp;
                                    <asp:ImageButton ID="btnLinkDelete" ImageUrl="~/Images/icons/Recycle_Bin_Full.png" style="width: 23px;"  runat="server" formnovalidate="" CommandName="Delete" Text="Delete"></asp:ImageButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                        </div>
                    </div>
                </div>
           </div>
        </div>
</asp:Content>

