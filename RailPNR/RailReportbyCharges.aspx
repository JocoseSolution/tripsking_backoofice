﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="RailReportbyCharges.aspx.vb" Inherits="RailPNR_RailReportbyCharges" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <script type="text/javascript">
            $(document).ready(function () {
                $("#From").datepicker({ dateFormat: 'dd-mm-yy' }).val();
                $(".cd").click(function () {
                    $("#From").focus();
                }
                );
                $("#To").datepicker({ dateFormat: 'dd-mm-yy' }).val();
                $(".cd1").click(function () {
                    $("#To").focus();
                }
                );
            });
    </script>


       <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>

    <div class="row">
       <div class="container-fluid" style="padding-right:35px;">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
        
                    <div class="panel-body">

                        <div class="row" >
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputEmail1">From Date</label>--%>
                                    <input type="text" placeholder="FROM DATE" name="From" id="From" readonly="readonly" class="form-control input-text full-width" id="dp1539064332191" />
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar cd" style="cursor: pointer;" ></span>
                               </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputEmail1">To Date</label>--%>
                                    <input type="text" placeholder="TO DATE" name="To" id="To" readonly="readonly" class="form-control input-text full-width" />
                                     <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar cd1"  style="cursor: pointer;" ></span>
                               </span>
                                </div>
                            </div>
                           
                            <div class="col-md-2">
                                <div class="input-group" id="tr_Agency" runat="server">
                                    <%--<label for="exampleInputEmail1">Agency</label>--%>
                                    <span id="tr_AgencyName" runat="server">
                                        <input type="text" placeholder="AGENCY NAME" id="txtAgencyName" name="txtAgencyName" <%--onfocus="focusObj(this);"--%>
                                           <%-- onblur="blurObj(this);"--%> <%--defvalue="Agency Name or ID"--%> autocomplete="off" <%--value="Agency Name or ID"--%> class="form-control input-text full-width" />
                                        <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" /></span>
                                      <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-black-tie"></span>
                               </span>
                                </div>
                            </div>

                              <div class="col-md-2">
                                <div class="input-group" id="Div2" runat="server">
                                
                                    <span id="Span1" runat="server">
                                        <input type="text" placeholder="DISTRIBUTOR" id="txtStockistName" name="txtStockistName"
                                           autocomplete="off" class="form-control input-text full-width" />
                                        <input type="hidden" id="hidtxtStockistName" name="hidtxtStockistName" value="" /></span>
                                      <span class="input-group-addon" style="background:#49cced">
                                  <span class="fa fa-black-tie"></span>
                               </span>
                                </div>
                            </div>
                                                        <div class="col-md-2">
                                <div class="input-group">
                                    <%--<label for="exampleInputEmail1">From Date</label>--%>
                                    <input type="text" placeholder="PNR NO." name="pnrno" id="pnrno"  class="form-control input-text full-width" />
                                    <span class="input-group-addon" style="background:#49cced">
                                  <span class="glyphicon glyphicon-calendar cd" style="cursor: pointer;" ></span>
                               </span>
                                </div>
                            </div>

                            

                            <div class="col-md-1">
                                <div class="input-group">
                               
                           <asp:Button ID="btn_search" runat="server" Text="Find" CssClass="btn btn-success" />
                       </div>
                            </div>  
                                <div class="col-md-1">
                                <div class="input-group">
                                
                                   <asp:Button ID="btn_export" runat="server" Text="Export To Excel" CssClass="btn btn-success"/>
                                </div>
                            </div> 

                        

                        </div>



                        <div class="row" style="margin-top:28px" >
                              <div class="col-md-2">
                                <div class="input-group">
                                    Total PNR:
                                    <asp:Label ID="lblpnr" runat="server"></asp:Label>
                                    </div>
                                </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    Total Ticket Fare:
                                    <asp:Label ID="lbltktare" runat="server"></asp:Label>
                                    </div>
                                </div>
                            <div class="col-md-2">
                                <div class="input-group">
                                    Total Pg:
                                    <asp:Label ID="lblpg" runat="server"></asp:Label>
                                    </div>
                                </div>

                            <div class="col-md-2">
                                <div class="input-group">
                                    Total Agent Charge:
                                    <asp:Label ID="lblAg" runat="server"></asp:Label>
                                    </div>
                                </div>

                            <div class="col-md-2">
                                <div class="input-group">
                                    Total Amount:
                                    <asp:Label ID="lbltotamount" runat="server"></asp:Label>
                                    </div>
                                </div>

                            </div>


                        <br />
                        <div id="Div1" class="row" style="background-color: #fff; overflow-y: scroll;" runat="server" visible="true">
                            <div class="col-md-12">

                                <asp:UpdatePanel ID="up" runat="server">
                                    <ContentTemplate>
                                        <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CssClass="table table-striped table-bordered table-hover" style="text-transform: uppercase;" GridLines="None" PageSize="30">

                                            <Columns>
                                                <asp:BoundField DataField="Agencyname" HeaderText="Agentid" />
                                                <%--<asp:BoundField DataField="ReferenceNo" HeaderText="ReferenceNo" />--%>
                                                <%--<asp:BoundField DataField="ReservationId" HeaderText="ReservationId" />--%>
                                                <asp:BoundField DataField="tid" HeaderText="No of Pnr" />
                                                <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                                <asp:BoundField DataField="pg" HeaderText="Pg charges"  />
                                                <asp:BoundField DataField="agcharge" HeaderText="Agent charges"  />
                                                <asp:BoundField DataField="Totalfare" HeaderText="Total Fare"  />
                                                 <asp:BoundField DataField="ReservationId" HeaderText="PNR No."  />
                                                 <%--<asp:BoundField DataField="type" HeaderText="Agent Type"  />--%>
                                                <%-- <asp:BoundField HeaderText="Payment Mode" DataField="PaymentMode" ></asp:BoundField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
</asp:Content>

