﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplicationpro.WebForm1" %>--%>

<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="false" CodeFile="ProfileCheck.aspx.cs" Inherits="ProfileCheck" %>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
           <div class="col-md-12 container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">
    <div class="Container-fluid" style="background: white">

       <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />

      
      



        <div class="row" style="background-repeat:no-repeat;background-size:cover">
            <div class="col-sm-10" style="margin-left:85px">
                <h1>Profile</h1>
            </div>
            <div class="col-sm-2">
                <a href="/users" class="pull-right"></a>
                   <%--<img title="profile image" class="img-circle img-responsive" src="http://www.gravatar.com/avatar/28fd20ccec6865e2d5f0e1f4446eb7bf?s=100"--%>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <!--left col-->


                <div class="text-center">
                    <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" class="avatar img-circle img-thumbnail" alt="avatar">
                    
                   <%-- <input type="file" class="text-center center-block file-upload">--%>
                </div>
                <br />


                <div class="panel panel-default">
                    <div class="panel-heading">Website <i class="fa fa-link fa-1x"></i></div>
                    <div class="panel-body"><a href="http://Travel villa.com">Travel villa.com</a></div>
                </div>


                <ul class="list-group">
                    <li class="list-group-item text-muted">Activity <i class="fa fa-dashboard fa-1x"></i></li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Number Of Executive</strong></span> 0</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Number Of SalesExecutive</strong></span> 0</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Number Of  Merchandising </strong></span> 0</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Number Of Companies</strong></span> 0</li>
                    <li class="list-group-item text-right"><span class="pull-left"><strong>Number Of Employees</strong></span> 0</li>
                
                     </ul>

                <div class="panel panel-default">
                    <div class="panel-heading">Social MediSocial Media</div>
                  
                </div>

            </div>
            <!--/col-3-->
            <div class="col-sm-9" style="top:21px">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Home </a> </li>               
                          

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="home">
                      
                        <form class="form" action="##" method="post" id="registrationForm">
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="User Id">
                                        User Id 
                                    </label>
                                    <asp:TextBox ID="Userid1" Style="height: 36px; width: 301px;" CssClass="form-control" ValidationGroup="LoginFrame" runat="server" placeholder="User ID" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" ForeColor="Red" ControlToValidate="Userid1" ValidationGroup="LoginFrame" runat="server" ErrorMessage="userid Required"></asp:RequiredFieldValidator>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="Name">
                                        Name
                                    </label>
                                    <asp:TextBox ID="TextBox4" Style="height: 36px; width: 301px;" CssClass="form-control" ValidationGroup="LoginFrame" runat="server" placeholder="NAME" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="TextBox4" ValidationGroup="LoginFrame" runat="server" ErrorMessage="Name Required"></asp:RequiredFieldValidator>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="Password">
                                        Password
                                    </label>
                                    <asp:TextBox ID="TextBox1" Style="height: 36px; width: 301px;" CssClass="form-control" ValidationGroup="LoginFrame" runat="server" placeholder="Password" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="TextBox1" ValidationGroup="LoginFrame" runat="server" ErrorMessage="Password Required"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-6" >
                                    <label for="email">
                                        Email
                                    </label>
                                    <asp:TextBox ID="TextBox5" Style="height: 36px; width: 301px;" CssClass="form-control" ValidationGroup="LoginFrame" runat="server" placeholder="Email" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="TextBox5" ValidationGroup="LoginFrame" runat="server" ErrorMessage="Email Required"></asp:RequiredFieldValidator>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-xs-6" >
                                    <label for="Mobile">
                                        Mobile
                                    </label>

                                    <asp:TextBox ID="TextBox6" Style="height: 36px; width: 301px;" CssClass="form-control" ValidationGroup="LoginFrame" runat="server" placeholder="Mobile" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ForeColor="Red" ControlToValidate="TextBox6" ValidationGroup="LoginFrame" runat="server" ErrorMessage="Mobile Required"></asp:RequiredFieldValidator>

                                </div>
                            </div>



                            <div class="form-group" >
                                <div class="col-xs-6" >
                                    <label for="Status">
                                        Status
                                    </label>


                                    <asp:TextBox ID="TextBox7" Style="height: 36px; width: 301px;" CssClass="form-control" ValidationGroup="LoginFrame" runat="server" placeholder="Mobile" ReadOnly="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" ControlToValidate="TextBox7" ValidationGroup="LoginFrame" runat="server" ErrorMessage="Mobile Required"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-12">
                                    <br />
                                    <asp:Button ID="bt" runat="server" CssClass="btn btn-lg btn-success" Text="Edit" OnClick="bt_Click" />
                                    <asp:Button ID="Button1" runat="server" CssClass="btn btn-lg btn-success" ValidationGroup="LoginFrame" Text="Update" Visible="false" OnClick="Button1_Click1" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
                        </div>
                    </div>
                </div>
               </div>  
</asp:Content>
