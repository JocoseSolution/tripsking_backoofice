﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageSignIn.master" AutoEventWireup="true" CodeFile="DTask.aspx.cs" Inherits="FlightReports_DTask" %>

<%@ Register Src="~/FlightReports/D_task/DomHoldPNRUpdate.ascx" TagPrefix="Search" TagName="DomHoldPNRUpdate" %>
<%@ Register Src="~/FlightReports/D_task/TktRptDom_RefundInProcess.ascx" TagPrefix="Search1" TagName="TktRptDom_RefundInProcess" %>
<%@ Register Src="~/FlightReports/D_task/TktRptDom_RefundRequest.ascx" TagPrefix="Search2" TagName="TktRptDom_RefundRequest" %>
<%@ Register Src="~/FlightReports/D_task/TktRptDom_ReIssueInProcess.ascx" TagPrefix="tktrpt" TagName="TktRptDom_ReIssueInProcess" %>
<%@ Register Src="~/FlightReports/D_task/TktRptDom_ReIssueRequest.ascx" TagPrefix="Search4" TagName="TktRptDom_ReIssueRequest" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <%--    <script type="text/javascript">
         var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>--%>
            <div class="col-md-12 container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">

                    <div class="panel-body">
    <div class="container">

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">D-Hold PNR Update</a></li>
        <li><a data-toggle="tab" href="#menu1">D-Refund Request</a></li>
    <li><a data-toggle="tab" href="#menu2">D-Refund In Process</a></li>
         <li><a data-toggle="tab" href="#menu3">D-ReIssue Request</a></li>
       <li><a data-toggle="tab" href="#menu4">D-ReIssue In Process</a></li>

  
  </ul>

  <div class="tab-content" style="overflow:hidden;">
    <div id="home" class="tab-pane fade in active">
     
     <Search:DomHoldPNRUpdate ID="DomHoldPNRUpdate1" runat="server" />
    </div>
      <div id="menu1" class="tab-pane fade">
              <Search2:TktRptDom_RefundRequest runat="server" ID="TktRptDom_RefundRequest1" />
   
      </div>
    <div id="menu2" class="tab-pane fade">
           <Search1:TktRptDom_RefundInProcess runat="server" ID="TktRptDom_RefundInProcess1" />

    </div>

      <div id="menu3" class="tab-pane fade">
     <Search4:TktRptDom_ReIssueRequest runat="server" ID="TktRptDom_ReIssueRequest1" />
  
    </div>

      <div id="menu4" class="tab-pane fade">
    
     <tktrpt:TktRptDom_ReIssueInProcess runat="server" ID="TktRptDom_ReIssueInProcess1" />
    </div>

     

  </div>
</div>
                        </div>
                    </div>
                </div>

            </div>


     
  




    
   
     
   <%--  <script type="text/javascript">
         jQuery.noConflict();
         $(document).ready(function () {
             $("#tabs").tabs();
         });
         </script>--%>

</asp:Content>

