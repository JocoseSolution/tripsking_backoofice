﻿Imports System.Collections.Generic
Imports System.Web
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration

Imports System

Imports System.Linq

Imports System.Web.UI
Imports System.Web.UI.WebControls
Partial Class FlightReports_Air_Reports_HoldPnrReport
    Inherits System.Web.UI.UserControl
    Dim Distr As New Distributor()
    Private STDom As New SqlTransactionDom()
    Dim AgencyDDLDS As New DataSet()
    Dim objDA As New SqlTransaction
    Dim con As New SqlConnection()
    Dim con1 As New SqlConnection()
    Dim adp As SqlDataAdapter
    Public pnrds As New DataSet
    Dim ds As New DataSet()
    'Private ST As New SqlTransaction()
    'Private STDom As New SqlTransactionDom()
    'Private con As New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    'Dim adap As SqlDataAdapter
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        If Session("UID") = "" Or Session("UID") Is Nothing Then
            Response.Redirect("~/Login.aspx")
        End If
        Try
            'If Session("User_Type").ToString().Trim().ToUpper().Equals("ADMIN") Then
            '    td_Agency.Visible = False
            'End If
            divPaymentMode.Visible = False
            divPartnerName.Visible = False
            If Session("User_Type") = "AGENT" Then
                td_Agency.Visible = False
            End If
            If Session("User_Type") = "EXEC" Then
                tr_ExecID.Visible = False
                tdTripNonExec1.Visible = False
                tdTripNonExec2.Visible = False
            End If
            'If Page.IsPostBack Then
            '    BindGrid(True)
            'Else
            '    BindGrid(False)
            'End If
            ''If Page.IsPostBack = False Then
            ''    CheckEmptyValue()
            ''End If
            If Not IsPostBack Then
                BindPartner()
                CheckEmptyValue()
                Dim dtExecutive As New DataTable
                Dim dtStatus As New DataTable
                Dim dsddls As New DataSet()
                dsddls = STDom.GetStatusExecutiveID("Hold")
                dtStatus = dsddls.Tables(0)
                dtExecutive = dsddls.Tables(1)

                ddl_ExecID.AppendDataBoundItems = True
                ddl_ExecID.Items.Clear()
                ddl_ExecID.Items.Insert(0, "--EXEC ID--")
                ddl_ExecID.DataSource = dtExecutive
                ddl_ExecID.DataTextField = "ExecutiveID"
                ddl_ExecID.DataValueField = "ExecutiveID"
                ddl_ExecID.DataBind()

                ddl_Status.AppendDataBoundItems = True
                ddl_Status.Items.Clear()
                ddl_Status.Items.Insert(0, "--SELECT TRIP--")
                ddl_Status.DataSource = dtStatus
                ddl_Status.DataTextField = "Status"
                ddl_Status.DataValueField = "Status"
                ddl_Status.DataBind()



                'AgencyDDLDS = objDA.GetAgencyDetailsDDL()
                'If AgencyDDLDS.Tables(0).Rows.Count > 0 Then
                '    ddl_AgencyName.AppendDataBoundItems = True
                '    ddl_AgencyName.Items.Clear()
                '    ddl_AgencyName.Items.Insert(0, "--Select Agency Name--")
                '    ddl_AgencyName.DataSource = AgencyDDLDS
                '    ddl_AgencyName.DataTextField = "Agency_Name"
                '    ddl_AgencyName.DataValueField = "user_id"
                '    ddl_AgencyName.DataBind()
                '    'BindGrid()
                'End If
            End If

        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try
    End Sub
    Public Sub BindGrid(ByVal isPost As Boolean)
        Try
            Dim dSet As New DataSet()
            ''''dSet = Distr.GetHoldPnrReport(Session("UID"), Session("User_Type"), "D")
            If isPost = True Then
                dSet = Distr.GetHoldPnrReportNew(Session("UID"), Session("User_Type"), IIf(ddlTripRefunDomIntl.SelectedItem.Value IsNot Nothing, ddlTripRefunDomIntl.SelectedItem.Value.Trim(), ""), _
                        IIf(Request("From") IsNot Nothing, Request("From").ToString(), ""), IIf(Request("To") IsNot Nothing, Request("To"), ""), _
                    IIf(txt_OrderId.Text IsNot Nothing, txt_OrderId.Text.Trim(), ""), IIf(txt_PNR.Text IsNot Nothing, txt_PNR.Text.Trim(), ""), _
                    IIf(txt_AirPNR.Text IsNot Nothing, txt_AirPNR.Text.Trim(), ""), IIf(txt_PaxName.Text IsNot Nothing, txt_PaxName.Text.Trim(), ""), IIf(txtPartnerName.Text IsNot Nothing, txtPartnerName.Text.Trim(), ""), IIf(txtPaymentmode.Text IsNot Nothing, txtPaymentmode.Text.Trim(), ""), _
                   IIf(txt_TktNo.Text IsNot Nothing, txt_TktNo.Text.Trim(), ""), "", "", "")
            Else
                dSet = Distr.GetHoldPnrReportNew(Session("UID"), Session("User_Type"), "", "", "", "", "", "", "", "", "", "", "", "", "")
            End If

            If dSet IsNot Nothing Then
                If dSet.Tables.Count > 0 Then
                    ''If dSet.Tables(0).Rows.Count > 0 Then
                    GridView1.DataSource = dSet
                    GridView1.DataBind()
                    If dSet.Tables(0).Rows.Count > 0 Then
                        divReport.Visible = True
                    Else
                        divReport.Visible = False
                    End If
                    ''End If
                End If
            End If
            'Dim ds As New DataSet()
            'If (Session("User_Type") = "AGENT") Then
            '    ds = ID.IntlHoldPNRAgentReport("Confirm", "D", Session("UID"), "InProcess")
            '    GridView1.DataSource = ds
            '    GridView1.DataBind()
            'Else
            '    ds = ID.IntlConfirmHoldPNR("Confirm", "D", "InProcess")
            '    GridView1.DataSource = ds
            '    GridView1.DataBind()
            'End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)
        End Try

    End Sub
    'Protected Sub GridHoldPNRAccept_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridHoldPNRAccept.PageIndexChanging

    'End Sub

    'Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
    '    Try
    '        GridView1.PageIndex = e.NewPageIndex
    '        '' BindGrid(Page.IsPostBack)
    '        CheckEmptyValue()


    '    Catch ex As Exception
    '        clsErrorLog.LogInfo(ex)

    '    End Try
    'End Sub
    Protected Sub GridView1_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        Try
            GridView1.PageIndex = e.NewPageIndex
            GridView1.DataSource = Session("pnrds")
            GridView1.DataBind()
            If DirectCast(GridView1.DataSource, DataSet).Tables(0).Rows.Count > 0 Then
                divReport.Visible = True
            Else
                divReport.Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub

    Public Sub CheckEmptyValue()
        Try
            pnrds.Clear()
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If

            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))


            Dim UserID As String = Session("UID").ToString
            Dim UserType As String = Session("User_Type")
            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PNR As String = If([String].IsNullOrEmpty(txt_PNR.Text), "", txt_PNR.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txt_TktNo.Text), "", txt_TktNo.Text.Trim)
            Dim Air As String = If([String].IsNullOrEmpty(txt_AirPNR.Text), "", txt_AirPNR.Text.Trim)
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
            If Partnername = "0" Then
                Partnername = ""
            End If
            Dim trip As String = "" ''''IIf(Session("User_Type") = "EXEC", IIf([String].IsNullOrEmpty(Session("TripExec")), "", Session("TripExec").ToString().Trim()), If([String].IsNullOrEmpty(ddlTripRefunDomIntl.SelectedItem.Value), "", ddlTripRefunDomIntl.SelectedItem.Value.Trim()))
            If Session("User_Type") = "EXEC" Then
                If [String].IsNullOrEmpty(Session("TripExec")) Then
                    trip = ""
                Else
                    trip = Session("TripExec").ToString().Trim()
                End If
            Else
                trip = If([String].IsNullOrEmpty(ddlTripRefunDomIntl.SelectedItem.Value), "", ddlTripRefunDomIntl.SelectedItem.Value.Trim())
            End If
            Dim ExecID As String
            If ddl_ExecID.SelectedIndex > 0 Then
                ExecID = ddl_ExecID.SelectedValue
            Else

                ExecID = ""
            End If

            Dim Status As String
            If ddl_Status.SelectedIndex > 0 Then
                Status = ddl_Status.SelectedValue
            Else
                Status = ""
            End If
            Dim distrHold As String = ""
            If UserType.Trim().ToUpper() = "DI" Then
                distrHold = UserID
            End If
            pnrds = Distr.GetHoldPnrReportNew(UserID, UserType, trip, FromDate, ToDate, OrderID, PNR, Air, PaxName, TicketNo, AgentID, ExecID, distrHold, Partnername, PaymentMode)
            'adp.Fill(pnrds)
            Session("pnrds") = pnrds
            GridView1.DataSource = pnrds
            GridView1.DataBind()
            If pnrds.Tables(0).Rows.Count > 0 Then
                divReport.Visible = True
            Else
                divReport.Visible = False
            End If
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try
    End Sub
    Protected Sub btn_result_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_result.Click
        CheckEmptyValue()
        txtPartnerName.SelectedValue = "0"
    End Sub

    Protected Sub btn_export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btn_export.Click
        Try
            pnrds.Clear()
            Dim FromDate As String
            Dim ToDate As String
            If [String].IsNullOrEmpty(Request("From")) Then
                FromDate = ""
            Else
                'FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + Strings.Left((Request("From")).Split(" ")(0), 2) + Strings.Right((Request("From")).Split(" ")(0), 4)

                FromDate = Strings.Mid((Request("From")).Split(" ")(0), 4, 2) + "/" + Strings.Left((Request("From")).Split(" ")(0), 2) + "/" + Strings.Right((Request("From")).Split(" ")(0), 4)
                FromDate = FromDate + " " + "12:00:00 AM"
            End If
            If [String].IsNullOrEmpty(Request("To")) Then
                ToDate = ""
            Else
                ToDate = Mid((Request("To")).Split(" ")(0), 4, 2) & "/" & Left((Request("To")).Split(" ")(0), 2) & "/" & Right((Request("To")).Split(" ")(0), 4)
                ToDate = ToDate & " " & "11:59:59 PM"
            End If

            Dim AgentID As String = If([String].IsNullOrEmpty(Request("hidtxtAgencyName")) Or Request("hidtxtAgencyName") = "Agency Name or ID", "", Request("hidtxtAgencyName"))

            Dim UserID As String = Session("UID").ToString
            Dim UserType As String = Session("User_Type")
            Dim OrderID As String = If([String].IsNullOrEmpty(txt_OrderId.Text), "", txt_OrderId.Text.Trim)
            Dim PNR As String = If([String].IsNullOrEmpty(txt_PNR.Text), "", txt_PNR.Text.Trim)
            Dim PaxName As String = If([String].IsNullOrEmpty(txt_PaxName.Text), "", txt_PaxName.Text.Trim)
            Dim TicketNo As String = If([String].IsNullOrEmpty(txt_TktNo.Text), "", txt_TktNo.Text.Trim)
            Dim Air As String = If([String].IsNullOrEmpty(txt_AirPNR.Text), "", txt_AirPNR.Text.Trim)
            Dim Partnername As String = txtPartnerName.SelectedItem.Value
            Dim PaymentMode As String = txtPaymentmode.SelectedItem.Value
            If Partnername = "0" Then
                Partnername = ""

            End If
            Dim trip As String = IIf(Session("User_Type") = "EXEC", IIf(Session("TripExec") IsNot Nothing, Session("TripExec").ToString().Trim(), ""), If([String].IsNullOrEmpty(ddlTripRefunDomIntl.SelectedItem.Value), "", ddlTripRefunDomIntl.SelectedItem.Value.Trim()))
            Dim ExecID As String
            If ddl_ExecID.SelectedIndex > 0 Then
                ExecID = ddl_ExecID.SelectedValue
            Else

                ExecID = ""
            End If

            Dim Status As String
            If ddl_Status.SelectedIndex > 0 Then
                Status = ddl_Status.SelectedValue
            Else
                Status = ""
            End If
            Dim distrHold As String = ""
            If UserType.Trim().ToUpper() = "DI" Then
                distrHold = UserID
            End If
            pnrds = Distr.GetHoldPnrReportNew(UserID, UserType, trip, FromDate, ToDate, OrderID, PNR, Air, PaxName, TicketNo, AgentID, ExecID, distrHold, Partnername, PaymentMode)
            STDom.ExportData(pnrds)
        Catch ex As Exception
            clsErrorLog.LogInfo(ex)

        End Try

    End Sub
    Public Sub BindPartner()
        Dim constr As String = ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString
        Dim con As New SqlConnection(constr)
        Dim cmd As New SqlCommand("BindPartnerNameSP_PP")

        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = con
        con.Open()
        txtPartnerName.DataSource = cmd.ExecuteReader()
        txtPartnerName.DataTextField = "PartnerName"
        txtPartnerName.DataValueField = "PartnerName"


        txtPartnerName.DataBind()
        con.Close()
        txtPartnerName.Items.Insert(0, New ListItem("--Select PartnerName--", "0"))
    End Sub
End Class