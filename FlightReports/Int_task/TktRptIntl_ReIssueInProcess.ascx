﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TktRptIntl_ReIssueInProcess.ascx.vb" Inherits="FlightReports_Int_task_TktRptIntl_ReIssueInProcess" %>

 <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }
    </style>
     <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
     <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        function Validate() {
            if (document.getElementById("ctl00_ContentPlaceHolder1_ReIssueInProcessGRD_ctl02_txtRemark").value == "") {

                alert('Remark can not be blank,Please Fill Remark');
                document.getElementById("ctl00_ContentPlaceHolder1_ReIssueInProcessGRD_ctl02_txtRemark").focus();
                return false;
            }
            if (confirm("Are you sure you want to Reject!"))
                return true;
            return false;
        }
    </script>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="container-fluid" style="padding-right: 35px">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
              
                    <div class="panel-body">
                        <hr />
                        <div class="row">
                               <div class="row" id="divReport" style= "overflow-y:scroll; " runat="server" >
                            <div class="col-md-6">
                                <asp:GridView ID="ReIssueInProcessGRD" runat="server" AutoGenerateColumns="False"
                                    CssClass="table">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Agent ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lbluserid" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Agency Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblagencyname" runat="server" Text='<%#Eval("Agency_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Order ID">
                                            <ItemTemplate>
                                                <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                    style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91;
                                                    font-weight: bold;">
                                                <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax ID">
                                            <ItemTemplate>
                                                <asp:Label ID="TID" runat="server" Text='<%#Eval("Counter")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Executive ID">
                                            <ItemTemplate>
                                                <asp:Label ID="ExId" runat="server" Text='<%#Eval("ExecutiveID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Type">
                                            <ItemTemplate>
                                                <asp:Label ID="PaxType" runat="server" Text='<%#Eval("pax_type")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pax Name">
                                            <ItemTemplate>
                                                <asp:Label ID="PaxName" runat="server" Text='<%#(Eval("Title").ToString()+" " + Eval("pax_fname").ToString()+" " + Eval("pax_lname").ToString()).ToUpper()%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PNR">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpnr" runat="server" Text='<%#Eval("pnr_locator") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ticket No">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkupdate" runat="server" Text='<%#Eval("Tkt_No") %>' ForeColor="Red"
                                                    CommandName="lnkupdate" Font-Bold="true" Font-Size="11px" CommandArgument='<%#Eval("Counter") %>'
                                                    OnClick="lnkupdate_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Airline" DataField="VC"></asp:BoundField>
                                        <asp:BoundField HeaderText="SECTOR" DataField="Sector"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Total Fare">
                                            <ItemTemplate>
                                                <a id="ancher1" href='TktRptIntl_ReIssueFare.aspx?Counter=<%#Eval("Counter") %>'
                                                    onclick="wopen('', 'popup',400, 300); return false;" rel="lyteframe" rev="width: 300px; height: 210px; overflow:hidden;"
                                                    style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: Red; font-weight: bold;">
                                                    <asp:Label ID="lbltotalfare" runat="server" Text='<%#Eval("TotalFare") %>'></asp:Label></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Net Fare">
                                            <ItemTemplate>
                                                <asp:Label ID="lbltotalfareafterdiscount" runat="server" Text='<%#Eval("TotalFareAfterDiscount") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBookdate" runat="server" Text='<%#Eval("SubmitDate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Journey Date" DataField="Journey_Date"></asp:BoundField>
                                        <asp:BoundField HeaderText="Partner Name" DataField="PartnerName"></asp:BoundField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkreject" runat="server" ForeColor="Red" Text="Reject" CommandName="reject"
                                                    CommandArgument='<%#Eval("Counter") %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reject Remark">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtRemark" runat="server" Height="47px" TextMode="MultiLine" Width="250px"
                                                    Visible="false" MaxLength="500"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ControlStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSubmit" runat="server" OnClick="btnCanFee_Click" Visible="false" OnClientClick="return Validate();"
                                                    CommandArgument='<%# Eval("Counter") %>' CommandName="submit"><img src="../../Images/Submit.png" alt="Ok" /></asp:LinkButton><br />
                                                <asp:LinkButton ID="lnkHides" runat="server" OnClick="lnkHides_Click" Visible="false"
                                                    CommandName="lnkHides" CommandArgument='<%# Eval("Counter") %>'><img src="../../Images/Cancel.png" alt="Cancel"/></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Payment Mode" DataField="Payment_Mode"></asp:BoundField>
                                           <asp:BoundField HeaderText="Convenience Fee" DataField="PgCharges"></asp:BoundField>
                                          <asp:BoundField HeaderText="Customer Remarks" DataField="RegardingIssue"></asp:BoundField>
                                    </Columns>

                                </asp:GridView>
                            </div>
                        </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

